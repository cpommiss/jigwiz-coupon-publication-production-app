framework.point = {
    fn: {
        set_date_range: function(event, picker) {
            jQuery('input[name="start"]').val(picker.startDate.format('YYYY-MM-DD'));
            jQuery('input[name="stop"]').val(picker.endDate.format('YYYY-MM-DD'));
        }
    }
};