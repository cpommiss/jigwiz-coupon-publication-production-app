jQuery(document).ready(function() {
    jQuery('.tabs a').click(function (e) {
        e.preventDefault();
        jQuery(this).tab('show');
    });
    jQuery('a[data-toggle="tab"]:first').trigger('click');

    jQuery('#acl-copy').bind('change', framework.users.fn.copy);
});
