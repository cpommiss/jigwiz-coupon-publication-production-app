var framework = {
    properties: {
        uid: 0,
        paths: {
            root: '',
            content: '',
            wysihtml5: {
                colors: '/css/vendor/plugins/wysihtml5-color.css'
            }
        },
        notifications: {
            connected: false,
            containers: {
                header: null,
                list: null
            },
            socket: null
        }
    },
    fn: {
        utilities: {
            html_entity_decode: function(string) {
                return  jQuery('<textarea />')
                            .html(string)
                            .text();
            },

            remove: function(event) {
                event.preventDefault();

                var url_remove  = jQuery(this).attr('data-remove-url');
                var message     = jQuery(this).attr('data-remove-message');

                if ((url_remove) && (message)) {
                    bootbox.dialog( message,
                                    [
                                        {
                                            label: 'Cancel',
                                            class: 'pull-left',
                                            callback: function() {
                                                // nothing, we're cancelling
                                            }
                                        },
                                        {
                                            label: 'Yes, continue',
                                            class: 'btn-danger',
                                            callback: function() {
                                                bootbox.dialog( '<p>Are you absolutely sure you want to continue with removal?</p><p><strong>THIS ACTION CANNOT BE UNDONE!</strong></p>',
                                                                [
                                                                    {
                                                                        label: 'No, cancel',
                                                                        class: 'pull-left',
                                                                        callback: function() {
                                                                            // nothing, we're cancelling
                                                                        }
                                                                    },
                                                                    {
                                                                        label: 'Yes, I am absolutely sure!',
                                                                        class: 'btn-danger',
                                                                        callback: function() {
                                                                            window.location     = url_remove;
                                                                        }
                                                                    }
                                                                ]);
                                            }
                                        }
                                    ]);
                }
            }
        },

        notifications: {
            init: function() {
                if (framework.properties.uid) {
                    framework.properties.notifications.socket               = io.connect('http://' + window.location.host + ':1337');

                    framework.properties.notifications.socket.on('connect', framework.fn.notifications.events.connect);
                    framework.properties.notifications.socket.on('notifications', framework.fn.notifications.events.handler);

                    framework.properties.notifications.containers.header    = jQuery('#header-notifications');
                    framework.properties.notifications.containers.list      = jQuery('#notifications-popover');

                    framework.properties.notifications.containers.header.popover(
                        {
                            html: true,
                            trigger: 'click',
                            placement: 'bottom',
                            content: function() {
                                return  '<div class="notifications-popover">' +
                                            jQuery('#notifications-popover').html() +
                                        '</div>';
                            }
                        });
                    framework.properties.notifications.containers.header.bind('click', framework.fn.notifications.display);

                    framework.fn.notifications.update_count();
                }
            },

            update_count: function() {
                var count_new       = jQuery('#notifications-popover li[class != "notified"][class != "empty"]').length;

                framework.properties.notifications.containers.header.find('>a>span.label')
                    .removeClass('hidden')
                    .removeClass('label-warning')
                    .addClass(((count_new) ? 'label-warning' : ''))
                    .html(count_new);
            },

            display: function() {
                if (!framework.properties.notifications.containers.header.hasClass('notified')) {
                    jQuery.post(    framework.properties.paths.root + 'helpers/notifications/dismiss/all',
                                    function() {
                                        framework.properties.notifications.containers.header.addClass('notified');

                                        framework.fn.notifications.update_count();
                                    });
                }
            },

            events: {
                connect: function() {
                    // connected!
                    framework.properties.notifications.connected    = true;
                },

                handler: function(data) {
                    if ((framework.properties.notifications.connected) && (data)) {
                        var data_json       = JSON.parse(data);

                        if ((data_json) && (data_json.for) && (parseInt(data_json.for) === framework.properties.uid) && (data_json.message) && (data_json.template)) {
                            framework.properties.notifications.containers.header.popover('hide');
                            framework.properties.notifications.containers.list.find('>ol>li.empty').remove();
                            framework.properties.notifications.containers.list.find('>ol').prepend(framework.fn.utilities.html_entity_decode(data_json.template));
                            framework.fn.notifications.update_count();

                            bootbox.alert(data_json.message);
                        }
                    }
                }
            }
        }
    }
};