framework.reminders = {
    properties: {
        format: 'YYYY-MM-DD HH:mm:ss'
    },

    fn: {
        helpers: {
            render_event: function (event, element) {
                element.find('.fc-event-title').html(element.find('.fc-event-title').text());
            }
        },

        views: {
            create: function(startDate, endDate, allDay, jsEvent, view) {
                Tipped.create(
                    jsEvent.target,
                    framework.properties.paths.root + 'helpers/reminders/manage',
                    {
                        ajax: {
                            data: {
                                start: moment(startDate).format(framework.reminders.properties.format),
                                end: moment(((endDate) && (!allDay)) ? endDate : startDate).format(framework.reminders.properties.format),
                                allday: (allDay) ? 1 : 0,
                                text: '',
                                client: ((jQuery('input[name="_client"]').length) ? jQuery('input[name="_client"]').val() : ''),
                                label: 'Create'
                            },
                            type: 'post'
                        },
                        skin: 'light',
                        position: 'top',
                        closeButton: true,
                        hideOn: false,
                        hideOnClickOutside: false,
                        hideOthers: true,
                        target: jsEvent.target,
                        offset: {
                            y: (jsEvent.target.clientHeight / 2)
                        },
                        onHide: function(content, element) {
                            jQuery('button[name="reminder_submit"]', content).unbind('click');
                            jQuery('.full-calendar').fullCalendar('unselect');
                        },
                        onShow: function(content, element) {
                            jQuery('button[name="reminder_submit"]', content).bind('click', framework.reminders.fn.actions.create);
                        }
                    }
                );
            },

            update: function(event, jsEvent, view) {
                Tipped.create(
                    jsEvent.target,
                    framework.properties.paths.root + 'helpers/reminders/manage',
                    {
                        ajax: {
                            data: {
                                id: event.id,
                                start: moment(event.start).format(framework.reminders.properties.format),
                                end: moment(((event.end) && (!event.allDay)) ? event.end : event.start).format(framework.reminders.properties.format),
                                allday: (event.allDay) ? 1 : 0,
                                text: event.text,
                                client: event.client_id,
                                when: event.when,
                                label: 'Update'
                            },
                            type: 'post'
                        },
                        skin: 'light',
                        position: 'top',
                        closeButton: true,
                        hideOn: false,
                        hideOnClickOutside: false,
                        hideOthers: true,
                        target: jsEvent.target,
                        offset: {
                            y: (jsEvent.target.clientHeight / 2)
                        },
                        onHide: function(content, element) {
                            jQuery('button[name="reminder_submit"]', content).unbind('click');
                            jQuery('.full-calendar').fullCalendar('unselect');
                        },
                        onShow: function(content, element) {
                            jQuery('button[name="reminder_submit"]', content).bind('click', framework.reminders.fn.actions.update);
                        }
                    }
                );
            }
        },

        actions: {
            create: function(jsEvent) {
                var element_parent      = jQuery(this).parent();

                if (element_parent) {
                    var reminder_client     = parseInt(element_parent.find('select[name="reminder_client"]').val());
                    var reminder_start      = element_parent.find('input[name="reminder_start"]').val();
                    var reminder_end        = element_parent.find('input[name="reminder_end"]').val();
                    var reminder_allday     = parseInt(element_parent.find('input[name="reminder_allday"]').val());
                    var reminder_text       = element_parent.find('textarea[name="reminder_text"]').val();
                    var reminder_when       = parseInt(element_parent.find('select[name="reminder_when"]').val());

                    if ((!isNaN(reminder_client)) &&
                        (reminder_client > 0) &&
                        (reminder_start) &&
                        (reminder_end) &&
                        (reminder_text.length) &&
                        (!isNaN(reminder_when)) &&
                        (reminder_when != 0)) {
                        jQuery.post(    framework.properties.paths.root + 'helpers/reminders/create',
                                        {
                                            client: reminder_client,
                                            start: reminder_start,
                                            end: reminder_end,
                                            allday: (reminder_allday) ? 1 : 0,
                                            text: reminder_text,
                                            when: reminder_when
                                        },
                                        function(response) {
                                            if (response.success) {
                                                Tipped.hideAll();
                                                jQuery('.full-calendar').fullCalendar('refetchEvents');
                                            } else {
                                                bootbox.alert('An error has occurred while saving this reminder.  Please make sure you have entered in some text for this reminder.');
                                            }
                                        },
                                        'json');
                    } else {
                        bootbox.alert('An error has occurred while saving this reminder.  Please make sure you have entered in some text for this reminder.');
                    }
                }
            },

            update_datetime: function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
                jQuery.post(    framework.properties.paths.root + 'helpers/reminders/update',
                                {
                                    id: event.id,
                                    client: event.client_id,
                                    start: moment(event.start).format(framework.reminders.properties.format),
                                    end: moment(((event.end) && (!event.allDay)) ? event.end : event.start).format(framework.reminders.properties.format),
                                    allday: (event.allDay) ? 1 : 0,
                                    text: event.text,
                                    when: event.when
                                },
                                function(response) {
                                    if (response.success) {
                                        Tipped.hideAll();
                                        jQuery('.full-calendar').fullCalendar('refetchEvents');
                                    } else {
                                        bootbox.alert('An error has occurred while moving this reminder.  Please try again.');
                                    }
                                },
                                'json');
            },

            update: function(jsEvent) {
                var element_parent      = jQuery(this).parent();

                if (element_parent) {
                    var reminder_id         = parseInt(element_parent.find('input[name="reminder_id"]').val());
                    var reminder_client     = parseInt(element_parent.find('select[name="reminder_client"]').val());
                    var reminder_start      = element_parent.find('input[name="reminder_start"]').val();
                    var reminder_end        = element_parent.find('input[name="reminder_end"]').val();
                    var reminder_allday     = parseInt(element_parent.find('input[name="reminder_allday"]').val());
                    var reminder_text       = element_parent.find('textarea[name="reminder_text"]').val();
                    var reminder_when       = parseInt(element_parent.find('select[name="reminder_when"]').val());

                    if ((!isNaN(reminder_id)) &&
                        (reminder_id > 0) &&
                        (!isNaN(reminder_client)) &&
                        (reminder_client > 0) &&
                        (reminder_start) &&
                        (reminder_end) &&
                        (reminder_text.length) &&
                        (!isNaN(reminder_when)) &&
                        (reminder_when != 0)) {
                        jQuery.post(    framework.properties.paths.root + 'helpers/reminders/update',
                                        {
                                            id: reminder_id,
                                            client: reminder_client,
                                            start: reminder_start,
                                            end: reminder_end,
                                            allday: (reminder_allday) ? 1 : 0,
                                            text: reminder_text,
                                            when: reminder_when
                                        },
                                        function(response) {
                                            if (response.success) {
                                                Tipped.hideAll();
                                                jQuery('.full-calendar').fullCalendar('refetchEvents');
                                            } else {
                                                bootbox.alert('An error has occurred while saving this reminder.  Please make sure you have entered in some text for this reminder.');
                                            }
                                        },
                                        'json');
                    } else {
                        bootbox.alert('An error has occurred while saving this reminder.  Please make sure you have entered in some text for this reminder.');
                    }
                }
            }
        },

        callbacks: {
            unselect: function(view, jsEvent) {
                Tipped.hideAll();
            }
        }
    }
};