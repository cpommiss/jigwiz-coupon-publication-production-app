framework.users = {
    properties: {
        presets: []
    },
    fn: {
        copy: function(event) {
            var counter;
            var cur_value           = jQuery(this).val();

            if (cur_value.length) {
                if (parseInt(cur_value) === -1) {
                    // copy from user
                } else {
                    if (framework.users.properties.presets[cur_value].permissions.length) {
                        var preset_title        = jQuery(this).find('option:selected').text();

                        jQuery('#user-access input[type="checkbox"]').attr('checked', false);

                        for (counter = 0; counter < framework.users.properties.presets[cur_value].flags.length; counter++) {
                            jQuery('#user-access input[value="' + framework.users.properties.presets[cur_value].flags[counter] + '"]').attr('checked', true);
                        }
                        for (counter = 0; counter < framework.users.properties.presets[cur_value].permissions.length; counter++) {
                            jQuery('#user-access input[value="' + framework.users.properties.presets[cur_value].permissions[counter] + '"]').attr('checked', true);
                        }

                        jQuery('#user-access .alert').remove();
                        jQuery('#user-access>h3').before('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><p>Permissions successfully copied from the \'' + preset_title + '\' preset!</p></div>');
                    }
                }
            }
        }
    }
}