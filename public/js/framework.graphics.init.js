jQuery(document).ready(function() {
    if (jQuery('.datatable').length) {
        jQuery('.datatable').dataTable( {
            'sDom': "<'row-fluid'<'span6 pull-left'l><'span6 pull-right'f>>t<'row-fluid graphics-queue-footer'<'span6'i><'span6 pull-right'p>>",
            'sPaginationType': 'bootstrap',
            'oLanguage': {
                'sLengthMenu': '_MENU_&nbsp;&nbsp;records per page'
            },
            'sScrollX': '100%',
            'bScrollCollapse': true
        });
    }

    jQuery('.tabbable a[data-toggle="tab"]').on('shown', function() {
        var tab_target      = jQuery(this).attr('href').replace('#', '');

        jQuery('#graphics-queue-data-' + tab_target)
            .css('width', '100%')
            .dataTable()
            .fnAdjustColumnSizing();
    });
    jQuery('.tabbable a[data-toggle="tab"]:first').tab('show');
});