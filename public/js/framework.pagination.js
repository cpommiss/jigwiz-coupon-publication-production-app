framework.pagination = {
    properties: {
        saving: false,
        loading: false,
        save_delay: 500,
        reservations: {
            add_counter: 0,
            premium_pages: []
        },
        col_margin: 2.38636364,
        inches_to_pixels: 50,
        templates: {
            cell_placeholder: '<div class="pagination-placeholder selectable" data-row="{{row}}" data-col="{{col}}"></div>'
        },
        elements: {
            current_page: null,
            media: null, 
            zone: null, 
            year: null, 
            month: null,
            current_year: null,
            current_month: null,
            current_zone: null,
            premium: null,
            issue_index: null,
            width: null,
            height: null,
            rows: null,
            cols: null,
            default_width: null,
            default_height: null,
            default_rows: null,
            default_cols: null,
            sliders: {
                page: null,
                month: null
            },
            modal: null
        },
        batch: {
            zones_all: [],
            zones_selected: [],
            processing: {
                index: 0
            }
        },
        months: {
            default: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            labels: []
        },
        timers: {
            wait: 250,
            pointers: {
                get_data: null
            }
        },
        issue_index: '',
        min_num_pages: 4,
        current_page: 1, 
        pages: [],
        placement_template: {id: 0, job: {id: 0, key: '', proof: ''}, start_row: 0, end_row: 0, start_col: 0, end_col: 0}
    },
    fn: {
        init_elements: function() {
            // hidden inputs and filtering inputs
            framework.pagination.properties.elements.current_page       = jQuery('[name="page"]');
            framework.pagination.properties.elements.media              = jQuery('[name="media"]');
            framework.pagination.properties.elements.zone               = jQuery('[name="zone"]');
            framework.pagination.properties.elements.year               = jQuery('[name="year"]');
            framework.pagination.properties.elements.month              = jQuery('[name="month"]');
            framework.pagination.properties.elements.premium            = jQuery('[name="premium"]');
            framework.pagination.properties.elements.issue_index        = jQuery('[name="issue_index"]');
            framework.pagination.properties.elements.width              = jQuery('[name="width"]');
            framework.pagination.properties.elements.height             = jQuery('[name="height"]');
            framework.pagination.properties.elements.rows               = jQuery('[name="rows"]');
            framework.pagination.properties.elements.cols               = jQuery('[name="cols"]');
            framework.pagination.properties.elements.current_year       = jQuery('[name="_year"]');
            framework.pagination.properties.elements.current_month      = jQuery('[name="_month"]');
            framework.pagination.properties.elements.current_zone       = jQuery('[name="_zone"]');
            framework.pagination.properties.elements.default_width      = jQuery('[name="_width"]');
            framework.pagination.properties.elements.default_height     = jQuery('[name="_height"]');
            framework.pagination.properties.elements.default_rows       = jQuery('[name="_rows"]');
            framework.pagination.properties.elements.default_cols       = jQuery('[name="_cols"]');

            // sliders
            framework.pagination.properties.elements.sliders.page       = jQuery('#page_select');
            framework.pagination.properties.elements.sliders.month      = jQuery('#month_select');

            // modal
            framework.pagination.properties.elements.modal              = jQuery('#pagination-modal');
        },

        build_page: function() {
            var media_default_width     = parseFloat(framework.pagination.properties.elements.default_width.val());
            var media_default_height    = parseFloat(framework.pagination.properties.elements.default_height.val());
            var media_default_max_rows  = parseInt(framework.pagination.properties.elements.default_rows.val());
            var media_default_max_cols  = parseInt(framework.pagination.properties.elements.default_cols.val());
            var media_width, media_height, media_max_rows, media_max_cols;
            var counter, counter_placement, counter_x, counter_y;

            for (counter = 0; counter < framework.pagination.properties.pages.length; counter++) {
                if (framework.pagination.properties.pages[counter].order.current === framework.pagination.properties.current_page) {
                    media_width             = framework.pagination.properties.pages[counter].dimensions.width;
                    media_height            = framework.pagination.properties.pages[counter].dimensions.height;
                    media_max_rows          = framework.pagination.properties.pages[counter].dimensions.rows;
                    media_max_cols          = framework.pagination.properties.pages[counter].dimensions.cols;
                    break;
                }
            }

            if ((!isNaN(media_width)) && (!isNaN(media_default_width)) &&
                (!isNaN(media_height)) && (!isNaN(media_default_height)) &&
                (!isNaN(media_max_rows)) && (!isNaN(media_default_max_rows)) &&
                (!isNaN(media_max_cols)) && (!isNaN(media_default_max_cols)) &&
                (framework.pagination.properties.current_page > 0) &&
                (media_width > 0) && (media_default_width > 0) &&
                (media_height > 0) && (media_default_height > 0) &&
                (media_max_rows > 0) && (media_default_max_rows > 0) &&
                (media_max_cols > 0) && (media_default_max_cols > 0)) {

                if ((media_width != media_default_width) || (media_height != media_default_height) || (media_max_rows != media_default_max_rows) || (media_max_cols != media_default_max_cols)) {
                    jQuery('.pagination-dimensions-alert').removeClass('hidden');
                } else {
                    jQuery('.pagination-dimensions-alert').addClass('hidden');
                }

                var page_html           = framework.pagination.fn.render_page(  media_width,
                                                                                media_height,
                                                                                media_max_rows,
                                                                                media_max_cols,
                                                                                jQuery('.pagination-preview .pagination-row'),
                                                                                false,
                                                                                '',
                                                                                framework.pagination.properties.templates.cell_placeholder);
                jQuery('.pagination-preview .pagination-row').html(page_html);

                for (counter = 0; counter < framework.pagination.properties.pages.length; counter++) {
                    if (framework.pagination.properties.pages[counter].order.current === framework.pagination.properties.current_page) {
                        framework.pagination.properties.elements.width.val((framework.pagination.properties.pages[counter].dimensions.width === 0) ? framework.pagination.properties.elements.default_width.val() : framework.pagination.properties.pages[counter].dimensions.width);
                        framework.pagination.properties.elements.height.val((framework.pagination.properties.pages[counter].dimensions.height === 0) ? framework.pagination.properties.elements.default_height.val() : framework.pagination.properties.pages[counter].dimensions.height);
                        framework.pagination.properties.elements.rows.val((framework.pagination.properties.pages[counter].dimensions.rows === 0) ? framework.pagination.properties.elements.default_rows.val() : framework.pagination.properties.pages[counter].dimensions.rows);
                        framework.pagination.properties.elements.cols.val((framework.pagination.properties.pages[counter].dimensions.cols === 0) ? framework.pagination.properties.elements.default_cols.val() : framework.pagination.properties.pages[counter].dimensions.cols);

                        if (framework.pagination.properties.pages[counter].placements.length) {
                            for (counter_placement = 0; counter_placement < framework.pagination.properties.pages[counter].placements.length; counter_placement++) {
                                jQuery('.ui-selected').removeClass('ui-selected');

                                for (counter_y = framework.pagination.properties.pages[counter].placements[counter_placement].start_row; counter_y <= framework.pagination.properties.pages[counter].placements[counter_placement].end_row; counter_y++) {
                                    for (counter_x = framework.pagination.properties.pages[counter].placements[counter_placement].start_col; counter_x <= framework.pagination.properties.pages[counter].placements[counter_placement].end_col; counter_x++) {
                                        jQuery('[data-row="' + counter_y + '"][data-col="' + counter_x + '"]').addClass('ui-selected');
                                    }
                                }

                                framework.pagination.fn.placements.place(   framework.pagination.properties.pages[counter].placements[counter_placement].job.id,
                                                                            framework.properties.paths.content + 'jobs/' + framework.pagination.properties.pages[counter].placements[counter_placement].job.key + '/' + framework.pagination.properties.pages[counter].placements[counter_placement].job.proof,
                                                                            jQuery('.pagination-row .ui-selected'),
                                                                            framework.pagination.properties.pages[counter].placements[counter_placement].start_row,
                                                                            framework.pagination.properties.pages[counter].placements[counter_placement].end_row,
                                                                            framework.pagination.properties.pages[counter].placements[counter_placement].start_col,
                                                                            framework.pagination.properties.pages[counter].placements[counter_placement].end_col);
                            }
                        }

                        framework.pagination.fn.set_premium_status(framework.pagination.properties.current_page, framework.pagination.properties.pages[counter].premium.current);
                        break;
                    }
                }
            }
        },

        render_page: function(width, height, rows, cols, container, create_rows, row_classes, cell_template) {
            var page_width          = (100 - (framework.pagination.properties.col_margin * (cols - 1))) / cols;
            var page_height         = ((jQuery(container).width() / width) * height) / rows;
            var page_html           = '';
            var cell_html           = '';
            var page_template       = Handlebars.compile(cell_template);

            for (var row_counter = 1; row_counter <= rows; row_counter++) {
                if (create_rows) {
                    page_html               += '<div class="' + row_classes + '">';
                }

                for (var col_counter = 1; col_counter <= cols; col_counter++) {
                    cell_html               = page_template({row: row_counter, col: col_counter});

                    cell_html               =   jQuery(cell_html).css({
                                                    marginLeft: ((col_counter == 1) ? 0 : framework.pagination.properties.col_margin) + '%',
                                                    height: page_height + 'px',
                                                    width: page_width + '%'
                                                });

                    page_html               += cell_html[0].outerHTML;
                }

                if (create_rows) {
                    page_html               += '</div>';
                }
            }

            return page_html;
        },

        fetch_pagination_data: function() {
            clearTimeout(framework.pagination.properties.timers.pointers.get_data);

            framework.pagination.properties.timers.pointers.get_data    = setTimeout(framework.pagination.fn.get_pagination_data, framework.pagination.properties.timers.wait);
        },

        get_pagination_data: function() {
            var media           = parseInt(framework.pagination.properties.elements.media.val());
            var zone            = parseInt(framework.pagination.properties.elements.zone.val());
            var year            = parseInt(framework.pagination.properties.elements.year.val());
            var month           = parseInt(framework.pagination.properties.elements.month.val());

            if (!(framework.pagination.properties.loading) && (!isNaN(media)) && (!isNaN(zone)) && (!isNaN(year)) && (!isNaN(month)) && (media > 0) && (zone > 0) && (year > 0) && (month > 0)) {
                framework.pagination.properties.loading         = true;

                jQuery.blockUI({
                    message: '<div class="alert alert-block"><h3><span class="awe-gear awe-spin"></span> Please wait...</h3><p>Loading pagination data, please wait...</p></div>',
                    css: {
                        padding: 0,
                        margin: 0,
                        backgroundColor: 'transparent',
                        border: 'none',
                        textAlign: 'left'
                    },
                    overlayCSS: {
                        opacity: 0.3
                    }
                });

                jQuery.post(    framework.properties.paths.root + 'helpers/pagination/get',
                                {
                                    media: media,
                                    zone: zone,
                                    month: month,
                                    year: year
                                },
                                framework.pagination.fn.parse_pagination_data,
                                'json');
            } else {
                bootbox.alert('Error: Some data missing');
            }
        },

        parse_pagination_data: function(data) {
            var counter, page, issue_index, issue_month;

            // reset months and pages
            framework.pagination.properties.pages           = [];

            // reset issue label
            framework.pagination.properties.elements.issue_index.val('');
            framework.pagination.properties.issue_index     = '';

            framework.pagination.fn.settings.show_issue_indexes(data.issues);

            // did we find pagination data for the requested month?
            if (data.found) {
                var counter_inner, counter_placement, counter_file, start_row, start_col, end_row, end_col, current_month, current_year, proof;

                // update page selector
                framework.pagination.properties.elements.sliders.page.slider('option', 'max', data.pagination.pages.length).slider('pips');

                // set month/year, just for redundancy
                current_month               = moment(data.pagination.issue_date).month() + 1;
                current_year                = moment(data.pagination.issue_date).year();

                framework.pagination.properties.elements.month.val(current_month);
                framework.pagination.properties.elements.year.val(current_year);
                framework.pagination.properties.elements.zone.val(data.pagination.zone);
                framework.pagination.properties.elements.current_month.val(current_month);
                framework.pagination.properties.elements.current_year.val(current_year);
                framework.pagination.properties.elements.current_zone.val(data.pagination.zone);

                framework.pagination.properties.elements.sliders.month.slider('option', 'value', current_month);

                for (counter = 0; counter < data.pagination.pages.length; counter++) {
                    page        = {
                        id: parseInt(data.pagination.pages[counter].id),
                        order: {
                            original: parseInt(data.pagination.pages[counter].page),
                            current: parseInt(data.pagination.pages[counter].page)
                        },
                        premium: {
                            original: parseInt(data.pagination.pages[counter].premium),
                            current: parseInt(data.pagination.pages[counter].premium)
                        },
                        dimensions: {
                            width: parseFloat(data.pagination.pages[counter].width),
                            height: parseFloat(data.pagination.pages[counter].height),
                            rows: parseInt(data.pagination.pages[counter].rows),
                            cols: parseInt(data.pagination.pages[counter].cols)
                        },
                        placements: []
                    };

                    if (data.pagination.pages[counter].placements.length) {
                        for (counter_placement = 0; counter_placement < data.pagination.pages[counter].placements.length; counter_placement++) {
                            start_row                   = parseInt(data.pagination.pages[counter].placements[counter_placement].rows_offset);
                            start_col                   = parseInt(data.pagination.pages[counter].placements[counter_placement].cols_offset);
                            end_row                     = start_row + (parseInt(data.pagination.pages[counter].placements[counter_placement].rows) - 1);
                            end_col                     = start_col + (parseInt(data.pagination.pages[counter].placements[counter_placement].cols) - 1);

                            if (data.pagination.pages[counter].placements[counter_placement].job.files.length) {
                                for (counter_file = 0; counter_file < data.pagination.pages[counter].placements[counter_placement].job.files.length; counter_file++) {
                                    if (parseInt(data.pagination.pages[counter].placements[counter_placement].job.files[counter_file].proof) === 1) {
                                        proof           = data.pagination.pages[counter].placements[counter_placement].job.files[counter_file].file;
                                    }
                                }
                            }

                            page.placements.push({
                                id: parseInt(data.pagination.pages[counter].placements[counter_placement].id),
                                job: {
                                    id: parseInt(data.pagination.pages[counter].placements[counter_placement].job_id),
                                    key: data.pagination.pages[counter].placements[counter_placement].job.key,
                                    proof: proof
                                },
                                start_row: start_row,
                                start_col: start_col,
                                end_row: end_row,
                                end_col: end_col
                            });
                        }
                    }

                    framework.pagination.properties.pages.push(page);
                }
            } else {
                framework.pagination.properties.elements.width.val(framework.pagination.properties.elements.default_width.val());
                framework.pagination.properties.elements.height.val(framework.pagination.properties.elements.default_height.val());
                framework.pagination.properties.elements.rows.val(framework.pagination.properties.elements.default_rows.val());
                framework.pagination.properties.elements.cols.val(framework.pagination.properties.elements.default_cols.val());

                framework.pagination.properties.elements.month.val(framework.pagination.properties.elements.sliders.month.slider('option', 'value'));
                framework.pagination.properties.elements.current_month.val(framework.pagination.properties.elements.sliders.month.slider('option', 'value'));
                framework.pagination.properties.elements.current_year.val(framework.pagination.properties.elements.year.val());
                framework.pagination.properties.elements.current_zone.val(framework.pagination.properties.elements.zone.val());

                for (counter = 1; counter <= framework.pagination.properties.min_num_pages; counter++) {
                    framework.pagination.properties.pages.push({
                        id: 0,
                        order: {
                            original: counter,
                            current: counter
                        },
                        premium: {
                            original: 0,
                            current: 0
                        },
                        dimensions: {
                            width: framework.pagination.properties.elements.default_width.val(),
                            height: framework.pagination.properties.elements.default_height.val(),
                            rows: framework.pagination.properties.elements.default_rows.val(),
                            cols: framework.pagination.properties.elements.default_cols.val()
                        },
                        placements: []
                    });
                }

                framework.pagination.properties.elements.sliders.page.slider('option', 'max', framework.pagination.properties.pages.length).slider('value', 1).slider('pips');
            }

            framework.pagination.properties.loading         = false;

            jQuery.unblockUI();

            framework.pagination.fn.build_page();
        },

        commit_pagination_data: function() {
            if (!framework.pagination.properties.saving) {
                framework.pagination.properties.saving  = true;

                jQuery.blockUI({
                    message: '<div class="alert alert-success alert-block"><h3><span class="awe-gear awe-spin"></span> Please wait...</h3><p>Saving pagination data, please wait...</p></div>',
                    css: {
                        padding: 0,
                        margin: 0,
                        backgroundColor: 'transparent',
                        border: 'none',
                        textAlign: 'left'
                    },
                    overlayCSS: {
                        opacity: 0.3
                    }
                });

                setTimeout( function() {
                                jQuery.post(    framework.properties.paths.root + 'helpers/pagination/put',
                                                {
                                                    media: framework.pagination.properties.elements.media.val(),
                                                    zone: framework.pagination.properties.elements.zone.val(),
                                                    month: framework.pagination.properties.elements.current_month.val(),
                                                    year: framework.pagination.properties.elements.current_year.val(),
                                                    issue_index: framework.pagination.properties.elements.issue_index.val(),
                                                    data: framework.pagination.properties.pages
                                                },
                                                function(response) {
                                                    framework.pagination.properties.saving  = false;

                                                    jQuery.unblockUI();

                                                    if (response.success) {
                                                        framework.pagination.fn.get_pagination_data();
                                                    }
                                                },
                                                'json');
                            }, framework.pagination.properties.save_delay);
            }
        },

        handle_premium_toggle: function() {
            framework.pagination.fn.set_premium_status(parseInt(framework.pagination.properties.elements.current_page.val()), ((this.checked) ? 1 : 0), true);
        },

        set_premium_status: function(page, is_premium, save) {
            for (var counter = 0; counter < framework.pagination.properties.pages.length; counter++) {
                if (framework.pagination.properties.pages[counter].order.current === page) {
                    framework.pagination.properties.pages[counter].premium.current  = is_premium;

                    jQuery('.pagination-premium-enabled, .pagination-premium-disabled').hide();
                    framework.pagination.properties.elements.premium.attr('checked', ((is_premium === 1) ? true : false));

                    if (is_premium === 1) {
                        jQuery('.pagination-premium-enabled').parent('.alert').removeClass('alert-info').removeClass('hidden');
                        jQuery('.pagination-premium-enabled').show();
                    } else {
                        jQuery('.pagination-premium-disabled').parent('.alert').addClass('alert-info').removeClass('hidden');
                        jQuery('.pagination-premium-disabled').show();
                    }

                    if (save) {
                        framework.pagination.fn.commit_pagination_data();
                    }
                }
            }
        },

        slider_set_month: function(event, ui) {
            framework.pagination.properties.elements.month.val(ui.value);

            if (!framework.pagination.properties.loading) {
                framework.pagination.fn.fetch_pagination_data();
            }
        },

        slider_set_page: function(event, ui) {
            framework.pagination.properties.current_page    = ui.value;
            framework.pagination.properties.elements.current_page.val(ui.value);

            framework.pagination.fn.build_page();

            jQuery('.pagination-page-settings').popover('hide');
            jQuery('.pagination-issue-index').popover('hide');
        },

        control_page: function(event) {
            event.preventDefault();

            var page_selector   = framework.pagination.properties.elements.sliders.page;
            var page_min        = page_selector.slider('option', 'min');
            var page_max        = page_selector.slider('option', 'max');
            var page_cur        = page_selector.slider('option', 'value');
            var page_next;

            switch (jQuery(this).attr('data-type')) {
                case 'back' :
                    if ((page_cur - 1) < page_min) {
                        page_target     = page_min;
                    } else {
                        page_target     = page_cur - 1;
                    }
                    break;

                case 'next' :
                    if ((page_cur + 1) > page_max) {
                        page_target     = page_max;
                    } else {
                        page_target     = page_cur + 1;
                    }
                    break;
            }

            page_selector.slider('option', 'value', page_target);
        },

        insert_page: function(direction) {
            var page_selector   = framework.pagination.properties.elements.sliders.page;
            var new_page_front, new_page_back, page_front, page_back;
            var cur_page        = parseInt(framework.pagination.properties.elements.current_page.val());

            switch (direction) {
                case 'before' :
                    if (cur_page === 1) {
                        new_page_front  = 1;
                        new_page_back   = 2;
                    } else {
                        new_page_front  = cur_page;
                        new_page_back   = cur_page + 1;
                    }
                    break;

                case 'after' :
                    new_page_front      = cur_page + 1;
                    new_page_back       = cur_page + 2;
                    break;
            }

            // reorder existing pages, if any
            for (var counter = 0; counter < framework.pagination.properties.pages.length; counter++) {
                if (framework.pagination.properties.pages[counter].order.current >= new_page_front) {
                    framework.pagination.properties.pages[counter].order.current    = framework.pagination.properties.pages[counter].order.current + 2;
                }
            }

            // add front
            framework.pagination.properties.pages.push({
                id: 0,
                order: {
                    original: 0,
                    current: new_page_front
                },
                premium: {
                    original: 0,
                    current: 0
                },
                dimensions: {
                    width: framework.pagination.properties.elements.default_width.val(),
                    height: framework.pagination.properties.elements.default_height.val(),
                    rows: framework.pagination.properties.elements.default_rows.val(),
                    cols: framework.pagination.properties.elements.default_cols.val()
                },
                placements: []
            });

            // add back
            framework.pagination.properties.pages.push({
                id: 0,
                order: {
                    original: 0,
                    current: new_page_back
                },
                premium: {
                    original: 0,
                    current: 0
                },
                dimensions: {
                    width: framework.pagination.properties.elements.default_width.val(),
                    height: framework.pagination.properties.elements.default_height.val(),
                    rows: framework.pagination.properties.elements.default_rows.val(),
                    cols: framework.pagination.properties.elements.default_cols.val()
                },
                placements: []
            });

            // update page slider range
            page_selector.slider('option', 'max', framework.pagination.properties.pages.length);
            page_selector.slider('pips');
            page_selector.slider('option', 'value', new_page_front);

            // save data
            framework.pagination.fn.commit_pagination_data();
        },

        manage_page: function(action) {
            switch (action) {
                case 'clear' :
                    bootbox.confirm(    'Are you sure you want to remove all of the advertisement placements on this page?',
                                        function(result) {
                                            if (result) {
                                                framework.pagination.fn.delete_page_placements();
                                            }
                                        });
                    break;

                case 'remove' :
                    bootbox.confirm(    'Are you sure you want to delete this page?',
                                        function(result) {
                                            if (result) {
                                                framework.pagination.fn.delete_page();
                                            }
                                        });
                    break;
            }
        },

        delete_page: function() {
            var current_page            = parseInt(framework.pagination.properties.elements.current_page.val());
            var current_page_flipside   = ((current_page % 2) === 0) ? (current_page - 1) : (current_page + 1);

            if ((!isNaN(current_page)) && (!isNaN(current_page_flipside)) && (current_page > 0) && (current_page_flipside > 0)) {
                for (var counter = 0; counter < framework.pagination.properties.pages.length; counter++) {
                    if ((framework.pagination.properties.pages[counter].order.current === current_page) ||
                        (framework.pagination.properties.pages[counter].order.current === current_page_flipside)) {
                        framework.pagination.properties.pages[counter].order.current        = -1;
                    } else if (framework.pagination.properties.pages[counter].order.current > current_page_flipside) {
                        // re-order pages after the deleted pages
                        framework.pagination.properties.pages[counter].order.original       = framework.pagination.properties.pages[counter].order.current;
                        framework.pagination.properties.pages[counter].order.current        = framework.pagination.properties.pages[counter].order.current - 2;
                    }
                }

                framework.pagination.fn.commit_pagination_data();
            }
        },

        delete_page_placements: function() {
            var current_page            = parseInt(framework.pagination.properties.elements.current_page.val());

            if ((!isNaN(current_page)) && (current_page > 0)) {
                for (var counter = 0; counter < framework.pagination.properties.pages.length; counter++) {
                    if (framework.pagination.properties.pages[counter].order.current === current_page) {
                        for (var counter_inner = 0; counter_inner < framework.pagination.properties.pages[counter].placements.length; counter_inner++) {
                            framework.pagination.properties.pages[counter].placements[counter_inner].id     = -1;
                        }
                        break;
                    }
                }

                framework.pagination.fn.commit_pagination_data();
            }
        },

        popovers: {
            settings: {
                get_template: function() {
                    var template        = Handlebars.compile(jQuery('#template-page-settings').html());

                    return template({
                        width: framework.pagination.properties.elements.width.val(),
                        height: framework.pagination.properties.elements.height.val(),
                        rows: framework.pagination.properties.elements.rows.val(),
                        cols: framework.pagination.properties.elements.cols.val()
                    });
                },

                setup: function() {
                    jQuery('#pagination-slider-rows')
                        .slider({min: 1, max: 10, value: parseInt(framework.pagination.properties.elements.rows.val())})
                        .slider('pips')
                        .slider('float')
                        .on('slidechange', function(event, ui) {
                            jQuery('input[name="pagination-page-rows"]').val(ui.value);
                        });

                    jQuery('#pagination-slider-cols')
                        .slider({min: 1, max: 10, value: parseInt(framework.pagination.properties.elements.cols.val())})
                        .slider('pips')
                        .slider('float')
                        .on('slidechange', function(event, ui) {
                            jQuery('input[name="pagination-page-cols"]').val(ui.value);
                        });

                    jQuery('#pagination-page-settings-cancel').bind('click', function() {
                        jQuery('.pagination-page-settings').popover('hide');
                    });
                    jQuery('#pagination-page-settings-default').bind('click', function(event) {
                        framework.pagination.fn.settings.set_dimensions(event, true);
                    });
                    jQuery('#pagination-page-settings-save').bind('click', framework.pagination.fn.settings.set_dimensions);
                }
            },

            issue_index: {
                get_template: function() {
                    var template        = Handlebars.compile(jQuery('#template-issue-index').html());

                    return template({
                        label: framework.pagination.properties.issue_index,
                        month: framework.pagination.properties.months.default[framework.pagination.properties.elements.current_month.val() - 1],
                        year: framework.pagination.properties.elements.current_year.val(),
                        zone: framework.pagination.properties.elements.zone.find('option[value="' + framework.pagination.properties.elements.current_zone.val() + '"]').text()
                    });
                },

                setup: function() {
                    jQuery('#pagination-issue-index-cancel').unbind('click').bind('click', function() {
                        jQuery('.pagination-issue-index').popover('hide');
                    });
                    jQuery('#pagination-issue-index-save').unbind('click').bind('click', framework.pagination.fn.settings.set_issue_index);
                }
            }
        },

        settings: {
            set_dimensions: function(event, set_default) {
                event.preventDefault();
                event.stopPropagation();

                var width           = parseFloat(jQuery('input[name="pagination-page-width"]').val());
                var height          = parseFloat(jQuery('input[name="pagination-page-height"]').val());
                var rows            = parseInt(jQuery('input[name="pagination-page-rows"]').val());
                var cols            = parseInt(jQuery('input[name="pagination-page-cols"]').val());

                var default_width   = parseFloat(framework.pagination.properties.elements.default_width.val());
                var default_height  = parseFloat(framework.pagination.properties.elements.default_height.val());
                var default_rows    = parseInt(framework.pagination.properties.elements.default_rows.val());
                var default_cols    = parseInt(framework.pagination.properties.elements.default_cols.val());

                if ((!isNaN(default_width)) &&
                    (!isNaN(default_height)) &&
                    (!isNaN(default_rows)) &&
                    (!isNaN(default_cols))) {
                    for (var counter = 0; counter < framework.pagination.properties.pages.length; counter++) {
                        if (framework.pagination.properties.pages[counter].order.current === framework.pagination.properties.current_page) {
                            framework.pagination.properties.pages[counter].dimensions.width     = ((!isNaN(width)) && (width > 0)) ? width : default_width;
                            framework.pagination.properties.pages[counter].dimensions.height    = ((!isNaN(height)) && (height > 0)) ? height : default_height;
                            framework.pagination.properties.pages[counter].dimensions.rows      = ((!isNaN(rows)) && (rows > 0)) ? rows : default_rows;
                            framework.pagination.properties.pages[counter].dimensions.cols      = ((!isNaN(cols)) && (cols > 0)) ? cols : default_cols;

                            framework.pagination.fn.commit_pagination_data();
                            break;
                        }
                    }
                }

                jQuery('.pagination-page-settings').popover('hide');
            },

            set_issue_index: function(event) {
                event.preventDefault();
                event.stopPropagation();

                // set loading = true to prevent slider auto-load events from triggering when we set the data here to ensure the correct issue index for the correct month is chosen.
                framework.pagination.properties.loading     = true;

                var issue_index     = jQuery('input[name="pagination-issue-index"]').val();

                framework.pagination.properties.elements.issue_index.val(issue_index);

                framework.pagination.properties.elements.sliders.month.slider('option', 'value', framework.pagination.properties.elements.current_month.val());
                framework.pagination.properties.elements.month.val(framework.pagination.properties.elements.current_month.val());
                framework.pagination.properties.elements.year.val(framework.pagination.properties.elements.current_year.val());
                framework.pagination.properties.elements.zone.val(framework.pagination.properties.elements.current_zone.val());

                framework.pagination.properties.loading     = false;

                // let's save it
                framework.pagination.fn.commit_pagination_data();

                jQuery('.pagination-issue-index').popover('hide');
            },

            get_issue_indexes: function() {
                var media           = parseInt(framework.pagination.properties.elements.media.val());
                var zone            = parseInt(framework.pagination.properties.elements.zone.val());
                var year            = parseInt(framework.pagination.properties.elements.year.val());

                if (zone > 0) {
                    // we must have a zone to fetch issue indexes, as they are zone-specific
                    jQuery.get( framework.properties.paths.root + 'helpers/pagination/get/indexes',
                                {
                                    media: media,
                                    zone: zone,
                                    year: year
                                },
                                function(data) {
                                    if (data.issues) {
                                        framework.pagination.fn.settings.show_issue_indexes(data.issues);
                                    }
                                },
                                'json');
                }
            },

            show_issue_indexes: function(data) {
                framework.pagination.properties.months.labels   = [];

                // set issue labels for this year
                for (counter = 0; counter < framework.pagination.properties.months.default.length; counter++) {
                    framework.pagination.properties.months.labels[counter]      = framework.pagination.properties.months.default[counter];

                    for (counter_inner = 0; counter_inner < data.length; counter_inner++) {
                        issue_index                                                 = data[counter_inner].issue_index;
                        issue_month                                                 = moment(data[counter_inner].issue_date).month();

                        if ((issue_month === counter) && (issue_index.length)) {
                            framework.pagination.properties.months.labels[counter]  += '<br /><span class="pagination-issue-index-label">(#' + issue_index + ')</span>';

                            if ((framework.pagination.properties.elements.issue_index) && ((parseInt(framework.pagination.properties.elements.month.val()) - 1) === issue_month)) {
                                framework.pagination.properties.elements.issue_index.val(issue_index);

                                framework.pagination.properties.issue_index             = issue_index;
                            }
                            break;
                        }
                    }
                }

                // update month slider pips with labels we just compiled
                framework.pagination.properties.elements.sliders.month.slider('pips', {rest: 'label', labels: framework.pagination.properties.months.labels});
            }
        },

        placements: {
            add_and_place: function(page, job_id, job_image, selection, start_row, end_row, start_col, end_col) {
                var placement;

                for (var counter = 0; counter < framework.pagination.properties.pages.length; counter++) {
                    if (framework.pagination.properties.pages[counter].order.current === page) {
                        placement                       =   jQuery.extend(
                                                                framework.pagination.properties.placement_template,
                                                                {
                                                                    id: 0,
                                                                    job: {
                                                                         id: job_id
                                                                    },
                                                                    start_row: start_row,
                                                                    end_row: end_row,
                                                                    start_col: start_col,
                                                                    end_col: end_col
                                                                });

                        framework.pagination.properties.pages[counter].placements.push(placement);

                        framework.pagination.fn.placements.place(job_id, job_image, selection, start_row, end_row, start_col, end_col);

                        framework.pagination.properties.elements.modal.modal('hide');

                        framework.pagination.fn.commit_pagination_data();
                        break;
                    }
                }
            },

            place: function(job_id, job_image, selection, start_row, end_row, start_col, end_col) {
                var total_width     = 0;
                var total_height    = 0;
                var start_x         = 0;
                var start_y         = 0;
                var first_elem, new_elem, elem_row, elem_col, counter;

                console.log(arguments, selection.length);

                for (counter = 0; counter < selection.length; counter++) {
                    elem_row        = parseInt(jQuery(selection[counter]).attr('data-row'));
                    elem_col        = parseInt(jQuery(selection[counter]).attr('data-col'));

                    if (elem_row === start_row) {
                        if (end_col > start_col) {
                            if (elem_col === start_col) {
                                total_width     += parseFloat(jQuery(selection[counter]).outerWidth(false));
                            } else {
                                total_width     += parseFloat(jQuery(selection[counter]).outerWidth(true));
                            }
                        } else {
                            total_width     += parseFloat(jQuery(selection[counter]).outerWidth(false));
                        }
                    }
                    if (elem_col === start_col) {
                        total_height    += parseFloat(jQuery(selection[counter]).outerHeight((elem_row === start_row) ? false : true));
                    }
                }

                first_elem      = jQuery('[data-row="' + start_row + '"][data-col="' + start_col + '"]');
                start_x         = first_elem.position().left + (jQuery(first_elem).outerWidth(true) - jQuery(first_elem).outerWidth());
                start_y         = first_elem.position().top;

                new_elem        =   jQuery('<div />')
                                        .attr('data-job-id', job_id)
                                        .addClass('preview')
                                        .css({
                                            position: 'absolute',
                                            top: start_y + 'px',
                                            left: start_x + 'px',
                                            width: total_width + 'px',
                                            height: total_height + 'px'
                                        })
                                        .html('<img src="' + job_image + '" />');

                jQuery('.pagination-row').prepend(new_elem);
            }
        },

        selection: {
            handler: function(event, ui) {
                framework.pagination.fn.selection.get_proofs();
            },

            get_proofs: function() {
                jQuery.get( framework.properties.paths.root + 'helpers/pagination/get/jobs/' + framework.pagination.properties.elements.media.val(),
                            function(data) {
                                framework.pagination.properties.elements.modal.html(data).modal();
                                framework.pagination.properties.elements.modal.unbind('hide').bind('hide', framework.pagination.fn.selection.clear_grid);

                                jQuery('select[name="modal-job-id"]', framework.pagination.properties.elements.modal).unbind('change').bind('change', framework.pagination.fn.selection.preview_proof);
                                jQuery('#modal-select', framework.pagination.properties.elements.modal).unbind('click').bind('click', framework.pagination.fn.selection.select_proof);
                            });
            },

            get_proof_details: function() {
                var data;
                var selector            = jQuery('select[name="modal-job-id"]', framework.pagination.properties.elements.modal);
                var selected_option     = jQuery('option:selected', selector);
                var selected_id         = parseInt(selector.val());

                jQuery('.preview-inner', framework.pagination.properties.elements.modal).html('');
                jQuery('.preview', framework.pagination.properties.elements.modal).addClass('hidden');

                if ((selected_id > 0) && (selected_option)) {
                    var media_color_value   = parseInt(jQuery('input[name="media-color-value"]').val());
                    var media_color_label   = jQuery('input[name="media-color-label"]').val();

                    var proof_image         = selected_option.attr('data-proof');
                    var proof_key           = selected_option.attr('data-job-key');
                    var proof_color_value   = parseInt(selected_option.attr('data-color-value'));
                    var proof_color_label   = selected_option.attr('data-color-label');

                    data                    = {
                        job: {
                            id: selected_id,
                            key: proof_key,
                            proof: proof_image
                        },
                        media_color: {
                            value: media_color_value,
                            label: media_color_label
                        },
                        color: {
                            value: proof_color_value,
                            label: proof_color_label
                        }
                    };
                }

                return data;
            },

            preview_proof: function(event) {
                var selector            = jQuery('select[name="modal-job-id"]', framework.pagination.properties.elements.modal);
                var proof_data          = framework.pagination.fn.selection.get_proof_details();

                if (proof_data) {
                    if (proof_data.color.value >= proof_data.media_color.value) {
                        jQuery('.preview .preview-inner', framework.pagination.properties.elements.modal).html('<img src="' + framework.properties.paths.content + 'jobs/' + proof_data.job.key + '/' + proof_data.job.proof + '" />');
                        jQuery('.preview', framework.pagination.properties.elements.modal).removeClass('hidden');
                    } else {
                        bootbox.alert('Sorry, but this proof cannot be added to this media type as this media type does not support graphics of the type: ' + proof_data.color.label + '.');

                        selector.val('0');
                    }
                } else {
                    // TODO: error message?
                    selector.val('0');
                }
            },

            select_proof: function(event) {
                var cur_page    = parseInt(framework.pagination.properties.elements.current_page.val());
                var elements    = jQuery('.pagination-row .ui-selected');
                var start_row   = 0;
                var end_row     = 0;
                var start_col   = 0;
                var end_col     = 0;
                var elem_row, elem_col, counter;

                var proof_data  = framework.pagination.fn.selection.get_proof_details();

                for (counter = 0; counter < elements.length; counter++) {
                    elem_row        = parseInt(jQuery(elements[counter]).attr('data-row'));
                    elem_col        = parseInt(jQuery(elements[counter]).attr('data-col'));

                    if ((start_row === 0) || (start_row > elem_row)) {
                        start_row       = elem_row;
                    }
                    if ((end_row === 0) || (end_row < elem_row)) {
                        end_row         = elem_row;
                    }

                    if ((start_col === 0) || (start_col > elem_col)) {
                        start_col       = elem_col;
                    }
                    if ((end_col === 0) || (end_col < elem_col)) {
                        end_col         = elem_col;
                    }
                }

                framework.pagination.fn.placements.add_and_place(   cur_page,
                                                                    proof_data.job.id,
                                                                    framework.properties.paths.content + 'jobs/' + proof_data.job.key + '/' + proof_data.job.proof,
                                                                    elements,
                                                                    start_row,
                                                                    end_row,
                                                                    start_col,
                                                                    end_col);
            },

            clear_grid: function() {
                jQuery('.pagination-row .ui-selected').removeClass('ui-selected');
            }
        },

        reservations: {
            add: function() {
                var zone_id             = parseInt(jQuery(this).attr('data-zone-id'));
                var proceed             = false;
                var counter;

                if ((!isNaN(zone_id)) && (zone_id > 0)) {
                    for (counter = 0; counter < framework.pagination.properties.reservations.premium_pages.length; counter++) {
                        if (framework.pagination.properties.reservations.premium_pages[counter].zone == zone_id) {
                            proceed             = true;
                            break;
                        }
                    }

                    if (proceed) {
                        var container           = jQuery(this).parent('div');
                        var template            = Handlebars.compile(jQuery('#template-pagination-reservation').html());
                        var options             = '';

                        framework.pagination.properties.reservations.add_counter++;

                        container.hide();
                        container.before(template({count: framework.pagination.properties.reservations.add_counter, zone: zone_id}));

                        for (counter = 0; counter < framework.pagination.properties.reservations.premium_pages.length; counter++) {
                            if (framework.pagination.properties.reservations.premium_pages[counter].zone == zone_id) {
                                jQuery('select[name="add-page-' + framework.pagination.properties.reservations.add_counter + '"]').append('<option value="' + framework.pagination.properties.reservations.premium_pages[counter].page + '">Page #' + framework.pagination.properties.reservations.premium_pages[counter].page + '</option>');
                            }
                        }
                    } else {
                        bootbox.alert('Sorry, but there are no pages marked as premium within this zone.');
                    }
                }
            }
        },

        batch: {
            populate_zones: function() {
                var counter, counter_inner, counter_outer
                var results         = jQuery('#pagination-batch-results table>tbody');
                var progress_bar    = jQuery('#pagination-progress-meter');
                var progress_zone   = jQuery('#pagination-progress-zone');
                var media           = parseInt(jQuery('input[name="media"]').val());
                var month           = parseInt(jQuery('input[name="month"]').val());
                var year            = parseInt(jQuery('input[name="year"]').val());
                var pages           = parseInt(jQuery('input[name="pages"]').val());
                var zone_name;

                if ((results) && (!isNaN(month)) && (!isNaN(year)) && (!isNaN(media)) && (!isNaN(pages)) && (month > 0) && (year > 0) && (media > 0) && (pages > 0)) {
                    if (framework.pagination.properties.batch.processing.index < framework.pagination.properties.batch.zones_selected.length) {
                        progress_bar.css('width', ((100 / framework.pagination.properties.batch.zones_selected.length) * (framework.pagination.properties.batch.processing.index + 1)) + '%');

                        for (counter = 0; counter < framework.pagination.properties.batch.zones_selected.length; counter++) {
                            for (state in framework.pagination.properties.batch.zones_all) {
                                for (zone_id in framework.pagination.properties.batch.zones_all[state]) {
                                    if (parseInt(zone_id) === parseInt(framework.pagination.properties.batch.zones_selected[framework.pagination.properties.batch.processing.index])) {
                                        zone_name           = framework.pagination.properties.batch.zones_all[state][zone_id];

                                        progress_zone.html('<strong>' + zone_name + '</strong>');

                                        jQuery.post(    framework.properties.paths.root + 'helpers/pagination/batch',
                                                        {
                                                            media: media,
                                                            year: year,
                                                            month: month,
                                                            pages: pages,
                                                            zone: zone_id
                                                        },
                                                        function(data) {
                                                            results.append( '<tr>' +
                                                                                '<td>' + zone_name + '</td>' +
                                                                                '<td>' +
                                                                                    '<span class="label label-' + ((data.success) ? 'success' : 'important') + '">' + data.message + '</span>' +
                                                                                '</td>' +
                                                                            '</tr>');

                                                            framework.pagination.properties.batch.processing.index++;

                                                            setTimeout(framework.pagination.fn.batch.populate_zones, 1000);
                                                        },
                                                        'json');

                                        return false;
                                    }
                                }
                            }
                        }
                    } else {
                        progress_bar.parent('.progress').removeClass('active');
                        progress_zone.html('<strong>Complete!</strong>');

                        jQuery('#pagination-batch-process').before('<div class="alert alert-success"><strong>Complete!</strong><p>Batch pagination is complete!</p></div>');
                        jQuery('#pagination-batch-finish').attr('disabled', false).removeClass('disabled');
                    }
                }
            }
        }
    }
};