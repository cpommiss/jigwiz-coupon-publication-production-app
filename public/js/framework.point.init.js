jQuery(document).ready(function() {
    var range_options       = {};
    var range_start         = jQuery('input[name="start"]').val();
    var range_stop          = jQuery('input[name="stop"]').val();

    if ((range_start.length) && (range_stop.length)) {
        range_options.startDate         = moment(range_start);
        range_options.endDate           = moment(range_stop);
    }

    jQuery('#point-range').daterangepicker(range_options);
    jQuery('#point-range').on('apply.daterangepicker', framework.point.fn.set_date_range);

    jQuery('.datatable').dataTable( {
        'sDom': "<'row-fluid'<'span4'T><'span8 pull-right'f>r><'row-fluid'<'span12'l>>t<'row-fluid point-footer'<'span6'i><'span6 pull-right'p>>",
        'sPaginationType': 'bootstrap',
        'oLanguage': {
            'sLengthMenu': '_MENU_&nbsp;&nbsp;records per page'
        },
        'sScrollX': '100%',
        'bScrollCollapse': true,
        'oTableTools': {
            'sSwfPath': '../../js/vendor/plugins/dataTables/tableTools/swf/copy_csv_xls_pdf.swf',
            'aButtons': [
                'csv',
                'xls'
            ]
        }
    });
});