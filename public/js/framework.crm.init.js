jQuery(document).ready(function() {
    jQuery('#btn-add-criteria').bind('click', framework.crm.fn.add_criteria_row);
    jQuery('.container')
        .delegate('.btn-remove-criteria', 'click', framework.crm.fn.remove_criteria_row)
        .delegate('.criteria-type', 'change', framework.crm.fn.build_criteria_selections);

    framework.crm.fn.restore_search_criteria();

    if (jQuery('.datatable').length) {
        jQuery('.datatable').each(function() {
            jQuery(this).dataTable( {
                'sDom': "<'row-fluid'<'span4'T><'span8 pull-right'f>r><'row-fluid'<'span12'l>>t<'row-fluid crm-footer'<'span6'i><'span6 pull-right'p>>",
                'sPaginationType': 'bootstrap',
                'oLanguage': {
                    'sLengthMenu': '_MENU_&nbsp;&nbsp;records per page'
                },
                'sScrollX': '100%',
                'bScrollCollapse': true,
                'oTableTools': {
                    'sSwfPath': '/js/vendor/plugins/dataTables/tableTools/swf/copy_csv_xls_pdf.swf',
                    'aButtons': [
                        'csv',
                        'xls'
                    ]
                }
            });
        });
    }

    jQuery('.tabbable a[data-toggle="tab"]').on('shown', function() {
        var tab_target      = jQuery(this).attr('href').replace('#', '');

        jQuery('#crm-data-' + tab_target)
            .css('width', '100%')
            .dataTable().fnAdjustColumnSizing();
    });
    jQuery('.tabbable a[data-toggle="tab"]:first').tab('show');
});