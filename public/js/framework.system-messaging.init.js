jQuery(document).ready(function() {
    jQuery('.wysihtml5').wysihtml5( {
                                        'font-styles': true,
                                        'color': true,
                                        'emphasis': true,
                                        'lists': true,
                                        'html': false,
                                        'link': true,
                                        'image': false,
                                        'stylesheets': [framework.properties.paths.wysihtml5.colors]
                                    });
});
