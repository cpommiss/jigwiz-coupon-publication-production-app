jQuery(document).ready(function() {
    framework.pagination.fn.init_elements();

    jQuery('.pagination-year')
        .spinner('changed', framework.pagination.fn.fetch_pagination_data);

    framework.pagination.properties.elements.sliders.month
        .slider({min: 1, max: 12, value: moment().month() + 1})
        .on('slidechange', framework.pagination.fn.slider_set_month);

    framework.pagination.properties.elements.sliders.page
        .slider({min: 1, max: 4})
        .slider('pips')
        .slider('float')
        .on('slidechange', framework.pagination.fn.slider_set_page);

    jQuery('.pagination-page-settings')
        .popover({
            html: true,
            trigger: 'click',
            placement: 'bottom',
            content: framework.pagination.fn.popovers.settings.get_template
        })
        .on('click', framework.pagination.fn.popovers.settings.setup);

    jQuery('.pagination-issue-index')
        .popover({
            html: true,
            trigger: 'click',
            placement: 'bottom',
            content: framework.pagination.fn.popovers.issue_index.get_template
        })
        .on('click', framework.pagination.fn.popovers.issue_index.setup);

    jQuery('.pagination-page-creation-options a').bind('click', function() { framework.pagination.fn.insert_page(jQuery(this).attr('data-type')) });
    jQuery('.pagination-page-management-options a').bind('click', function() { framework.pagination.fn.manage_page(jQuery(this).attr('data-type')) });
    jQuery('input[name="year"], #pagination-settings select[name="zone"]').bind('change', framework.pagination.fn.fetch_pagination_data);
    jQuery('input[name="premium"]').bind('change', framework.pagination.fn.handle_premium_toggle);
    jQuery('.pagination-controls a').bind('click', framework.pagination.fn.control_page);

    framework.pagination.fn.get_pagination_data();

    jQuery('.pagination-row')
        .selectable({
            filter: '.selectable',
            stop: framework.pagination.fn.selection.handler
        });
});
