framework.media = {
    properties: {
        templates: {
            cell_placeholder: '<div class="mockup-placeholder"><span>Ad</span></div>'
        }
    },
    fn: {
        show_preview: function() {
            var media_width         = parseFloat(jQuery('#media_type_width').val());
            var media_height        = parseFloat(jQuery('#media_type_height').val());
            var media_max_rows      = parseInt(jQuery('#media_type_max_rows').val());
            var media_max_cols      = parseInt(jQuery('#media_type_max_cols').val());

            if ((!isNaN(media_width)) && (!isNaN(media_height)) && (!isNaN(media_max_rows)) && (!isNaN(media_max_cols)) && (media_width > 0) && (media_height > 0) && (media_max_rows > 0) && (media_max_cols > 0)) {
                var preview_html        = framework.pagination.fn.render_page(  media_width,
                                                                                media_height,
                                                                                media_max_rows,
                                                                                media_max_cols,
                                                                                jQuery('#pagination-preview .page-preview'),
                                                                                true,
                                                                                'row-fluid page-preview-mockup',
                                                                                framework.media.properties.templates.cell_placeholder);

                jQuery('#pagination-preview').removeClass('hidden');
                jQuery('#pagination-preview .page-preview>.page-preview-inner>.page-preview-mockup').remove();

                // set width and height labels
                jQuery('#pagination-preview cite.width span.value').html(media_width + ' in.');
                jQuery('#pagination-preview cite.height span.value').html(media_height + ' in.');

                jQuery('#pagination-preview .page-preview>.page-preview-inner>cite.height').after(preview_html);
            } else {
                jQuery('#pagination-preview').addClass('hidden');
            }
        }
    }
};