jQuery(document).ready(function() {
    jQuery('.tabs a').click(function (event) {
        event.preventDefault();

        jQuery(this).tab('show');
        jQuery('select[name="notes_visibility"]').val(jQuery(this).attr('data-vis-type'));
    });
    jQuery('.tabs:first a').trigger('click');

    jQuery('.wysihtml5').wysihtml5({'font-styles': false, 'color': false, 'emphasis': true, 'lists': true, 'html': false, 'link': true, 'image': false, 'stylesheets': false});

    jQuery('img.note-image').each(function() {
        anno.makeAnnotatable(this);
        //anno.hideSelectionWidget(this.src);
    });

    jQuery('.annotations textarea').each(function() {
        var annotation_file_id  = parseInt(jQuery(this).attr('data-annotation-file-id'));
        var annotation_id       = parseInt(jQuery(this).attr('data-annotation-id'));
        var annotation_data     = JSON.parse(jQuery(this).val());
        var annotation_target   = jQuery('img[data-id=' + annotation_file_id + ']');
        var annotation_parent   = annotation_target.parents('fieldset');
        var annotation_image    = annotation_target.attr('src');

        if ((annotation_data) && (annotation_image.length) && (!isNaN(annotation_file_id)) && (!isNaN(annotation_id)) && (annotation_file_id > 0) && (annotation_id > 0)) {
            annotation_data.src     = annotation_image;

            anno.addAnnotation(annotation_data);

            annotation_parent.find('.alert-annotations').removeClass('hidden');
        }
    });

    anno.addHandler('onAnnotationCreated', function(annotation) {
        var image_target        = jQuery('img[src="' + annotation.src + '"]');
        var file_id             = parseInt(image_target.attr('data-id'));

        if ((!isNaN(file_id)) && (file_id > 0)) {
            image_target.parents('fieldset.well').find('.alert-annotations').removeClass('hidden');

            jQuery.post(    framework.properties.paths.root + 'helpers/annotation/add',
                            {
                                file_id: file_id,
                                data: JSON.stringify(annotation)
                            },
                            function(data) {
                                if (data.status) {
                                    if (data.status == 'success') {
                                        // TODO: refresh annotations

                                    } else {
                                        bootbox.alert(data.status);
                                    }
                                }
                            },
                            'json');
        }
    });

    anno.addHandler('onAnnotationUpdated', function(annotation) {
        if (annotation.annotation_id) {
            jQuery.post(    framework.properties.paths.root + 'helpers/annotation/update',
                            {
                                annotation_id: annotation.annotation_id,
                                data: JSON.stringify(annotation)
                            },
                            function(data) {
                                if (data.status) {
                                    if (data.status == 'success') {
                                        // TODO: refresh annotations

                                    } else {
                                        bootbox.alert(data.status);
                                    }
                                }
                            },
                            'json');
        }
    });

    anno.addHandler('onAnnotationRemoved', function(annotation) {
        if (annotation.annotation_id) {
            // TODO: confirmation box
            jQuery.post(    framework.properties.paths.root + 'helpers/annotation/delete',
                            {
                                annotation_id: annotation.annotation_id
                            },
                            function(data) {
                                if (data.status) {
                                    if (data.status == 'success') {
                                        // TODO: refresh annotations

                                    } else {
                                        bootbox.alert(data.status);
                                    }
                                }
                            },
                            'json');
        }
    });

    jQuery('.btn.push-to-client').bind('click', function(event) {
        event.preventDefault();

        var element         = jQuery(this);

        bootbox.confirm(    'Are you sure you want to push this note out to the client?',
                            function(result) {
                                if (result) {
                                    window.location     = element.attr('href');
                                }
                            });
    });
});
