framework.document = {
    fn: {
        push: {
            setup: function(event) {
                event.preventDefault();

                var modal           = jQuery('#document-modal');

                if (modal) {
                    jQuery.get( framework.properties.paths.root + 'helpers/document/push/' + jQuery(this).attr('data-document-id'),
                                function(data) {
                                    modal.html(data).modal();

                                    jQuery('#modal-send', modal).bind('click', framework.document.fn.push.send);
                                });
                }
            },

            send: function() {
                var modal           = jQuery('#document-modal');

                if (modal) {
                    var document        = parseInt(jQuery('input[name="modal-document-id"]', modal).val());
                    var customer        = parseInt(jQuery('select[name="modal-customer-id"]', modal).val());
                    var notes           = jQuery('textarea[name="modal-notes"]', modal).val();

                    if ((document > 0) && (customer > 0)) {
                        jQuery.post(    framework.properties.paths.root + 'helpers/document/push/send',
                                        {
                                            document: document,
                                            customer: customer,
                                            notes: notes
                                        },
                                        function(data) {
                                            jQuery(modal).modal('hide');

                                            bootbox.alert('Document pushed to customer successfully!');
                                        });
                    } else {
                        alert('Please make sure you have chosen a customer to send this document to.');
                    }
                }
            }
        }
    }
};