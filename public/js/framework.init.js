jQuery(document).ready(function() {
    framework.fn.notifications.init();

    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-Token': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

	jQuery(window).on('resize load', function(){
		var navigation = jQuery('.navigation-block').height() + 176; // offsets
		var content = jQuery(window).height();

		if (navigation < content) {
			jQuery('.navigation-block').affix({
				offset: {
					top: 116,
					bottom: 60
				}
			});
		}
	});
	
	jQuery('.main-navigation li ul').hide();
	jQuery('.main-navigation li.current a').parent().find('ul').slideToggle('slow');
	jQuery('.main-navigation li a').click(
		function () {
			jQuery(this).parent().siblings().find('ul').slideUp('normal');
			jQuery(this).parent().find('ul').slideToggle('normal');
			return false;
		}
	);
	jQuery('.main-navigation li a.no-submenu, .main-navigation li li a').click(
		function () {
			window.location.href = this.href;
			return false;
		}
	);

    jQuery('.remove-modal').bind('click', framework.fn.utilities.remove);
});