jQuery(document).ready(function() {
    jQuery('.tabs a').click(function (event) {
        event.preventDefault();

        jQuery(this).tab('show');
    });

    jQuery('input.colorpicker').each(function() {
        jQuery(this)
            .parent()
            .find('.colorpicker-preview')
            .css('backgroundColor', jQuery(this).val());

        jQuery(this)
            .colorpicker()
            .on('changeColor', function(event) {
                jQuery(event.target)
                    .parent()
                    .find('.colorpicker-preview')
                    .css('backgroundColor', event.color.toHex());
            });
    });
});
