jQuery(document).ready(function() {
    jQuery('.tabs a').click(function (e) {
        e.preventDefault();
        jQuery(this).tab('show');
    });

    jQuery('.wysihtml5').wysihtml5({'font-styles': false, 'color': false, 'emphasis': true, 'lists': true, 'html': false, 'link': true, 'image': false, 'stylesheets': false});
});