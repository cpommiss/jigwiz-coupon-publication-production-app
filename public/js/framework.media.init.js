jQuery(document).ready(function() {
    jQuery('#media_type_width, #media_type_height, #media_type_max_rows, #media_type_max_cols').bind('change', function() {
        setTimeout(framework.media.fn.show_preview, 250);
    });

    setTimeout(framework.media.fn.show_preview, 250);
});