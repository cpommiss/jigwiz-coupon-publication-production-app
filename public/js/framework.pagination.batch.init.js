jQuery(document).ready(function() {
    jQuery('.spinner').spinner();

    jQuery('#month_select')
        .slider({min: 1, max: 12, value: jQuery('input[name="month"]').val()})
        .slider('pips', {rest: 'label', labels: framework.pagination.properties.months.default})
        .on('slidechange', function(event, ui) {
            jQuery('input[name="month"]').val(ui.value);
        });
});
