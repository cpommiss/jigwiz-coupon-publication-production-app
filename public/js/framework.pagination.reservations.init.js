jQuery(document).ready(function() {
    framework.pagination.fn.init_elements();

    jQuery('.spinner').spinner();

    jQuery('#month_select')
        .slider({min: 1, max: 12, value: jQuery('input[name="month"]').val()})
        .slider('pips', {rest: 'label', labels: framework.pagination.properties.months.default})
        .on('slidechange', function(event, ui) {
            jQuery('input[name="month"]').val(ui.value);
        });

    jQuery('.reservation-add').bind('click', framework.pagination.fn.reservations.add);

    framework.pagination.fn.settings.get_issue_indexes();
});
