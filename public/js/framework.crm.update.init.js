jQuery(document).ready(function() {
    jQuery('.tabs a').click(function (e) {
        e.preventDefault();
        jQuery(this).tab('show');
        jQuery('.full-calendar').fullCalendar('render');
    });

    jQuery('.wysihtml5').wysihtml5({'font-styles': false, 'color': false, 'emphasis': true, 'lists': true, 'html': false, 'link': true, 'image': false, 'stylesheets': false});

    jQuery('.full-calendar-client').fullCalendar({
        header: {
            left: 'title',
            center: '',
            right: 'today month,agendaWeek,agendaDay prev,next'
        },
        buttonText: {
            prev: '<span class="awe-arrow-circle-left"></span>',
            next: '<span class="awe-arrow-circle-right"></span>'
        },
        events: framework.properties.paths.root + 'helpers/reminders/client/' + jQuery('input[name="_client"]').val(),
        selectable: true,
        unselectAuto: false,
        selectHelper: true,
        editable: true,
        timezone: 'UTC',
        eventRender: framework.reminders.fn.helpers.render_event,
        eventClick: framework.reminders.fn.views.update,
        eventDrop: framework.reminders.fn.actions.update_datetime,
        eventResize: framework.reminders.fn.actions.update_datetime,
        select: framework.reminders.fn.views.create,
        unselect: framework.reminders.fn.callbacks.unselect
    });
});