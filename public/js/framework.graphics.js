framework.graphics = {
    properties: {
        wizard: {
            containers: {
                main: null
            }
        },
        uploads: {
            containers: {
                main: null,
                list: null,
                dropzone: null
            },
            key: '',
            files: []
        },
        annotations: []
    },
    fn: {
        wizard: {
            init: function() {
                framework.graphics.properties.wizard.containers.main        = jQuery('#wizard');

                framework.graphics.properties.wizard.containers.main.bootstrapWizard({
                    onTabShow: framework.graphics.fn.wizard.show_tab
                });

                /*
                jQuery('.finish', framework.graphics.properties.wizard.containers.main).click(function(e) {
                    framework.graphics.properties.wizard.containers.main
                        .find('a[href*="step1"]')
                        .trigger('click');
                });
                */
            },

            show_tab: function(tab, navigation, index) {
                if (index === 0) {
                    if (!jQuery('#job_zone').hasClass('initialized')) {
                        jQuery('#job_zone')
                            .addClass('initialized')
                            .chosen();
                    }
                }

                var total       = navigation.find('li').length;
                var current     = index + 1;
                var percent     = (current / total) * 100;

                jQuery('#bar', framework.graphics.properties.wizard.containers.main)
                    .find('.bar')
                    .css({
                        width: percent + '%'
                    });

                // If it's the last tab then hide the next button and show the finish instead
                if (current >= total) {
                    framework.graphics.properties.wizard.containers.main
                        .find('.pager .next')
                        .hide();
                    framework.graphics.properties.wizard.containers.main
                        .find('.pager .finish')
                        .show()
                        .removeClass('disabled');
                } else {
                    framework.graphics.properties.wizard.containers.main
                        .find('.pager .next')
                        .show();
                    framework.graphics.properties.wizard.containers.main
                        .find('.pager .finish')
                        .hide();
                }
            }
        },
        uploads: {
            init: function() {
                framework.graphics.properties.uploads.containers.main       = jQuery('#upload');
                framework.graphics.properties.uploads.containers.list       = jQuery('ul', framework.graphics.properties.uploads.containers.main);  // ul
                framework.graphics.properties.uploads.containers.dropzone   = jQuery('#drop');

                framework.graphics.properties.uploads.key                   = jQuery('input[name="job_key"]').val();

                jQuery('a', framework.graphics.properties.uploads.containers.dropzone).click(function() {
                    jQuery(this)
                        .parent()
                        .find('input')
                        .click();
                });

                // Initialize the jQuery File Upload plugin
                framework.graphics.properties.uploads.containers.main.fileupload({
                    dropZone: framework.graphics.properties.uploads.containers.dropzone,
                    formData: {'key': framework.graphics.properties.uploads.key},
                    url: framework.properties.paths.root + 'helpers/uploads/multi',
                    add: framework.graphics.fn.uploads.add,
                    progress: framework.graphics.fn.uploads.progress,
                    done: framework.graphics.fn.uploads.done,
                    fail: framework.graphics.fn.uploads.fail
                });

                // Prevent the default action when a file is dropped on the window
                jQuery(document).on('drop dragover', function(e) {
                    e.preventDefault();
                });
            },

            add: function(e, data) {
                var tpl         =   jQuery(
                                        '<li class="working">' +
                                            '<input type="text" value="0" data-width="48" data-height="48" data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" />' +
                                            '<p></p>' +
                                            '<span></span>' +
                                        '</li>'
                                    );

                // Append the file name and file size
                tpl.find('p')
                    .text(data.files[0].name)
                    .append('<i>' + framework.graphics.fn.utils.format_file_size(data.files[0].size) + '</i>');

                // Add the HTML to the UL element
                data.context    = tpl.appendTo(framework.graphics.properties.uploads.containers.list);

                // Initialize the knob plugin
                tpl.find('input')
                    .knob();

                // Listen for clicks on the cancel icon
                tpl.find('span').click(function(){
                    if (tpl.hasClass('working')) {
                        jqXHR.abort();
                    }

                    tpl.fadeOut(function(){
                        tpl.remove();
                    });
                });

                // Automatically upload the file once it is added to the queue
                var jqXHR   = data.submit();
            },

            progress: function(e, data){
                // Calculate the completion percentage of the upload
                var progress = parseInt(data.loaded / data.total * 100, 10);

                // Update the hidden input field and trigger a change
                // so that the jQuery knob plugin knows to update the dial
                data.context.find('input').val(progress).change();

                if (progress == 100){
                    data.context.removeClass('working');
                }
            },

            done: function(e, data) {
                framework.graphics.properties.uploads.files.push(data.result);

                jQuery('input[name="job_files"]').val(JSON.stringify(framework.graphics.properties.uploads.files));
            },

            fail: function(e, data){
                data.context.addClass('error');
            }
        },
        utils: {
            format_file_size: function(bytes) {
                if (typeof bytes !== 'number') {
                    return '';
                }

                if (bytes >= 1000000000) {
                    return (bytes / 1000000000).toFixed(2) + ' GB';
                }

                if (bytes >= 1000000) {
                    return (bytes / 1000000).toFixed(2) + ' MB';
                }

                return (bytes / 1000).toFixed(2) + ' KB';
            }
        }
    }
};