jQuery(document).ready(function() {
    framework.pagination.fn.init_elements();

    jQuery('.spinner').spinner();

    jQuery('#month_select')
        .slider({min: 1, max: 12, value: jQuery('input[name="month"]').val()})
        .slider('pips', {rest: 'label', labels: framework.pagination.properties.months.default})
        .on('slidechange', function(event, ui) {
            jQuery('input[name="month"]').val(ui.value);
        });

    if (jQuery('.datatable').length) {
        jQuery('.datatable').each(function() {
            jQuery(this).dataTable( {
                'sDom': "<'row-fluid'<'span4'T><'span8 pull-right'f>r><'row-fluid'<'span12'l>>t<'row-fluid report-footer'<'span6'i><'span6 pull-right'p>>",
                'sPaginationType': 'bootstrap',
                'oLanguage': {
                    'sLengthMenu': '_MENU_&nbsp;&nbsp;records per page'
                },
                'sScrollX': '100%',
                'bScrollCollapse': true,
                'oTableTools': {
                    'sSwfPath': '/js/vendor/plugins/dataTables/tableTools/swf/copy_csv_xls_pdf.swf',
                    'aButtons': [
                        'csv',
                        'xls'
                    ]
                }
            });
        });
    }

    Tipped.create(  'td[data-image-proof]',
                    function(target) {
                        return '<img src="' + jQuery(target).attr('data-image-proof') + '">';
                    });

    framework.pagination.fn.settings.get_issue_indexes();
});
