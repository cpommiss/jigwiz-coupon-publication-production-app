jQuery(document).ready(function() {
    jQuery('.wysihtml5').wysihtml5({'font-styles': false, 'color': false, 'emphasis': true, 'lists': true, 'html': false, 'link': true, 'image': false, 'stylesheets': false});

    framework.graphics.fn.wizard.init();
    framework.graphics.fn.uploads.init();
});