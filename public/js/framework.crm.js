framework.crm = {
    properties: {
        search_types: [],
        search_criteria: []
    },

    fn: {
        add_criteria_row: function() {
            var last_criteria_row           = jQuery('.criteria-row:last');

            if (last_criteria_row.length) {
                var next_criteria_id            = parseInt(last_criteria_row.attr('data-row-id'));

                if ((!isNaN(next_criteria_id)) && (next_criteria_id > 0)) {
                    next_criteria_id++;

                    var new_criteria_row            = last_criteria_row.clone();

                    last_criteria_row.after(framework.crm.fn.generate_criteria_row(new_criteria_row, next_criteria_id));

                    framework.crm.fn.update_remove_button_visibility();
                }
            }
        },

        remove_criteria_row: function() {
            var num_rows                    = jQuery('.criteria-row').length;

            if (num_rows === 1) {
                bootbox.alert('Sorry, but you cannot remove the only search criteria selection box')
            } else {
                var element                     = jQuery(this);

                bootbox.confirm(    'Are you sure you want to remove this search criteria?',
                                    function(result) {
                                        if (result) {
                                            element.parents('.criteria-row')
                                                .slideUp(function() {
                                                    jQuery(this).remove();

                                                    framework.crm.fn.update_remove_button_visibility();
                                                });
                                        }
                                    });
            }
        },

        update_remove_button_visibility: function() {
            var num_rows                    = jQuery('.criteria-row').length;

            if (num_rows === 1) {
                jQuery('.criteria-row')
                    .find('.criteria-remove-control')
                    .addClass('hidden');
            } else {
                jQuery('.criteria-row')
                    .find('.criteria-remove-control')
                        .removeClass('hidden');
            }
        },

        generate_criteria_entry_html: function(criteria_type, criteria_row_id) {
            var html                        = '';

            if (framework.crm.properties.search_types.length) {
                for (var counter = 0; counter < framework.crm.properties.search_types.length; counter++) {
                    if (framework.crm.properties.search_types[counter].ordinal === criteria_type) {
                        html                     +=     '<div class="span4">' +
                                                            '<label class="control-label" for="criteria_data_primary_' + criteria_row_id + '">&hellip;is ' + ((framework.crm.properties.search_types[counter].match === 'like') ? 'like' : 'equal to') + '</label>' +
                                                            '<div class="controls">' +
                                                                '<input id="criteria_data_primary_' + criteria_row_id + '" name="criteria_data_primary_' + criteria_row_id + '" type="text" class="span12 criteria-data-primary">' +
                                                            '</div>' +
                                                        '</div>';
                    }
                }
            }

            return html;
        },

        generate_criteria_row: function(row, criteria_row_id) {
            row
                .find('.criteria-controls')
                .html('');
            row
                .attr('data-row-id', criteria_row_id)
                .find('select.criteria-type')
                .attr('id', 'criteria_' + criteria_row_id)
                .attr('name', 'criteria_' + criteria_row_id)
                .val('');
            row
                .find('label')
                .attr('for', 'criteria_' + criteria_row_id);

            return row;
        },

        build_criteria_selections: function() {
            var parent                      = jQuery(this).parents('.criteria-row');
            var cur_criteria_type           = parseInt(jQuery(this).val());
            var cur_criteria_row_id         = parseInt(parent.attr('data-row-id'));

            if ((!isNaN(cur_criteria_type)) && (!isNaN(cur_criteria_row_id)) && (cur_criteria_type > 0) && (cur_criteria_row_id > 0)) {
                parent
                    .find('.criteria-controls')
                        .html(framework.crm.fn.generate_criteria_entry_html(cur_criteria_type, cur_criteria_row_id));
            } else {
                parent
                    .find('.criteria-controls')
                        .html('');
            }
        },

        restore_search_criteria: function() {
            var html                        = '';
            var counter, counter_inner, criteria_index, cur_criteria;

            if ((framework.crm.properties.search_types.length) && (framework.crm.properties.search_criteria.length)) {
                if (framework.crm.properties.search_criteria.length > 1) {
                    // build blank rows
                    for (counter = 1; counter < framework.crm.properties.search_criteria.length; counter++) {
                        framework.crm.fn.add_criteria_row();
                    }
                }

                // set search criteria
                for (counter = 0; counter < framework.crm.properties.search_criteria.length; counter++) {
                    criteria_index                = (counter + 1);
                    cur_criteria                  = framework.crm.properties.search_criteria[counter];

                    // set criteria type
                    jQuery('#criteria_' + criteria_index).val(cur_criteria.type);

                    // generate extra data HTML
                    html                         = framework.crm.fn.generate_criteria_entry_html(cur_criteria.type, criteria_index);

                    jQuery('.criteria-row[data-row-id="' + criteria_index + '"]')
                        .find('.criteria-controls')
                        .html(html);

                    // populate extra data with search options
                    for (counter_inner = 0; counter_inner < framework.crm.properties.search_types.length; counter_inner++) {
                        if (framework.crm.properties.search_types[counter_inner].ordinal === cur_criteria.type) {
                            jQuery('#criteria_data_primary_' + criteria_index)
                                .val(cur_criteria.primary);
                            break;
                        }
                    }
                }
            }
        },

        authorization: {
            push: function() {
                var client              = parseInt(jQuery('select[name="client"]').val());
                var media               = parseInt(jQuery('select[name="media"]').val());
                var month               = parseInt(jQuery('input[name="month"]').val());
                var year                = parseInt(jQuery('input[name="year"]').val());

                if ((!isNaN(client)) && (client > 0) && (!isNaN(media)) && (media > 0) && (!isNaN(month)) && (month > 0) && (!isNaN(year)) && (year > 0)) {
                    jQuery.post(    framework.properties.paths.root + 'helpers/crm/auth/request',
                                    {
                                        client: client,
                                        media: media,
                                        month: month,
                                        year: year
                                    },
                                    function(response) {
                                        if (response.success) {
                                            if (response.message) {
                                                bootbox.alert(response.message);
                                            }
                                        } else {
                                            if (response.message) {
                                                bootbox.alert(response.message)
                                            } else {
                                                bootbox.alert('An unexpected error has occurred. Please try again.');
                                            }
                                        }
                                    },
                                    'json');
                } else {
                    bootbox.alert('Some information was missing. Please complete all fields, and try again.');
                }

                return false;
            }
        }
    }
};