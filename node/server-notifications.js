var express         = require('express'),
    http            = require('http'),
    server          = http.createServer(app);

var app             = express();

const io            = require('socket.io').listen(1337);
const redis         = require('redis');
const redis_client  = redis.createClient();

redis_client.subscribe('notifications');
redis_client.on('message', function(channel, message) {
    io.sockets.emit(channel, message);
});