<?php

    class AppUtils {
        public static function strip_tag_whitespace($string) {
            return preg_replace('~>\s+<~', '><', $string);
        }

        public static function config_reduce_complex_array_to_kvp($source, $key, $val) {
            $output         = array();

            foreach ($source as $source_val) {
                $output[$source_val[$key]]  = $source_val[$val];
            }

            return $output;
        }

        public static function config_get_ordinal_by_value($source, $key, $val, $key_return = 'ordinal') {
            foreach ($source as $source_val) {
                if ($source_val[$key] == $val) {
                    return $source_val[$key_return];
                }
            }

            return false;
        }

        public static function config_get_key_by_value($source, $lookup) {
            foreach ($source as $key => $value) {
                if ($value == $lookup) {
                    return $key;
                }
            }

            return false;
        }
    }

?>