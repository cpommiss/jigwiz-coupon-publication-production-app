<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProcessNotifications extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'scheduled:notifications';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Processes user notifications and dispatches Redis events as needed.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler) {
		return $scheduler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire() {
        $now                =   time();

        // find CRM reminders that need to fire
        $reminders          =   CRMReminder::with('client', 'creator')
                                    ->where('notified', '=', false)
                                    ->whereRaw('DATE_ADD(start, INTERVAL `when` MINUTE) <= ?', array(date(Config::get('settings.datetime.formats.sql_date'), $now)))
                                    ->get();

        if (!$reminders->isEmpty()) {
            foreach ($reminders as $reminder) {
                $reminder_url       = route('crm_update_client', $reminder->client->id);
                $reminder_time      = Carbon::parse($reminder->start);

                Notification::dispatch( 'settings.notifications.types.crm-reminder',
                                        $reminder->creator->id,
                                        $reminder->client->id,
                                        $reminder_url,
                                        $reminder_time,
                                        'notifications.crm.reminder',
                                        array(
                                            'customer'  => $reminder->client->name,
                                            'minutes'   => abs($reminder->when),
                                            'text'      => $reminder->text
                                        ),
                                        true);

                // send e-mail
                if ($reminder->creator->email) {
                    Mail::send( 'emails.crm.internal-reminder',
                                array(
                                    'creator_name'      => $reminder->creator->name,
                                    'customer_name'     => $reminder->client->name,
                                    'reminder_time'     => $reminder_time->format(Config::get('settings.datetime.formats.crm_reminders')),
                                    'reminder_before'   => abs($reminder->when),
                                    'reminder_notes'    => $reminder->text
                                ),
                                function($message) use ($reminder) {
                                    $message
                                        ->to($reminder->creator->email)
                                        ->subject(  Lang::get(  'emails.crm.reminder',
                                                                array(
                                                                    'creator'       =>  $reminder->creator->name,
                                                                    'customer'      =>  $reminder->client->name
                                                                )));
                                });
                }

                // mark as notified
                $reminder->notified = true;
                $reminder->save();
            }
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments() {
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions() {
		return array();
	}

}
