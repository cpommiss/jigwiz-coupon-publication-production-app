<?php

return array(
    'user' => array(
        'authenticated_user' => array(
            'update' => 'Profile updated successfully!'
        ),
        'create' => 'Successfully created a new user!',
        'update' => 'Successfully updated user!',
        'delete' => 'Successfully deleted user!'
    ),
    'system_message' => array(
        'create' => 'Successfully added a new system message!',
        'delete' => 'Successfully deleted system message!'
    ),
    'crm' => array(
        'create' => 'Successfully added a new client record!',
        'create_no_email' => 'Successfully added a new client record!<br /><br /><strong>NOTE: The client you created has no e-mail address on-file. E-mail notifications for this client will not work until an e-mail address is placed on file.</strong>',
        'update' => 'Successfully updated client record!',
        'update_no_email' => 'Successfully updated client record!<br /><br /><strong>NOTE: There is still no e-mail address on file for this client. E-mail notifications for this client will not work until an e-mail address is placed on file.</strong>',
        'delete' => 'Successfully deleted client record!',
        'authorization' => array(
            'sent' => 'Successfully sent publishing authorization request to :customer!',
            'authorized' => 'Authorization complete! Thank you!'
        )
    ),
    'document' => array(
        'create' => 'Successfully added document!',
        'delete' => 'Successfully deleted document!'
    ),
    'media_type' => array(
        'create' => 'Successfully added media type!',
        'update' => 'Successfully updated media type!',
        'delete' => 'Successfully deleted media type!'
    ),
    'zone' => array(
        'create' => 'Successfully added new zone!',
        'update' => 'Successfully updated zone!',
        'delete' => 'Successfully deleted zone!'
    ),
    'graphics_queue' => array(
        'create' => 'Successfully added job to graphics queue!',
        'update' => 'Successfully updated job in graphics queue!',
        'update_settings' => 'Successfully updated job settings!',
        'delete' => 'Successfully deleted job!',
        'note' => 'Successfully posted note!',
        'note_push' => 'Successfully pushed that note to the client!',
        'renew' => 'Successfully renewed the graphics job!'
    ),
    'pagination' => array(
        'batch_add' => 'Successfully added :pages pages!'
    ),
    'reservation' => array(
        'delete' => 'Successfully deleted reservation!'
    ),
    'branding' => array(
        'update' => 'Successfully updated branding options!'
    )
);