<?php

return array(
    'system_message' => array(
        'remove_warning' => '   <p>Are you sure you want to remove this system message?</p>'
    ),
    'user' => array(
        'remove_warning' => '   <p>Are you sure you want to remove this user?</p>
                                <p><strong>THIS ACTION CANNOT BE UNDONE! PLEASE CHOOSE CAREFULLY!</strong></p>'
    ),
    'document' => array(
        'remove_warning' => '   <p>Are you sure you want to remove this document?</p>
                                <p><strong>THIS ACTION CANNOT BE UNDONE! PLEASE CHOOSE CAREFULLY!</strong></p>'
    ),
    'reservation' => array(
        'remove_warning' => '   <p>Are you sure you want to remove this reservation?</p>
                                <p><strong>THIS ACTION CANNOT BE UNDONE! PLEASE CHOOSE CAREFULLY!</strong></p>'
    ),
    'crm' => array(
        'remove_warning' => '   <p>Are you sure you want to remove this client?</p>
                                <p><strong>Deleting a client will...</strong></p>
                                <ul>
                                    <li>...delete all graphics jobs associated with this client.</li>
                                    <li>...delete all reservations associated with this client.</li>
                                </ul>
                                <p><strong>THIS ACTION CANNOT BE UNDONE! PLEASE CHOOSE CAREFULLY!</strong></p>'
    ),
    'graphics_queue' => array(
        'remove_warning' => '   <p>Are you sure you want to remove this graphics job?</p>
                                <p><strong>Deleting a graphics job will...</strong></p>
                                <ul>
                                    <li>...delete all instances of this graphics job in all paginations.</li>
                                    <li>...remove points assigned as a result of the completion of the job.</li>
                                </ul>
                                <p><strong>THIS ACTION CANNOT BE UNDONE! PLEASE CHOOSE CAREFULLY!</strong></p>'
    ),
    'zone' => array(
        'remove_warning' => '   <p>Are you sure you want to remove this zone?</p>
                                <p><strong>Deleting a zone will...</strong></p>
                                <ul>
                                    <li>...delete all paginations associated with this zone.</li>
                                    <li>...delete all graphics jobs associated with this zone.</li>
                                </ul>
                                <p><strong>THIS ACTION CANNOT BE UNDONE! PLEASE CHOOSE CAREFULLY!</strong></p>'
    ),
    'media_type' => array(
        'remove_warning' => '   <p>Are you sure you want to remove this media type?</p>
                                <p><strong>Deleting a media type will...</strong></p>
                                <ul>
                                    <li>...delete all paginations associated with this media type.</li>
                                    <li>...delete all reservations associated with this media type.</li>
                                    <li>...delete all graphics jobs associated with this media type.</li>
                                    <li>...delete all graphics job point rewards associated with this media type.</li>
                                    <li>...delete all client publishing authorizations associated with this media type.</li>
                                </ul>
                                <p><strong>THIS ACTION CANNOT BE UNDONE! PLEASE CHOOSE CAREFULLY!</strong></p>'
    )
);