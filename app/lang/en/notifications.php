<?php

return array(
    'generic' => array(
        'default' => ':text'
    ),
    'crm' => array(
        'reminder' => 'Reminder: You set a calendar event for :customer that occurs in :minutes minutes - <em>&ldquo;:text&rdquo;</em>.',
        'authorization' => 'Publishing authorization has been received for <strong>:customer</strong> for the <strong>:issue</strong> issue of <strong>:media</strong>!'
    ),
    'graphics_queue' => array(
        'created' => 'A new graphics job, <strong>&ldquo;:title&rdquo;</strong>, has been created.',
        'assigned' => 'A new graphics job, <strong>&ldquo;:title&rdquo;</strong>, has been assigned to you.',
        'note' => 'A new note has been posted on the <strong>&ldquo;:title&rdquo;</strong> graphics job.'
    )
);