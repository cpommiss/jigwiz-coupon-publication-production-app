<?php

return array(
    'crm' => array(
        'reminder' => ':creator, this is a reminder about your calendar event for :customer',
        'authorization' => array(
            'client' => ':customer, publishing authorization is needed!',
            'client_resend' => ':customer, we still need your publishing authorization!',
            'internal' => 'Publishing authorization received from :customer.'
        )
    ),
    'document' => array(
        'push' => array(
            'client' => ':customer, a document has been shared with you!'
        )
    ),
    'graphics_queue' => array(
        'create' => array(
            'assigned' => 'You have been assigned to a new graphics job!',
            'created' => 'A new graphics job has been created!'
        ),
        'note_posted' => array(
            'client' => ':customer, a new note has been posted on one of your advertisement jobs!',
            'internal' => 'A new note has been posted on the ":title" graphics job!'
        )
    )
);