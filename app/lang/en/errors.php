<?php

return array(
    'general' => array(
        'no_access' => array(
            'area' => 'You do not have access to that area of the system.',
            'media_zone' => 'You do not have access to either the media type or zone you specified.'
        )
    ),
    'user' => array(
        'credentials_incorrect' => 'Either your username or password was incorrect.',
        'no_access' => array(
            'update' => 'That user record was not found. You may not have access to it, or it may be missing.'
        ),
        'create' => array(
            'general' => 'An unexpected error occurred while trying to save that user account to the database. Please try again.'
        )
    ),
    'system_message' => array(
        'no_access' => array(
            'create' => 'You do not have access to create system messages.',
            'delete' => 'You do not have access to delete that system message.'
        )
    ),
    'crm' => array(
        'no_access' => array(
            'read' => 'You do not have access to view client records.',
            'create' => 'You do not have access to add clients.',
            'update' => 'You do not have access to make changes to that client account.',
            'delete' => 'You do not have access to delete that client account.',
            'master' => 'You do not have access to the Master CRM.',
            'personal' => 'You do not have access to a Personal CRM.',
            'authorization' => 'You do not have access to request authorizations from client accounts.'
        ),
        'authorization' => array(
            'data_missing' => 'Some data was missing and/or incomplete, or you may not have access to that client or media type. Please try again.',
            'already_authorized' => 'This customer has already completed an authorization for this date/media type.',
            'no_email_on_file' => 'That customer has no e-mail address on file, so a publication authorization request could not be sent.',
            'general_client' => 'That authorization could not be found. You may not have access to it, or it may be missing.'
        ),
        'not_found' => 'That CRM record was not found. You may not have access to it, or it may be missing.'
    ),
    'document' => array(
        'no_access' => array(
            'read' => 'You do not have access to view documents.',
            'create' => 'You do not have access to add documents.',
            'delete' => 'You do not have access to delete that document.'
        ),
        'create' => array(
            'general' => 'An unexpected error occurred while trying to save the document to the database. Please try again.',
            'disallowed_file_type' => 'The file you uploaded is not of an allowed file type.'
        )
    ),
    'media_type' => array(
        'no_access' => array(
            'read' => 'You do not have access to view media types.',
            'create' => 'You do not have access to add media types.',
            'update' => 'You do not have access to make changes to that media type.',
            'delete' => 'You do not have access to delete that media type.'
        ),
        'not_found' => 'That media type was not found. You may not have access to it, or it may be missing.'
    ),
    'graphics_queue' => array(
        'no_access' => array(
            'read' => 'You do not have access to view graphics jobs.',
            'create' => 'You do not have access to create graphics jobs.',
            'update' => 'You do not have access to that graphics job.',
            'delete' => 'You do not have access to delete that graphics job.',
            'renew' => 'You do not have access to renew that graphics job.',
            'detail' => 'You do not have access to view that graphics job.',
            'create_note' => 'You do not have access to view that graphics job.',
            'push_note' => 'You do not have access to push notes out to the client.'
        ),
        'upload' => array(
            'no_file_found' => 'No file was found to upload.',
            'disallowed_file_type' => 'The file, :original, is not an allowed file type (:extension). Only files of types :allowed_list are allowed.'
        ),
        'note' => array(
            'disallowed_file_type' => 'Your note has been added to this job, however, your file was not added because the type of file you uploaded is not allowed.',
            'upload_error' => 'Your note has been added to this job, however, your file was not added because of an unexpected error when saving the file to the server.',
            'general' => 'An unexpected error was encountered while trying to save that note to the database. Please try again.',
            'not_found' => 'That note was not found. You may not have access to it, or it may be missing.'
        ),
        'renew' => array(
            'general' => 'An unexpected error was encountered while trying to renew that job in the graphics queue. Please try again.'
        ),
        'annotations' => array(
            'data_missing' => 'Some data was missing and/or incomplete. Please try again.',
            'general' => 'An unexpected error was encountered while trying to save that annotation to the database. Please try again.',
            'not_found' => 'That annotation was not found. You may not have access to it, or it may be missing.'
        ),
        'not_found' => 'That graphics job was not found. You may not have access to it, or it may be missing.'
    ),
    'pagination' => array(
        'no_access' => array(
            'read' => 'You do not have access to view pagination data.',
            'update' => 'You do not have access edit pagination data.',
            'batch' => 'You do not have access to edit pagination data in batch.'
        ),
        'batch' => array(
            'pagination_exists' => 'Failed, because pagination has already started in this zone.',
            'partial' => 'Some pages may have been added, but at least one unexpected error occurred while performing batch setup.',
            'validation' => 'Batch setup failed since some data was either inaccessible or missing. Please try again.'
        )
    ),
    'reservation' => array(
        'no_access' => array(
            'update' => 'You do not have access to update reservations.',
            'delete' => 'You do not have access to delete that reservation.'
        )
    ),
    'point' => array(
        'no_access' => array(
            'read' => 'You do not have access to view point standings.'
        )
    ),
    'zone' => array(
        'no_access' => array(
            'read' => 'You do not have access to view zones.',
            'create' => 'You do not have access to add zones.',
            'update' => 'You do not have access to make changes to that zone.',
            'delete' => 'You do not have access to delete that zone.'
        ),
        'create' => array(
            'general' => 'An unexpected error occurred while trying to save the zone to the database. Please try again.'
        ),
        'not_found' => 'That zone was not found. You may not have access to it, or it may be missing.'
    ),
    'branding' => array(
        'update' => array(
            'general_error' => 'An error occurred and your styling options could not be updated.  Please try again.'
        )
    )
);