@extends('layout')
@section('content')

    <h1><a href="/login/" class="brand">Installation</a></h1>

    <div class="data-block">
        <div class="data-container">

            @include('chrome.page.alerts')

            {{ Form::open(array('url' => 'login', 'autocomplete' => 'off', 'novalidate' => 'true')) }}

                <fieldset>
                    <div class="control-group">
                        {{ Form::label('icon', 'Username', array('class' => 'control-label')) }}
                        <div class="controls">
                            {{ Form::text('username', Input::old('username'), array('id' => 'icon', 'required' => 'true', 'data-validation-required-message' => 'You must fill in your username.', 'placeholder' => 'Your username')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        {{ Form::label('password', 'Password', array('class' => 'control-label')) }}
                        <div class="controls">
                            {{ Form::password('password', array('id' => 'password', 'required' => 'true', 'data-validation-required-message' => 'You must fill in your password', 'placeholder' => 'Your password')) }}
                        </div>
                    </div>
                    <div class="form-actions">
                        {{ Form::button('<span class="awe-signin"></span> Log In', array('class' => 'btn btn-block btn-large btn-inverse btn-alt', 'type' => 'submit')) }}
                    </div>
                </fieldset>

            {{ Form::close() }}

        </div>
    </div>

@stop
