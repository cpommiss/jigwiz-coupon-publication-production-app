@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-th"></span> Users</h1>
                        <p>All of the users with access to the system are shown below.</p>
					</article>

                    @include('chrome.page.alerts')

                    <div class="row">
                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2>
                                        <span class="awe-paste"></span> Active User Accounts
                                    </h2>
                                    <ul class="data-header-actions">
                                        <li>
                                            <a class="btn btn-success" href="{{ route('user_create') }}/">Create New User</a>
                                        </li>
                                    </ul>
                                </header>

                                <section>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Title</th>
                                                <th>Username</th>
                                                <th>E-mail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($users as $user)
                                            <tr>
                                                <td>
                                                    @if ($user->avatar)
                                                    <img src="{{ Croppa::url('content/installations/' . Auth::user()->installation . '/avatars/' . $user->avatar, 18, 18) }}">
                                                    @else
                                                    <img src="http://placehold.it/18x18">
                                                    @endif
                                                    &nbsp;
                                                    <a href="{{ route('user_update', $user->id) }}">{{ $user->name }}</a>
                                                </td>
                                                <td>{{ $user->title }}</td>
                                                <td>{{ $user->username }}</td>
                                                <td>{{ $user->email }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </section>
                            </div>
                        </article>
                    </div>
                </div>

@stop