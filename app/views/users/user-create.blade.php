@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1><span class="awe-plus"></span> Create User</h1>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::open(array('route' => 'user_create', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-horizontal form-unstyled', 'files' => true)) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2><span class="awe-gear"></span> User Profile Settings</h2>
                                        <ul class="data-header-actions">
                                            <li class="tabs active"><a href="#user-personal" class="btn btn-flat">Personal Info</a></li>
                                            <li class="tabs"><a href="#user-access" class="btn btn-flat">Access</a></li>
                                        </ul>
                                    </header>
                                    <section class="tab-content">
                                        <div class="tab-pane active" id="user-personal">
                                            <h3>Personal Information</h3>

                                            <div class="row-fluid">
                                                <div class="span4">
                                                    <h3>Avatar Image</h3>

                                                    <p>You may set this user's avatar by selecting an image using the button below.</p>

                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-new fileupload-large thumbnail hidden" style="display: none;"></div>
                                                        <div class="fileupload-preview fileupload-exists fileupload-large thumbnail flexible full"></div>
                                                        <div>
                                                            <span class="btn btn-alt btn-file">
                                                                <span class="fileupload-new">Select image</span>
                                                                <span class="fileupload-exists">Change</span>
                                                                {{ Form::file('avatar', array('id' => 'profile_avatar')) }}
                                                            </span>
                                                            <a class="btn btn-alt btn-danger fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span8">
                                                    <fieldset class="well">
                                                        <div class="control-group">
                                                            {{ Form::label('profile_username', 'Username', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('username', Input::old('username'), array('id' => 'profile_username', 'class' => 'input-large', 'placeholder' => 'Username')) }}
                                                                <span class="help-block">No spaces, punctuation or special characters allowed.</span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('profile_name', 'Name', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('name', Input::old('name'), array('id' => 'profile_name', 'class' => 'input-large', 'placeholder' => 'Name')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('profile_email', 'E-mail', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('email', Input::old('email'), array('id' => 'profile_email', 'class' => 'input-xlarge', 'placeholder' => 'E-mail')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('profile_title', 'Title', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('title', Input::old('title'), array('id' => 'profile_title', 'class' => 'input-large', 'placeholder' => 'Position/Title')) }}
                                                            </div>
                                                        </div>
                                                    </fieldset>

                                                    <h3>Password</h3>

                                                    <fieldset class="well">
                                                        <div class="control-group">
                                                            {{ Form::label('profile_password', 'Password', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::password('password', array('id' => 'profile_password', 'class' => 'input-xlarge', 'placeholder' => 'Password')) }}
                                                                <span class="help-block">At least 5 characters long.</span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('profile_password_confirm', 'Confirm Password', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::password('password_confirm', array('id' => 'profile_password_confirm', 'class' => 'input-xlarge', 'placeholder' => 'Confirm Password')) }}
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="user-access">
                                            <h3 class="pull-left">User Access</h3>
                                            <div class="pull-right" style="margin-top: -5px;">
                                                <label>
                                                    Copy access from:

                                                    <select id="acl-copy">
                                                        <option value="">&nbsp;</option>
                                                        <optgroup label="Presets">
                                                            @foreach (Config::get('settings.acl_structure.presets') as $preset_key => $preset_data)
                                                            <option value="{{ $preset_key }}">{{ $preset_data['title'] }}</option>
                                                            @endforeach
                                                        </optgroup>
                                                    </select>
                                                </label>
                                            </div>

                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="tabbable tabs-left">
                                                        <ul class="nav nav-tabs">
                                                            @foreach (Config::get('settings.acl_structure.nouns') as $acl_noun => $acl_noun_details)
                                                                @if (!isset($acl_noun_details['is_special']))
                                                                    <li><a href="#acl-{{ Str::slug($acl_noun_details['title']) }}" data-toggle="tab">{{ $acl_noun_details['title'] }}</a></li>
                                                                @endif
                                                            @endforeach
                                                            @if (count(Config::get('settings.acl_structure.flags')))
                                                                <li><a href="#acl-flags" data-toggle="tab">Special Access Privileges</a></li>
                                                            @endif
                                                        </ul>
                                                        <div class="tab-content">
                                                            @foreach (Config::get('settings.acl_structure.nouns') as $acl_noun => $acl_noun_details)
                                                                @if (!isset($acl_noun_details['is_special']))
                                                                    <div class="tab-pane" id="acl-{{ Str::slug($acl_noun_details['title']) }}">
                                                                        <fieldset class="well">
                                                                            <h3>{{ $acl_noun_details['title'] }}</h3>

                                                                            @foreach (Config::get('acl') as $acl_verb => $acl_nouns)
                                                                                @foreach ($acl_nouns as $acl_cur_noun => $acl_cur_noun_data)
                                                                                    @if ($acl_cur_noun == $acl_noun)
                                                                                        @if (!is_array($acl_cur_noun_data))
                                                                                            <label class="checkbox">
                                                                                                {{ Form::checkbox('acl[]', $acl_cur_noun_data) }}
                                                                                                <strong>{{ Config::get('settings.acl_structure.verbs.' . $acl_verb) }}</strong> {{ $acl_noun_details['description'] }}<br />
                                                                                            </label>
                                                                                        @else
                                                                                            @foreach ($acl_cur_noun_data as $acl_cur_special_verb => $acl_cur_special_nouns)
                                                                                                @foreach ($acl_cur_special_nouns as $acl_cur_special_noun => $acl_cur_special_noun_data)
                                                                                                    <label class="checkbox special">
                                                                                                        {{ Form::checkbox('acl[]', $acl_cur_special_noun_data) }}
                                                                                                        <strong>{{ Config::get('settings.acl_structure.verbs.' . $acl_cur_special_verb) }}</strong> {{ Config::get(Config::get('settings.acl_structure.nouns.' . $acl_cur_noun . '.extended_lookup') . '.' . $acl_cur_noun . '.' . $acl_cur_special_verb . '.' . $acl_cur_special_noun) }}<br />
                                                                                                    </label>
                                                                                                @endforeach
                                                                                            @endforeach
                                                                                        @endif
                                                                                    @endif
                                                                                @endforeach
                                                                            @endforeach
                                                                        </fieldset>
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                            @if (count(Config::get('settings.acl_structure.flags')))
                                                                <div class="tab-pane" id="acl-flags">
                                                                    <fieldset class="well">
                                                                        <h3>Special Access Privileges</h3>

                                                                        @foreach (Config::get('settings.acl_structure.flags') as $flag_key => $flag_data)
                                                                            <label class="checkbox">
                                                                                {{ Form::checkbox('flags[]', $flag_key) }}
                                                                                <strong>{{ $flag_data }}</strong><br />
                                                                            </label>
                                                                        @endforeach
                                                                    </fieldset>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </section>

                                    {{ Form::button('Create', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/fileupload/bootstrap-fileupload.js') }}
    {{ HTML::script('js/framework.users.js') }}
    {{ HTML::script('js/framework.users.manage.init.js') }}

    <script>
        if ((framework) && (framework.users)) {
            @foreach (Config::get('settings.acl_structure.presets') as $preset_key => $preset_data)

            framework.users.properties.presets['{{ $preset_key }}'] = {flags: [], permissions: []};
                @if (isset($preset_data['flags']))
                @foreach ($preset_data['flags'] as $preset_flag_key => $preset_flag_data)
                framework.users.properties.presets['{{ $preset_key }}'].flags.push('{{ $preset_flag_key }}');
                @endforeach
                @endif
                @foreach ($preset_data['permissions'] as $preset_permission)
                framework.users.properties.presets['{{ $preset_key }}'].permissions.push({{ Config::get('acl.'. $preset_permission) }});
                @endforeach

            @endforeach
        }
    </script>
@stop