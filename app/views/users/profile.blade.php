@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1><span class="awe-pencil"></span> Your Profile</h1>
					</article>

					<div class="row">
						<article class="span12 data-block">
							<div class="data-container">
								<header>
									<h2><span class="awe-gear"></span> Profile Settings</h2>
								</header>
								<section>

                                    {{ Form::open(array('route' => 'profile', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-horizontal form-unstyled', 'files' => true)) }}

                                        <div class="row-fluid">
                                            <div class="span4">
                                                <h3>Avatar Image</h3>

                                                <p>You may use an avatar throughout the system if you like -- just upload a photo below.</p>
                                                <p>We'll take care of resizing it for you, though for best results we recommend an image that is relatively square in nature.</p>

                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    @if (Auth::user()->avatar)
                                                    <div class="fileupload-new fileupload-large thumbnail">
                                                        <img src="{{ Croppa::url('content/installations/' . Auth::user()->installation . '/avatars/' . Auth::user()->avatar, 240, 240) }}">
                                                    </div>
                                                    @else
                                                    <div class="fileupload-new fileupload-large thumbnail hidden" style="display: none;"></div>
                                                    @endif
                                                    <div class="fileupload-preview fileupload-exists fileupload-large thumbnail flexible full"></div>
                                                    <div>
                                                        <span class="btn btn-alt btn-file">
                                                            <span class="fileupload-new">Select image</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            {{ Form::file('avatar', array('id' => 'profile_avatar')) }}
                                                        </span>
                                                        <a class="btn btn-alt btn-danger fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="span8">

                                                @include('chrome.page.alerts')

                                                <fieldset class="well">
													<div class="control-group">
														<label class="control-label" for="input">Username</label>
														<div class="controls">
															<span class="uneditable-input">{{ Auth::user()->username }}</span>
														</div>
													</div>
													<div class="control-group">
                                                        {{ Form::label('profile_name', 'Name', array('class' => 'control-label')) }}
														<div class="controls">
                                                            {{ Form::text('name', Auth::user()->name, array('id' => 'profile_name', 'class' => 'input-large', 'placeholder' => 'Your Name')) }}
														</div>
													</div>
													<div class="control-group">
                                                        {{ Form::label('profile_email', 'E-mail', array('class' => 'control-label')) }}
														<div class="controls">
                                                            {{ Form::text('email', Auth::user()->email, array('id' => 'profile_email', 'class' => 'input-xlarge', 'placeholder' => 'Your E-mail')) }}
														</div>
													</div>
                                                    <div class="control-group">
                                                        {{ Form::label('profile_title', 'Title', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::text('title', Auth::user()->title, array('id' => 'profile_title', 'class' => 'input-large', 'placeholder' => 'Your Position/Title')) }}
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <h3>Password</h3>

                                                <fieldset class="well">
                                                    <div class="control-group">
                                                        {{ Form::label('profile_password', 'New Password', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::password('password', array('id' => 'profile_password', 'class' => 'input-xlarge', 'placeholder' => 'New Password')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('profile_password_confirm', 'Confirm Password', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::password('password_confirm', array('id' => 'profile_password_confirm', 'class' => 'input-xlarge', 'placeholder' => 'Confirm New Password')) }}
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                {{ Form::button('Update', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                            </div>
                                        </div>

                                    {{ Form::close() }}

								</section>
							</div>
						</article>
					</div>
                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/fileupload/bootstrap-fileupload.js') }}
@stop