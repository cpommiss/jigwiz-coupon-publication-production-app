<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>A new graphics job has been created!</h2>

		<div>
            <p>Hello!</p>
            <p>A new graphics job has been created by {{ $creator_name }} entitled <strong>{{ $job_title }}</strong>.  Use the link shown below to view the details of this job and/or assign it to an artist.</p>
            <p>{{ $job_url }}</p>
		</div>
	</body>
</html>