<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>A new note has been posted on one of your jobs!</h2>

		<div>
            <p>Hello, {{ $customer_name }}!</p>
            <p>A new note has been posted on your advertisement job entitled <strong>{{ $job_title }}</strong>.  Use the link shown below to view the latest notes and proofs.</p>
            <p>{{ $job_url }}</p>
		</div>
	</body>
</html>