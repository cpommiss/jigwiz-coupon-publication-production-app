<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>A new note has been posted on a job that you are participant in!</h2>

		<div>
            <p>Hello!</p>
            <p>A new note has been posted on the graphics job entitled <strong>{{ $job_title }}</strong>.  Use the link shown below to view the latest notes and proofs.</p>
            <p>{{ $job_url }}</p>
		</div>
	</body>
</html>