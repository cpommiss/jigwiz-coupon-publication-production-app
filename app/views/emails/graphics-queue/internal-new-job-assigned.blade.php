<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>You have been assigned to a new graphics job!</h2>

		<div>
            <p>Hello, {{ $user_name }}!</p>
            <p>A new graphics job has been assigned to you, entitled <strong>{{ $job_title }}</strong>.  Use the link shown below to view the details of this job.</p>
            <p>{{ $job_url }}</p>
		</div>
	</body>
</html>