<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>A document has been shared with you!</h2>

		<div>
            <p>Hello, {{ $customer_name }}!</p>
            <p>{{ $sender_name }} has shared a document entitled <strong>"{{ $document_title }}"</strong> with you.</p>
            @if (strlen($document_notes))
            <p>
                <strong>Notes from {{ $sender_name }}:</strong>
                <br />
                "{{ $document_notes }}"
            </p>
            @endif
            <p><a href="{{ $document_url }}">Click here to download/view the file.</a></p>
		</div>
	</body>
</html>