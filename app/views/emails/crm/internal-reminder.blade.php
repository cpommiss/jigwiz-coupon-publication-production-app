<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Calendar Reminder!</h2>

		<div>
            <p>Hello, {{ $creator_name }}!</p>
            <p>This is an automatic e-mail for the calendar event you entered for the customer <strong>{{ $customer_name }}</strong> to happen at <strong>{{ $reminder_time }}</strong>. You asked to be notified <strong>{{ $reminder_before }}</strong> minutes before this calendar event.</p>
            @if ($reminder_notes)
            <p>You wrote the following as a note to go along with this reminder:</p>
            <p><em>{{ $reminder_notes }}</em></p>
            @endif
		</div>
	</body>
</html>