<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Publishing authorization received!</h2>

		<div>
            <p>Publishing authorization from <strong>{{ $customer_name }}</strong> for the <strong>{{ $issue_date }}</strong> issue of <strong>{{ $media_name }}</strong> received!</p>
		</div>
	</body>
</html>