<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Publishing authorization is needed!</h2>

		<div>
            <p>Hello, {{ $customer_name }}!</p>
            <p>We need your authorization to proceed with placing and publishing your advertisements in our publications. Use the link provided below to be taken to our online application where you can perform this authorization.</p>
            <p>{{ $auth_url }}</p>
		</div>
	</body>
</html>