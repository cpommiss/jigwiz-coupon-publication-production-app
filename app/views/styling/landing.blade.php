@extends('layout')
@section('header')
    @parent

@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1><span class="awe-tint"></span> Styling</h1>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::model($styling, array('route' => 'styling', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled', 'files' => true)) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2><span class="awe-tint"></span> Styling</h2>
                                        <ul class="data-header-actions">
                                            <li class="tabs active"><a href="#styling-logo" class="btn btn-flat"><span class="awe-picture-o"></span> Logo Image</a></li>
                                            <li class="tabs"><a href="#styling-colors" class="btn btn-flat"><span class="awe-adjust"></span> Colors</a></li>
                                            <li class="tabs"><a href="#styling-typography" class="btn btn-flat"><span class="awe-font"></span> Typography</a></li>
                                        </ul>
                                    </header>
                                    <section class="tab-content">
                                        <div class="tab-pane active" id="styling-logo">
                                            <h2>Logo Image</h2>

                                            <fieldset class="well form-horizontal">
                                                <div class="control-group">
                                                    {{ Form::label('styling_image_logo', 'Logo Image', array('class' => 'control-label')) }}

                                                    <div class="controls">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                            <div class="fileupload-new fileupload-large thumbnail">
                                                                @if (strlen($styling->image_logo))
                                                                <img src="{{ url('content/installations/' . Auth::user()->installation . '/' . $styling->image_logo) }}">
                                                                @else
                                                                <img src="http://placehold.it/200x100">
                                                                @endif
                                                            </div>
                                                            <div class="fileupload-preview fileupload-exists fileupload-large flexible thumbnail"></div>
                                                            <div>
                                                                <span class="btn btn-alt btn-file">
                                                                    <span class="fileupload-new">Select image</span>
                                                                    <span class="fileupload-exists">Change</span>
                                                                    {{ Form::file('image_logo', array('id' => 'styling_image_logo')) }}
                                                                </span>
                                                                <a class="btn btn-alt btn-danger fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="tab-pane" id="styling-colors">
                                            <h2>Main Colors</h2>

                                            <fieldset class="well form-horizontal">
                                                <div class="control-group">
                                                    {{ Form::label('styling_color_primary', 'Primary Color', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        <div class="input-append" data-color-format="rgb">
                                                            {{ Form::text('color_primary', null, array('id' => 'styling_color_primary', 'class' => 'span1 colorpicker')) }}
                                                            <span class="add-on"><i class="colorpicker-preview"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    {{ Form::label('styling_color_secondary', 'Secondary Color', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        <div class="input-append" data-color-format="rgb">
                                                            {{ Form::text('color_secondary', null, array('id' => 'styling_color_secondary', 'class' => 'span1 colorpicker')) }}
                                                            <span class="add-on"><i class="colorpicker-preview"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <h2>Text Colors</h2>

                                            <fieldset class="well form-horizontal">
                                                <div class="control-group">
                                                    {{ Form::label('styling_color_text', 'Main Text Color', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        <div class="input-append" data-color-format="rgb">
                                                            {{ Form::text('color_text', null, array('id' => 'styling_color_text', 'class' => 'span1 colorpicker')) }}
                                                            <span class="add-on"><i class="colorpicker-preview"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    {{ Form::label('styling_color_headings', 'Headings Color', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        <div class="input-append" data-color-format="rgb">
                                                            {{ Form::text('font_headings_color', null, array('id' => 'styling_color_headings', 'class' => 'span1 colorpicker')) }}
                                                            <span class="add-on"><i class="colorpicker-preview"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    {{ Form::label('styling_color_block', 'Block Text Color', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        <div class="input-append" data-color-format="rgb">
                                                            {{ Form::text('color_block', null, array('id' => 'styling_color_block', 'class' => 'span1 colorpicker')) }}
                                                            <span class="add-on"><i class="colorpicker-preview"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <h2>Background Colors</h2>

                                            <fieldset class="well form-horizontal">
                                                <div class="control-group">
                                                    {{ Form::label('styling_background_body', 'Page Background Color', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        <div class="input-append" data-color-format="rgb">
                                                            {{ Form::text('background_body', null, array('id' => 'styling_background_body', 'class' => 'span1 colorpicker')) }}
                                                            <span class="add-on"><i class="colorpicker-preview"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    {{ Form::label('styling_background_block', 'Block Background Color', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        <div class="input-append" data-color-format="rgb">
                                                            {{ Form::text('background_block', null, array('id' => 'styling_background_block', 'class' => 'span1 colorpicker')) }}
                                                            <span class="add-on"><i class="colorpicker-preview"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="tab-pane" id="styling-typography">
                                            <h2>Typography</h2>

                                            <fieldset class="well form-horizontal">
                                                <div class="control-group">
                                                    {{ Form::label('styling_font_family', 'Main Font Family', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::select('font_family', Config::get('settings.styling.font_families'), null, array('id' => 'styling_font_family', 'class' => 'span4')) }}
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    {{ Form::label('styling_font_size', 'Main Font Size', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::select('font_size', Config::get('settings.styling.font_sizes'), null, array('id' => 'styling_font_size', 'class' => 'span1')) }}
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    {{ Form::label('styling_font_headings_family', 'Headings Font Family', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::select('font_headings_family', Config::get('settings.styling.font_families'), null, array('id' => 'styling_font_headings_family', 'class' => 'span4')) }}
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </section>

                                    {{ Form::button('Update Styling', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/colorpicker/bootstrap-colorpicker.js') }}
    {{ HTML::script('js/vendor/plugins/fileupload/bootstrap-fileupload.js') }}
    {{ HTML::script('js/framework.styling.init.js') }}
@stop