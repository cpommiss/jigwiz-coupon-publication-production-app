@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-globe"></span> Zones</h1>
                        <p>The zones within which you distribute your media are shown here.</p>
					</article>

                    @include('chrome.page.alerts')

                    <div class="row">
                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2>
                                        <span class="awe-globe"></span> Zones
                                    </h2>
                                    @if (UserACL::can('create.zone'))
                                    <ul class="data-header-actions">
                                        <li>
                                            <a class="btn btn-success" href="{{ route('zone_create') }}/">Create New Zone</a>
                                        </li>
                                    </ul>
                                    @endif
                                </header>

                                <section>
                                    @if (!$zones->isEmpty())
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>State</th>
                                                <th>Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($zones as $zone)
                                            <tr>
                                                <td>{{ $zone->code }}</td>
                                                <td>
                                                    @if (UserACL::can('update.zone'))
                                                    <a href="{{ route('zone_update', $zone->id) }}">{{ $zone->area }}</a>
                                                    @else
                                                    {{ $zone->area }}
                                                    @endif
                                                </td>
                                                <td>{{ $zone->statedata->name }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                    <div class="alert alert-info">
                                        <strong>No zones</strong><br />
                                        There are no zones to show.
                                    </div>
                                    @endif
                                </section>
                            </div>
                        </article>
                    </div>
                </div>

@stop