@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1><span class="awe-plus"></span> Creating New Zone</h1>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::open(array('route' => 'zone_create', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2><span class="awe-globe"></span> Zone Details</h2>
                                    </header>
                                    <section>
                                        <div class="row-fluid">
                                            <div class="span4">
                                                <p>Define the details of this zone.</p>
                                            </div>
                                            <div class="span8">
                                                <fieldset class="well form-horizontal">
                                                    <div class="control-group">
                                                        {{ Form::label('zone_state', 'State', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::select('state', $states, Input::old('state'), array('id' => 'zone_state', 'class' => 'input-medium')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('zone_area', 'Area', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::text('area', Input::old('area'), array('id' => 'zone_area', 'class' => 'input-large', 'placeholder' => 'i.e., \'Central Florida\'')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('zone_code', 'Code', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::text('code', Input::old('code'), array('id' => 'zone_code', 'class' => 'input-small', 'maxlength' => '5', 'placeholder' => 'i.e., \'A\'')) }}
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </section>

                                    {{ Form::button('Create Zone', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/framework.media.js') }}
    {{ HTML::script('js/framework.media.init.js') }}
@stop