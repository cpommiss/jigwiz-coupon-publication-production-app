@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/tipped-v3.css') }}
    {{ HTML::style('css/vendor/plugins/jquery.fullcalendar.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
                    @include('chrome.page.alerts')

                    <div class="row-fluid">
                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2><span class="awe-tachometer"></span> Dashboard</h2>

                                    <ul class="data-header-actions">
                                        @if (UserACL::can('read.system_message'))
                                        <li class="tabs"><a href="#messages" class="btn">System Messages</a></li>
                                        @endif
                                        @if (UserACL::can('special.crm.use.reminders'))
                                        <li class="tabs"><a href="#calendar" class="btn">Calendar</a></li>
                                        @endif
                                        @if (UserACL::can('read.graphics_queue'))
                                        <li class="tabs"><a href="#graphics-queue" class="btn">Graphics Queue</a></li>
                                        @endif
                                    </ul>
                                </header>
                                <section class="tab-content">
                                    @if (UserACL::can('read.system_message'))
                                    <div class="tab-pane" id="messages">
                                        @if (!$messages->isEmpty())
                                            @foreach ($messages as $message)
                                            <div class="row">
                                                <article class="span12 data-block">
                                                    <div class="data-container">
                                                        <header>
                                                            <h2>{{ $message->subject }}</h2>
                                                            @if (UserACL::can('delete.system_message'))
                                                            <ul class="data-header-actions">
                                                                <li>
                                                                    <a href="javascript:;" class="btn btn-danger remove-modal" data-remove-url="{{ route('system_messaging_delete', $message->id) }}" data-remove-message="{{ AppUtils::strip_tag_whitespace(Lang::get('warnings.system_message.remove_warning')) }}">Delete Message</a>
                                                                </li>
                                                            </ul>
                                                            @endif
                                                        </header>
                                                        <section>
                                                            {{ $message->message }}
                                                        </section>
                                                        <footer>
                                                            <p>Posted by {{ $message->poster->name }} on {{ date(Config::get('settings.datetime.formats.system_messages'), strtotime($message->created_at)) }}</p>
                                                        </footer>
                                                    </div>
                                                </article>
                                            </div>
                                            @endforeach
                                        @else
                                        <div class="alert alert-info">
                                            <strong>No messages!</strong><br />
                                            There are no system messages to show at this time.
                                        </div>
                                        @endif
                                    </div>
                                    @endif

                                    @if (UserACL::can('special.crm.use.reminders'))
                                    <div class="tab-pane" id="calendar">
                                        <div class="full-calendar full-calendar-today"></div>
                                    </div>
                                    @endif

                                    @if (UserACL::can('read.graphics_queue'))
                                    <div class="tab-pane" id="graphics-queue">
                                        <div class="tabbable tabs-left">
                                            <ul class="nav nav-tabs">
                                                @foreach (Config::get('settings.graphics_queue.statuses') as $status_value => $status_title)
                                                @if (($status_title !== 'Unassigned') || (($status_title === 'Unassigned') && (UserACL::can('special.graphics_queue.read.unassigned_jobs'))))
                                                <li>
                                                    <a href="#status-{{ Str::slug($status_title) }}" data-toggle="tab">
                                                        {{ $status_title }}
                                                    </a>
                                                </li>
                                                @endif
                                                @endforeach
                                            </ul>
                                            <div class="tab-content">
                                                @foreach (Config::get('settings.graphics_queue.statuses') as $status_value => $status_title)
                                                    @if (($status_title !== 'Unassigned') || (($status_title === 'Unassigned') && (UserACL::can('special.graphics_queue.read.unassigned_jobs'))))
                                                    <div class="tab-pane" id="status-{{ Str::slug($status_title) }}">
                                                        <div class="graphics-queue-wrapper">
                                                            <table class="datatable table table-striped table-bordered table-hover" id="graphics-queue-data-status-{{ Str::slug($status_title) }}" style="width: 100%;">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Title</th>
                                                                        <th>Date</th>
                                                                        <th>Client</th>
                                                                        <th>Type</th>
                                                                        <th>Assigned To</th>
                                                                        <th>Created By</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach ($jobs as $job)
                                                                    @if ($job->status == $status_value)
                                                                    <tr>
                                                                        <td><a href="{{ route('graphics_queue_job_details', $job->id) }}"><strong>{{ $job->title }}</strong></a></td>
                                                                        <td>{{ date(Config::get('settings.datetime.formats.graphics_queue'), strtotime($job->created_at)) }}</td>
                                                                        <td>{{ $job->customer->name }}</td>
                                                                        <td>{{ Config::get('settings.media_types.color_types')[$job->color_type] }}</td>
                                                                        <td>{{ ((is_null($job->assignee)) ? '&mdash;' : $job->assignee->name) }}</td>
                                                                        <td>{{ $job->creator->name }}</td>
                                                                    </tr>
                                                                    @endif
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </section>
                            </div>
                        </article>
                    </div>
                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/tipped/tipped-v3.js') }}
    {{ HTML::script('js/vendor/plugins/moment/moment.min.js') }}
    {{ HTML::script('js/vendor/plugins/fullCalendar/jquery.fullcalendar.min.js') }}
    {{ HTML::script('js/vendor/plugins/dataTables/jquery.datatables.js') }}
    {{ HTML::script('js/vendor/plugins/dataTables/tableTools/js/TableTools.min.js') }}
    {{ HTML::script('js/vendor/plugins/dataTables/tableTools/js/ZeroClipboard.js') }}
    {{ HTML::script('js/framework.datatables.init.js') }}
    {{ HTML::script('js/framework.reminders.js') }}

    <script>
        jQuery(document).ready(function() {
            jQuery('.tabs a').click(function (event) {
                event.preventDefault();

                jQuery(this).tab('show');

                switch (true) {
                    case ((jQuery(this).attr('href').indexOf('calendar') > -1) && (!jQuery(this).hasClass('initialized'))) :
                        jQuery(this).addClass('initialized');

                        jQuery('.full-calendar-today').fullCalendar({
                            defaultView: 'agendaDay',
                            height: 500,
                            header: {
                                left: 'title',
                                center: '',
                                right: 'today prev,next'
                            },
                            buttonText: {
                                prev: '<span class="awe-arrow-circle-left"></span>',
                                next: '<span class="awe-arrow-circle-right"></span>'
                            },
                            events: framework.properties.paths.root + 'helpers/reminders/all',
                            selectable: true,
                            unselectAuto: false,
                            selectHelper: true,
                            editable: true,
                            timezone: 'UTC',
                            eventRender: framework.reminders.fn.helpers.render_event,
                            eventClick: framework.reminders.fn.views.update,
                            eventDrop: framework.reminders.fn.actions.update_datetime,
                            eventResize: framework.reminders.fn.actions.update_datetime,
                            select: framework.reminders.fn.views.create,
                            unselect: framework.reminders.fn.callbacks.unselect
                        });
                        break;

                    case ((jQuery(this).attr('href').indexOf('graphics-queue') > -1) && (!jQuery(this).hasClass('initialized'))) :
                        jQuery(this).addClass('initialized');

                        if (jQuery('.datatable').length) {
                            jQuery('.datatable').dataTable( {
                                'sDom': "<'row-fluid'<'span6 pull-left'l><'span6 pull-right'f>>t<'row-fluid graphics-queue-footer'<'span6'i><'span6 pull-right'p>>",
                                'sPaginationType': 'bootstrap',
                                'oLanguage': {
                                    'sLengthMenu': '_MENU_&nbsp;&nbsp;records per page'
                                },
                                'sScrollX': '100%',
                                'bScrollCollapse': true
                            });
                        }

                        jQuery('.tabbable a[data-toggle="tab"]').on('shown', function() {
                            var tab_target      = jQuery(this).attr('href').replace('#', '');

                            jQuery('#graphics-queue-data-' + tab_target)
                                .css('width', '100%')
                                .dataTable()
                                .fnAdjustColumnSizing();
                        });

                        jQuery('.tabbable a[data-toggle="tab"]:first').tab('show');
                        break;
                }
            });
            jQuery('.tabs:first a').trigger('click');
        });
    </script>
@stop