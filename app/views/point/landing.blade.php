@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/daterangepicker.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-star"></span> Points Report</h1>
                        <p>Shown below are the points earned by all of the artists in the system. To filter the report by a specific artist and/or date range, use the filtering tools shown below.</p>
					</article>

                    @include('chrome.page.alerts')

                    <div class="row">
                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2>
                                        <span class="awe-cogs"></span>
                                        Filtering and Search Tools
                                    </h2>
                                </header>

                                {{ Form::open(array('route' => Route::currentRouteName(), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled')) }}

                                    <div class="row-fluid criteria-row" data-row-id="1">
                                        <div class="span3">
                                            {{ Form::label('point-range', 'Date Range', array('class' => 'control-label')) }}
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                    {{ Form::text('range', ((Input::get('start')) && (Input::get('stop'))) ? (Input::get('start') . ' - ' . Input::get('stop')) : '', array('id' => 'point-range', 'class' => 'span11')) }}
                                                    {{ Form::hidden('start', Input::get('start')) }}
                                                    {{ Form::hidden('stop', Input::get('stop')) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span4">
                                            {{ Form::label('point-assignee', 'Artist', array('class' => 'control-label')) }}
                                            <div class="controls">
                                                {{ Form::select('assigned_to', $users, Input::get('assigned_to'), array('id' => 'point-assignee', 'class' => 'span12')) }}
                                            </div>
                                        </div>
                                    </div>

                                    {{ Form::button('Search', array('type' => 'submit', 'class' => 'pull-right btn btn-success')) }}

                                    <br /><br />

                                {{ Form::close() }}
                            </div>
                        </article>

                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2>
                                        <span class="awe-star"></span>
                                        Earned Points
                                    </h2>
                                </header>

                                <div class="point-wrapper">
                                    <table class="datatable table table-striped table-bordered table-hover" id="point-data">
                                        <thead>
                                            <tr>
                                                <th>Client</th>
                                                <th>Job Title</th>
                                                <th>Points</th>
                                                <th>Type</th>
                                                <th>Completion Date</th>
                                                <th>Created By</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($jobs as $job)
                                                <tr>
                                                    <td>{{ $job->customer->name }}</td>
                                                    <td>{{ $job->title }}</td>
                                                    <td>{{ $job->points }}</td>
                                                    <td>{{ Config::get('settings.media_types.color_types')[$job->color_type] }}</td>
                                                    <td>{{ $job->updated_at }}</td>
                                                    <td>{{ $job->creator->name }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/dataTables/jquery.datatables.js') }}
    {{ HTML::script('js/vendor/plugins/dataTables/tableTools/js/TableTools.min.js') }}
    {{ HTML::script('js/vendor/plugins/dataTables/tableTools/js/ZeroClipboard.js') }}
    {{ HTML::script('js/vendor/plugins/moment/moment.min.js') }}
    {{ HTML::script('js/vendor/plugins/dateRangePicker/daterangepicker.js') }}
    {{ HTML::script('js/framework.datatables.init.js') }}
    {{ HTML::script('js/framework.point.js') }}
    {{ HTML::script('js/framework.point.init.js') }}
@stop