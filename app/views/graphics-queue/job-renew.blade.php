@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/jquery.fileupload-ajax.css') }}
    {{ HTML::style('css/vendor/plugins/bootstrap-wysihtml5.css') }}
    {{ HTML::style('css/vendor/plugins/chosen.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1><span class="awe-refresh"></span> Renewing Graphics Job: {{ $job->title }}</h1>
					</article>

                    @include('chrome.page.alerts')

                    <div class="alert alert-success">
                        <strong>You are renewing an existing graphics job.</strong>
                        <br />
                        The information pertaining to your selected graphics job is shown below. You may edit it in any way that you deem necessary.
                    </div>

                    {{ Form::model($job, array('route' => array(Route::currentRouteName(), $job->id), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled', 'files' => true)) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2><span class="awe-picture-o"></span> Graphics Job Details</h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span4">
                                                <p>Please review the details for the graphics job you are renewing.</p>
                                                <p><strong>All fields are required.</strong></p>
                                                <p><strong>NOTE:</strong> The last approved proof image on the job you are renewing will be automatically copied to the new job.</p>
                                            </div>
                                            <div class="span8">
                                                <fieldset class="well form-horizontal">
                                                    <div class="control-group">
                                                        {{ Form::label('job_customer', 'Customer', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::select('customer', $clients, null, array('id' => 'job_customer', 'class' => 'input-xlarge')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('job_media', 'Media/Publication', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::select('media', $media, null, array('id' => 'job_media', 'class' => 'input-xlarge')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('job_zone', 'Zone(s)', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::select('zone[]', $zones, $job->zones->lists('zone_id'), array('id' => 'job_zone', 'class' => 'input-xlarge', 'multiple' => true)) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('job_title', 'Title', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::text('title', null, array('id' => 'job_title', 'class' => 'input-large', 'placeholder' => 'Title')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('job_color_type', 'Color Type', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::select('color_type', Config::get('settings.media_types.color_types'), null, array('id' => 'job_color_type', 'class' => 'input-medium')) }}
                                                        </div>
                                                    </div>
                                                    @if ((UserACL::can('special.graphics_queue.assign.artist')) || (UserACL::can('special.graphics_queue.assign.points')))
                                                        @if (UserACL::can('special.graphics_queue.assign.artist'))
                                                        <div class="control-group">
                                                            {{ Form::label('job_assigned_to', 'Assign To', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::select('assigned_to', array_merge(array('0' => '-- Unassigned --'), $users), null, array('id' => 'job_assigned_to', 'class' => 'input-xlarge')) }}
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if (UserACL::can('special.graphics_queue.assign.points'))
                                                        <div class="control-group">
                                                            {{ Form::label('job_points', 'Points Rewarded', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('points', null, array('id' => 'job_points', 'class' => 'input-small', 'placeholder' => 'Points')) }}
                                                            </div>
                                                        </div>
                                                        @endif
                                                    @endif
                                                </fieldset>
                                            </div>
                                        </div>
                                    </section>

                                    {{ Form::button('Submit and Renew Job', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/chosen/chosen.jquery.min.js') }}
    {{ HTML::script('js/framework.graphics.renew.init.js') }}
@stop