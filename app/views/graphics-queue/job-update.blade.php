@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/jquery.fileupload-ajax.css') }}
    {{ HTML::style('css/vendor/plugins/bootstrap-wysihtml5.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1><span class="awe-plus"></span> Updating Graphics Job: {{ $job->title }}</h1>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::model($job, array('route' => array(Route::currentRouteName(), $job->id), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled', 'files' => true)) }}

                        {{ Form::hidden('job_files', Input::old('job_files')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2><span class="awe-picture-o"></span> Graphics Job Details</h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span4">
                                                <p>Please enter the details for this graphics job.</p>
                                                <p><strong>All fields are required.</strong></p>
                                            </div>
                                            <div class="span8">
                                                <fieldset class="well form-horizontal">
                                                    <div class="control-group">
                                                        {{ Form::label('job_customer', 'Customer', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::select('customer', $clients, null, array('id' => 'job_customer', 'class' => 'input-xlarge')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('job_media', 'Media/Publication', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::select('media', $media, null, array('id' => 'job_media', 'class' => 'input-xlarge')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('job_zone', 'Zone', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::select('zone', $zones, null, array('id' => 'job_zone', 'class' => 'input-xlarge')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('job_title', 'Title', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::text('title', null, array('id' => 'job_title', 'class' => 'input-large', 'placeholder' => 'Title')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('job_color_type', 'Color Type', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::select('color_type', Config::get('settings.media_types.color_types'), null, array('id' => 'job_color_type', 'class' => 'input-medium')) }}
                                                        </div>
                                                    </div>
                                                    @if ((UserACL::can('special.graphics_queue.assign.artist')) || (UserACL::can('special.graphics_queue.assign.points')))
                                                        @if (UserACL::can('special.graphics_queue.assign.artist'))
                                                        <div class="control-group">
                                                            {{ Form::label('job_assigned_to', 'Assign To', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::select('assigned_to', array_merge(array('0' => '-- Unassigned --'), $users), null, array('id' => 'job_assigned_to', 'class' => 'input-xlarge')) }}
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if (UserACL::can('special.graphics_queue.assign.points'))
                                                        <div class="control-group">
                                                            {{ Form::label('job_points', 'Points Rewarded', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('points', null, array('id' => 'job_points', 'class' => 'input-small', 'placeholder' => 'Points')) }}
                                                            </div>
                                                        </div>
                                                        @endif
                                                    @endif
                                                </fieldset>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>

@stop