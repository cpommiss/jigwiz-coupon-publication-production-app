@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-picture-o"></span> Graphics Queue</h1>
                        <p>Shown below are the graphics jobs currently in the queue.</p>
					</article>

                    @include('chrome.page.alerts')

                    <div class="row">
                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2>
                                        <span class="awe-picture-o"></span>
                                        Graphics Queue
                                    </h2>
                                    @if (UserACL::can('create.graphics_queue'))
                                    <ul class="data-header-actions">
                                        <li>
                                            <a class="btn btn-success" href="{{ route('graphics_queue_create_job') }}/">New Graphics Job</a>
                                        </li>
                                    </ul>
                                    @endif
                                </header>

                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs">
                                        @foreach (Config::get('settings.graphics_queue.statuses') as $status_value => $status_title)
                                            @if (($status_title !== 'Unassigned') || (($status_title === 'Unassigned') && (UserACL::can('special.graphics_queue.read.unassigned_jobs'))))
                                            <li>
                                                <a href="#status-{{ Str::slug($status_title) }}" data-toggle="tab">
                                                    {{ $status_title }}
                                                </a>
                                            </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach (Config::get('settings.graphics_queue.statuses') as $status_value => $status_title)
                                            @if (($status_title !== 'Unassigned') || (($status_title === 'Unassigned') && (UserACL::can('special.graphics_queue.read.unassigned_jobs'))))
                                            <div class="tab-pane" id="status-{{ Str::slug($status_title) }}">
                                                <div class="graphics-queue-wrapper">
                                                    <table class="datatable table table-striped table-bordered table-hover" id="graphics-queue-data-status-{{ Str::slug($status_title) }}" style="width: 100%;">
                                                        <thead>
                                                            <tr>
                                                                <th>Title</th>
                                                                <th>Date</th>
                                                                <th>Client</th>
                                                                <th>Type</th>
                                                                <th>Assigned To</th>
                                                                <th>Created By</th>
                                                                @if (($status_title === 'Completed') && (UserACL::can('special.graphics_queue.renew.job')))
                                                                <th>Actions</th>
                                                                @endif
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($jobs as $job)
                                                            @if ($job->status == $status_value)
                                                            <tr>
                                                                <td><a href="{{ route('graphics_queue_job_details', $job->id) }}"><strong>{{ $job->title }}</strong></a></td>
                                                                <td>{{ date(Config::get('settings.datetime.formats.graphics_queue'), strtotime($job->created_at)) }}</td>
                                                                <td>{{ $job->customer->name }}</td>
                                                                <td>{{ Config::get('settings.media_types.color_types')[$job->color_type] }}</td>
                                                                <td>{{ ((is_null($job->assignee)) ? '&mdash;' : $job->assignee->name) }}</td>
                                                                <td>{{ $job->creator->name }}</td>
                                                                @if (($status_title === 'Completed') && (UserACL::can('special.graphics_queue.renew.job')))
                                                                <td>
                                                                    <a href="{{ route('graphics_queue_job_renew', $job->id) }}" class="btn btn-mini btn-success">Renew Job</a>
                                                                </td>
                                                                @endif
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/dataTables/jquery.datatables.js') }}
    {{ HTML::script('js/vendor/plugins/dataTables/tableTools/js/TableTools.min.js') }}
    {{ HTML::script('js/vendor/plugins/dataTables/tableTools/js/ZeroClipboard.js') }}
    {{ HTML::script('js/framework.datatables.init.js') }}
    {{ HTML::script('js/framework.graphics.js') }}
    {{ HTML::script('js/framework.graphics.init.js') }}
@stop