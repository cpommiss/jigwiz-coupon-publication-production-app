@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/jquery.fileupload-ajax.css') }}
    {{ HTML::style('css/vendor/plugins/bootstrap-wysihtml5.css') }}
    {{ HTML::style('css/vendor/plugins/chosen.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1><span class="awe-plus"></span> Creating New Graphics Job</h1>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::open(array('route' => Route::currentRouteName(), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled', 'files' => true)) }}

                        {{ Form::hidden('job_key', GraphicsJob::generateJobKey()) }}
                        {{ Form::hidden('job_files', Input::old('job_files')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2><span class="awe-picture-o"></span> Graphics Job Details</h2>
                                    </header>

                                    <div id="wizard">
                                        <ul>
                                            <li><a href="#step1" data-toggle="tab">Step 1: Client Information</a></li>
                                            <li><a href="#step2" data-toggle="tab">Step 2: Graphics and Media</a></li>
                                            <li><a href="#step3" data-toggle="tab">Step 3: Notes</a></li>
                                            <li><a href="#step4" data-toggle="tab">Step 4: Finish</a></li>
                                        </ul>

                                        <div id="bar" class="progress progress-inverse progress-line progress-striped">
                                            <div class="bar"></div>
                                        </div>

                                        <hr />

                                        <div class="tab-content">
                                            <div class="tab-pane" id="step1">
                                                <h2>Job Details</h2>

                                                <div class="row-fluid">
                                                    <div class="span4">
                                                        <p>Please enter the details for this graphics job.</p>
                                                        <p><strong>All fields are required.</strong></p>
                                                    </div>
                                                    <div class="span8">
                                                        <fieldset class="well form-horizontal">
                                                            <div class="control-group">
                                                                {{ Form::label('job_customer', 'Customer', array('class' => 'control-label')) }}
                                                                <div class="controls">
                                                                    {{ Form::select('customer', $clients, Input::old('customer'), array('id' => 'job_customer', 'class' => 'input-xlarge')) }}
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                {{ Form::label('job_media', 'Media/Publication', array('class' => 'control-label')) }}
                                                                <div class="controls">
                                                                    {{ Form::select('media', $media, Input::old('media'), array('id' => 'job_media', 'class' => 'input-xlarge')) }}
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                {{ Form::label('job_zone', 'Zone(s)', array('class' => 'control-label')) }}
                                                                <div class="controls">
                                                                    {{ Form::select('zone[]', $zones, Input::old('zone'), array('id' => 'job_zone', 'class' => 'input-xlarge', 'multiple' => true)) }}
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                {{ Form::label('job_title', 'Title', array('class' => 'control-label')) }}
                                                                <div class="controls">
                                                                    {{ Form::text('title', Input::old('title'), array('id' => 'job_title', 'class' => 'input-large', 'placeholder' => 'Title')) }}
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                {{ Form::label('job_color_type', 'Color Type', array('class' => 'control-label')) }}
                                                                <div class="controls">
                                                                    {{ Form::select('color_type', Config::get('settings.media_types.color_types'), Input::old('color_type'), array('id' => 'job_color_type', 'class' => 'input-medium')) }}
                                                                </div>
                                                            </div>
                                                            @if ((UserACL::can('special.graphics_queue.assign.artist')) || (UserACL::can('special.graphics_queue.assign.points')))
                                                                @if (UserACL::can('special.graphics_queue.assign.artist'))
                                                                <div class="control-group">
                                                                    {{ Form::label('job_assigned_to', 'Assign To', array('class' => 'control-label')) }}
                                                                    <div class="controls">
                                                                        {{ Form::select('assigned_to', array_merge(array('0' => '-- Unassigned --'), $users), Input::old('assigned_to'), array('id' => 'job_assigned_to', 'class' => 'input-xlarge')) }}
                                                                    </div>
                                                                </div>
                                                                @endif
                                                                @if (UserACL::can('special.graphics_queue.assign.points'))
                                                                <div class="control-group">
                                                                    {{ Form::label('job_points', 'Points Rewarded', array('class' => 'control-label')) }}
                                                                    <div class="controls">
                                                                        {{ Form::text('points', Input::old('points'), array('id' => 'job_points', 'class' => 'input-small', 'placeholder' => 'Points')) }}
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            @endif
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="step2">
                                                <h2>Graphics and Media</h2>

                                                <div class="row-fluid">
                                                    <div class="span4">
                                                        <p>Upload the graphics, media and other assets that pertain to this graphics job.</p>

                                                    </div>

                                                    <div class="span8">
                                                        <fieldset class="well form-horizontal">
                                                            <div id="upload">
                                                                <div id="drop">
                                                                    Drag-and-drop files here, or&nbsp;

                                                                    <a class="btn btn-large btn-inverse">Browse</a>
                                                                    <input type="file" name="upl" multiple />
                                                                </div>

                                                                <ul>
                                                                    <!-- The file uploads will be shown here -->
                                                                </ul>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="step3">
                                                <h2>Initial Notes and Directions</h2>

                                                <div class="row-fluid">
                                                    <div class="span4">
                                                        <p>Notes on client records are saved chronologically as individual notes.  Since this is a new client, you can specify an initial note to be added upon creation of this client record.</p>
                                                    </div>
                                                    <div class="span8">
                                                        <fieldset class="well">
                                                            <div class="control-group">
                                                                <div class="controls">
                                                                    {{ Form::textarea('notes_add', Input::old('notes'), array('id' => 'job_notes', 'class' => 'wysihtml5')) }}
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="step4">
                                                <h2>Submit Job?</h2>

                                                <p><strong>Please make sure everything within this graphics job is correct.  You may review the information by clicking on the "steps" above.</strong></p>
                                                <p>You may go back to a previous step using the navigation buttons at the top of the page or the <strong>Back</strong> button shown below.</p>
                                            </div>

                                            <ul class="wizard pager">
                                                <li class="previous"><a href="javascript:;">Previous</a></li>
                                                <li class="next"><a href="javascript:;">Next</a></li>
                                                <li class="next finish" style="display:none;">{{ Form::button('Finish', array('class' => 'btn btn-alt btn-inverse pull-right', 'type' => 'submit')) }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/wysihtml5/wysihtml5-0.3.0.js') }}
    {{ HTML::script('js/vendor/plugins/wysihtml5/bootstrap-wysihtml5.js') }}
    {{ HTML::script('js/vendor/plugins/bootstrapWizard/jquery.bootstrap.wizard.js') }}
    {{ HTML::script('js/vendor/plugins/chosen/chosen.jquery.min.js') }}
    {{ HTML::script('js/vendor/plugins/fileupload-ajax/jquery.knob.js') }}
    {{ HTML::script('js/vendor/plugins/fileupload-ajax/jquery.iframe-transport.js') }}
    {{ HTML::script('js/vendor/plugins/fileupload-ajax/jquery.fileupload.js') }}
    {{ HTML::script('js/framework.graphics.js') }}
    {{ HTML::script('js/framework.graphics.create.init.js') }}
@stop