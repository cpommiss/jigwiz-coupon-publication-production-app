                    <div class="row">
                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2><span class="awe-picture-o"></span> Graphics and Media</h2>
                                </header>
                                <section>
                                    <div class="row">
                                        @if ($last_date = date('Y-m-d', strtotime($job->files->first()->created_at))) @endif
                                        <h3>Files added on {{ $last_date }}</h3>
                                        <hr />

                                        <ul style="list-style: none; margin: 0px; padding: 0px;">

                                        @foreach ($job->files as $file)
                                            @if ($last_date != date('Y-m-d', strtotime($file->created_at)))
                                                @if ($last_date = date('Y-m-d', strtotime($file->created_at))) @endif
                                                    </ul>
                                                </div>
                                                <div class="row-fluid">
                                                    <h3>Files added on {{ $last_date }}</h3>
                                                    <hr />

                                                    <ul style="list-style: none; margin: 0px; padding: 0px;">
                                            @endif

                                            <li class="span3" style="list-style: none;">
                                                <a href="{{ asset('content/installations/' . $job->customer->installation . '/jobs/' . $job->key . '/' . $file->file) }}" target="_blank"><img src="{{ asset('images/icons/filetypes/' . strtolower(pathinfo($file->original, PATHINFO_EXTENSION)) . '.png') }}"></a>
                                                <strong>{{ $file->original }}</strong>
                                            </li>
                                        @endforeach
                                    </div>
                                </section>
                            </div>
                        </article>
                    </div>
