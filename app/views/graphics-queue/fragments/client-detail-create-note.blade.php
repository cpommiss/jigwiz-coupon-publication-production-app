                                <br />

                                <header>
                                    <h2><span class="awe-comment"></span> Add Note</h2>
                                </header>
                                <section>

                                    {{ Form::open(array('route' => array('client_view_graphics_queue_job_add_note', $job->id), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled', 'files' => true)) }}

                                        <fieldset class="well">
                                            <div class="control-group">
                                                <div class="controls">
                                                    {{ Form::textarea('notes_add', null, array('id' => 'job_notes', 'class' => 'wysihtml5')) }}
                                                </div>
                                            </div>
                                        </fieldset>

                                        {{ Form::button('Add Note', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                        <br /><br />

                                    {{ Form::close(); }}

                                </section>
