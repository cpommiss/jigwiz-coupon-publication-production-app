                                <br />

                                <header>
                                    <h2><span class="awe-comment"></span> Add Note</h2>
                                </header>
                                <section>

                                    {{ Form::open(array('route' => array('graphics_queue_job_add_note', $job->id), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled', 'files' => true)) }}

                                        <fieldset class="well">
                                            <div class="control-group">
                                                <div class="controls">
                                                    {{ Form::textarea('notes_add', null, array('id' => 'job_notes', 'class' => 'wysihtml5')) }}
                                                </div>
                                            </div>
                                            @if (UserACL::can('special.graphics_queue.set.note_visibility'))
                                            <div class="control-group">
                                                {{ Form::label('job_notes_visibility', 'Note Visibility', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    {{ Form::select('notes_visibility', AppUtils::config_reduce_complex_array_to_kvp(Config::get('settings.graphics_queue.notes.visibility_types'), 'ordinal', 'label'), null, array('id' => 'job_notes_visibility')) }}
                                                </div>
                                            </div>
                                            @endif
                                            <div class="control-group">
                                                {{ Form::label('job_notes_attachment', 'Attach an Image?', array('class' => 'control-label')) }}

                                                <div class="controls">
                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-new fileupload-large thumbnail hidden" style="display: none;"></div>
                                                        <div class="fileupload-preview fileupload-exists fileupload-large thumbnail flexible full"></div>
                                                        <div>
                                                            <span class="btn btn-alt btn-file">
                                                                <span class="fileupload-new">Select image</span>
                                                                <span class="fileupload-exists">Change</span>
                                                                {{ Form::file('notes_attachment', array('id' => 'job_notes_attachment')) }}
                                                            </span>
                                                            <a class="btn btn-alt btn-danger fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                                                        </div>
                                                        <div>
                                                            <div class="form-inline" style="margin-top: 10px;">
                                                                {{ Form::checkbox('proof', 'true', Input::old('proof'), array('id' => 'job_notes_attachment_proof')) }}
                                                                {{ Form::label('job_notes_attachment_proof', 'Is this image a proof image?', array('style' => 'position: relative; top: 2px; margin-bottom: 0px;')) }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        {{ Form::button('Add Note', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                        <br /><br />

                                    {{ Form::close(); }}

                                </section>
