@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/jquery.fileupload-ajax.css') }}
    {{ HTML::style('css/vendor/plugins/bootstrap-wysihtml5.css') }}
    {{ HTML::style('css/vendor/plugins/annotorious-dark.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1><span class="awe-pencil"></span> Graphics Job Details: {{ $job->title }}</h1>
					</article>

                    @include('chrome.page.alerts')

                    <div class="row">
                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2><span class="awe-info-circle"></span> Job Details</h2>
                                    @if (UserACL::can('delete.graphics_queue'))
                                    <ul class="data-header-actions">
                                        <li>
                                            <a href="javascript:;" class="btn btn-danger remove-modal" data-remove-url="{{ route('graphics_queue_delete_job', $job->id) }}" data-remove-message="{{ AppUtils::strip_tag_whitespace(Lang::get('warnings.graphics_queue.remove_warning')) }}">Delete Job</a>
                                        </li>
                                    </ul>
                                    @endif
                                </header>
                                <section>
                                    <div class="row">
                                        <div class="span2">
                                            @if ($job->customer->attrs->logo)
                                            <img src="{{ Croppa::url('content/installations/' . $job->customer->installation . '/logos/' . $job->customer->attrs->logo, 200) }}">
                                            @else
                                            <img src="{{ asset('images/logo-client.png') }}">
                                            @endif
                                        </div>
                                        <div class="span10">
                                            <h2>{{ $job->customer->name }} &mdash; &ldquo;{{ $job->title }}&rdquo;</h2>

                                            <div class="row graphics-queue-details">
                                                <div class="span2"><span class="awe-tag"></span>&nbsp;&nbsp;<strong>Status</strong></div>
                                                <div class="span2">{{ Config::get('settings.graphics_queue.statuses')[$job->status] }}</div>
                                                <div class="span2"><span class="awe-tint"></span>&nbsp;&nbsp;<strong>Color Type</strong></div>
                                                <div class="span2">{{ Config::get('settings.media_types.color_types')[$job->color_type] }}</div>
                                            </div>
                                            <div class="row graphics-queue-details">
                                                <div class="span2"><span class="awe-paste"></span>&nbsp;&nbsp;<strong>Media</strong></div>
                                                <div class="span2">{{ $job->mediatype->name }}</div>
                                                <div class="span2"><span class="awe-clock-o"></span>&nbsp;&nbsp;<strong>Created</strong></div>
                                                <div class="span4">{{ date(Config::get('settings.datetime.formats.graphics_queue'), strtotime($job->created_at)) }}</div>
                                            </div>
                                            <div class="row graphics-queue-details">
                                                <div class="span2"><span class="awe-user"></span>&nbsp;&nbsp;<strong>Assigned To</strong></div>
                                                <div class="span2">{{ ((is_null($job->assignee)) ? '&mdash;' : $job->assignee->name) }}</div>
                                                <div class="span2"><span class="awe-edit"></span>&nbsp;&nbsp;<strong>Created By</strong></div>
                                                <div class="span2">{{ $job->creator->name }}</div>
                                            </div>
                                            <div class="row graphics-queue-details">
                                                <div class="span2"><span class="awe-globe"></span>&nbsp;&nbsp;<strong>Zone(s)</strong></div>
                                                <div class="span6">
                                                    @if ($job->zones->isEmpty())
                                                    &mdash;
                                                    @else
                                                    @foreach ($job->zones as $zone)
                                                        @if ($zone->zone->code)
                                                        <span class="label label-inverse">[{{ $zone->zone->code }}] {{ $zone->zone->area }}</span>
                                                        @else
                                                        <span class="label label-inverse">{{ $zone->zone->area }}</span>
                                                        @endif
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </article>
                    </div>

                    @if ((Auth::check()) && (!$job->files->isEmpty()))
                        @include('graphics-queue.fragments.internal-detail-downloads', array('job' => $job))
                    @endif

                    <div class="row">
                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2><span class="awe-comments"></span> Job Notes</h2>
                                    <ul class="data-header-actions">
                                        @foreach (Config::get('settings.graphics_queue.notes.visibility_types') as $visibility_type)
                                            @if (GraphicsQueueDetailController::canAccessNoteType($visibility_type))
                                            <li class="tabs"><a href="#{{ Str::slug($visibility_type['button_label']) }}" data-vis-type="{{ $visibility_type['ordinal'] }}" class="btn">{{ $visibility_type['button_label'] }}</a></li>
                                            @endif
                                        @endforeach
									</ul>
                                </header>
                                <section class="tab-content">
                                    @foreach (Config::get('settings.graphics_queue.notes.visibility_types') as $visibility_type)
                                    <div class="tab-pane" id="{{ Str::slug($visibility_type['button_label']) }}">
                                        @if (!$job->notes->isEmpty())
                                            @foreach ($job->notes as $note)
                                                @if (intval($note->visibility) === $visibility_type['ordinal'])
                                                <div class="row">
                                                    <div class="span12 data-block">
                                                        @if (($visibility_type['show_statuses']) && (isset($note->attachment)) && (!is_null($note->attachment)) && ($note->attachment->proof))
                                                        <div class="alert alert-success">
                                                            <p><span class="awe-flag"></span>&nbsp;&nbsp;{{ $note->poster->name }} <strong>added a new proof for review</strong> on {{ date(Config::get('settings.datetime.formats.graphics_queue_notes'), strtotime($note->created_at)) }}.</p>
                                                        </div>
                                                        @endif

                                                        <div class="data-container">
                                                            <header>
                                                                <h2>Posted by {{ (((isset($note->poster)) && (!is_null($note->poster))) ? $note->poster->name : ($job->customer->name . '&nbsp;<span class="label label-important">Client</span>')) }} on {{ date(Config::get('settings.datetime.formats.graphics_queue_notes'), strtotime($note->created_at)) }}</h2>
                                                                @if ((Auth::check()) && (UserACL::can('special.graphics_queue.push.note_to_client')) && ($visibility_type['require_acl']) && (isset($note->attachment)) && (!is_null($note->attachment)))
                                                                <div class="btn-group">
                                                                    @if ($note->pushed)
                                                                    <a href="javascript:;" class="btn disabled"><span class="awe-check"></span> Sent to Client</a>
                                                                    @else
                                                                    <a href="{{ route('graphics_queue_job_push_note', $note->id) }}" class="btn btn-success push-to-client"><span class="awe-mail-forward"></span> Send to Client</a>
                                                                    @endif
                                                                </div>
                                                                @endif
                                                            </header>
                                                            <section>
                                                                @if ((isset($note->attachment)) && (!is_null($note->attachment)))
                                                                <fieldset class="well">
                                                                    @if ($note->attachment->proof)
                                                                    <div class="alert alert-success">
                                                                        <p><span class="awe-flag"></span>&nbsp;&nbsp;This is a proof image. Please review it and then annotate it or post a comment.</p>
                                                                    </div>
                                                                    @endif

                                                                    <div class="alert alert-info alert-annotations hidden">
                                                                        <p><span class="awe-search"></span>&nbsp;&nbsp;This image has annotations. Roll your mouse over the outlined areas on the image to read them.</p>
                                                                    </div>

                                                                    <img data-id="{{ $note->attachment->id }}" src="{{ asset('content/installations/' . $job->customer->installation . '/jobs/' . $job->key . '/' . $note->attachment->file) }}" class="note-image">
                                                                    <div class="annotations" style="display: none;">
                                                                        @if (!is_null($note->attachment->annotations))
                                                                            @foreach ($note->attachment->annotations as $annotation)
                                                                            <textarea name="annotation_{{ $annotation->id }}" data-annotation-file-id="{{ $annotation->file_id }}" data-annotation-id="{{ $annotation->id }}">{{ $annotation->data }}</textarea>
                                                                            @endforeach
                                                                        @endif
                                                                    </div>
                                                                </fieldset>
                                                                @endif

                                                                {{ $note->notes }}
                                                            </section>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            @endforeach
                                        @else
                                        <div class="alert alert-info">
                                            <strong>No {{ strtolower($visibility_type['button_label']) }}!</strong><br />
                                            There are no {{ strtolower($visibility_type['button_label']) }} on file right now for this job.
                                        </div>
                                        @endif
                                    </div>
                                    @endforeach
                                </section>

                                @if ($job->status != AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Completed'))
                                    @if (Auth::check())
                                        @include('graphics-queue.fragments.internal-detail-create-note', array('job' => $job))
                                    @else
                                        @include('graphics-queue.fragments.client-detail-create-note', array('job' => $job))
                                    @endif
                                @else
                                    <div class="alert alert-info">
                                        <p><span class="awe-check"></span>&nbsp;&nbsp;This graphics job is marked as <strong>complete</strong>, so you can no longer add any notes. Notes are shown for archival and reference purposes.</p>
                                    </div>
                                @endif
                            </div>
                        </article>
                    </div>

                    @if ((Auth::check()) && (((UserACL::can('special.graphics_queue.finalize.own_job')) && ($job->created_by == Auth::user()->id))) || (UserACL::can('special.graphics_queue.finalize.job')) || (UserACL::can('special.graphics_queue.assign.artist')) || (UserACL::can('special.graphics_queue.assign.points')))
                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2><span class="awe-gear"></span> Administration</h2>
                                    </header>
                                    <section>
                                        {{ Form::open(array('route' => array('graphics_queue_job_update_settings', $job->id), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled form-vertical', 'files' => true)) }}

                                            <fieldset class="well">
                                                <div class="row-fluid">
                                                    @if (UserACL::can('special.graphics_queue.assign.artist'))
                                                    <div class="span4">
                                                        <div class="control-group">
                                                            {{ Form::label('job_settings_assigned_to', 'Assigned To', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::select('assigned_to', array_merge(array('0' => '-- Unassigned --'), $users), $job->assigned_to, array('id' => 'job_settings_assigned_to', 'class' => 'input-xlarge')) }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @if (UserACL::can('special.graphics_queue.assign.points'))
                                                    <div class="span2">
                                                        <div class="control-group">
                                                            {{ Form::label('job_settings_points', 'Points', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('points', $job->points, array('id' => 'job_settings_points', 'class' => 'input-small', 'placeholder' => 'Points')) }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @if (((UserACL::can('special.graphics_queue.finalize.own_job')) && ($job->created_by == Auth::user()->id)) || (UserACL::can('special.graphics_queue.finalize.job')))
                                                    <div class="pull-right">
                                                        <div class="control-group">
                                                            {{ Form::label('job_settings_completed', 'Mark as Complete?', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                <div class="switch" data-on="success" data-off="danger" data-on-label="Yes" data-off-label="No">
                                                                    {{ Form::checkbox('completed', 'true', (($job->status == AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Completed')) ? true : false), array('id' => 'job_settings_completed')) }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                            </fieldset>

                                            {{ Form::button('Update Job Settings', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                            <br /><br />

                                        {{ Form::close() }}
                                    </section>
                                </div>
                            </article>
                        </div>
                    @endif
@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/wysihtml5/wysihtml5-0.3.0.js') }}
    {{ HTML::script('js/vendor/plugins/wysihtml5/bootstrap-wysihtml5.js') }}
    {{ HTML::script('js/vendor/plugins/fileupload/bootstrap-fileupload.js') }}
    {{ HTML::script('js/vendor/plugins/annotorious/annotorious.min.js') }}
    {{ HTML::script('js/vendor/plugins/annotorious/plugins/anno-fancybox.min.js') }}
    {{ HTML::script('js/vendor/plugins/bootstrapSwitch/bootstrapSwitch.js') }}
    {{ HTML::script('js/framework.graphics.js') }}
    {{ HTML::script('js/framework.graphics.details.init.js') }}
@stop