@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-cloud-download"></span> Document Storage</h1>
                        <p>The media types you have defined within your system are shown below.</p>
					</article>

                    @include('chrome.page.alerts')

                    <div class="row">
                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2>
                                        <span class="awe-paste"></span> Documents
                                    </h2>
                                </header>

                                <section>
                                    <div class="row-fluid">
                                        <div class="span4">
                                            <h3>Add a File</h3>
                                            @if (UserACL::can('create.document'))
                                            {{ Form::open(array('route' => Route::currentRouteName(), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled', 'files' => true)) }}
                                                <p>Use the form below to add a file to the document storage area.</p>

                                                <div class="row-fluid">
                                                    {{ Form::label('document_title', 'Title', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::text('title', Input::old('title'), array('id' => 'document_title', 'class' => 'input-large')) }}
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    {{ Form::label('document_notes', 'Notes', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::textarea('notes', Input::old('notes'), array('id' => 'document_notes', 'rows' => 4, 'class' => 'input-large')) }}
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    {{ Form::label('document_file', 'File', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::file('file', array('id' => 'profile_avatar')) }}
                                                    </div>
                                                </div>

                                                <br />

                                                {{ Form::button('Upload', array('class' => 'btn btn-large btn-primary', 'type' => 'submit')) }}

                                            {{ Form::close() }}
                                            @else
                                                <p><strong>Sorry, but you do not have access to add files.</strong></p>
                                            @endif
                                        </div>
                                        <div class="span8">
                                            <h3>Documents</h3>

                                            @if (!$documents->isEmpty())
                                            <table class="table table-striped table-bordered table-condensed table-hover table-media">
                                                <thead>
                                                    <tr>
                                                        <th>Type</th>
                                                        <th>Title</th>
                                                        <th>Filename</th>
                                                        <th>Uploaded By</th>
                                                        <th>Uploaded On</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($documents as $document)
                                                    <tr>
                                                        <td class="thumbnail"><img src="{{ asset('images/icons/filetypes/' . Str::lower(File::extension($document->file)) . '.png') }}"></td>
                                                        <td>{{ $document->title }}</td>
                                                        <td>{{ $document->original }}</td>
                                                        <td>{{ $document->uploader->name }}</td>
                                                        <td>{{ $document->created_at }}</td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <a href="{{ asset('content/installations/' . $document->installation . '/documents/' . $document->file) }}" target="_blank" class="btn btn-small document-download"><span class="awe-download"></span></a>
                                                                <button class="btn btn-small document-share" data-document-id="{{ $document->id }}"><span class="awe-share"></span></button>
                                                                @if (UserACL::can('delete.document'))
                                                                <button class="btn btn-small btn-danger document-delete remove-modal" data-remove-url="{{ route('document_delete', $document->id) }}" data-remove-message="{{ AppUtils::strip_tag_whitespace(Lang::get('warnings.document.remove_warning')) }}"><span class="awe-trash-o"></span></button>
                                                                @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            @else
                                            <div class="alert">
                                                <strong>Sorry!</strong>
                                                <br />
                                                There are no documents to show at this time.
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                               </section>
                            </div>
                        </article>
                    </div>
                </div>

@stop
@section('footer')
    @parent

    <div id="document-modal" class="modal hide fade" role="dialog">
    </div>

    {{ HTML::script('js/framework.document.js') }}
    {{ HTML::script('js/framework.document.init.js') }}
@stop