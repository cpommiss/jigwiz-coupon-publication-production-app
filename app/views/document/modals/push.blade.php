<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3>Document Push Settings</h3>
</div>
<div class="modal-body">
    <p>Select the customer you'd like to push this document to from the drop-down list below. You may also add a personal message to be sent along with the document, if you wish.</p>

    <hr />

    <input type="hidden" name="modal-document-id" value="{{ $document->id }}">

    <div class="form-horizontal">
        <div class="row-fluid">
            <div class="span12">
                <label class="control-label" for="modal-customer-id">Customer</label>
                <div class="controls">
                    <select name="modal-customer-id" id="modal-customer-id" class="span12">
                        <option value="0">-- Select --</option>
                        @if (!$customers->isEmpty())
                            @foreach ($customers as $customer)
                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <label class="control-label" for="modal-notes">Message/Notes</label>
                <div class="controls">
                    <textarea name="modal-notes" id="modal-notes" class="span12" rows="4"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button id="modal-send" class="btn btn-primary">Send</button>
</div>