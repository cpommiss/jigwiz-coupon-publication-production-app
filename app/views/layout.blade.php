<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    @include('chrome.header')

    <body>
        @if ((Auth::check()) || (Session::has('visiting')))
        <div id="wrapper">
        @endif
        @if (Auth::check())
            @if ($notifications = Notification::where('assigned_to', '=', Auth::user()->id)->orderBy('when', 'desc')->orderBy('notified', 'desc')->take(10)->get()) @endif
        @endif

            @include('chrome.page.header')

            <section class="container{{{ isset($container_class) ? (' ' . $container_class) : '' }}}" role="main">
                @include('chrome.page.navigation')

                @yield('content')
            </section>

            <div id="push"></div>

        @if ((Auth::check()) || (Session::has('visiting')))
        </div>
        @endif

        @include('chrome.footer')
    </body>
</html>