@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-th"></span> Pagination</h1>
                        <p>To begin pagination, choose the media you'd like to paginate from the list below.</p>
					</article>

                    @include('chrome.page.alerts')

                    <div class="row">
                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2>
                                        <span class="awe-paste"></span> Available Media Types
                                    </h2>
                                </header>

                                <section>
                                    @if (!$mediatypes->isEmpty())
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Width</th>
                                                <th>Height</th>
                                                <th>Attributes</th>
                                                <th>Max Rows</th>
                                                <th>Max Cols</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($mediatypes as $mediatype)
                                            <tr>
                                                <td>
                                                    @if (UserACL::can('update.media_type'))
                                                    <a href="{{ route('pagination_update', $mediatype->id) }}">{{ $mediatype->name }}</a>
                                                    @else
                                                    {{ $mediatype->name }}
                                                    @endif
                                                </td>
                                                <td>{{ $mediatype->width }} inches</td>
                                                <td>{{ $mediatype->height }} inches</td>
                                                <td>
                                                    <span class="label label-success color-{{ Str::slug(Config::get('settings.media_types.color_types')[$mediatype->color]) }}">{{ Config::get('settings.media_types.color_types')[$mediatype->color] }}</span>
                                                    @if ($mediatype->premium)
                                                    <span class="label label-primary">Premium Pages Enabled</span>
                                                    @else
                                                    <span class="label label-default">No Premium Pages</span>
                                                    @endif
                                                </td>
                                                <td>{{ $mediatype->max_rows }}</td>
                                                <td>{{ $mediatype->max_cols }}</td>
                                                <td>
                                                    <a href="{{ route('pagination_update', $mediatype->id) }}" class="btn btn-mini btn-success">Begin</a>
                                                    <a href="{{ route('pagination_batch', $mediatype->id) }}" class="btn btn-mini btn-inverse">Batch Setup</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                    <div class="alert alert-info">
                                        <strong>No media types</strong><br />
                                        There are no media types to show.
                                    </div>
                                    @endif
                                </section>
                            </div>
                        </article>
                    </div>
                </div>

@stop