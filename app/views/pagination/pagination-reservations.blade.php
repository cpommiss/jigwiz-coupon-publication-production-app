@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/jquery-ui-slider.css') }}
    {{ HTML::style('css/vendor/plugins/jquery-ui-slider-pips.css') }}
    {{ HTML::style('css/vendor/plugins/bootstrap-spinner.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-th"></span> Pagination Reservations</h1>
                        <p>To reserve a page, use the form below.</p>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::open(array('route' => Route::currentRouteName(), 'id' => 'pagination-settings', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled form-vertical')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-cogs"></span>
                                            Pagination Settings
                                        </h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span6">
                                                {{ Form::label('pagination_media', 'Media', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    {{ Form::select('media', array_merge(array('0' => 'Select media'), $mediatypes), Input::get('media'), array('id' => 'pagination_media', 'class' => 'span12')) }}
                                                </div>
                                            </div>
                                            <div class="span6">
                                                {{ Form::label('pagination_zone', 'Zone', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    {{ Form::select('zone', array_merge(array('0' => 'Show all zones'), $zones), Input::get('zone'), array('id' => 'pagination_zone', 'class' => 'span12')) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span10">
                                                {{ Form::label('pagination_month', 'Month', array('class' => 'control-label')) }}
                                                {{ Form::hidden('month', (Input::get('month')) ? Input::get('month') : date('n')) }}
                                                <div class="controls">
                                                    <div id="month_select"></div>
                                                </div>
                                            </div>
                                            <div class="span2">
                                                {{ Form::label('pagination_year', 'Year', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    <div class="input-append spinner" data-trigger="spinner">
                                                        {{ Form::text('year', (Input::get('year')) ? Input::get('year') : date('Y'), array('id' => 'pagination_year', 'class' => 'span6')) }}
                                                        <div class="add-on">
                                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="awe-sort-up"></i></a>
                                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="awe-sort-down"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <hr />

                                            {{ Form::button('Search', array('type' => 'submit', 'class' => 'btn btn-success pull-right')) }}
                                        </div>
                                    </section>
                                </div>
                            </article>

                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-th"></span>
                                            Reservations
                                        </h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span12">
                                                @if (Input::get('media'))
                                                    @foreach ($zones as $state_name => $state_zones)
                                                        @foreach ($state_zones as $state_zone_id => $state_zone_label)
                                                            @if ((Input::get('zone') == 0) || (Input::get('zone') == $state_zone_id))
                                                            <table class="table table-striped table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="{{ ((UserACL::can('delete.reservation')) ? 6 : 5) }}" class="pagination-reservation-zone-header">
                                                                            <strong>{{ $state_name }}: {{ $state_zone_label }}</strong>
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Type</th>
                                                                        <th>Page</th>
                                                                        <th>Client</th>
                                                                        <th>Reserved By</th>
                                                                        <th>Reserved On</th>
                                                                        @if (UserACL::can('delete.reservation'))
                                                                        <th>Actions</th>
                                                                        @endif
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($reservations as $reservation_zone => $reservation_data)
                                                                        @if ($reservation_zone == $state_zone_id)
                                                                            @foreach ($reservation_data as $page_data)
                                                                                @foreach ($page_data as $page)
                                                                                <tr>
                                                                                    <td>
                                                                                        @if ($page['type'] == Config::get('settings.pagination.reservations.types.reserved-and-placed'))
                                                                                        <span class="label label-success">Reserved and Placed</span>
                                                                                        @elseif ($page['type'] == Config::get('settings.pagination.reservations.types.reservation'))
                                                                                        <span class="label label-inverse">Reservation</span>
                                                                                        @elseif ($page['type'] == Config::get('settings.pagination.reservations.types.placement'))
                                                                                        <span class="label label-info">Placed on Page</span>
                                                                                        @endif
                                                                                    </td>
                                                                                    <td>{{ $page['page'] }}</td>
                                                                                    <td>{{ $page['customer']['name'] }}</td>
                                                                                    <td>{{ $page['rep']['name'] }}</td>
                                                                                    <td>{{ $page['date'] }}</td>
                                                                                    @if (UserACL::can('delete.document'))
                                                                                    <td>
                                                                                        <a href="javascript:;" class="btn btn-small btn-danger document-delete remove-modal" data-remove-url="{{ route('pagination_reservations_delete', $page['id']) }}" data-remove-message="{{ AppUtils::strip_tag_whitespace(Lang::get('warnings.reservation.remove_warning')) }}"><span class="awe-trash-o"></span></a>
                                                                                    </td>
                                                                                    @endif
                                                                                </tr>
                                                                                @endforeach
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                            <div class="row-fluid">
                                                                {{ Form::button('Add Reservation', array('type' => 'button', 'class' => 'btn btn-success reservation-add pull-right', 'data-zone-id' => $state_zone_id)) }}
                                                            </div>
                                                            <hr />
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                @else
                                                    <div class="alert alert-info">
                                                        <p><span class="awe-info-circle"></span>&nbsp;&nbsp;Select a media type from the controls above to view reservations and placements.</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/moment/moment.min.js') }}
    {{ HTML::script('js/vendor/plugins/handlebars/handlebars-v1.3.0.js') }}
    {{ HTML::script('js/vendor/plugins/slider-pips/jquery-ui-slider-pips.min.js') }}
    {{ HTML::script('js/vendor/plugins/spinner/jquery.spinner.min.js') }}
    {{ HTML::script('js/framework.pagination.js') }}
    {{ HTML::script('js/framework.pagination.reservations.init.js') }}

    <script id="template-pagination-reservation" type="text/x-handlebars-template">
        <div class="row-fluid form-vertical">
            <div class="span6">
                <label class="control-label" for="add-client-id-@{{count}}">Client</label>
                <div class="controls">
                    <select name="add-client-id-@{{count}}" id="add-client-id-@{{count}}" class="span12 add-client">
                        <option value="0">-- Select --</option>
                        @foreach ($customers as $customer)
                        <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="span6">
                <label class="control-label" for="add-page-@{{count}}">Page #</label>
                <div class="controls form-horizontal">
                    <select name="add-page-@{{count}}" id="add-page-@{{count}}" class="span8 add-page"></select>
                    <input type="hidden" name="add-zone-id-@{{count}}" value="@{{zone}}">
                    <button class="btn btn-success reservation-create pull-right" type="submit">Save Reservation</button>
                </div>
            </div>
        </div>
    </script>

    <script>
        jQuery(document).ready(function() {
            if ((framework) && (framework.pagination)) {
                @if (Input::get('media'))
                    @foreach ($premium_pages as $page)
                        framework.pagination.properties.reservations.premium_pages.push({page: {{ $page->page }}, zone: {{ $page->zone }}});
                    @endforeach
                @endif
            }
        });
    </script>
@stop