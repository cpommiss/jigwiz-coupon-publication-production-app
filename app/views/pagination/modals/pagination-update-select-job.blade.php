<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3>Select a Proof</h3>
</div>
<div class="modal-body">
    <p>Select a proof from the drop-down list below to add to the area you selected.</p>

    <hr />

    <div class="row-fluid form-horizontal">
        <label class="control-label" for="modal-job-id">Proof</label>
        <div class="controls">
            <select name="modal-job-id" id="modal-job-id" class="span12">
                <option value="0">-- Select --</option>
                @foreach ($customers as $customer_id => $customer_name)
                <optgroup label="{{ $customer_name }}">
                    @foreach ($data as $job)
                        @if ($job['crm_id'] == $customer_id)
                        <option value="{{ $job['id'] }}" data-job-key="{{ $job['key'] }}" data-proof="{{ $job['proof'] }}" data-color-value="{{ $job['color'] }}" data-color-label="{{ Config::get('settings.media_types.color_types')[$job['color']] }}">{{ $job['title'] }} ({{ Config::get('settings.media_types.color_types')[$job['color']] }})</option>
                        @endif
                    @endforeach
                </optgroup>
                @endforeach
            </select>
        </div>
        <div class="preview hidden">
            <label class="control-label">Preview</label>
            <div class="preview-inner"></div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button id="modal-select" class="btn btn-primary">Select</button>
</div>