@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/jquery-ui-slider.css') }}
    {{ HTML::style('css/vendor/plugins/jquery-ui-slider-pips.css') }}
    {{ HTML::style('css/vendor/plugins/bootstrap-spinner.css') }}
    {{ HTML::style('css/vendor/plugins/tipped-v3.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-th"></span> Pagination Usage Report</h1>
                        <p>To view the usage of each page within a paginated media type, use the controls below to fine-tune your results.</p>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::open(array('route' => Route::currentRouteName(), 'id' => 'pagination-settings', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled form-vertical')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-cogs"></span>
                                            Pagination Settings
                                        </h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span6">
                                                {{ Form::label('pagination_media', 'Media', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    {{ Form::select('media', array_merge(array('0' => 'Select media'), $mediatypes), Input::get('media'), array('id' => 'pagination_media', 'class' => 'span12')) }}
                                                </div>
                                            </div>
                                            <div class="span6">
                                                {{ Form::label('pagination_zone', 'Zone', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    {{ Form::select('zone', array_merge(array('0' => 'Show all zones'), $zones), Input::get('zone'), array('id' => 'pagination_zone', 'class' => 'span12')) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span10">
                                                {{ Form::label('pagination_month', 'Month', array('class' => 'control-label')) }}
                                                {{ Form::hidden('month', (Input::get('month')) ? Input::get('month') : date('n')) }}
                                                <div class="controls">
                                                    <div id="month_select"></div>
                                                </div>
                                            </div>
                                            <div class="span2">
                                                {{ Form::label('pagination_year', 'Year', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    <div class="input-append spinner" data-trigger="spinner">
                                                        {{ Form::text('year', (Input::get('year')) ? Input::get('year') : date('Y'), array('id' => 'pagination_year', 'class' => 'span6')) }}
                                                        <div class="add-on">
                                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="awe-sort-up"></i></a>
                                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="awe-sort-down"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <hr />

                                            {{ Form::button('Search', array('type' => 'submit', 'class' => 'btn btn-success pull-right')) }}
                                        </div>
                                    </section>
                                </div>
                            </article>

                            @if (Input::all())
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-th"></span>
                                            Usage Report
                                        </h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span12">
                                                @if ($pagination)
                                                <div class="report-wrapper">
                                                    <table class="datatable table table-striped table-bordered table-hover" id="report-data">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="4">Page Data</th>
                                                                <th colspan="4">Graphics/Placement Details</th>
                                                            </tr>
                                                            <tr>
                                                                <th>Page</th>
                                                                <th>Page Type</th>
                                                                <th>Total Rows</th>
                                                                <th>Total Columns</th>
                                                                <th>Client</th>
                                                                <th>Job Title/Type</th>
                                                                <th>Job Completed</th>
                                                                <th>Rows</th>
                                                                <th>Columns</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($pagination->pages as $page)
                                                            @if ($page->placements->isEmpty())
                                                            <tr>
                                                                <td>{{ $page->page }}</td>
                                                                <td>
                                                                    @if ($page->premium)
                                                                        Premium
                                                                    @else
                                                                        Regular
                                                                    @endif
                                                                </td>
                                                                <td>{{ $page->rows }}</td>
                                                                <td>{{ $page->cols }}</td>
                                                                <td>-</td>
                                                                <td>-</td>
                                                                <td>-</td>
                                                                <td>-</td>
                                                                <td>-</td>
                                                            </tr>
                                                            @else
                                                                @foreach ($page->placements as $placement)
                                                                <tr>
                                                                    <td>{{ $page->page }}</td>
                                                                    <td>
                                                                        @if ($page->premium)
                                                                            Premium
                                                                        @else
                                                                            Regular
                                                                        @endif
                                                                    </td>
                                                                    <td>{{ $page->rows }}</td>
                                                                    <td>{{ $page->cols }}</td>
                                                                    @if (!$placement->job->files->isEmpty())
                                                                    <td>{{ $placement->job->customer->name }}</td>
                                                                    <td data-image-proof="{{ Croppa::url('content/installations/' . Auth::user()->installation . '/jobs/' . $placement->job->key . '/' . $placement->job->files->first()->file, 480) }}">
                                                                        <a href="{{ asset('content/installations/' . Auth::user()->installation . '/jobs/' . $placement->job->key . '/' . $placement->job->files->first()->file) }}" target="_blank">{{ $placement->job->title }}</a> ({{ Config::get('settings.media_types.color_types')[$placement->job->color_type] }})
                                                                    </td>
                                                                    <td>{{ $placement->job->updated_at }}</td>
                                                                    <td>{{ $placement->rows }}</td>
                                                                    <td>{{ $placement->cols }}</td>
                                                                    @else
                                                                    <td>-</td>
                                                                    <td>-</td>
                                                                    <td>-</td>
                                                                    <td>-</td>
                                                                    <td>-</td>
                                                                    @endif
                                                                </tr>
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                @else
                                                    <div class="alert alert-danger">
                                                        <strong>Sorry!</strong>
                                                        <br />
                                                        No pagination data was found for the search criteria you specified above. Refine your search criteria and try again.
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </article>
                            @endif

                        </div>

                    {{ Form::close() }}

                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/dataTables/jquery.datatables.js') }}
    {{ HTML::script('js/vendor/plugins/dataTables/tableTools/js/TableTools.min.js') }}
    {{ HTML::script('js/vendor/plugins/dataTables/tableTools/js/ZeroClipboard.js') }}
    {{ HTML::script('js/framework.datatables.init.js') }}
    {{ HTML::script('js/vendor/plugins/tipped/tipped-v3.js') }}
    {{ HTML::script('js/vendor/plugins/imagesloaded/imagesloaded.pkgd.min.js') }}
    {{ HTML::script('js/vendor/plugins/slider-pips/jquery-ui-slider-pips.min.js') }}
    {{ HTML::script('js/vendor/plugins/spinner/jquery.spinner.min.js') }}
    {{ HTML::script('js/vendor/plugins/moment/moment.min.js') }}
    {{ HTML::script('js/framework.pagination.js') }}
    {{ HTML::script('js/framework.pagination.report.usage.init.js') }}
@stop