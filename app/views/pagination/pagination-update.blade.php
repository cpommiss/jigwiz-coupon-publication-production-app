@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/jquery-ui-slider.css') }}
    {{ HTML::style('css/vendor/plugins/jquery-ui-slider-pips.css') }}
    {{ HTML::style('css/vendor/plugins/bootstrap-spinner.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-th"></span> Pagination</h1>
                        <p>To begin pagination, choose the media you'd like to paginate from the list below.</p>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::open(array('route' => Route::currentRouteName(), 'id' => 'pagination-settings', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled form-vertical')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-tag"></span>
                                            Media Attributes
                                        </h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <h2>{{ $mediatype->name }}</h2>
                                                <p>Shown below are the attributes of the media type you have selected. To change these, <a href="{{ route('media_management_update_media', $mediatype->id) }}">click here</a>.</p>

                                                <hr />

                                                {{ Form::hidden('media', $mediatype->id) }}
                                                {{ Form::hidden('media-color-value', $mediatype->color) }}
                                                {{ Form::hidden('media-color-label', Config::get('settings.media_types.color_types')[$mediatype->color]) }}

                                                {{ Form::hidden('issue_index', '') }}

                                                {{ Form::hidden('_month', date('n')) }}
                                                {{ Form::hidden('_year', date('Y')) }}
                                                {{ Form::hidden('_zone', 0) }}

                                                {{ Form::hidden('width', $mediatype->width) }}
                                                {{ Form::hidden('height', $mediatype->height) }}
                                                {{ Form::hidden('rows', $mediatype->max_rows) }}
                                                {{ Form::hidden('cols', $mediatype->max_cols) }}

                                                {{ Form::hidden('_width', $mediatype->width) }}
                                                {{ Form::hidden('_height', $mediatype->height) }}
                                                {{ Form::hidden('_rows', $mediatype->max_rows) }}
                                                {{ Form::hidden('_cols', $mediatype->max_cols) }}

                                                <div class="row-fluid media-type-details">
                                                    <div class="span2"><span class="awe-tint"></span>&nbsp;&nbsp;<strong>Color Type</strong></div>
                                                    <div class="span2">{{ Config::get('settings.media_types.color_types')[$mediatype->color] }}</div>
                                                    <div class="span2"><span class="awe-arrows-h"></span>&nbsp;&nbsp;<strong>Width (in.)</strong></div>
                                                    <div class="span2">{{ $mediatype->width }}</div>
                                                    <div class="span2"><span class="awe-arrows-v"></span>&nbsp;&nbsp;<strong>Height (in.)</strong></div>
                                                    <div class="span2">{{ $mediatype->height }}</div>
                                                </div>
                                                <div class="row-fluid media-type-details">
                                                    <div class="span2"><span class="awe-star"></span>&nbsp;&nbsp;<strong>Premium Pages</strong></div>
                                                    <div class="span2">{{ (($mediatype->premium) ? 'Yes' : 'No') }}</div>
                                                    <div class="span2"><span class="awe-ellipsis-v"></span>&nbsp;&nbsp;<strong>Max. Rows</strong></div>
                                                    <div class="span2">{{ $mediatype->max_rows }}</div>
                                                    <div class="span2"><span class="awe-ellipsis-h"></span>&nbsp;&nbsp;<strong>Max. Columns</strong></div>
                                                    <div class="span2">{{ $mediatype->max_cols }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </article>
                        </div>
                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-cogs"></span>
                                            Pagination Settings
                                        </h2>
                                        <div class="btn-group">
                                            <button class="btn pagination-issue-index" type="button">Set Issue Label</button>
                                        </div>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span7">
                                                {{ Form::label('pagination_month', 'Month', array('class' => 'control-label')) }}
                                                {{ Form::hidden('month', date('n')) }}
                                                <div class="controls">
                                                    <div id="month_select"></div>
                                                </div>
                                            </div>
                                            <div class="span2">
                                                {{ Form::label('pagination_year', 'Year', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    <div class="input-append spinner pagination-year" data-trigger="spinner">
                                                        {{ Form::text('year', date('Y'), array('id' => 'pagination_year', 'class' => 'span6')) }}
                                                        <div class="add-on">
                                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="awe-sort-up"></i></a>
                                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="awe-sort-down"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="span3">
                                                {{ Form::label('pagination_zone', 'Zone', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    {{ Form::select('zone', $zones, null, array('id' => 'pagination_zone', 'class' => 'span12')) }}
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </article>

                        {{ Form::close() }}

                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2>
                                        <span class="awe-th"></span>
                                        Layout
                                    </h2>
                                    @if (UserACL::can('create.pagination'))
                                    <div class="btn-group dropdown data-header-actions pull-right">
                                        <button class="btn pagination-page-creation" type="button">Add Page...</button><button class="btn dropdown-toggle" data-toggle="dropdown" type="button"><span class="caret"></span></button>
                                        <ul class="dropdown-menu pull-right pagination-page-creation-options">
                                            <li><a href="javascript:;" data-type="before">Before this page</a></li>
                                            <li><a href="javascript:;" data-type="after">After this page</a></li>
                                        </ul>
                                    </div>
                                    <div class="btn-group pagination-page-management dropdown data-header-actions pull-right">
                                        <button class="btn btn-danger pagination-page-delete" type="button">Delete...</button><button class="btn btn-danger dropdown-toggle" data-toggle="dropdown" type="button"><span class="caret"></span></button>
                                        <ul class="dropdown-menu pull-right pagination-page-management-options">
                                            <li><a href="javascript:;" data-type="clear">Clear all placements</a></li>
                                            <li><a href="javascript:;" data-type="remove">Delete (front &amp; back)</a></li>
                                        </ul>
                                    </div>
                                    <button class="btn pagination-page-settings pull-right" type="button">Page Settings</button>
                                    @endif
                                </header>

                                <div class="row-fluid">
                                    <div class="span1">&nbsp;</div>
                                    <div class="span10">
                                        {{ Form::label('pagination_page', 'Page', array('class' => 'control-label')) }}
                                        {{ Form::hidden('page', 1) }}
                                        <div class="controls">
                                            <div id="page_select"></div>
                                        </div>
                                    </div>
                                    <div class="span1">&nbsp;</div>
                                </div>

                                <div class="row-fluid">
                                    <div class="span1">
                                        <div class="pagination-controls">
                                            <a href="javascript:;" data-type="back"><span class="awe-arrow-circle-left"></span></a>
                                        </div>
                                    </div>
                                    <div class="span10 data-block">
                                        <div class="data-container">
                                            <div class="alert pagination-dimensions-alert hidden">
                                                <p><span class="awe-arrows"></span> Note that the dimensions and/or rows/column layout of this page differ from that of the publication.</p>
                                            </div>
                                            <div class="alert form-inline pagination-premium hidden">
                                                <div class="pull-left pagination-premium-enabled"><span class="awe-star"></span> This is a <strong>premium</strong> page.</div>
                                                <div class="pull-left pagination-premium-disabled"><span class="awe-star-o"></span> This is <strong>not a premium</strong> page.</div>

                                                <div class="pull-right form-inline pagination-premium-toggle">
                                                    {{ Form::label('pagination_page_premium', 'Toggle premium page status', array('class' => 'control-label')) }}
                                                    {{ Form::checkbox('premium', '1', 0, array('id' => 'pagination_page_premium')) }}
                                                </div>
                                            </div>
                                            <div class="pagination-preview">
                                                <div class="row-fluid pagination-row">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span1">
                                        <div class="pagination-controls">
                                            <a href="javascript:;" data-type="next"><span class="awe-arrow-circle-right"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>

@stop
@section('footer')
    @parent

    <div id="pagination-modal" class="modal hide fade" role="dialog">
    </div>

    {{ HTML::script('js/vendor/plugins/handlebars/handlebars-v1.3.0.js') }}
    {{ HTML::script('js/vendor/plugins/slider-pips/jquery-ui-slider-pips.min.js') }}
    {{ HTML::script('js/vendor/plugins/spinner/jquery.spinner.min.js') }}
    {{ HTML::script('js/vendor/plugins/moment/moment.min.js') }}
    {{ HTML::script('js/framework.pagination.js') }}
    {{ HTML::script('js/framework.pagination.init.js') }}

    <script id="template-page-settings" type="text/x-handlebars-template">
        <div class="pagination-popover">
            <div class="row-fluid form-vertical">
                <div class="span6">
                    <label for="pagination-page-width" class="control-label"><span class="awe-arrows-h"></span> Width</label>
                    <div class="controls">
                        <div class="input-append">
                            <input type="text" name="pagination-page-width" id="pagination-page-width" value="@{{width}}" class="span6">
                            <span class="add-on">in.</span>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <label for="pagination-page-height" class="control-label"><span class="awe-arrows-v"></span> Height</label>
                    <div class="controls">
                        <div class="input-append">
                            <input type="text" name="pagination-page-height" id="pagination-page-height" value="@{{height}}" class="span6">
                            <span class="add-on">in.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid form-vertical">
                <label for="pagination-page-rows" class="control-label"><span class="awe-ellipsis-v"></span> Rows</label>
                <div class="controls">
                    <div class="span9">
                        <div id="pagination-slider-rows"></div>
                    </div>
                    <div class="span3">
                        <input type="text" name="pagination-page-rows" id="pagination-page-rows" value="@{{rows}}" class="span12">
                    </div>
                </div>
            </div>
            <div class="row-fluid form-vertical">
                <label for="pagination-page-cols" class="control-label"><span class="awe-ellipsis-h"></span> Columns</label>
                <div class="controls">
                    <div class="span9">
                        <div id="pagination-slider-cols"></div>
                    </div>
                    <div class="span3">
                        <input type="text" name="pagination-page-cols" id="pagination-page-cols" value="@{{cols}}" class="span12">
                    </div>
                </div>
            </div>
            <div class="row-fluid form-horizontal">
                <hr />

                <button id="pagination-page-settings-cancel" type="button" class="btn pull-left">Cancel</button>
                <button id="pagination-page-settings-save" type="button" class="btn btn-success pull-right">Save</button>
                <button id="pagination-page-settings-default" type="button" class="btn pull-right" style="margin-right: 10px;">Reset</button>
            </div>
        </div>
    </script>

    <script id="template-issue-index" type="text/x-handlebars-template">
        <div class="pagination-popover">
            <div class="row-fluid form-vertical">
                <div class="alert">
                    <p>Setting label/# for:<br /><strong>@{{month}} @{{year}}</strong> in <strong>@{{zone}}</strong>.</p>
                </div>
                <div class="span12">
                    <label for="pagination-issue-index" class="control-label"><span class="awe-tag"></span> Issue Label/#</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on">#</span>
                            <input type="text" name="pagination-issue-index" id="pagination-issue-index" value="@{{label}}" class="span6">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid form-horizontal">
                <hr />

                <button id="pagination-issue-index-cancel" type="button" class="btn pull-left">Cancel</button>
                <button id="pagination-issue-index-save" type="button" class="btn btn-success pull-right">Save</button>
            </div>
        </div>
    </script>
@stop