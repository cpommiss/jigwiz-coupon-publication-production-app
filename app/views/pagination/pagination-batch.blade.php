@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/jquery-ui-slider.css') }}
    {{ HTML::style('css/vendor/plugins/jquery-ui-slider-pips.css') }}
    {{ HTML::style('css/vendor/plugins/bootstrap-spinner.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-th"></span> Pagination Batch Setup</h1>
                        <p>Batch setup lets you create a series of blank pages, ready to be paginated, with a given media type for all (or a select number) of your zones.</p>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::open(array('route' => array(Route::currentRouteName(), $mediatype->id), 'id' => 'pagination-batch-setup', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled form-vertical')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-tag"></span>
                                            Media Attributes
                                        </h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <h2>{{ $mediatype->name }}</h2>
                                                <p>Shown below are the attributes of the media type you have selected. To change these, <a href="{{ route('media_management_update_media', $mediatype->id) }}">click here</a>.</p>

                                                <hr />

                                                {{ Form::hidden('media', $mediatype->id) }}

                                                <div class="row-fluid media-type-details">
                                                    <div class="span2"><span class="awe-tint"></span>&nbsp;&nbsp;<strong>Color Type</strong></div>
                                                    <div class="span2">{{ Config::get('settings.media_types.color_types')[$mediatype->color] }}</div>
                                                    <div class="span2"><span class="awe-arrows-h"></span>&nbsp;&nbsp;<strong>Width (in.)</strong></div>
                                                    <div class="span2">{{ $mediatype->width }}</div>
                                                    <div class="span2"><span class="awe-arrows-v"></span>&nbsp;&nbsp;<strong>Height (in.)</strong></div>
                                                    <div class="span2">{{ $mediatype->height }}</div>
                                                </div>
                                                <div class="row-fluid media-type-details">
                                                    <div class="span2"><span class="awe-star"></span>&nbsp;&nbsp;<strong>Premium Pages</strong></div>
                                                    <div class="span2">{{ (($mediatype->premium) ? 'Yes' : 'No') }}</div>
                                                    <div class="span2"><span class="awe-ellipsis-v"></span>&nbsp;&nbsp;<strong>Max. Rows</strong></div>
                                                    <div class="span2">{{ $mediatype->max_rows }}</div>
                                                    <div class="span2"><span class="awe-ellipsis-h"></span>&nbsp;&nbsp;<strong>Max. Columns</strong></div>
                                                    <div class="span2">{{ $mediatype->max_cols }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </article>
                        </div>
                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-cogs"></span>
                                            Pagination Settings
                                        </h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span10">
                                                {{ Form::label('pagination_month', 'Month', array('class' => 'control-label')) }}
                                                {{ Form::hidden('month', date('n')) }}
                                                <div class="controls">
                                                    <div id="month_select"></div>
                                                </div>
                                            </div>
                                            <div class="span2">
                                                {{ Form::label('pagination_year', 'Year', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    <div class="input-append spinner pagination-year" data-trigger="spinner">
                                                        {{ Form::text('year', date('Y'), array('id' => 'pagination_year', 'class' => 'span6')) }}
                                                        <div class="add-on">
                                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="awe-sort-up"></i></a>
                                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="awe-sort-down"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </article>
                        </div>
                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-th"></span>
                                            Batch Setup
                                        </h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span4">
                                                <p>Notes on client records are saved chronologically as individual notes.  Since this is a new client, you can specify an initial note to be added upon creation of this client record.</p>
                                            </div>
                                            <div class="span8">
                                                <fieldset class="well form-horizontal">
                                                    <div class="control-group">
                                                        {{ Form::label('batch_pages', '# Pages to Insert', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            <div class="input-append">
                                                                {{ Form::text('pages', Input::old('pages'), array('id' => 'batch_pages', 'class' => 'span3', 'placeholder' => '#')) }}
                                                                <span class="add-on">pages</span>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <span class="help-block">NOTE: Pages are double-sided, so the number of pages you enter will be multiplied by a factor of two (2).</span>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('batch_zones', 'Zones', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            @foreach ($zones as $state_name => $state_zones)
                                                            <ul class="pagination-zones-list">
                                                                <li class="title">{{ $state_name }}</li>
                                                                @foreach ($state_zones as $zone_id => $zone_label)
                                                                <li>
                                                                    <label class="checkbox">
                                                                        {{ Form::checkbox('zone[]', $zone_id, true) }}
                                                                        {{ $zone_label }}
                                                                    </label>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                {{ Form::button('Submit', array('type' => 'submit', 'class' => 'btn btn-primary btn-large pull-right')) }}

                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </article>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/handlebars/handlebars-v1.3.0.js') }}
    {{ HTML::script('js/vendor/plugins/slider-pips/jquery-ui-slider-pips.min.js') }}
    {{ HTML::script('js/vendor/plugins/spinner/jquery.spinner.min.js') }}
    {{ HTML::script('js/framework.pagination.js') }}
    {{ HTML::script('js/framework.pagination.batch.init.js') }}
@stop