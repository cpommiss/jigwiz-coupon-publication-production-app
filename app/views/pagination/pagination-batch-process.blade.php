@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-th"></span> Pagination Batch Setup</h1>
                        <p>Batch setup lets you create a series of blank pages, ready to be paginated, with a given media type for all (or a select number) of your zones.</p>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::open(array('url' => '#', 'id' => 'pagination-batch-process', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled form-vertical')) }}

                        {{ Form::hidden('media', Input::get('media')) }}
                        {{ Form::hidden('month', Input::get('month')) }}
                        {{ Form::hidden('year', Input::get('year')) }}
                        {{ Form::hidden('pages', Input::get('pages')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-th"></span>
                                            Processing...
                                        </h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="progress progress-striped active">
                                                <div class="bar" id="pagination-progress-meter" style="width: 0%;"></div>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span6"><p id="pagination-progress-zone"></p></div>
                                        </div>
                                    </section>
                                </div>
                            </article>
                        </div>

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-globe"></span>
                                            Processed Zones
                                        </h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span4">
                                                <p>The zones shown on the right have been processed.  The result of each processed zone is denoted to the right of the zone name.</p>
                                            </div>
                                            <div id="pagination-batch-results" class="span8">
                                                <table class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Zone</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </article>
                        </div>

                        <a href="{{ route('pagination') }}" id="pagination-batch-finish" class="btn btn-primary btn-large disabled pull-right">Finish</a>

                    {{ Form::close() }}
                    </div>
                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/handlebars/handlebars-v1.3.0.js') }}
    {{ HTML::script('js/vendor/plugins/slider-pips/jquery-ui-slider-pips.min.js') }}
    {{ HTML::script('js/vendor/plugins/spinner/jquery.spinner.min.js') }}
    {{ HTML::script('js/vendor/plugins/moment/moment.min.js') }}
    {{ HTML::script('js/framework.pagination.js') }}
    {{ HTML::script('js/framework.pagination.batch.update.init.js') }}

    <script>
        if ((framework) && (framework.pagination)) {
            framework.pagination.properties.batch.zones_all         = {{ json_encode($zones) }};
            framework.pagination.properties.batch.zones_selected    = {{ json_encode(Input::get('zone')) }}
        }
    </script>
@stop