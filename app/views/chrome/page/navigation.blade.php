@section('page_navigation')
    @if (Auth::check())
                <div class="navigation-block">
                    <section class="user-profile">
                        <figure>
                            @if (Auth::user()->avatar)
                            <img src="{{ Croppa::url('content/installations/' . Auth::user()->installation . '/avatars/' . Auth::user()->avatar, 70, 70) }}">
                            @else
                            <img src="http://placehold.it/60x60">
                            @endif
                            <figcaption>
                                <strong><a href="{{ url('/') }}/" class="">{{ Auth::user()->name }}</a></strong>
                                <em>User #{{ Auth::user()->id }}</em>
                                <ul>
                                    <li><a class="btn btn-primary btn-flat" href="{{ route('profile') }}/" title="Settings">profile</a></li>
                                </ul>
                            </figcaption>
                        </figure>
                    </section>

                    <div class="navbar">
                        <a class="btn btn-navbar btn-block btn-large" data-toggle="collapse" data-target=".nav-collapse"><span class="awe-home"></span> Dashboard</a>
                    </div>

                    <nav class="main-navigation nav-collapse collapse" role="navigation">
                        <ul>
                            <li{{ ((in_array(Route::currentRouteName(), array('dashboard'))) ? ' class="current"' : '') }}>
                                <a href="{{ route('dashboard') }}" class="no-submenu"><span class="awe-home"></span>Dashboard</a>
                            </li>
                            @if ((UserACL::can('read.crm')) && ((UserACL::can('special.crm.use.master')) || (UserACL::can('special.crm.use.personal'))))
                            <li{{ ((in_array(Route::currentRouteName(), array('crm', 'crm_personal', 'crm_create_client', 'crm_personal_create_client', 'crm_update_client', 'crm_personal_update_client'))) ? ' class="current"' : '') }}>
                                @if ((UserACL::can('special.crm.use.personal')) && (UserACL::can('special.crm.use.master')))
                                    <a href="javascript:;">
                                        <span class="awe-book"></span>CRM
                                        <ul>
                                            <li><a{{ ((in_array(Route::currentRouteName(), array('crm', 'crm_create_client', 'crm_update_client'))) ? ' class="current"' : '') }} href="{{ route('crm') }}">Master CRM</a></li>
                                            <li><a{{ ((in_array(Route::currentRouteName(), array('crm_personal', 'crm_personal_create_client', 'crm_personal_update_client'))) ? ' class="current"' : '') }} href="{{ route('crm_personal') }}">Personal CRM</a></li>
                                        </ul>
                                    </a>
                                @elseif (UserACL::can('special.crm.use.master'))
                                    <a href="{{ route('crm') }}" class="no-submenu"><span class="awe-book"></span>Master CRM</a>
                                @elseif (UserACL::can('special.crm.use.personal'))
                                    <a href="{{ route('crm_personal') }}" class="no-submenu"><span class="awe-book"></span>Personal CRM</a>
                                @endif
                            </li>
                            @endif
                            @if (UserACL::can('special.crm.send.publishing_auth'))
                            <!--
                            <li{{ ((in_array(Route::currentRouteName(), array('crm_request_authorization'))) ? ' class="current"' : '') }}>
                                <a href="{{ route('crm_request_authorization') }}" class="no-submenu"><span class="awe-check"></span>Authorization Requests</a>
                            </li>
                            -->
                            @endif
                            @if (UserACL::can('create.system_message'))
                            <li{{ ((in_array(Route::currentRouteName(), array('system_messaging'))) ? ' class="current"' : '') }}>
                                <a href="{{ route('system_messaging') }}" class="no-submenu"><span class="awe-comment"></span>System Messaging</a>
                            </li>
                            @endif
                            @if (UserACL::can('read.media_type'))
                            <li{{ ((in_array(Route::currentRouteName(), array('media_management', 'media_management_create_media', 'media_management_update_media'))) ? ' class="current"' : '') }}>
                                <a href="{{ route('media_management') }}" class="no-submenu"><span class="awe-paste"></span>Media Management</a>
                            </li>
                            @endif
                            @if (UserACL::can('read.zone'))
                            <li{{ ((in_array(Route::currentRouteName(), array('zone', 'zone_create', 'zone_update'))) ? ' class="current"' : '') }}>
                                <a href="{{ route('zone') }}" class="no-submenu"><span class="awe-globe"></span>Zones</a>
                            </li>
                            @endif
                            @if (UserACL::can('read.document'))
                            <li{{ ((in_array(Route::currentRouteName(), array('document'))) ? ' class="current"' : '') }}>
                                <a href="{{ route('document') }}" class="no-submenu"><span class="awe-cloud-download"></span>Documents</a>
                            </li>
                            @endif
                            @if (UserACL::can('read.point'))
                            <li{{ ((in_array(Route::currentRouteName(), array('point'))) ? ' class="current"' : '') }}>
                                <a href="{{ route('point') }}" class="no-submenu"><span class="awe-star"></span>Points</a>
                            </li>
                            @endif
                            @if (UserACL::can('read.graphics_queue'))
                            <li{{ ((in_array(Route::currentRouteName(), array('graphics_queue', 'graphics_queue_create_job', 'graphics_queue_job_details', 'graphics_queue_job_renew'))) ? ' class="current"' : '') }}>
                                <a href="{{ route('graphics_queue') }}" class="no-submenu"><span class="awe-picture-o"></span>Graphics Queue</a>
                            </li>
                            @endif
                            @if ((UserACL::can('read.pagination')) || (UserACL::can('read.reservation')))
                            <li{{ ((in_array(Route::currentRouteName(), array('pagination', 'pagination_update', 'pagination_batch', 'pagination_report_usage', 'pagination_reeservations'))) ? ' class="current"' : '') }}>
                                <a href="javascript:;">
                                    <span class="awe-th"></span>Pagination
                                    <ul>
                                        @if (UserACL::can('read.pagination'))
                                        <li><a{{ ((in_array(Route::currentRouteName(), array('pagination', 'pagination_update'))) ? ' class="current"' : '') }} href="{{ route('pagination') }}">Edit Pagination</a></li>
                                        <li><a{{ ((in_array(Route::currentRouteName(), array('pagination_report_usage'))) ? ' class="current"' : '') }} href="{{ route('pagination_report_usage') }}">Usage Report</a></li>
                                        @endif
                                        @if (UserACL::can('read.reservation'))
                                        <li><a{{ ((in_array(Route::currentRouteName(), array('pagination_reservations'))) ? ' class="current"' : '') }} href="{{ route('pagination_reservations') }}">Reservations</a></li>
                                        @endif
                                    </ul>
                                </a>
                            </li>
                            @endif
                            <li>
                                <a href="{{ route('logout') }}/" class="no-submenu"><span class="awe-sign-out"></span>Logout</a>
                            </li>
                        </ul>
                    </nav>
                </div>
    @elseif (Session::has('visiting'))
                <div class="navigation-block">
                    <div class="navbar">
                        <a class="btn btn-navbar btn-block btn-large" data-toggle="collapse" data-target=".nav-collapse"><span class="awe-home"></span> Dashboard</a>
                    </div>

                    <nav class="main-navigation nav-collapse collapse" role="navigation">
                        <ul>
                            @if (Session::has('visiting.job_key'))
                            <li{{ ((in_array(Route::currentRouteName(), array('client_view_graphics_queue_job_details'))) ? ' class="current"' : '') }}>
                                <a href="{{ route('client_view_graphics_queue_job_details', Session::get('visiting.job_key')) }}" class="no-submenu"><span class="awe-picture-o"></span>Reviewing Job</a>
                            </li>
                            @endif
                            @if (Session::has('visiting.publishing_auth_key'))
                            <li{{ ((in_array(Route::currentRouteName(), array('client_view_crm_request_authorization'))) ? ' class="current"' : '') }}>
                                <a href="{{ route('client_view_crm_request_authorization', Session::get('visiting.publishing_auth_key')) }}" class="no-submenu"><span class="awe-globe"></span>Authorizing Publishing</a>
                            </li>
                            @endif
                            <li>
                                <a href="#" class="no-submenu"><span class="awe-sign-out"></span>Logout</a>
                            </li>
                        </ul>
                    </nav>
                </div>
    @endif
@show