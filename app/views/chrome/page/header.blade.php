@section('header_page')
            @if ((Auth::check()) || (Session::has('visiting')))
                <header id="header" class="container">
                    <nav>
                        <ul>
                            @if ((Auth::check()) && ((UserACL::is('admin')) || (UserACL::can('special.system.update.branding'))))
                            <li>
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                        <span class="awe-cog"></span> Administration <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        @if (UserACL::is('admin'))
                                        <li><a href="{{ route('user') }}"><span class="awe-tags"></span> Manage User Accounts</a></li>
                                        @endif
                                        @if (UserACL::can('special.system.update.branding'))
                                        <li><a href="{{ route('styling') }}"><span class="awe-tint"></span> Styling Options</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </li>
                            @endif
                            @if (Auth::check())
                            <li><a href="{{ route('profile') }}/"><span class="awe-user"></span> Your Profile</a></li>
                            <li id="header-notifications">
                                <a href="#">
                                    <span class="awe-bullhorn"></span>
                                    Notifications
                                    &nbsp;
                                    <span class="label hidden"></span>
                                </a>
                            </li>
                            <li><a href="{{ route('logout') }}/"><span class="awe-sign-out"></span> Logout</a></li>
                            @endif
                        </ul>
                    </nav>

                    @include('chrome.notifications.list')

                    <h1><a href="{{ ((Auth::check()) ? route('dashboard') : '#') }}/" class="brand">Site Name</a></h1>
                </header>
            @endif
@show