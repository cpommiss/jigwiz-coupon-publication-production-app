@section('alerts')
            @if ((isset($errors)) && (count($errors)))
                <div class="alert alert-danger">
                    <strong>Oops!</strong>
                    <br />
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br />
                    @endforeach
                </div>
            @elseif (Session::get('success'))
                <div class="alert alert-success">
                    <strong>Success!</strong>
                    <br />
                    {{ Session::get('success') }}<br />
                </div>
            @elseif (Session::get('failure'))
                <div class="alert alert-danger">
                    <strong>Sorry!</strong>
                    <br />
                    {{ Session::get('failure') }}<br />
                </div>
            @endif
@show