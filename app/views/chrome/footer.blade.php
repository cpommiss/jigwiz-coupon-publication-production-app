@section('footer')
        {{ HTML::script('js/bootstrap/bootstrap.min.js') }}
        {{ HTML::script('js/vendor/plugins/chosen/chosen.jquery.min.js') }}
        {{ HTML::script('js/vendor/plugins/bootbox/bootbox.min.js') }}
        {{ HTML::script('js/vendor/plugins/blockUI/jquery.blockUI.js') }}
        {{ HTML::script('js/vendor/plugins/socket.io-client/socket.io.js') }}
        {{ HTML::script('js/framework.js') }}
        {{ HTML::script('js/framework.init.js') }}

        <script>
            if (framework) {
                framework.properties.paths.root = '{{{ url() }}}/';
            }
        </script>
        @if (Auth::check())
        <script>
            if (framework) {
                framework.properties.uid = {{{ Auth::user()->id }}};
                framework.properties.paths.content = '{{{ url() }}}/content/installations/{{{ Auth::user()->installation }}}/';
            }
        </script>
        @endif
@show