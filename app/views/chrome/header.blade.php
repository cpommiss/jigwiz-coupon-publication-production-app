@section('header')
    <head>
        <meta charset="utf-8">

        <title>{{ Config::get('app.title') }}</title>

        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="robots" content="index, follow">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="favicon.ico">

        @if ((Auth::check()) || (Session::has('visiting.installation')))
        {{ HTML::style('content/installations/' . ((Session::has('visiting.installation')) ? Session::get('visiting.installation') : Auth::user()->installation) . '/style.css?v=' . time()) }}
        @else
        {{ HTML::style('css/style_default.css?v=' . time()) }}
        @endif

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="{{ asset('js/vendor/jquery.js') }}"><\/script>')</script>
        {{ HTML::script('js/vendor/jquery-ui-1.10.2.custom.min.js') }}
        {{ HTML::script('js/vendor/modernizr.js') }}
        {{ HTML::script('js/vendor/selectivizr.js') }}
    </head>
@show