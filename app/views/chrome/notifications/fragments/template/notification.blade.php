<li data-id="{{ $id }}">
    @if ($link)
    <a href="{{ $link }}">
    @endif
        <p>{{ $message }}</p>
        <cite><span class="label label-success">New</span><span class="awe-{{ $icon }}"></span> {{ $time }}</cite>
    @if ($link)
    </a>
    @endif
</li>