<li data-id="{{ $notification->id }}"{{ (($notification->notified) ? ' class="notified"' : '') }}>
    <a href="{{ route('graphics_queue_job_details', $notification->related_id) }}">
        <p>{{ $notification->message }}</p>
        <cite><span class="label label-success">New</span><span class="awe-picture-o"></span> {{ $notification->created_at->diffForHumans() }}</cite>
    </a>
</li>