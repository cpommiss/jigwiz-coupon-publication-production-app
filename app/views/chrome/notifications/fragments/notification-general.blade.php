<li data-id="{{ $notification->id }}"{{ (($notification->notified) ? ' class="notified"' : '') }}>
    <p>{{ $notification->message }}</p>
    <cite><span class="label label-success">New</span><span class="awe-comment"></span> {{ $notification->created_at->diffForHumans() }}</cite>
</li>
