<li data-id="{{ $notification->id }}"{{ (($notification->notified) ? ' class="notified"' : '') }}>
    <a href="{{ route('dashboard') }}">
        <p>{{ $notification->message }}</p>
        <cite><span class="label label-success">New</span><span class="awe-book"></span> {{ $notification->when->diffForHumans() }}</cite>
    </a>
</li>