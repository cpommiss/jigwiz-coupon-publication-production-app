<li data-id="{{ $notification->id }}"{{ (($notification->notified) ? ' class="notified"' : '') }}>
    <a href="{{ route('crm_update_client', $notification->related_id) }}">
        <p>{{ $notification->message }}</p>
        <cite><span class="label label-success">New</span><span class="awe-check"></span> {{ $notification->when->diffForHumans() }}</cite>
    </a>
</li>