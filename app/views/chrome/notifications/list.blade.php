@if (Auth::check())
<div id="notifications-popover" class="hidden">
    <ol class="row-fluid">
        @if (!$notifications->isEmpty())
            @foreach ($notifications as $notification)
                @foreach (Config::get('settings.notifications.types') as $notification_key => $notification_data)
                    @if ($notification->type == $notification_data['ordinal'])
                        @include('chrome.notifications.fragments.notification-' . $notification_key, array('notification' => $notification))
                    @endif
                @endforeach
            @endforeach
        @else
            <li class="empty"><p>You have no notifications.</p></li>
        @endif
    </ol>
</div>
@endif