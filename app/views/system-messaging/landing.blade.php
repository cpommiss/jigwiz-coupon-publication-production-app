@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/bootstrap-wysihtml5.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1><span class="awe-pencil"></span> System Messaging</h1>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::open(array('route' => 'system_messaging', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2><span class="awe-gear"></span> System Message</h2>
                                    </header>
                                    <section>
                                        <p>You can add a new system message to be shown to users on their <strong>Dashboard</strong> upon a successful login by completing the form below.</p>
                                        <br />


                                        <fieldset class="well">
                                            <div class="control-group">
                                                {{ Form::label('message_subject', 'Title/Subject', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    {{ Form::text('subject', null, array('id' => 'message_subject', 'class' => 'span8', 'placeholder' => 'Title/Subject')) }}
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                {{ Form::label('message_text', 'Message', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    {{ Form::textarea('message', null, array('id' => 'message_text', 'class' => 'wysihtml5')) }}
                                                </div>
                                            </div>
                                        </fieldset>
                                    </section>

                                    {{ Form::button('Save Message', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/wysihtml5/wysihtml5-0.3.0.js') }}
    {{ HTML::script('js/vendor/plugins/wysihtml5/bootstrap-wysihtml5.js') }}
    {{ HTML::script('js/framework.system-messaging.init.js') }}
@stop