@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1>
                            <span class="awe-pencil"></span> Updating Media Type: {{ $mediatype->name }}
                        </h1>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::model($mediatype, array('route' => array('media_management_update_media', $mediatype->id), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2><span class="awe-book"></span> Media Details</h2>
                                        @if (UserACL::can('delete.media_type'))
                                        <ul class="data-header-actions">
                                            <li>
                                                <a href="javascript:;" class="btn btn-danger remove-modal" data-remove-url="{{ route('media_management_delete_media', $mediatype->id) }}" data-remove-message="{{ AppUtils::strip_tag_whitespace(Lang::get('warnings.media_type.remove_warning')) }}">Delete Media Type</a>
                                            </li>
                                        </ul>
                                        @endif
                                    </header>
                                    <section>
                                        <div class="row-fluid">
                                            <div class="span4">
                                                <p>Define the details of this media type, such as dimensions and smallest allowable advertisement size.</p>

                                                <br />

                                                <div id="pagination-preview" class="hidden">
                                                    <h3><span class="awe-th-list"></span> Pagination Preview</h3>

                                                    <div class="row-fluid page-preview">
                                                        <cite class="width"><strong><span class="value">0 in.</span></strong></cite>

                                                        <div class="span12 page-preview-inner">
                                                            <cite class="height"><strong><em><span class="value">0 in.</span></em></strong></cite>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="span8">
                                                <fieldset class="well form-horizontal">
                                                    <div class="control-group">
                                                        {{ Form::label('media_type_name', 'Label/Name', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::text('name', null, array('id' => 'media_type_name', 'class' => 'input-xlarge', 'placeholder' => 'Label/Name, i.e. \'Great Deals\'')) }}
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <h2>Dimensions</h2>
                                                <p>Set the dimensions of this media type, in inches, below.  You can use up to two (2) decimal places.</p>

                                                <fieldset class="well form-horizontal">
                                                    <div class="control-group">
                                                        {{ Form::label('media_type_width', 'Width', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::text('width', null, array('id' => 'media_type_width', 'class' => 'input-small', 'placeholder' => 'Width (in.)')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('media_type_height', 'Height', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::text('height', null, array('id' => 'media_type_height', 'class' => 'input-small', 'placeholder' => 'Height (in.)')) }}
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <h2>Minimum Advertising Size</h2>
                                                <p>Choose the smallest possible layout available on a page within this media type.</p>

                                                <fieldset class="well form-horizontal">
                                                    <div class="control-group">
                                                        {{ Form::label('media_type_max_rows', 'Maximum Rows', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::text('max_rows', null, array('id' => 'media_type_max_rows', 'maxlength' => 2, 'class' => 'input-small', 'placeholder' => 'Rows')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('media_type_max_cols', 'Maximum Columns', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::text('max_cols', null, array('id' => 'media_type_max_cols', 'maxlength' => 2, 'class' => 'input-small', 'placeholder' => 'Columns')) }}
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <h2>Attributes</h2>
                                                <p>Define the special attributes of this media type.</p>

                                                <fieldset class="well form-horizontal">
                                                    <div class="control-group">
                                                        {{ Form::label('media_type_color', 'Color Style', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::select('color', Config::get('settings.media_types.color_types'), null, array('id' => 'media_type_color', 'class' => 'input-medium')) }}
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        {{ Form::label('media_type_premium', 'Has Premium Pages?', array('class' => 'control-label')) }}
                                                        <div class="controls">
                                                            {{ Form::select('premium', array('1' => 'Yes', '0' => 'No'), null, array('id' => 'media_type_premium', 'class' => 'input-small')) }}
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </section>

                                    {{ Form::button('Update Media Type', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/handlebars/handlebars-v1.3.0.js') }}
    {{ HTML::script('js/framework.pagination.js') }}
    {{ HTML::script('js/framework.media.js') }}
    {{ HTML::script('js/framework.media.init.js') }}
@stop