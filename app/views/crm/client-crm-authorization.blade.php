@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-check"></span> Publishing Authorization</h1>
					</article>

                    @include('chrome.page.alerts')

                    @if (!$auth->authorized)

                        {{ Form::open(array('route' => 'client_view_crm_create_authorization', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled form-vertical')) }}

                            {{ Form::hidden('key', Session::get('visiting.publishing_auth_key')) }}

                            <div class="row">
                                <article class="span12 data-block">
                                    <div class="data-container">
                                        <header>
                                            <h2>
                                                <span class="awe-globe"></span>
                                                Review Details and Select Areas
                                            </h2>
                                        </header>

                                        <section>
                                            <div class="row-fluid">
                                                <div class="span4">
                                                    <p>We need your authorization to add your advertisements to our <strong>{{ $auth->mediatype->name }}</strong> publication in the <strong>{{ $auth->auth_date->format('F Y') }}</strong> issue.</p>
                                                    <p>Please select the geographic areas in which you'd like us to include you when we publish this issue.</p>
                                                </div>
                                                <div class="span8">
                                                    <fieldset class="well form-horizontal">
                                                        <div class="control-group">
                                                            <label class="control-label" for="input">Customer Name</label>
                                                            <div class="controls">
                                                                <span class="uneditable-input">{{ $auth->customer->name }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="input">Publication</label>
                                                            <div class="controls">
                                                                <span class="uneditable-input">{{ $auth->mediatype->name }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="input">Issue Date</label>
                                                            <div class="controls">
                                                                <span class="uneditable-input">{{ $auth->auth_date->format('F Y') }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('auth_zone', 'Area(s)', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::select('zone[]', $zones, Input::old('zone'), array('id' => 'auth_zone', 'class' => 'input-xlarge', 'multiple' => true)) }}
                                                            </div>
                                                        </div>
                                                    </fieldset>

                                                    <h3>Digital Authorization</h3>
                                                    <p>By signing below, you agree that you are authorizing the use of your advertising within this publication.</p>

                                                    <fieldset class="well form-horizontal">
                                                        <div class="control-group">
                                                            {{ Form::label('auth_signature', 'Your Signature', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('signature', null, array('id' => 'auth_signature', 'class' => 'input-large', 'placeholder' => 'Your Signature')) }}
                                                            </div>
                                                        </div>
                                                    </fieldset>

                                                    {{ Form::button('Authorize', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </article>
                            </div>

                        {{ Form::close() }}

                    @else
                        <div class="alert alert-success">
                            <strong>Success!</strong>
                            <p>{{ Lang::get('success.crm.authorization.authorized') }}</p>
                        </div>
                    @endif
                </div>
@stop
@section('footer')
    @parent

    {{ HTML::script('js/framework.crm.authorization.client.init.js') }}
@stop