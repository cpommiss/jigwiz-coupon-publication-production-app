@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/bootstrap-wysihtml5.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1><span class="awe-plus"></span> Creating New Client</h1>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::open(array('route' => Route::currentRouteName(), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2><span class="awe-book"></span> Client Information</h2>
                                        <ul class="data-header-actions">
                                            <li class="tabs active"><a href="#client-basic" class="btn btn-flat"><span class="awe-info"></span> Name/Info</a></li>
                                            <li class="tabs"><a href="#client-address" class="btn btn-flat"><span class="awe-phone"></span> Address/Contact</a></li>
                                            <li class="tabs"><a href="#client-notes" class="btn btn-flat"><span class="awe-comments"></span> Notes</a></li>
                                        </ul>
                                    </header>
                                    <section class="tab-content">
                                        <div class="tab-pane active" id="client-basic">
                                            <h2>Basic Information</h2>

                                            <fieldset class="well form-horizontal">
                                                <div class="control-group">
                                                    {{ Form::label('client_name', 'Client Name', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::text('name', Input::old('name'), array('id' => 'client_name', 'class' => 'input-xlarge', 'placeholder' => 'Client Name')) }}
                                                    </div>
                                                </div>
                                            </fieldset>

                                            @if (CRMController::isRoutePersonal())
                                            <h2>Type</h2>
                                            <p>If this is a prospect and they are now a customer, you can copy them into the "master" CRM by setting them as a "Customer".</p>

                                            <fieldset class="well form-horizontal">
                                                <div class="control-group">
                                                    {{ Form::label('client_type', 'Type', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::select('type', AppUtils::config_reduce_complex_array_to_kvp(Config::get('settings.crm.types'), 'value', 'label'), null, array('id' => 'client_type', 'class' => 'input-xlarge')) }}
                                                    </div>
                                                </div>
                                            </fieldset>
                                            @endif

                                            @if (!CRMController::isRoutePersonal())
                                            <h2>Representative Assignment</h2>
                                            <p>You can assign a sales representative to this client by choosing one from the list below.</p>

                                            <fieldset class="well form-horizontal">
                                                <div class="control-group">
                                                    {{ Form::label('client_assigned_to', 'Assigned To', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::select('assigned_to', array_merge(array('0' => '-- Select --'), $users), Input::old('assigned_to') ? Input::old('assigned_to') : Auth::user()->id, array('id' => 'client_assigned_to', 'class' => 'input-xlarge')) }}
                                                    </div>
                                                </div>
                                            </fieldset>
                                            @endif
                                        </div>
                                        <div class="tab-pane" id="client-address">
                                            <h2>Address and Contact Information</h2>

                                            <div class="row-fluid">
                                                <div class="span4">
                                                    <h3>Logo</h3>

                                                    <p>You can upload a logo to go along with this client.  This may be included in reports, or in other areas, to add to the visual language of the system.  It is not essential that you upload a logo, however, and you can always do so later if you choose.</p>

                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-new fileupload-large thumbnail hidden" style="display: none;"></div>
                                                        <div class="fileupload-preview fileupload-exists fileupload-large thumbnail flexible full"></div>
                                                        <div>
                                                            <span class="btn btn-alt btn-file">
                                                                <span class="fileupload-new">Select image</span>
                                                                <span class="fileupload-exists">Change</span>
                                                                {{ Form::file('logo', array('id' => 'client_logo')) }}
                                                            </span>
                                                            <a class="btn btn-alt btn-danger fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span8">
                                                    <h3>Address Information</h3>

                                                    <fieldset class="well form-horizontal">
                                                        <div class="control-group">
                                                            {{ Form::label('client_address1', 'Address (Line 1)', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('address1', Input::old('address1'), array('id' => 'client_address1', 'class' => 'input-xlarge', 'placeholder' => 'Address (Line 1)')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('client_address2', 'Address (Line 2)', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('address2', Input::old('address2'), array('id' => 'client_address2', 'class' => 'input-xlarge', 'placeholder' => 'Address (Line 2)')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('client_city', 'City/State/ZIP', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('city', Input::old('city'), array('id' => 'client_city', 'class' => 'span7', 'placeholder' => 'City')) }}
                                                                {{ Form::text('state', Input::old('state'), array('id' => 'client_state', 'class' => 'span2', 'placeholder' => 'State', 'maxlength' => 2)) }}
                                                                {{ Form::text('zip', Input::old('zip'), array('id' => 'client_zip', 'class' => 'span3', 'placeholder' => 'ZIP')) }}
                                                            </div>
                                                        </div>
                                                    </fieldset>

                                                    <h3>Contact Information</h3>

                                                    <fieldset class="well form-horizontal">
                                                        <div class="control-group">
                                                            {{ Form::label('client_contact_name', 'Contact Name', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('contact_name', Input::old('contact_name'), array('id' => 'client_contact_name', 'class' => 'span8', 'placeholder' => 'Contact Name')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('client_phone', 'Primary Phone #', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('phone', Input::old('phone'), array('id' => 'client_address1', 'class' => 'span6', 'placeholder' => 'Phone')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('client_email', 'E-mail', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('email', Input::old('email'), array('id' => 'client_email', 'class' => 'span8', 'placeholder' => 'E-mail')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('client_website', 'Website URL', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('website', Input::old('website'), array('id' => 'client_website', 'class' => 'input-xlarge', 'placeholder' => 'Website URL')) }}
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="client-notes">
                                            <h2>Notes</h2>

                                            <div class="row-fluid">
                                                <div class="span4">
                                                    <p>Notes on client records are saved chronologically as individual notes.  Since this is a new client, you can specify an initial note to be added upon creation of this client record.</p>
                                                </div>
                                                <div class="span8">
                                                    <h3>Initial Client Notes</h3>

                                                    <fieldset class="well">
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                {{ Form::textarea('notes_add', Input::old('notes'), array('id' => 'client_notes', 'class' => 'wysihtml5')) }}
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </section>

                                    {{ Form::button('Create Client', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/fileupload/bootstrap-fileupload.js') }}
    {{ HTML::script('js/vendor/plugins/wysihtml5/wysihtml5-0.3.0.js') }}
    {{ HTML::script('js/vendor/plugins/wysihtml5/bootstrap-wysihtml5.js') }}
    {{ HTML::script('js/framework.crm.create.init.js') }}
@stop