@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/jquery-ui-slider.css') }}
    {{ HTML::style('css/vendor/plugins/jquery-ui-slider-pips.css') }}
    {{ HTML::style('css/vendor/plugins/bootstrap-spinner.css') }}
@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        <h1><span class="awe-check"></span> Request Client Authorization</h1>
                        <p>You can request publishing authorization for a given client by using the settings below.</p>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::open(array('route' => Route::currentRouteName(), 'id' => 'crm-authorization-settings', 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled form-vertical')) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2>
                                            <span class="awe-cogs"></span>
                                            Authorization Settings
                                        </h2>
                                    </header>

                                    <section>
                                        <div class="row-fluid">
                                            <div class="span6">
                                                {{ Form::label('auth_client', 'Client', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    {{ Form::select('client', array_merge(array('0' => 'Select client'), $clients), null, array('id' => 'auth_client', 'class' => 'span12')) }}
                                                </div>
                                            </div>
                                            <div class="span6">
                                                {{ Form::label('auth_media', 'Media', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    {{ Form::select('media', array_merge(array('0' => 'Select media'), $mediatypes), null, array('id' => 'auth_media', 'class' => 'span12')) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span10">
                                                {{ Form::label('auth_month', 'Month', array('class' => 'control-label')) }}
                                                {{ Form::hidden('month', date('n')) }}
                                                <div class="controls">
                                                    <div id="month_select"></div>
                                                </div>
                                            </div>
                                            <div class="span2">
                                                {{ Form::label('auth_year', 'Year', array('class' => 'control-label')) }}
                                                <div class="controls">
                                                    <div class="input-append spinner" data-trigger="spinner">
                                                        {{ Form::text('year', date('Y'), array('id' => 'auth_year', 'class' => 'span6')) }}
                                                        <div class="add-on">
                                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="awe-sort-up"></i></a>
                                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="awe-sort-down"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <hr />

                                            {{ Form::button('Request Authorization', array('type' => 'submit', 'class' => 'btn btn-success pull-right')) }}
                                        </div>
                                    </section>
                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>
@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/slider-pips/jquery-ui-slider-pips.min.js') }}
    {{ HTML::script('js/vendor/plugins/spinner/jquery.spinner.min.js') }}
    {{ HTML::script('js/framework.pagination.js') }}
    {{ HTML::script('js/framework.crm.js') }}
    {{ HTML::script('js/framework.crm.authorization.init.js') }}
@stop