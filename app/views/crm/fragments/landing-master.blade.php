                                <div class="crm-wrapper">
                                    <table class="datatable table table-striped table-bordered table-hover" id="crm-data-status-master">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Name</th>
                                                <th colspan="4">Address Information</th>
                                                <th colspan="2">Contact Information</th>
                                            </tr>
                                            <tr>
                                                <th>Address</th>
                                                <th>City</th>
                                                <th>State</th>
                                                <th>ZIP</th>
                                                <th>Phone</th>
                                                <th>E-mail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($clients as $client)
                                                <tr data-row-id="{{ $client->id }}">
                                                    <td>
                                                        @if (UserACL::can('update.crm'))
                                                        <a href="{{ route('crm_update_client', $client->id) }}/">{{ $client->name }}</a>
                                                        @else
                                                        {{ $client->name }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ $client->attrs->address1 }}<br />
                                                        {{ $client->attrs->address2 }}
                                                    </td>
                                                    <td>{{ $client->attrs->city }}</td>
                                                    <td>{{ $client->attrs->state }}</td>
                                                    <td>{{ $client->attrs->zip }}</td>
                                                    <td>{{ $client->attrs->phone }}</td>
                                                    <td>{{ $client->attrs->email }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
