<div class="row-fluid form-vertical">
    <input type="hidden" name="reminder_id" value="{{ Input::get('id') }}">
    <input type="hidden" name="reminder_start" value="{{ Input::get('start') }}">
    <input type="hidden" name="reminder_end" value="{{ Input::get('end') }}">
    <input type="hidden" name="reminder_allday" value="{{ Input::get('allday') }}">

    <div class="control-group">
        <label class="control-label">Client</label>
        <div class="controls">
            <select name="reminder_client" class="input-xlarge">
                <option value="0"></option>
                @if (($customers_personal) && (!$customers_personal->isEmpty()))
                    <optgroup label="Personal CRM">
                        @foreach ($customers_personal as $customer)
                        <option value="{{ $customer->id }}"{{ (($customer->id == Input::get('client')) ? ' selected' : '') }}>{{ $customer->name }}</option>
                        @endforeach
                    </optgroup>
                @endif
                @if (($customers_master) && (!$customers_master->isEmpty()))
                    <optgroup label="Master CRM">
                        @foreach ($customers_master as $customer)
                        <option value="{{ $customer->id }}"{{ (($customer->id == Input::get('client')) ? ' selected' : '') }}>{{ $customer->name }}</option>
                        @endforeach
                    </optgroup>
                @endif
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label ">Remind me to&hellip;</label>
        <div class="controls">
            <textarea name="reminder_text" rows="2" class="input-xlarge" style="resize: none;">{{ Input::get('text') }}</textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Remind me&hellip;</label>
        <div class="controls">
            <select name="reminder_when" class="input-medium">
                @foreach (Config::get('settings.crm.reminders.durations') as $duration_length => $duration_label)
                <option value="{{ $duration_length }}"{{ (($duration_length == Input::get('when')) ? ' selected' : '') }}>{{ $duration_label }}</option>
                @endforeach
            </select>
        </div>
        <span class="block-help">NOTE: Reminders occur on the day of the event.</span>
    </div>
    <br />
    <button type="button" name="reminder_submit" class="btn btn-primary pull-right">{{ ((Input::get('label')) ? Input::get('label') : 'Submit') }}</button>
</div>
