@extends('layout')
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
                        @if (CRMController::isRoutePersonal())
                        <h1><span class="awe-book"></span> Your Client List</h1>
                        <p>Shown below is your personal client list.  You may filter the results shown by using the controls below.</p>
                        @else
                        <h1><span class="awe-book"></span> Master Client List</h1>
                        <p>Shown below is the master client list for your organization.  You may filter the results shown by using the controls below.</p>
                        @endif
					</article>

                    @include('chrome.page.alerts')

                    <div class="row">
                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2>
                                        <span class="awe-cogs"></span>
                                        Filtering and Search Tools
                                    </h2>
                                </header>

                                {{ Form::open(array('route' => Route::currentRouteName(), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled')) }}

                                    <div class="row-fluid criteria-row" data-row-id="1">
                                        <hr />

                                        <div class="row-fluid">
                                            <div class="span3">
                                                {{ Form::label('criteria_1', 'Criteria', array('for' => 'criteria_1', 'class' => 'control-label')) }}
                                                <div class="controls">
                                                    {{ Form::select('criteria_1', array_merge(array('0' => '-- None --'), $search_types_simple), '0', array('id' => 'criteria_1', 'class' => 'span12 criteria-type')) }}
                                                </div>
                                            </div>
                                            <div class="span8 criteria-controls"></div>
                                            <div class="span1 pull-right criteria-remove-control hidden">
                                                <label class="control-label text-center">Delete</label>
                                                <div class="controls text-center">
                                                    <button type="button" class="btn btn-inverse btn-remove-criteria"><span class="awe-times"></span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span12">
                                            <hr />

                                            <a href="javascript:" id="btn-add-criteria" class="btn pull-left">Add More Criteria</a>
                                            <button type="submit" id="btn-add-criteria" class="btn btn-success pull-right">Search/Update Results</button>
                                        </div>
                                    </div>

                                {{ Form::close() }}
                            </div>
                        </article>

                        <article class="span12 data-block">
                            <div class="data-container">
                                <header>
                                    <h2>
                                        <span class="awe-book"></span>
                                        @if (CRMController::isRoutePersonal())
                                        Client/Prospect List
                                        @else
                                        Master Client List
                                        @endif
                                    </h2>
                                    @if (UserACL::can('create.crm'))
                                    <ul class="data-header-actions">
                                        <li>
                                            <a class="btn btn-success" href="{{ route(CRMController::isRoutePersonal() ? 'crm_personal_create_client' : 'crm_create_client') }}/">Create New Client</a>
                                        </li>
                                    </ul>
                                    @endif
                                </header>

                                @include('crm.fragments.landing-' . ((CRMController::isRoutePersonal()) ? 'personal' : 'master'), array('clients' => $clients))
                            </div>
                        </article>
                    </div>
                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/dataTables/jquery.datatables.js') }}
    {{ HTML::script('js/vendor/plugins/dataTables/tableTools/js/TableTools.min.js') }}
    {{ HTML::script('js/vendor/plugins/dataTables/tableTools/js/ZeroClipboard.js') }}
    {{ HTML::script('js/framework.datatables.init.js') }}
    {{ HTML::script('js/framework.crm.js') }}
    {{ HTML::script('js/framework.crm.init.js') }}

    <script>
        if ((framework) && (framework.crm)) {
            framework.crm.properties.search_types = {{ $search_types }};
            framework.crm.properties.search_criteria = {{ $search_criteria }};
        }
    </script>
@stop