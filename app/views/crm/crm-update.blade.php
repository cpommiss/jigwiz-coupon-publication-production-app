@extends('layout')
@section('header')
    @parent

    {{ HTML::style('css/vendor/plugins/bootstrap-wysihtml5.css') }}
    {{ HTML::style('css/vendor/plugins/tipped-v3.css') }}
    {{ HTML::style('css/vendor/plugins/jquery.fullcalendar.css') }}

@stop
@section('content')

                <div class="content-block" role="main">
					<article class="page-header">
						<h1><span class="awe-pencil"></span> Updating Client: {{ $crm->name }}</h1>
					</article>

                    @include('chrome.page.alerts')

                    {{ Form::model($crm, array('route' => array(Route::currentRouteName(), $crm->id), 'autocomplete' => 'off', 'novalidate' => 'true', 'class' => 'form-unstyled', 'files' => true)) }}

                        {{ Form::hidden('_client', $crm->id) }}

                        <div class="row">
                            <article class="span12 data-block">
                                <div class="data-container">
                                    <header>
                                        <h2><span class="awe-book"></span> Client Information</h2>
                                        <ul class="data-header-actions">
                                            <li class="tabs active"><a href="#client-basic" class="btn btn-flat"><span class="awe-info"></span> Name/Info</a></li>
                                            <li class="tabs"><a href="#client-address" class="btn btn-flat"><span class="awe-phone"></span> Address/Contact</a></li>
                                            <li class="tabs"><a href="#client-notes" class="btn btn-flat"><span class="awe-comments"></span> Notes</a></li>
                                            <!--<li class="tabs"><a href="#client-auth" class="btn btn-flat"><span class="awe-check"></span> Authorizations</a></li>-->
                                            <li class="tabs"><a href="#client-calendar" class="btn btn-flat"><span class="awe-calendar"></span> Calendar</a></li>
                                            @if (UserACL::can('delete.crm'))
                                            <li>
                                                <a href="javascript:;" class="btn btn-danger remove-modal" data-remove-url="{{ route(((Str::contains(Route::currentRouteName(), 'personal') === true) ? 'crm_personal_delete_client' : 'crm_delete_client'), $crm->id) }}" data-remove-message="{{ AppUtils::strip_tag_whitespace(Lang::get('warnings.crm.remove_warning')) }}">Delete Client</a>
                                            </li>
                                            @endif
                                        </ul>
                                    </header>
                                    <section class="tab-content">
                                        <div class="tab-pane active" id="client-basic">
                                            <h2>Basic Information</h2>

                                            <fieldset class="well form-horizontal">
                                                <div class="control-group">
                                                    {{ Form::label('client_name', 'Client Name', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::text('name', null, array('id' => 'client_name', 'class' => 'input-xlarge', 'placeholder' => 'Client Name')) }}
                                                    </div>
                                                </div>
                                            </fieldset>

                                            @if (CRMController::isRoutePersonal())
                                            <h2>Type</h2>
                                            <p>If this is a prospect and they are now a customer, you can copy them into the "master" CRM by setting them as a "Customer".</p>

                                            <fieldset class="well form-horizontal">
                                                <div class="control-group">
                                                    {{ Form::label('client_type', 'Type', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::select('type', AppUtils::config_reduce_complex_array_to_kvp(Config::get('settings.crm.types'), 'value', 'label'), null, array('id' => 'client_type', 'class' => 'input-xlarge')) }}
                                                    </div>
                                                </div>
                                            </fieldset>
                                            @endif

                                            @if (!CRMController::isRoutePersonal())
                                            <h2>Representative Assignment</h2>
                                            <p>You can change who this client is assigned to by choosing a user from the list below.</p>

                                            <fieldset class="well form-horizontal">
                                                <div class="control-group">
                                                    {{ Form::label('client_assigned_to', 'Assigned To', array('class' => 'control-label')) }}
                                                    <div class="controls">
                                                        {{ Form::select('assigned_to', array_merge(array('0' => '-- Select --'), $users), null, array('id' => 'client_assigned_to', 'class' => 'input-xlarge')) }}
                                                    </div>
                                                </div>
                                            </fieldset>
                                            @endif
                                        </div>
                                        <div class="tab-pane" id="client-address">
                                            <h2>Address and Contact Information</h2>

                                            <div class="row-fluid">
                                                <div class="span4">
                                                    <h3>Logo</h3>

                                                    <p>You amy update the logo for this client using the upload feature below.</p>

                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        @if ($crm->attrs->logo)
                                                        <div class="fileupload-new fileupload-large thumbnail">
                                                            <img src="{{ Croppa::url('content/installations/' . Auth::user()->installation . '/logos/' . $crm->attrs->logo, 240) }}">
                                                        </div>
                                                        @else
                                                        <div class="fileupload-new fileupload-large thumbnail hidden" style="display: none;"></div>
                                                        @endif
                                                        <div class="fileupload-preview fileupload-exists fileupload-large thumbnail flexible full"></div>
                                                        <div>
                                                            <span class="btn btn-alt btn-file">
                                                                <span class="fileupload-new">Select image</span>
                                                                <span class="fileupload-exists">Change</span>
                                                                {{ Form::file('logo', array('id' => 'client_logo')) }}
                                                            </span>
                                                            <a class="btn btn-alt btn-danger fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span8">
                                                    <h3>Address Information</h3>

                                                    <fieldset class="well form-horizontal">
                                                        <div class="control-group">
                                                            {{ Form::label('client_address1', 'Address (Line 1)', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('attrs[address1]', null, array('id' => 'client_address1', 'class' => 'input-xlarge', 'placeholder' => 'Address (Line 1)')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('client_address2', 'Address (Line 2)', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('attrs[address2]', null, array('id' => 'client_address2', 'class' => 'input-xlarge', 'placeholder' => 'Address (Line 2)')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('client_city', 'City/State/ZIP', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('attrs[city]', null, array('id' => 'client_city', 'class' => 'span7', 'placeholder' => 'City')) }}
                                                                {{ Form::text('attrs[state]', null, array('id' => 'client_state', 'class' => 'span2', 'placeholder' => 'State', 'maxlength' => 2)) }}
                                                                {{ Form::text('attrs[zip]', null, array('id' => 'client_zip', 'class' => 'span3', 'placeholder' => 'ZIP')) }}
                                                            </div>
                                                        </div>
                                                    </fieldset>

                                                    <h3>Contact Information</h3>

                                                    <fieldset class="well form-horizontal">
                                                        <div class="control-group">
                                                            {{ Form::label('client_contact_name', 'Contact Name', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('attrs[contact_name]', null, array('id' => 'client_contact_name', 'class' => 'span8', 'placeholder' => 'Contact Name')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('client_phone', 'Primary Phone #', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('attrs[phone]', null, array('id' => 'client_address1', 'class' => 'span6', 'placeholder' => 'Phone')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('client_email', 'E-mail', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('attrs[email]', null, array('id' => 'client_email', 'class' => 'span8', 'placeholder' => 'E-mail')) }}
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            {{ Form::label('client_website', 'Website URL', array('class' => 'control-label')) }}
                                                            <div class="controls">
                                                                {{ Form::text('attrs[website]', null, array('id' => 'client_website', 'class' => 'input-xlarge', 'placeholder' => 'Website URL')) }}
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="client-notes">
                                            <h2>Notes</h2>

                                            <div class="row-fluid">
                                                <div class="span4">
                                                    <p>Notes for this client are displayed here in chronological order.  You amy add notes using the note entry area below the current notes (if any).</p>
                                                </div>

                                                <div class="span8">
                                                    @if (!$crm->notes->isEmpty())
                                                        @foreach ($crm->notes as $note)
                                                        <div class="row-fluid">
                                                            <div class="span12 data-block">
                                                                <div class="data-container">
                                                                    <header>
                                                                        <h2>Posted by {{ $note->poster->name }} on {{ date(Config::get('settings.datetime.formats.crm_notes'), strtotime($note->created_at)) }}</h2>
                                                                    </header>
                                                                    <section>
                                                                        {{ $note->notes }}
                                                                    </section>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    @else
                                                    <div class="alert alert-info">
                                                        <strong>No notes!</strong><br />
                                                        There are no notes on file right now for this client.
                                                    </div>
                                                    @endif

                                                    <hr />

                                                    <h3>Add Notes</h3>

                                                    <fieldset class="well">
                                                        <div class="control-group">
                                                            <div class="controls">
                                                                {{ Form::textarea('notes_add', null, array('id' => 'client_notes', 'class' => 'wysihtml5')) }}
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <!--
                                        <div class="tab-pane" id="client-auth">
                                            <h2>Publishing Authorizations</h2>

                                            @if (!$crm->authorizations->isEmpty())
                                                <div class="row-fluid">
                                                    <div class="span4">
                                                        <p>This table shows all of the publishing authorization requests that have been sent to this client by e-mail.</p>
                                                        @if (UserACL::can('special.crm.send.publishing_auth'))
                                                        <a href="{{ route('crm_request_authorization') }}" class="btn btn-success">Send a New Authorization Request</a>
                                                        @endif
                                                    </div>
                                                    <div class="span8">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Media</th>
                                                                    <th>Issue Date</th>
                                                                    <th>Zones</th>
                                                                    <th>Authorized</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($crm->authorizations as $authorization)
                                                                <tr>
                                                                    <td>{{ $authorization->mediatype->name }}</td>
                                                                    <td>{{ $authorization->auth_date->format('F Y') }}</td>
                                                                    <td>
                                                                        @foreach ($authorization->zones as $zone)
                                                                            @if ($zone->zone->code)
                                                                            <span class="label label-inverse">[{{ $zone->zone->code }}] {{ $zone->zone->area }}</span>
                                                                            @else
                                                                            <span class="label label-inverse">{{ $zone->zone->area }}</span>
                                                                            @endif
                                                                        @endforeach
                                                                    </td>
                                                                    <td>
                                                                        @if ($authorization->authorized)
                                                                        <span class="label label-success">Yes</span> by <strong>{{ $authorization->signature }}</strong></span>
                                                                        @else
                                                                        <span class="label">No</span>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="alert alert-info">
                                                    <strong>No authorizations found.</strong>
                                                    <p>
                                                        No authorizations were found for this customer.
                                                        @if (UserACL::can('special.crm.send.publishing_auth'))
                                                        <a href="{{ route('crm_request_authorization') }}">You can send an authorization by clicking here.</a>
                                                        @endif
                                                    </p>
                                                </div>
                                            @endif
                                        </div>
                                        -->
                                        <div class="tab-pane" id="client-calendar">
                                            <h2>Calendar/Reminders</h2>

                                            <div class="row-fluid">
                                                <p>The calendar shown below displays reminders only for the currently selected client. You may add, edit or delete reminders by clicking or dragging anywhere on the calendar.</p>

                                                <hr />

                                                <div class="full-calendar full-calendar-client"></div>

                                                <br /><br />
                                            </div>
                                        </div>
                                    </section>

                                    {{ Form::button('Update Client', array('class' => 'btn btn-large btn-primary pull-right', 'type' => 'submit')) }}

                                </div>
                            </article>
                        </div>

                    {{ Form::close() }}

                </div>

@stop
@section('footer')
    @parent

    {{ HTML::script('js/vendor/plugins/fileupload/bootstrap-fileupload.js') }}
    {{ HTML::script('js/vendor/plugins/wysihtml5/wysihtml5-0.3.0.js') }}
    {{ HTML::script('js/vendor/plugins/wysihtml5/bootstrap-wysihtml5.js') }}
    {{ HTML::script('js/vendor/plugins/tipped/tipped-v3.js') }}
    {{ HTML::script('js/vendor/plugins/imagesloaded/imagesloaded.pkgd.min.js') }}
    {{ HTML::script('js/vendor/plugins/moment/moment.min.js') }}
    {{ HTML::script('js/vendor/plugins/fullCalendar/jquery.fullcalendar.min.js') }}
    {{ HTML::script('js/vendor/plugins/handlebars/handlebars-v1.3.0.js') }}
    {{ HTML::script('js/framework.reminders.js') }}
    {{ HTML::script('js/framework.crm.update.init.js') }}
@stop