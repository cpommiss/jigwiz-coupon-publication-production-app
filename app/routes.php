<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Login
Route::get('login', array('as' => 'login', 'uses' => 'UserController@getLogin'));
Route::post('login', array('as' => 'login', 'uses' => 'UserController@postLogin'));

// Logout
Route::get('logout', array('as' => 'logout', 'uses' => 'UserController@getLogout'));

Route::group(['before' => 'guest'], function() {
    // Default
    Route::get('/*', function() {
        return Redirect::to('login_landing');
    });

    // Job Details
    Route::get('job/view/{key}', array('as' => 'client_view_graphics_queue_job_details', 'uses' => 'GraphicsQueueDetailController@getPublic_JobDetails'));
    Route::post('job/view/add/note/{id}', array('as' => 'client_view_graphics_queue_job_add_note', 'uses' => 'GraphicsQueueDetailController@postJobNote'));

    // CRM Authorization
    Route::get('crm/auth/{key}', array('as' => 'client_view_crm_request_authorization', 'uses' => 'CRMAuthorizationController@getPublic_ClientAuthorization'));
    Route::post('crm/auth/submit', array('as' => 'client_view_crm_create_authorization', 'uses' => 'CRMAuthorizationController@postPublic_ClientAuthorization'));

    // Helpers
    Route::post('helpers/annotation/add', array('as' => 'client_view_helper_annotation_create', 'uses' => 'HelpersGraphicsJobController@postAnnotationCreate'));
    Route::post('helpers/annotation/update', array('as' => 'client_view_helper_annotation_update', 'uses' => 'HelpersGraphicsJobController@postAnnotationUpdate'));
    Route::post('helpers/annotation/delete', array('as' => 'client_view_helper_annotation_delete', 'uses' => 'HelpersGraphicsJobController@postAnnotationDelete'));
});
Route::group(['before' => 'auth'], function() {
    // Dashboard
    Route::any('/', array('as' => 'dashboard', 'uses' => 'DashboardController@getDashboardLanding'));
    Route::post('/event-test', array('as' => 'test-event', 'uses' => 'DashboardController@postTestEvent'));

    // User profile
    Route::get('profile', array('as' => 'profile', 'uses' => 'UserController@getProfile'));
    Route::post('profile', array('as' => 'profile', 'uses' => 'UserController@postProfile'));

    // Users
    Route::get('user', array('as' => 'user', 'uses' => 'UserManagementController@getUserLanding'));
    Route::get('user/create', array('as' => 'user_create', 'uses' => 'UserManagementController@getUserCreate'));
    Route::post('user/create', array('as' => 'user_create', 'uses' => 'UserManagementController@postUserCreate'));
    Route::get('user/update/{id}', array('as' => 'user_update', 'uses' => 'UserManagementController@getUserUpdate'));
    Route::post('user/update/{id}', array('as' => 'user_update', 'uses' => 'UserManagementController@postUserUpdate'));
    Route::get('user/delete/{id}', array('as' => 'user_delete', 'uses' => 'UserManagementController@getUserDelete'));

    // CRM
    Route::get('crm', array('as' => 'crm', 'uses' => 'CRMController@getLanding'));
    Route::post('crm', array('as' => 'crm', 'uses' => 'CRMController@getLanding'));
    Route::get('crm/personal', array('as' => 'crm_personal', 'uses' => 'CRMController@getLanding'));
    Route::post('crm/personal', array('as' => 'crm_personal', 'uses' => 'CRMController@getLanding'));
    Route::get('crm/create', array('as' => 'crm_create_client', 'uses' => 'CRMController@getClientCreate'));
    Route::get('crm/personal/create', array('as' => 'crm_personal_create_client', 'uses' => 'CRMController@getClientCreate'));
    Route::post('crm/create', array('as' => 'crm_create_client', 'uses' => 'CRMController@postClientCreate'));
    Route::post('crm/personal/create', array('as' => 'crm_personal_create_client', 'uses' => 'CRMController@postClientCreate'));
    Route::get('crm/update/{id}', array('as' => 'crm_update_client', 'uses' => 'CRMController@getClientUpdate'));
    Route::get('crm/personal/update/{id}', array('as' => 'crm_personal_update_client', 'uses' => 'CRMController@getClientUpdate'));
    Route::post('crm/update/{id}', array('as' => 'crm_update_client', 'uses' => 'CRMController@postClientUpdate'));
    Route::post('crm/personal/update/{id}', array('as' => 'crm_personal_update_client', 'uses' => 'CRMController@postClientUpdate'));
    Route::get('crm/delete/{id}', array('as' => 'crm_delete_client', 'uses' => 'CRMController@getClientDelete'));
    Route::get('crm/personal/delete/{id}', array('as' => 'crm_personal_delete_client', 'uses' => 'CRMController@getClientDelete'));
    Route::get('crm/auth', array('as' => 'crm_request_authorization', 'uses' => 'CRMAuthorizationController@getClientAuthorization'));

    // System Messaging
    Route::get('system-messaging', array('as' => 'system_messaging', 'uses' => 'SystemMessagingController@getMessageCreate'));
    Route::post('system-messaging', array('as' => 'system_messaging', 'uses' => 'SystemMessagingController@postMessageCreate'));
    Route::get('system-messaging/delete/{id}', array('as' => 'system_messaging_delete', 'uses' => 'SystemMessagingController@getMessageDelete'));

    // Media Management
    Route::get('media-management', array('as' => 'media_management', 'uses' => 'MediaManagementController@getLanding'));
    Route::get('media-management/create', array('as' => 'media_management_create_media', 'uses' => 'MediaManagementController@getMediaCreate'));
    Route::post('media-management/create', array('as' => 'media_management_create_media', 'uses' => 'MediaManagementController@postMediaCreate'));
    Route::get('media-management/update/{id}', array('as' => 'media_management_update_media', 'uses' => 'MediaManagementController@getMediaUpdate'));
    Route::post('media-management/update/{id}', array('as' => 'media_management_update_media', 'uses' => 'MediaManagementController@postMediaUpdate'));
    Route::get('media-management/delete/{id}', array('as' => 'media_management_delete_media', 'uses' => 'MediaManagementController@getMediaDelete'));

    // Zones
    Route::get('zone', array('as' => 'zone', 'uses' => 'ZoneController@getLanding'));
    Route::get('zone/create', array('as' => 'zone_create', 'uses' => 'ZoneController@getZoneCreate'));
    Route::post('zone/create', array('as' => 'zone_create', 'uses' => 'ZoneController@postZoneCreate'));
    Route::get('zone/update/{id}', array('as' => 'zone_update', 'uses' => 'ZoneController@getZoneUpdate'));
    Route::post('zone/update/{id}', array('as' => 'zone_update', 'uses' => 'ZoneController@postZoneUpdate'));
    Route::get('zone/delete/{id}', array('as' => 'zone_delete', 'uses' => 'ZoneController@getZoneDelete'));

    // Graphics Queue
    Route::get('graphics-queue', array('as' => 'graphics_queue', 'uses' => 'GraphicsQueueController@getLanding'));
    Route::get('graphics-queue/create', array('as' => 'graphics_queue_create_job', 'uses' => 'GraphicsQueueController@getJobCreate'));
    Route::post('graphics-queue/create', array('as' => 'graphics_queue_create_job', 'uses' => 'GraphicsQueueController@postJobCreate'));
    Route::post('graphics-queue/update/settings/{id}', array('as' => 'graphics_queue_job_update_settings', 'uses' => 'GraphicsQueueController@postJobUpdateSettings'));
    Route::get('graphics-queue/delete/{id}', array('as' => 'graphics_queue_delete_job', 'uses' => 'GraphicsQueueController@getJobDelete'));
    Route::get('graphics-queue/renew/{id}', array('as' => 'graphics_queue_job_renew', 'uses' => 'GraphicsQueueRenewController@getJobRenew'));
    Route::post('graphics-queue/renew/{id}', array('as' => 'graphics_queue_job_renew', 'uses' => 'GraphicsQueueRenewController@postJobRenew'));
    Route::get('graphics-queue/details/{id}', array('as' => 'graphics_queue_job_details', 'uses' => 'GraphicsQueueDetailController@getJobDetails'));
    Route::post('graphics-queue/add/note/{id}', array('as' => 'graphics_queue_job_add_note', 'uses' => 'GraphicsQueueDetailController@postJobNote'));
    Route::get('graphics-queue/push/note/{id}', array('as' => 'graphics_queue_job_push_note', 'uses' => 'HelpersGraphicsJobController@getJobPushNoteToClient'));

    // Documents
    Route::get('document', array('as' => 'document', 'uses' => 'DocumentController@getLanding'));
    Route::post('document', array('as' => 'document_upload', 'uses' => 'DocumentController@postDocumentUpload'));
    Route::get('document/delete/{id}', array('as' => 'document_delete', 'uses' => 'DocumentController@getDocumentDelete'));

    // Points
    Route::get('point', array('as' => 'point', 'uses' => 'PointController@getLanding'));
    Route::post('point', array('as' => 'point', 'uses' => 'PointController@getLanding'));

    // Pagination
    Route::get('pagination', array('as' => 'pagination', 'uses' => 'PaginationController@getLanding'));
    Route::get('pagination/update/{id}', array('as' => 'pagination_update', 'uses' => 'PaginationController@getPaginationUpdate'));
    Route::get('pagination/batch/{id}', array('as' => 'pagination_batch', 'uses' => 'PaginationBatchController@getPaginationBatch'));
    Route::post('pagination/batch/{id}', array('as' => 'pagination_batch', 'uses' => 'PaginationBatchController@postPaginationBatch'));
    Route::any('pagination/reservations', array('as' => 'pagination_reservations', 'uses' => 'PaginationReservationController@handlePaginationReservations'));
    Route::get('pagination/reservations/delete/{id}', array('as' => 'pagination_reservations_delete', 'uses' => 'PaginationReservationController@getPaginationReservationDelete'));
    Route::any('pagination/report/usage', array('as' => 'pagination_report_usage', 'uses' => 'PaginationReportController@handlePaginationReportUsage'));

    // Styling
    Route::get('styling', array('as' => 'styling', 'uses' => 'StylingController@getStyling'));
    Route::post('styling', array('as' => 'styling', 'uses' => 'StylingController@postStyling'));

    // Helpers
    Route::get('helpers/reminders/all', array('as' => 'helper_reminder_get', 'uses' => 'HelpersCRMController@getCRMReminders'));
    Route::get('helpers/reminders/upcoming', array('as' => 'helper_reminder_get', 'uses' => 'HelpersCRMController@getCRMUpcomingReminders'));
    Route::get('helpers/reminders/client/{id}', array('as' => 'helper_reminder_get', 'uses' => 'HelpersCRMController@getCRMRemindersClient'));
    Route::post('helpers/reminders/manage', array('as' => 'helper_reminder_create', 'uses' => 'HelpersCRMController@getCRMReminderModal'));
    Route::post('helpers/reminders/create', array('as' => 'helper_reminder_create', 'uses' => 'HelpersCRMController@postCRMReminder'));
    Route::post('helpers/reminders/update', array('as' => 'helper_reminder_update', 'uses' => 'HelpersCRMController@postCRMReminderUpdate'));
    Route::post('helpers/uploads/multi', array('as' => 'helper_upload_multifile', 'uses' => 'HelpersGraphicsJobController@postMultiFileUploads'));
    Route::post('helpers/notifications/dismiss/all', array('as' => 'helper_reminder_dismiss', 'uses' => 'HelpersNotificationController@postDismissAll'));
    Route::post('helpers/annotation/add', array('as' => 'helper_annotation_create', 'uses' => 'HelpersGraphicsJobController@postAnnotationCreate'));
    Route::post('helpers/annotation/update', array('as' => 'helper_annotation_update', 'uses' => 'HelpersGraphicsJobController@postAnnotationUpdate'));
    Route::post('helpers/annotation/delete', array('as' => 'helper_annotation_delete', 'uses' => 'HelpersGraphicsJobController@postAnnotationDelete'));
    Route::post('helpers/pagination/get', array('as' => 'helper_pagination_data_get', 'uses' => 'HelpersPaginationController@getPaginationData'));
    Route::get('helpers/pagination/get/indexes', array('as' => 'helper_pagination_data_get_indexes', function() { return Response::json(array('issues' => HelpersPaginationController::getPaginationIssueIndexes())); }));
    Route::post('helpers/pagination/put', array('as' => 'helper_pagination_data_put', 'uses' => 'HelpersPaginationController@putPaginationData'));
    Route::get('helpers/pagination/get/jobs/{id}', array('as' => 'helper_pagination_data_get', 'uses' => 'HelpersPaginationController@getCompletedJobs'));
    Route::post('helpers/pagination/set/premium/{id}', array('as' => 'helper_pagination_data_set_premium', 'uses' => 'HelpersPaginationController@postPaginationPremiumStatus'));
    Route::post('helpers/pagination/batch', array('as' => 'helper_pagination_batch', 'uses' => 'HelpersPaginationController@postPaginationBatch'));
    Route::get('helpers/document/push/{id}', array('as' => 'helper_document_push', 'uses' => 'HelpersDocumentController@getDocumentPush'));
    Route::post('helpers/document/push/send', array('as' => 'helper_document_push_send', 'uses' => 'HelpersDocumentController@postDocumentPush'));
    Route::post('helpers/crm/auth/request', array('as' => 'helper_crm_authorization_request', 'uses' => 'HelpersCRMAuthorizationController@pushCRMAuthorization'));

    // Logout
    Route::get('logout', array('as' => 'logout', 'uses' => 'UserController@getLogout'));
});