<?php

class CRMAttribute extends Eloquent {
    protected $table    = 'crm_attributes';

    public function crm() {
        return $this->belongsTo('CRM', 'crm_id', 'id');
    }
}