<?php

class Notification extends Eloquent {
    protected $table = 'notifications';
    protected $guarded = array('id');
    protected $with = array('assignee');

    public function assignee() {
        return $this->belongsTo('User', 'assigned_to', 'id');
    }

    public function getDates() {
        return array('when', 'created_at', 'updated_at');
    }

    public static function dispatch($type, $assigned_to, $related_id, $related_url, $when, $message, $message_data, $force_immediate = false) {
        $notification_text  =   Lang::get($message, $message_data);

        $notification       =   Notification::create(   array(
                                                            'type'          =>  Config::get($type)['ordinal'],
                                                            'assigned_to'   =>  $assigned_to,
                                                            'related_id'    =>  $related_id,
                                                            'when'          =>  $when,
                                                            'message'       =>  $notification_text
                                                        ));

        if ($notification->id) {
            if ((Config::get($type . '.immediate')) || ($force_immediate)) {
                // dispatch redis notification
                $template           = View::make(   'chrome.notifications.fragments.template.notification',
                                                    array(
                                                        'id'        =>  $notification->id,
                                                        'icon'      =>  Config::get($type)['icon'],
                                                        'link'      =>  $related_url,
                                                        'message'   =>  $notification_text,
                                                        'time'      =>  $when
                                                    ));

                // redis notification
                Event::fire(    Config::get('settings.notifications.event'),
                                json_encode(
                                    array(
                                        'type'      =>  Config::get($type)['ordinal'],
                                        'icon'      =>  Config::get($type)['icon'],
                                        'title'     =>  Config::get($type)['title'],
                                        'for'       =>  $assigned_to,
                                        'message'   =>  $notification_text,
                                        'template'  =>  htmlentities($template)
                                    )
                                ));
            }
        }
    }
}