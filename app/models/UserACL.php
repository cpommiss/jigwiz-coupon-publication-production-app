<?php

class UserACL extends Eloquent {
    protected $table    = 'users_acl';

    public function user() {
        return $this->belongsTo('User', 'user_id', 'id');
    }

    public static function can($requests) {
        if (!empty(Auth::user()->acl)) {
            if (!is_array($requests)) {
                $requests       = array($requests);
            }

            // convert phrasing to actual ACL numeric identifiers, weeding out any non-existent ones
            $requests_parsed    = array();

            foreach ($requests as $request) {
                if (Config::get('acl.' . $request)) {
                    array_push($requests_parsed, intval(Config::get('acl.' . $request)));
                }
            }

            $requests           = $requests_parsed;
            $requests_parsed    = null;

            if (!empty($requests)) {
                foreach ($requests as $request) {
                    foreach (Auth::user()->acl as $privilege) {
                        if ($request == $privilege->action) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static function is($flag) {
        if (Auth::user()->{$flag}) {
            return true;
        } else {
            return false;
        }
    }
}