<?php

class CRMAuthorization extends Eloquent {
    protected $table    = 'crm_authorizations';

    public function customer() {
        return $this->belongsTo('CRM', 'crm_id', 'id');
    }

    public function mediatype() {
        return $this->belongsTo('MediaType', 'media', 'id');
    }

    public function zones() {
        return $this->hasMany('CRMAuthorizationZone', 'auth_id', 'id');
    }

    public function getDates() {
        return array('created_at', 'updated_at', 'auth_date');
    }

    public static function generateAuthorizationKe0y() {
        return md5(uniqid());
    }
}