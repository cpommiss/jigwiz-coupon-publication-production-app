<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {
	protected $table    = 'users';
	protected $hidden   = array('password');
    protected $with     = array('acl');

    public function acl() {
        return $this->hasMany('UserACL', 'user_id', 'id');
    }

	public function getAuthIdentifier() {
		return $this->getKey();
	}

	public function getAuthPassword() {
		return $this->password;
	}

	public function getReminderEmail() {
		return $this->email;
	}

    public function getRememberToken() {
        return $this->remember_token;
    }

    public function setRememberToken($value) {
        $this->remember_token = $value;
    }

    public function getRememberTokenName() {
        return 'remember_token';
    }

    public static function accessible($id) {
        $record         =   User::where('id', '=', $id)
                                ->where('installation', '=', Auth::user()->installation)
                                ->get();

        if (!$record->isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
}