<?php

class PaginationPagePlacement extends Eloquent {
    protected $table = 'pagination_pages_placement';
    protected $with = array('job');

    public function page() {
        return $this->belongsTo('PaginationPage', 'pagination_page_id', 'id');
    }
    public function job() {
        return $this->hasOne('GraphicsJob', 'id', 'job_id');
    }
}