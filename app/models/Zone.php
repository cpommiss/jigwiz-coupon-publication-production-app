<?php

class Zone extends Eloquent {
    protected $table = 'zones';

    public function statedata() {
        return $this->hasOne('ZoneState', 'id', 'state');
    }

    public static function zonelist_with_codes($zones) {
        $output     = array();

        foreach ($zones as $zone) {
            if (!isset($output[$zone->statedata->name])) {
                $output[$zone->statedata->name]     = array();
            }

            $output[$zone->statedata->name][$zone->id]      = (strlen($zone->code)) ? ('[' . $zone->code . '] ' . $zone->area) : $zone->area;
        }

        return $output;
    }

    public static function accessible($id, $privileges = false) {
        if (is_array($id)) {
            $record         =   Zone::whereIn('id', $id)
                                    ->where('installation', '=', Auth::user()->installation)
                                    ->get();
        } else {
            $record         =   Zone::where('id', '=', $id)
                                    ->where('installation', '=', Auth::user()->installation)
                                    ->get();
        }

        if (!$record->isEmpty()) {
            if ($privileges) {
                return UserACL::can($privileges);
            } else {
                return true;
            }
        }

        return false;
    }
}