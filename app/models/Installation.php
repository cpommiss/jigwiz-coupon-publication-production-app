<?php

class Installation extends Eloquent {
    protected $table = 'installations';

    public function attrs() {
        return $this->hasOne('InstallationAttribute', 'installation_id', 'id');
    }
}