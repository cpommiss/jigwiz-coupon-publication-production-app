<?php

class MediaType extends Eloquent {
    protected $table = 'media_types';

    public static function accessible($id, $privileges = false) {
        $record         =   MediaType::where('id', '=', $id)
                                ->where('installation', '=', Auth::user()->installation)
                                ->get();

        if (!$record->isEmpty()) {
            if ($privileges) {
                return UserACL::can($privileges);
            } else {
                return true;
            }
        }

        return false;
    }
}