<?php

class CRMNote extends Eloquent {
    protected $table    = 'crm_notes';
    protected $with     = array('poster');

    public function poster() {
        return $this->hasOne('User', 'id', 'created_by');
    }
}