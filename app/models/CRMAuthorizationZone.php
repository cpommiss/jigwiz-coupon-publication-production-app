<?php

class CRMAuthorizationZone extends Eloquent {
    protected $table    = 'crm_authorizations_zones';

    public function auth() {
        return $this->belongsTo('CRMAuthorization', 'auth_id', 'id');
    }

    public function zone() {
        return $this->hasOne('Zone', 'id', 'zone_id');
    }
}