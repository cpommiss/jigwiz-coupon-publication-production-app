<?php

class Pagination extends Eloquent {
    protected $table = 'pagination';
    protected $with = array('pages');

    public function pages() {
        return $this->hasMany('PaginationPage', 'pagination_id', 'id');
    }

    public static function accessible($id, $privileges = false) {
        $record         =   Pagination::where('id', '=', $id)
                                ->with( array(  'media'     =>  function($query) {
                                                                    $query->where('installation', '=', Auth::user()->installation);
                                                                },
                                                'zone'      =>  function($query) {
                                                                    $query->where('installation', '=', Auth::user()->installation);
                                                                }
                                        ))
                                ->get();

        if (!$record->isEmpty()) {
            if ($privileges) {
                return UserACL::can($privileges);
            } else {
                return true;
            }
        }

        return false;
    }
}