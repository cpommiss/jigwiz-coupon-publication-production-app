<?php

class GraphicsJobNote extends Eloquent {
    protected $table    = 'graphics_jobs_notes';
    protected $with     = array('poster', 'attachment');

    public function job() {
        return $this->hasOne('GraphicsJob', 'id', 'job_id');
    }

    public function poster() {
        return $this->hasOne('User', 'id', 'created_by');
    }

    public function attachment() {
        return $this->hasOne('GraphicsJobFile', 'note_id', 'id');
    }

    public static function accessible($id, $privileges = false) {
        $note           =   GraphicsJobNote::where('id', '=', $id)
                                ->get();

        if (!$note->isEmpty()) {
            $note           = $note->first();

            return GraphicsJob::accessible($note->job_id, $privileges);
        } else {
            return false;
        }
    }
}