<?php

class CRMReminder extends Eloquent {
    protected $table    = 'crm_reminders';
    protected $with     = array('client');

    public function client() {
        return $this->hasOne('CRM', 'id', 'crm_id');
    }

    public function creator() {
        return $this->hasOne('User', 'id', 'created_by');
    }
}