<?php

class PaginationReservation extends Eloquent {
    protected $table = 'pagination_reservations';

    public function client() {
        return $this->hasOne('CRM', 'id', 'crm_id');
    }

    public function rep() {
        return $this->hasOne('User', 'id', 'reserved_by');
    }

    public static function accessible($id, $privileges = false) {
        $reservation    =   PaginationReservation::where('id', '=', $id)
                                ->get();

        if (!$reservation->isEmpty()) {
            if ($reservation->first()->reserved_by == Auth::user()->id) {
                if ($privileges) {
                    return UserACL::can($privileges);
                } else {
                    return true;
                }
            } else {
                return CRM::accessible($reservation->crm_id, $privileges);
            }
        } else {
            return false;
        }
    }
}