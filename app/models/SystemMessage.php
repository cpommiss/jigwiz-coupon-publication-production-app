<?php

class SystemMessage extends Eloquent {
    protected $table = 'system_messages';

    public function poster() {
        return $this->hasOne('User', 'id', 'created_by');
    }

    public static function accessible($id, $privileges = false) {
        $record         =   SystemMessage::where('id', '=', $id)
                                ->where('installation', '=', Auth::user()->installation)
                                ->get();

        if (!$record->isEmpty()) {
            if ($privileges) {
                return UserACL::can($privileges);
            } else {
                return true;
            }
        }

        return false;
    }
}