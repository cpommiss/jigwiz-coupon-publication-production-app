<?php

class GraphicsJobZone extends Eloquent {
    protected $table    = 'graphics_jobs_zones';
    protected $with     = array('zone');

    public function zone() {
        return $this->hasOne('Zone', 'id', 'zone_id');
    }

    public function job() {
        return $this->belongsTo('GraphicsJob', 'job_id', 'id');
    }
}