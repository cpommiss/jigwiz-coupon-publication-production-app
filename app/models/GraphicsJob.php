<?php

class GraphicsJob extends Eloquent {
    protected $table = 'graphics_jobs';
    protected $with = array('files');

    public function customer() {
        return $this->hasOne('CRM', 'id', 'crm_id');
    }

    public function notes() {
        return $this->hasMany('GraphicsJobNote', 'job_id', 'id');
    }

    public function files() {
        return $this->hasMany('GraphicsJobFile', 'job_id', 'id');
    }

    public function zones() {
        return $this->hasMany('GraphicsJobZone', 'job_id', 'id');
    }

    public function creator() {
        return $this->hasOne('User', 'id', 'created_by');
    }

    public function assignee() {
        return $this->hasOne('User', 'id', 'assigned_to');
    }

    public function mediatype() {
        return $this->hasOne('MediaType', 'id', 'media');
    }

    public static function generateJobKey() {
        return md5(uniqid());
    }

    public static function accessible($id, $privileges = false) {
        if (Auth::check()) {
            if (UserACL::can('special.graphics_queue.read.assigned_only')) {
                $record         =   GraphicsJob::where('id', '=', $id)
                                        ->where('assigned_to', '=', Auth::user()->id)
                                        ->with( array(  'customer'  =>  function($query) {
                                                                            $query->where('installation', '=', Auth::user()->installation);
                                                                        }
                                                        ))
                                        ->get();
            } else {
                $record         =   GraphicsJob::where('id', '=', $id)
                                        ->with( array(  'customer'  =>  function($query) {
                                                                            $query->where('installation', '=', Auth::user()->installation);
                                                                        }
                                                        ))
                                        ->get();
            }
        } elseif ((!Auth::check()) && (Session::has('visiting.job_key'))) {
            $record         =   GraphicsJob::where('id', '=', $id)
                                    ->where('key', '=', Session::get('visiting.job_key'))
                                    ->get();
        } else {
            return false;
        }

        if (!$record->isEmpty()) {
            if ($privileges) {
                return UserACL::can($privileges);
            } else {
                return true;
            }
        }

        return false;
    }
}