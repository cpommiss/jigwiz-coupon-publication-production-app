<?php

class PaginationPage extends Eloquent {
    protected $table = 'pagination_pages';
    protected $with = array('placements');

    public function placements() {
        return $this->hasMany('PaginationPagePlacement', 'pagination_page_id', 'id');
    }

    public function pagination() {
        return $this->belongsTo('Pagination', 'pagination_id', 'id');
    }

    public static function accessible($id, $privileges = false) {
        $page           =   PaginationPage::where('id', '=', $id)
                                ->get();

        if (!$page->isEmpty()) {
            return Pagination::accessible($page->pagination_id, $privileges);
        } else {
            return false;
        }
    }
}