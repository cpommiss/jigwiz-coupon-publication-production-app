<?php

class Document extends Eloquent {
    protected $table = 'documents';

    public function uploader() {
        return $this->hasOne('User', 'id', 'created_by');
    }

    public static function accessible($id, $privileges = false) {
        $record         =   Document::where('id', '=', $id)
                                ->where('installation', '=', Auth::user()->installation)
                                ->get();

        if (!$record->isEmpty()) {
            if ($privileges) {
                return UserACL::can($privileges);
            } else {
                return true;
            }
        }

        return false;
    }
}