<?php

class GraphicsJobFileAnnotation extends Eloquent {
    protected $table    = 'graphics_jobs_files_annotations';
    protected $with     = array('poster');

    public function poster() {
        return $this->hasOne('User', 'id', 'created_by');
    }

    public function file() {
        return $this->belongsTo('GraphicsJobFile', 'file_id', 'id');
    }
}