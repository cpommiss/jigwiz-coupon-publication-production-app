<?php

class CRM extends Eloquent {
    protected $table = 'crm';

    public function attrs() {
        return $this->hasOne('CRMAttribute', 'crm_id', 'id');
    }

    public function notes() {
        return $this->hasMany('CRMNote', 'crm_id', 'id');
    }

    public function rep() {
        return $this->hasOne('User', 'id', 'assigned_to');
    }

    public function authorizations() {
        return $this->hasMany('CRMAuthorization', 'crm_id', 'id');
    }

    public static function accessible($id, $privileges = false) {
        if (Str::contains(Route::currentRouteName(), 'personal') === true) {
            $record         =   CRM::where('id', '=', $id)
                                    ->where('installation', '=', Auth::user()->installation)
                                    ->where('assigned_to', '=', Auth::user()->id)
                                    ->get();
        } else {
            $record         =   CRM::where('id', '=', $id)
                                    ->where('installation', '=', Auth::user()->installation)
                                    ->get();
        }

        if (!$record->isEmpty()) {
            if ($privileges) {
                return UserACL::can($privileges);
            } else {
                return true;
            }
        }

        return false;
    }

    public static function accessibleType() {
        return (Str::contains(Route::currentRouteName(), 'personal') === true) ? UserACL::can('special.crm.use.personal') : UserACL::can('special.crm.use.master');
    }
}