<?php

class GraphicsJobFile extends Eloquent {
    protected $table    = 'graphics_jobs_files';
    protected $with     = array('poster');

    public function poster() {
        return $this->hasOne('User', 'id', 'created_by');
    }

    public function annotations() {
        return $this->hasMany('GraphicsJobFileAnnotation', 'file_id', 'id');
    }

    public function job() {
        return $this->belongsTo('GraphicsJob', 'job_id', 'id');
    }

    public static function accessible($id, $privileges = false) {
        $file           =   GraphicsJobFile::where('id', '=', $id)
                                ->get();

        if (!$file->isEmpty()) {
            $file           = $file->first();

            return GraphicsJob::accessible($file->job_id, $privileges);
        } else {
            return false;
        }
    }
}