<?php

class NotificationHandler {
    public function handle($data) {
        $connection     = Redis::connection();
        $connection->publish(Config::get('settings.notifications.channel'), $data);
    }
}