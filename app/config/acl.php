<?php

return array(
    'create'    => array(
        'system_message'                => 200,
        'crm'                           => 300,
        'media_type'                    => 400,
        'graphics_queue'                => 500,
        'pagination'                    => 600,
        'zone'                          => 700,
        'document'                      => 900,
        'reservation'                   => 1000,
    ),
    'read'      => array(
        'system_message'                => 210,
        'crm'                           => 310,
        'media_type'                    => 410,
        'graphics_queue'                => 510,
        'pagination'                    => 610,
        'zone'                          => 710,
        'point'                         => 810,
        'document'                      => 910,
        'reservation'                   => 1010
    ),
    'update'    => array(
        'system_message'                => 220,
        'crm'                           => 320,
        'media_type'                    => 420,
        'graphics_queue'                => 520,
        'pagination'                    => 620,
        'zone'                          => 720,
    ),
    'delete'    => array(
        'system_message'                => 290,
        'crm'                           => 390,
        'media_type'                    => 490,
        'graphics_queue'                => 590,
        'pagination'                    => 690,
        'zone'                          => 790,
        'document'                      => 990,
        'reservation'                   => 1090
    ),
    'special'   => array(
        'crm'           => array(
              'use'           => array(
                'personal'                  => 10000,
                'master'                    => 10001,
                'reminders'                 => 10002
            ),
            'send'          => array(
                'publishing_auth'           => 10003
            )
        ),
        'graphics_queue'    => array(
            'finalize'          => array(
                'job'                       => 10100,
                'own_job'                   => 10101
            ),
            'assign'            => array(
                'artist'                    => 10200,
                'points'                    => 10201
            ),
            'read'              => array(
                'unassigned_jobs'           => 10300,
                'assigned_only'             => 10301
            ),
            'renew'             => array(
                'job'                       => 10400
            ),
            'set'               => array(
                'note_visibility'           => 10500
            ),
            'push'              => array(
                'note_to_client'            => 10600
            )
        ),
        'pagination'        => array(
            'assign'            => array(
                'issue_indexes'             => 11000,
                'premium_page_status'       => 11001
            ),
            'create'            => array(
                'batch_setup'               => 11100
            )
        ),
        'document'          => array(
            'email'             => array(
                'to_client'                 => 12000
            )
        ),
        'system'            => array(
            'act_as'            => array(
                'artist'                    => 20000,
                'artist_director'           => 20001
            ),
            'update'            => array(
                'branding'                  => 30000
            )
        )
    )
);