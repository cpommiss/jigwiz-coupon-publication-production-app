<?php

return array(
    'datetime' => array(
        'formats'   => array(
            'sql_date'              => 'Y-m-d H:i:s',
            'crm_notes'             => 'M jS, Y g:i a',
            'crm_reminders'         => 'F jS, Y g:i a',
            'graphics_queue'        => 'M j Y g:i a',
            'graphics_queue_notes'  => 'M jS, Y g:i a',
            'system_messages'       => 'M jS, Y g:i a'
        )
    ),
    'acl_structure' => array(
        'flags'         => array(
            'admin'                 => 'Administrator'
        ),
        'verbs'         => array(
            'create'                => 'Can create',
            'update'                => 'Can update',
            'delete'                => 'Can delete',
            'read'                  => 'Can access',
            'act_as'                => 'Appears in the system as',
            'use'                   => 'Can use',
            'finalize'              => 'Can finalize',
            'assign'                => 'Can assign',
            'renew'                 => 'Can renew',
            'email'                 => 'Can e-mail',
            'send'                  => 'Can send',
            'set'                   => 'Can set',
            'push'                  => 'Can push'
        ),
        'nouns'         => array(
            'system_message'        => array(
                'title'                 => 'System Messaging',
                'description'           => 'system messages',
                'extended_lookup'       => 'settings.acl_structure.nouns.special.permissions'
            ),
            'crm'                   => array(
                'title'                 => 'CRM',
                'description'           => 'CRM records',
                'extended_lookup'       => 'settings.acl_structure.nouns.special.permissions'
            ),
            'media_type'            => array(
                'title'                 => 'Media Types & Publications',
                'description'           => 'media types and publications',
                'extended_lookup'       => 'settings.acl_structure.nouns.special.permissions'
            ),
            'zone'                  => array(
                'title'                 => 'Zones',
                'description'           => 'zones',
                'extended_lookup'       => 'settings.acl_structure.nouns.special.permissions'
            ),
            'graphics_queue'        => array(
                'title'                 => 'Graphics Jobs & Queue',
                'description'           => 'jobs in the graphics queue',
                'extended_lookup'       => 'settings.acl_structure.nouns.special.permissions'
            ),
            'pagination'            => array(
                'title'                 => 'Pagination Data',
                'description'           => 'paginations within a media type/publication',
                'extended_lookup'       => 'settings.acl_structure.nouns.special.permissions'
            ),
            'reservation'           => array(
                'title'                 => 'Pagination Reservations',
                'description'           => 'pagination reservations',
                'extended_lookup'       => 'settings.acl_structure.nouns.special.permissions'
            ),
            'point'                 => array(
                'title'                 => 'Points and Point Data Reporting',
                'description'           => 'point data reporting',
                'extended_lookup'       => 'settings.acl_structure.nouns.special.permissions'
            ),
            'document'              => array(
                'title'                 => 'Document Storage',
                'description'           => 'items in document storage',
                'extended_lookup'       => 'settings.acl_structure.nouns.special.permissions'
            ),
            'system'                => array(
                'title'                 => 'Special Account Settings',
                'extended_lookup'       => 'settings.acl_structure.nouns.special.permissions'
            ),
            'special'               => array(
                'is_special'            => true,
                'permissions'               => array(
                    'crm'                   => array(
                        'use'                   => array(
                            'personal'              => 'a Personal (user-specific) CRM',
                            'master'                => 'the Master (company-wide) CRM',
                            'reminders'             => 'the client reminder/calendaring system'
                        ),
                        'send'                  => array(
                            'publishing_auth'       => 'a request for publishing authorization to a customer'
                        )
                    ),
                    'graphics_queue'    => array(
                        'finalize'          => array(
                            'job'                   => 'any job in the graphics queue',
                            'own_job'               => 'only their own submitted jobs in the graphics queue'
                        ),
                        'assign'            => array(
                            'artist'                => 'an artist to a job in the graphics queue',
                            'points'                => 'points to a job in the graphics queue'
                        ),
                        'read'              => array(
                            'unassigned_jobs'       => 'and see unassigned jobs in the graphics queue',
                            'assigned_only'         => 'only their own assigned jobs'
                        ),
                        'renew'             => array(
                            'job'                   => 'a job in the graphics queue to be used again'
                        ),
                        'set'               => array(
                            'note_visibility'       => 'the visibility (internal/client-only) of a note on a job in the graphics queue'
                        ),
                        'push'              => array(
                            'note_to_client'        => 'a copy of a note on a job containing a proof image out to the client'
                        )
                    ),
                    'pagination'        => array(
                        'assign'            => array(
                            'issue_indexes'         => 'issue numbers/indexes to paginated publications',
                            'premium_page_status'   => 'premium page status to pages within a paginated publication'
                        ),
                        'create'            => array(
                            'batch_setup'           => 'many blank pages for a media type/publication in many zones at once'
                        )
                    ),
                    'document'          => array(
                        'email'             => array(
                            'to_client'             => 'a document to a client'
                        )
                    ),
                    'system'            => array(
                        'act_as'            => array(
                            'artist'                => 'an artist',
                            'artist_director'       => 'an art director'
                        ),
                        'update'            => array(
                            'branding'              => 'the brand (logo, colors, fonts, etc.) of the system'
                        )
                    )
                )
            )
        ),
        'presets'   => array(
            'administrator'         => array(
                'title'                 => 'Administrator',
                'user'                  => 'admin',
                'flags'                 => array(
                    'admin'                 => true
                ),
                'permissions'           => array(
                    'read.system_message',
                    'read.crm',
                    'read.media_type',
                    'read.graphics_queue',
                    'read.pagination',
                    'read.zone',
                    'read.point',
                    'read.document',
                    'read.reservation',
                    'create.system_message',
                    'create.crm',
                    'create.media_type',
                    'create.graphics_queue',
                    'create.pagination',
                    'create.zone',
                    'create.document',
                    'create.reservation',
                    'update.system_message',
                    'update.crm',
                    'update.media_type',
                    'update.graphics_queue',
                    'update.pagination',
                    'update.zone',
                    'delete.system_message',
                    'delete.crm',
                    'delete.media_type',
                    'delete.graphics_queue',
                    'delete.pagination',
                    'delete.zone',
                    'delete.document',
                    'delete.reservation',
                    'special.crm.use.master',
                    'special.crm.use.reminders',
                    'special.crm.send.publishing_auth',
                    'special.graphics_queue.finalize.job',
                    'special.graphics_queue.assign.artist',
                    'special.graphics_queue.assign.points',
                    'special.graphics_queue.read.unassigned_jobs',
                    'special.graphics_queue.renew.job',
                    'special.graphics_queue.set.note_visibility',
                    'special.graphics_queue.push.note_to_client',
                    'special.pagination.assign.issue_indexes',
                    'special.pagination.assign.premium_page_status',
                    'special.pagination.create.batch_setup',
                    'special.document.email.to_client',
                    'special.system.update.branding'
                )
            ),
            'art_director'          => array(
                'title'                 => 'Art Director',
                'user'                  => 'director',
                'permissions'           => array(
                    'read.system_message',
                    'read.crm',
                    'read.media_type',
                    'read.zone',
                    'read.graphics_queue',
                    'read.pagination',
                    'read.reservation',
                    'read.point',
                    'read.document',
                    'create.graphics_queue',
                    'create.pagination',
                    'update.graphics_queue',
                    'update.pagination',
                    'delete.graphics_queue',
                    'delete.pagination',
                    'special.crm.use.master',
                    'special.crm.send.publishing_auth',
                    'special.graphics_queue.assign.artist',
                    'special.graphics_queue.assign.points',
                    'special.graphics_queue.read.unassigned_jobs',
                    'special.graphics_queue.renew.job',
                    'special.graphics_queue.set.note_visibility',
                    'special.pagination.assign.issue_indexes',
                    'special.pagination.assign.premium_page_status',
                    'special.pagination.create.batch_setup',
                    'special.system.act_as.artist_director'
                )
            ),
            'artist'                => array(
                'title'                 => 'Artist',
                'user'                  => 'artist',
                'permissions'           => array(
                    'read.system_message',
                    'read.media_type',
                    'read.zone',
                    'read.graphics_queue',
                    'update.graphics_queue',
                    'special.graphics_queue.read.assigned_only',
                    'special.system.act_as.artist'
                )
            ),
            'sales'                 => array(
                'title'                 => 'Sales Representative',
                'user'                  => 'sales',
                'permissions'           => array(
                    'read.system_message',
                    'read.crm',
                    'read.media_type',
                    'read.zone',
                    'read.graphics_queue',
                    'read.pagination',
                    'read.document',
                    'read.reservation',
                    'create.crm',
                    'create.graphics_queue',
                    'create.reservation',
                    'create.document',
                    'update.crm',
                    'update.graphics_queue',
                    'delete.graphics_queue',
                    'delete.reservation',
                    'delete.document',
                    'special.crm.use.personal',
                    'special.crm.send.publishing_auth',
                    'special.crm.use.reminders',
                    'special.graphics_queue.finalize.own_job',
                    'special.graphics_queue.read.unassigned_jobs',
                    'special.graphics_queue.renew.job',
                    'special.graphics_queue.set.note_visibility',
                    'special.graphics_queue.push.note_to_client',
                    'special.document.email.to_client'
                )
            )
        )
    ),
    'media_types' => array(
        'color_types' => array(
            '1'                 => 'Full-color',
            '2'                 => 'Spot color',
            '3'                 => 'Black and White'
        )
    ),
    'styling' => array(
        'font_families' => array(
            'Arial, sans-serif'         => 'Arial',
            'Georgia, serif'            => 'Georgia',
            'Helvetica, sans-serif'     => 'Helvetica',
            '"PT Sans", sans-serif'     => 'PT Sans',
            '"Open Sans", sans-serif'   => 'Open Sans',
            'Verdana, sans-serif'       => 'Verdana',
            'Tahoma, sans-serif'        => 'Tahoma'
        ),
        'font_sizes'    => array(
            '9px'                       => '9',
            '10px'                      => '10',
            '11px'                      => '11',
            '12px'                      => '12',
            '13px'                      => '13',
            '14px'                      => '14',
            '15px'                      => '15',
            '16px'                      => '16',
            '17px'                      => '17',
            '18px'                      => '18',
            '19px'                      => '19',
            '20px'                      => '20',
            '22px'                      => '22',
            '24px'                      => '24',
        ),
    ),
    'user' => array(
        'files' => array(
            'allowed_image_extensions'  => array(
                'png',
                'jpg',
                'jpeg',
                'gif'
            )
        )
    ),
    'crm' => array(
        'search_criteria' => array(
            array(
                'label'         => 'Client Name',
                'ordinal'       => 1,
                'relationship'  => '',
                'column'        => 'name',
                'match'         => 'exact'
            ),
            array(
                'label'         => 'City',
                'ordinal'       => 2,
                'relationship'  => 'attrs',
                'column'        => 'city',
                'match'         => 'like'
            ),
            array(
                'label'         => 'State',
                'ordinal'       => 3,
                'relationship'  => 'attrs',
                'column'        => 'state',
                'match'         => 'like'
            )
        ),
        'types'         => array(
            array(
                'value'         => 'prospect',
                'label'         => 'Sales Prospect'
            ),
            array(
                'value'         => 'client',
                'label'         => 'Active Customer'
            )
        ),
        'files'         => array(
            'allowed_image_extensions'  => array(
                'png',
                'jpg',
                'jpeg',
                'gif'
            )
        ),
        'reminders'     => array(
            'durations'     => array(
                '-5'            => '5 minutes before',
                '-10'           => '10 minutes before',
                '-15'           => '15 minutes before',
                '-30'           => '30 minutes before',
                '-60'           => '1 hour before',
                '-120'          => '2 hours before'
            )
        )
    ),
    'document' => array(
        'files'         => array(
            'allowed_extensions'        => array(
                'png',
                'jpg',
                'jpeg',
                'gif',
                'pdf',
                'tiff',
                'zip'
            )
        )
    ),
    'graphics_queue' => array(
        'notes'         => array(
            'visibility_types' => array(
                array(
                    'ordinal'       => 1,
                    'label'         => 'Visible to internal company staff',
                    'button_label'  => 'Internal Notes',
                    'show_statuses' => true,
                    'require_acl'   => array('update.graphics_queue'),
                    'hide_if'       => false
                ),
                array(
                    'ordinal'       => 2,
                    'label'         => 'Visible to client',
                    'button_label'  => 'Client Notes',
                    'show_statuses' => false,
                    'require_acl'   => false,
                    'hide_if'       => array('special.system.act_as.artist', 'special.system.act_as.artist_director')
                )
            )
        ),
        'files'         => array(
            'allowed_extensions'        => array(
                'png',
                'jpg',
                'jpeg',
                'gif',
                'pdf',
                'tiff',
                'zip'
            ),
            'allowed_image_extensions'  => array(
                'png',
                'jpg',
                'jpeg',
                'gif'
            )
        ),
        'statuses'      => array(
            '0'             => 'Unassigned',
            '1'             => 'In Production',
            '2'             => 'Initial Proof',
            '3'             => 'In Revisions',
            '4'             => 'Completed'
        )
    ),
    'notifications' => array(
        'event' => 'dispatch',
        'channel' => 'notifications',
        'types' => array(
            'generic'               => array(
                'ordinal'               => 1,
                'title'                 => 'Notification',
                'icon'                  => 'comment',
                'immediate'             => true
            ),
            'crm-reminder'          => array(
                'ordinal'               => 100,
                'title'                 => 'Calendar Reminder',
                'icon'                  => 'book',
                'immediate'             => false
            ),
            'crm-authorization'     => array(
                'ordinal'               => 101,
                'title'                 => 'Client Publishing Authorization Notification',
                'icon'                  => 'check',
                'immediate'             => true
            ),
            'graphics-queue'        => array(
                'ordinal'               => 200,
                'title'                 => 'Graphics Queue Job Notification',
                'icon'                  => 'picture-o',
                'immediate'             => true
            )
        )
    ),
    'pagination' => array(
        'reservations' => array(
            'types'             => array(
                'reservation'           => 1,
                'placement'             => 2,
                'reserved-and-placed'   => 3
            )
        )
    )
);