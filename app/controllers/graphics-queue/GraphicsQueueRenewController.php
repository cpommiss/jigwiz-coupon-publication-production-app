<?php

class GraphicsQueueRenewController extends Controller {
    public function getJobRenew($id) {
        if (GraphicsJob::accessible($id, 'special.graphics_queue.renew.job')) {
            $job            =   GraphicsJob::where('id', '=', $id)
                                    ->with( array(  'customer'  =>  function($query) {
                                                                        $query->where('installation', '=', Auth::user()->installation);
                                                                    },
                                                    'zones'))
                                    ->get();

            if (!$job->isEmpty()) {
                $job            = $job->first();

                return View::make(  'graphics-queue/job-renew',
                                    array(
                                        'job'       =>  $job,
                                        'clients'   =>  CRM::where('installation', '=', Auth::user()->installation)
                                                            ->where('type', '=', 'client')
                                                            ->lists('name', 'id'),
                                        'media'     =>  MediaType::where('installation', '=', Auth::user()->installation)
                                                            ->lists('name', 'id'),
                                        'zones'     =>  Zone::zonelist_with_codes(
                                                            Zone::where('installation', '=', Auth::user()->installation)
                                                                ->get()
                                                        ),
                                        'users'     =>  User::where('installation', '=', Auth::user()->installation)
                                                            ->lists('name', 'id')
                                    ));
            } else {
                return  Redirect::route('graphics_queue')
                            ->with('failure', Lang::get('errors.graphics_queue.not_found'));
            }
        } else {
            return  Redirect::route('graphics_queue')
                        ->with('failure', Lang::get('errors.graphics_queue.no_access.renew'));
        }
    }

    public function postJobRenew($id) {
        if (GraphicsJob::accessible($id, 'special.graphics_queue.renew.job')) {
            $job_existing           =   GraphicsJob::where('id', '=', $id)
                                            ->with( array(
                                                'files'     =>  function($query) {
                                                                    $query->where('proof', '=', true)
                                                                        ->orderBy('updated_at', 'desc')
                                                                        ->take(1);
                                                                }
                                                ))
                                            ->get();

            if (!$job_existing->isEmpty()) {
                $job_existing           = $job_existing->first();

                $validator  = Validator::make(  Input::all(),
                                                array(
                                                    'customer'          => 'required|numeric|min:1',
                                                    'media'             => 'required|numeric|min:1',
                                                    'title'             => 'required',
                                                    'color_type'        => 'required|numeric|min:1',
                                                    'zone'              => 'required|array'
                                                ));

                if (($validator->passes()) && (MediaType::accessible(Input::get('media'))) && (Zone::accessible(Input::get('zone')))) {
                    $job                    = new GraphicsJob;
                    $job->key               = GraphicsJob::generateJobKey();
                    $job->crm_id            = Input::get('customer');
                    $job->created_by        = Auth::user()->id;
                    $job->title             = Input::get('title');
                    $job->media             = Input::get('media');
                    $job->color_type        = Input::get('color_type');
                    $job->status            = AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'In Production');

                    if ((UserACL::can('special.graphics_queue.assign.artist')) && (User::accessible(Input::get('assigned_to')))) {
                        $job->assigned_to       = Input::get('assigned_to');
                    }

                    if (UserACL::can('special.graphics_queue.assign.points')) {
                        $job->points            = Input::get('points');
                    }

                    if ($job->assigned_to == 0) {
                        $job->status            = AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Unassigned');
                    }

                    $job->save();

                    if ($job->id) {
                        // save zones
                        DB::table('graphics_jobs_zones')->insert(   array_map(
                                                                        function($element) use ($job) {
                                                                            return array('job_id' => $job->id, 'zone_id' => $element);
                                                                        },
                                                                        Input::get('zone')
                                                                    ));

                        // copy last proof image
                        $job_existing_path              = public_path() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . 'installations' . DIRECTORY_SEPARATOR . Auth::user()->installation . DIRECTORY_SEPARATOR . 'jobs' . DIRECTORY_SEPARATOR . $job_existing->first()->key . DIRECTORY_SEPARATOR;
                        $job_new_path                   = public_path() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . 'installations' . DIRECTORY_SEPARATOR . Auth::user()->installation . DIRECTORY_SEPARATOR . 'jobs' . DIRECTORY_SEPARATOR . $job->key . DIRECTORY_SEPARATOR;

                        if (!file_exists($job_new_path)) {
                            mkdir($job_new_path, 0777, true);
                        }

                        if (!$job_existing->files->isEmpty()) {
                            $job_existing_proof_original    = $job_existing->files->first()->original;
                            $job_existing_proof_filename    = $job_existing->files->first()->file;

                            if (file_exists($job_existing_path . $job_existing_proof_filename)) {
                                copy(   $job_existing_path . $job_existing_proof_filename,
                                        $job_new_path . $job_existing_proof_filename);

                                if (file_exists($job_new_path . $job_existing_proof_filename)) {
                                    $job_note                       = new GraphicsJobNote;
                                    $job_note->job_id               = $job->id;
                                    $job_note->created_by           = Auth::user()->id;
                                    $job_note->visibility           = AppUtils::config_get_ordinal_by_value(Config::get('settings.graphics_queue.notes.visibility_types'), 'button_label', 'Internal Notes');
                                    $job_note->save();

                                    if ($job_note->id) {
                                        $file                           = new GraphicsJobFile;
                                        $file->job_id                   = $job->id;
                                        $file->created_by               = Auth::user()->id;
                                        $file->file                     = $job_existing_proof_filename;
                                        $file->original                 = $job_existing_proof_original;
                                        $file->proof                    = false;
                                        $file->note_id                  = $job_note->id;
                                        $file->save();
                                    }
                                }
                            }
                        }

                        // send assignee e-mail, if this job was assigned to someone
                        if ((UserACL::can('special.graphics_queue.assign.artist')) && ($job->assigned_to)) {
                            $assignee           = User::where('id', '=', $job->assigned_to)
                                                    ->where('installation', '=', Auth::user()->installation)
                                                    ->get();

                            if ((!$assignee->isEmpty()) && ($assignee->first()->email)) {
                                $assignee           = $assignee->first();

                                // dispatch notification
                                Notification::dispatch( 'settings.notifications.types.graphics-queue',
                                                        $assignee->id,
                                                        $job->id,
                                                        route('graphics_queue_job_details', $job->id),
                                                        date('Y-m-d H:i:s'),
                                                        'notifications.graphics_queue.assigned',
                                                        array(
                                                            'title'         => $job->title
                                                        ));

                                // dispatch e-mail
                                Mail::send( 'emails.graphics-queue.internal-new-job-assigned',
                                            array(
                                                'user_name'         => $assignee->name,
                                                'job_title'         => $job->title,
                                                'job_url'           => link_to(route('graphics_queue_job_details', $job->id))
                                            ),
                                            function($message) use ($assignee) {
                                                $message
                                                    ->to($assignee->email)
                                                    ->subject(Lang::get('emails.graphics_queue.create.assigned'));
                                            });
                            }
                        }

                        // send notification e-mail
                        foreach (HelpersGraphicsJobController::getJobParticipants($job->id) as $participant_id => $participant_email) {
                            // dispatch notification
                            Notification::dispatch( 'settings.notifications.types.graphics-queue',
                                                    $participant_id,
                                                    $job->id,
                                                    route('graphics_queue_job_details', $job->id),
                                                    date('Y-m-d H:i:s'),
                                                    'notifications.graphics_queue.created',
                                                    array(
                                                        'title'         => $job->title
                                                    ));

                            // send notification e-mail
                            Mail::send( 'emails.graphics-queue.internal-new-job-created',
                                        array(
                                            'creator_name'      => Auth::user()->name,
                                            'job_title'         => $job->title,
                                            'job_url'           => link_to(route('graphics_queue_job_details', $job->id))
                                        ),
                                        function($message) use ($participant_email, $job) {
                                            $message
                                                ->to($participant_email)
                                                ->subject(Lang::get('emails.graphics_queue.create.created'));
                                        });
                        }

                        return  Redirect::route('graphics_queue')
                                    ->with('success', Lang::get('success.graphics_queue.renew'));
                    } else {
                        return  Redirect::route('graphics_queue')
                                    ->with('failure', Lang::get('errors.graphics_queue.renew.general'));
                    }
                } else {
                    return  Redirect::route('graphics_queue_job_renew', $id)
                                ->withErrors($validator)
                                ->withInput();
                }
            } else {
                return  Redirect::route('graphics_queue')
                            ->with('failure', Lang::get('errors.graphics_queue.not_found'));
            }
        } else {
            return  Redirect::route('graphics_queue')
                        ->with('failure', Lang::get('errors.graphics_queue.no_access.renew'));
        }
    }
}
