<?php

class GraphicsQueueController extends Controller {
    public function getLanding() {
        if (UserACL::can('read.graphics_queue')) {
            return  View::make( 'graphics-queue/landing',
                                array(
                                    'jobs'      =>  (
                                                        (UserACL::can('special.graphics_queue.read.assigned_only'))
                                                        ?
                                                        GraphicsJob::where('assigned_to', '=', Auth::user()->id)
                                                                        ->with( array(  'customer'  =>  function($query) {
                                                                                            $query->where('installation', '=', Auth::user()->installation);
                                                                                        }
                                                                                    ))
                                                                        ->get()
                                                        :
                                                        GraphicsJob::with(  array(  'customer'  =>  function($query) {
                                                                                        $query->where('installation', '=', Auth::user()->installation);
                                                                                    }
                                                                                ))
                                                                        ->get()
                                                    )
                                ));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.graphics_queue.no_access.read'));
        }
    }

    public function getJobCreate() {
        if (UserACL::can('create.graphics_queue')) {
            return  View::make( 'graphics-queue/job-create',
                                array(
                                    'clients'   =>  (
                                                        (!UserACL::can('special.crm.use.master'))
                                                        ?
                                                        CRM::where('installation', '=', Auth::user()->installation)
                                                            ->where('type', '=', 'client')
                                                            ->where('assigned_to', '=', Auth::user()->id)
                                                            ->lists('name', 'id')
                                                        :
                                                        CRM::where('installation', '=', Auth::user()->installation)
                                                            ->where('type', '=', 'client')
                                                            ->lists('name', 'id')
                                                    ),
                                    'media'     =>  MediaType::where('installation', '=', Auth::user()->installation)
                                                        ->lists('name', 'id'),
                                    'zones'     =>  Zone::zonelist_with_codes(
                                                        Zone::where('installation', '=', Auth::user()->installation)
                                                            ->get()
                                                    ),
                                    'users'     =>   User::where('installation', '=', Auth::user()->installation)
                                                        ->whereHas( 'acl',
                                                                    function($query) {
                                                                        $query->where('action', '=', Config::get('acl.special.system.act_as.artist'));
                                                                    })
                                                        ->lists('name', 'id')
                                ));
        } else {
            return  Redirect::route('graphics_queue')
                        ->with('failure', Lang::get('errors.graphics_queue.no_access.create'));
        }
    }

    public function getJobUpdate($id) {

    }

    public function postJobCreate() {
        if (UserACL::can('create.graphics_queue')) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'job_key'           => 'required|min:32',
                                                'customer'          => 'required|numeric|min:1',
                                                'media'             => 'required|numeric|min:1',
                                                'title'             => 'required',
                                                'color_type'        => 'required|numeric|min:1',
                                                'zone'              => 'required|array'
                                            ));

            if ($validator->passes()) {
                // save client record
                $job                 = new GraphicsJob;
                $job->key           = Input::get('job_key');
                $job->crm_id        = Input::get('customer');
                $job->created_by    = Auth::user()->id;
                $job->title         = Input::get('title');
                $job->media         = Input::get('media');
                $job->color_type    = Input::get('color_type');
                $job->status        = AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'In Production');

                if ((UserACL::can('special.graphics_queue.assign.artist')) && (User::accessible(Input::get('assigned_to')))) {
                    $job->assigned_to   = Input::get('assigned_to');
                }

                if (UserACL::can('special.graphics_queue.assign.points')) {
                    $job->points        = Input::get('points');
                }

                if ($job->assigned_to == 0) {
                    $job->status        = AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Unassigned');
                }

                $job->save();

                // save job zones
                DB::table('graphics_jobs_zones')->insert(   array_map(
                                                                function($element) use ($job) {
                                                                    return array(
                                                                        'job_id'    =>  $job->id,
                                                                        'zone_id'   =>  $element
                                                                    );
                                                                },
                                                                Input::get('zone')
                                                            ));

                // save job notes
                $this->_insertJobNote($job->id, Input::get('notes_add'));

                // save job files, if any
                if (strlen(Input::get('job_files'))) {
                    $files                  = json_decode(Input::get('job_files'));

                    if (is_array($files)) {
                        foreach ($files as $file) {
                            $job_file               = new GraphicsJobFile;
                            $job_file->created_by   = Auth::user()->id;
                            $job_file->job_id       = $job->id;
                            $job_file->file         = $file->file;
                            $job_file->original     = $file->original;
                            $job_file->save();
                        }
                    }
                }

                // send assignee e-mail, if this job was assigned to someone
                if ($job->assigned_to) {
                    $assignee           = User::find($job->assigned_to);

                    if ((!$assignee->get()->isEmpty()) && ($assignee->email)) {
                        // dispatch notification
                        Notification::dispatch( 'settings.notifications.types.graphics-queue',
                                                $assignee->id,
                                                $job->id,
                                                route('graphics_queue_job_details', $job->id),
                                                date('Y-m-d H:i:s'),
                                                'notifications.graphics_queue.assigned',
                                                array(
                                                    'title'         => $job->title
                                                ));

                        // dispatch e-mail
                        Mail::send( 'emails.graphics-queue.internal-new-job-assigned',
                                    array(
                                        'user_name'         => $assignee->name,
                                        'job_title'         => $job->title,
                                        'job_url'           => link_to(route('graphics_queue_job_details', $job->id))
                                    ),
                                    function($message) use ($assignee) {
                                        $message
                                            ->to($assignee->email)
                                            ->subject(Lang::get('emails.graphics_queue.create.assigned'));
                                    });
                    }
                }

                foreach (HelpersGraphicsJobController::getJobParticipants($job->id) as $participant_id => $participant_email) {
                    // dispatch notification
                    Notification::dispatch( 'settings.notifications.types.graphics-queue',
                                            $participant_id,
                                            $job->id,
                                            route('graphics_queue_job_details', $job->id),
                                            date('Y-m-d H:i:s'),
                                            'notifications.graphics_queue.created',
                                            array(
                                                'title'         => $job->title
                                            ));

                    // dispatch e-mail
                    Mail::send( 'emails.graphics-queue.internal-new-job-created',
                                array(
                                    'creator_name'      => Auth::user()->name,
                                    'job_title'         => $job->title,
                                    'job_url'           => link_to(route('graphics_queue_job_details', $job->id))
                                ),
                                function($message) use ($participant_email, $job) {
                                    $message
                                        ->to($participant_email)
                                        ->subject(Lang::get('emails.graphics_queue.create.created'));
                                });
                }

                return  Redirect::route('graphics_queue')
                            ->with('success', Lang::get('success.graphics_queue.create'));
            } else {
                return  Redirect::route('graphics_queue_create_job')
                            ->withErrors($validator)
                            ->withInput();
            }
        } else {
            return  Redirect::route('graphics_queue')
                        ->with('failure', Lang::get('errors.graphics_queue.no_access.create'));
        }
    }

    public function postJobUpdateSettings($id) {
        if (GraphicsJob::accessible($id, array('special.graphics_queue.finalize.own_job', 'special.graphics_queue.finalize.job', 'special.graphics_queue.assign.artist', 'special.graphics_queue.assign.points'))) {
            $job                    = GraphicsJob::where('id', '=', $id)
                                        ->with( array(  'customer'  =>  function($query) {
                                                                            $query->where('installation', '=', Auth::user()->installation);
                                                                        }
                                                        ))
                                        ->get();

            if (!$job->isEmpty()) {
                $job                    = $job->first();

                if ((UserACL::can('special.graphics_queue.assign.artist')) && (Input::has('assigned_to'))) {
                    // send assignee e-mail, if this job was assigned to someone new
                    if ((Input::get('assigned_to') > 0) && ($job->assigned_to != Input::get('assigned_to')) && (User::accessible(Input::get('assigned_to')))) {
                        $assignee           =   User::where('id', '=', Input::get('assigned_to'))
                                                    ->where('installation', '=', Auth::user()->installation)
                                                    ->get();

                        if ((!$assignee->isEmpty()) && ($assignee->first()->email)) {
                            $assignee           = $assignee->first();

                            // dispatch notification
                            Notification::dispatch( 'settings.notifications.types.graphics-queue',
                                                    $assignee->id,
                                                    $job->id,
                                                    route('graphics_queue_job_details', $job->id),
                                                    date('Y-m-d H:i:s'),
                                                    'notifications.graphics_queue.assigned',
                                                    array(
                                                        'title'         => $job->title
                                                    ));

                            // dispatch e-mail
                            Mail::send( 'emails.graphics-queue.internal-new-job-assigned',
                                        array(
                                            'user_name'         => $assignee->name,
                                            'job_title'         => $job->title,
                                            'job_url'           => link_to(route('graphics_queue_job_details', $job->id))
                                        ),
                                        function($message) use ($assignee) {
                                            $message
                                                ->to($assignee->email)
                                                ->subject(Lang::get('emails.graphics_queue.create.assigned'));
                                        });
                        }
                    }

                    // save assignment
                    $job->assigned_to       = Input::get('assigned_to');

                    if (($job->assigned_to > 0) && ($job->status == AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Unassigned'))) {
                        $job->previous_status   = $job->status;
                        $job->status            = AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'In Production');
                    }
                }

                if ((UserACL::can('special.graphics_queue.assign.points')) && (Input::has('points'))) {
                    $job->points            = Input::get('points');
                }

                if (((UserACL::can('special.graphics_queue.finalize.own_job')) && ($job->created_by == Auth::user()->id)) || (UserACL::can('special.graphics_queue.finalize.job'))) {
                    $job->previous_status   = $job->status;

                    if (Input::get('completed')) {
                        $job->status            = AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Completed');
                    } else if ((!Input::get('completed')) && ($job->status == AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Completed'))) {
                        $job->status            = AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'In Revisions');
                    }
                } else {
                    if ((UserACL::can('special.graphics_queue.finalize.own_job')) && ($job->created_by != Auth::user()->id)) {
                        return Redirect::route('graphics_queue')
                            ->with('failure', Lang::get('errors.graphics_queue.no_access.update'));
                    }
                }

                $job->save();

                return  Redirect::route('graphics_queue_job_details', $id)
                            ->with('success', Lang::get('success.graphics_queue.update_settings'));
            } else {
                return Redirect::route('graphics_queue')
                    ->with('failure', Lang::get('errors.graphics_queue.not_found'));
            }
        } else {
            return Redirect::route('graphics_queue')
                ->with('failure', Lang::get('errors.graphics_queue.no_access.update'));
        }
    }

    public function getJobDelete($id) {
        if (GraphicsJob::accessible($id, 'delete.graphics_queue')) {
            GraphicsJob::find($id)->delete();

            return Redirect::route('graphics_queue')
                ->with('success', Lang::get('success.graphics_queue.delete'));
        } else {
            return Redirect::route('graphics_queue')
                ->with('failure', Lang::get('errors.graphics_queue.no_access.delete'));
        }
    }

    private function _insertJobNote($id, $notes) {
        if (Input::get('notes_add')) {
            $note               = new GraphicsJobNote;
            $note->job_id       = $id;
            $note->created_by   = Auth::user()->id;
            $note->notes        = $notes;
            $note->visibility   = AppUtils::config_get_ordinal_by_value(Config::get('settings.graphics_queue.notes.visibility_types'), 'button_label', 'Internal Notes');
            $note->save();
        }
    }
}
