<?php

class GraphicsQueueDetailController extends Controller {
    public function getJobDetails($id) {
        if (GraphicsJob::accessible($id, 'read.graphics_queue')) {
            $job                =   GraphicsJob::where('id', '=', $id)
                                        ->with( array(
                                                    'customer'  =>  function($query) {
                                                                        $query->where('installation', '=', Auth::user()->installation);
                                                                    },
                                                    'customer.attrs',
                                                    'files'     =>  function($query) {
                                                                        $query->orderBy('created_at', 'desc');
                                                                    },
                                                    'zones'
                                                ))
                                        ->get();

            if (!$job->isEmpty()) {
                $job                = $job->first();

                return  View::make( 'graphics-queue/job-details',
                                    array(
                                        'job'       =>  $job,
                                        'users'     =>  User::where('installation', '=', Auth::user()->installation)
                                                            ->whereHas( 'acl',
                                                                        function($query) {
                                                                            $query->where('action', '=', Config::get('acl.special.system.act_as.artist'));
                                                                        })
                                                            ->lists('name', 'id')
                                    ));
            } else {
                return  Redirect::route('graphics_queue')
                            ->with('failure', Config::get('errors.graphics_queue.not_found'));
            }
        } else {
            return  Redirect::route('graphics_queue')
                        ->with('failure', Config::get('errors.graphics_queue.no_access.detail'));
        }
    }

    public function getPublic_JobDetails($key) {
        Session::flush();
        Session::put('visiting.job_key', $key);

        $job                =   GraphicsJob::where('key', '=', $key)
                                    ->with( array(
                                                'customer',
                                                'files'     =>  function($query) {
                                                                    $query->orderBy('created_at', 'desc');
                                                                }
                                            ))
                                    ->get();

        if (!$job->isEmpty()) {
            $job                = $job->first();

            Session::put('visiting.installation', $job->customer->installation);

            return  View::make( 'graphics-queue/job-details',
                                array(
                                    'job'   => $job
                                ));
        } else {
            // TODO: Show error message
            die();
        }
    }

    public function postJobNote($id) {
        if (GraphicsJob::accessible($id)) {
            $job                =   GraphicsJob::where('id', '=', $id)
                                        ->get();

            if (!$job->isEmpty()) {
                $job                = $job->first();

                $note                = new GraphicsJobNote;
                $note->job_id        = $id;
                $note->created_by    = ((Auth::check()) ? Auth::user()->id : 0);
                $note->notes         = Input::get('notes_add');

                if (Auth::check()) {
                    if ((UserACL::can('special.graphics_queue.set.note_visibility')) && (Input::get('notes_visibility'))) {
                        $note->visibility   = Input::get('notes_visibility');
                    } else {
                        $note->visibility   = AppUtils::config_get_ordinal_by_value(Config::get('settings.graphics_queue.notes.visibility_types'), 'button_label', 'Internal Notes');
                    }
                } else {
                    $note->visibility   = AppUtils::config_get_ordinal_by_value(Config::get('settings.graphics_queue.notes.visibility_types'), 'button_label', 'Client Notes');
                }

                $note->save();

                if ($note->id) {
                    // send notification e-mail
                    foreach (HelpersGraphicsJobController::getJobParticipants($job->id) as $participant_id => $participant_email) {
                        // dispatch notification
                        Notification::dispatch( 'settings.notifications.types.graphics-queue',
                                                $participant_id,
                                                $job->id,
                                                route('graphics_queue_job_details', $job->id),
                                                date('Y-m-d H:i:s'),
                                                'notifications.graphics_queue.note',
                                                array(
                                                    'title'         => $job->title
                                                ));

                        // dispatch e-mail
                        Mail::send( 'emails.graphics-queue.internal-new-note',
                                    array(
                                        'job_title'         => $job->title,
                                        'job_url'           => link_to(route('graphics_queue_job_details', $job->id))
                                    ),
                                    function($message) use ($participant_email, $job) {
                                        $message
                                            ->to($participant_email)
                                            ->subject(  Lang::get(  'emails.graphics_queue.note_posted.internal',
                                                                    array(
                                                                        'title'  => $job->title
                                                                    )));
                                    });
                    }

                    // save attachment, if any
                    if (Input::hasFile('notes_attachment')) {
                        $original_filename      = Input::file('notes_attachment')->getClientOriginalName();
                        $extension              = Input::file('notes_attachment')->getClientOriginalExtension();

                        if (in_array(strtolower($extension), Config::get('settings.graphics_queue.files.allowed_image_extensions'))) {
                            $upload_path                    = public_path() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . 'installations' . DIRECTORY_SEPARATOR . Auth::user()->installation . DIRECTORY_SEPARATOR . 'jobs' . DIRECTORY_SEPARATOR . $job->key . DIRECTORY_SEPARATOR;
                            $upload_filename                = uniqid() . '.' . $extension;

                            if (!file_exists($upload_path)) {
                                mkdir($upload_path, 0777, true);
                            }

                            if (Input::file('notes_attachment')->move($upload_path, $upload_filename)) {
                                $file                       = new GraphicsJobFile;
                                $file->job_id               = $id;
                                $file->note_id              = $note->id;
                                $file->created_by           = ((Auth::check()) ? Auth::user()->id : 0);
                                $file->file                 = $upload_filename;
                                $file->original             = $original_filename;
                                $file->proof                = ((Input::get('proof')) ? true : false);
                                $file->save();

                                if ($file->proof) {
                                    if ($job->status == AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'In Production')) {
                                        $job->previous_status    = $job->status;
                                        $job->status             = AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Initial Proof');
                                        $job->save();
                                    } elseif ($job->status == AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Initial Proof')) {
                                        $job->previous_status    = $job->status;
                                        $job->status             = AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'In Revisions');
                                        $job->save();
                                    }
                                }
                            } else {
                                if (Auth::check()) {
                                    return  Redirect::route('graphics_queue_job_details', $id)
                                                ->with('failure', Lang::get('errors.graphics_queue.note.upload_error'));
                                } else {
                                    return  Redirect::route('client_view_graphics_queue_job_details', $job->key)
                                                ->with('failure', Lang::get('errors.graphics_queue.note.upload_error'));
                                }
                            }
                        } else {
                            if (Auth::check()) {
                                return  Redirect::route('graphics_queue_job_details', $id)
                                            ->with('failure', Lang::get('errors.graphics_queue.note.disallowed_file_type'));
                            } else {
                                return  Redirect::route('client_view_graphics_queue_job_details', $job->key)
                                            ->with('failure', Lang::get('errors.graphics_queue.note.disallowed_file_type'));
                            }
                        }
                    }

                    if (Auth::check()) {
                        return  Redirect::route('graphics_queue_job_details', $id)
                                    ->with('success', Lang::get('success.graphics_queue.note'));
                    } else {
                        return  Redirect::route('client_view_graphics_queue_job_details', $job->key)
                                    ->with('success', Lang::get('success.graphics_queue.note'));
                    }
                } else {
                    if (Auth::check()) {
                        return  Redirect::route('graphics_queue_job_details', $id)
                                    ->with('failure', Lang::get('errors.graphics_queue.note.general'));
                    } else {
                        return  Redirect::route('client_view_graphics_queue_job_details', $job->key)
                                    ->with('failure', Lang::get('errors.graphics_queue.note.general'));
                    }
                }
            } else {
                if (Auth::check()) {
                    return  Redirect::route('graphics_queue_job_details', $id)
                                ->with('failure', Lang::get('errors.graphics_queue.no_access.detail'));
                } else {
                    return  Redirect::route('client_view_graphics_queue_job_details', $job->key)
                                ->with('failure', Lang::get('errors.graphics_queue.no_access.detail'));
                }
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.graphics_queue.no_access.detail'));
        }
    }

    public static function canAccessNoteType($type) {
        if ((!Auth::check()) && (!$type['require_acl'])) {
            // allowed for non-logged in users (client notes, probably)
            return true;
        } else if (Auth::check()) {
            // logged-in users
            if (!$type['require_acl']) {
                if (!$type['hide_if']) {
                    // no restrictions, okay to show it
                    return true;
                } else if (!UserACL::can($type['hide_if'])) {
                    // user does not have any of the permissions in the "hide from" permissions list
                    return true;
                } else {
                    // user is denied access
                    return false;
                }
            } elseif (is_array($type['require_acl'])) {
                if (UserACL::can($type['require_acl'])) {
                    if (!$type['hide_if']) {
                        // no restrictions, okay to show it
                        return true;
                    } else if (!UserACL::can($type['hide_if'])) {
                        // user does not have any of the permissions in the "hide from" permissions list
                        return true;
                    } else {
                        // user is denied access
                        return false;
                    }
                } else {
                    // user does not have any of the required permissions
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}
