<?php

class CRMController extends Controller {
    public function getLanding() {
        if (CRM::accessibleType()) {
            if (UserACL::can('read.crm')) {
                $clients            =   CRM::where('installation', '=', Auth::user()->installation)
                                            ->with('attrs');

                // gather search types for use on the front-end
                $search_types       =   array();
                $search_criteria    =   array();

                foreach (Config::get('settings.crm.search_criteria') as $type) {
                    $search_types[$type['ordinal']]  = $type['label'];
                }

                foreach (Input::all() as $key => $value) {
                    if (strpos($key, 'criteria_') !== false) {
                        $parts      = explode('criteria_', $key);

                        if (count($parts) === 2) {
                            $criteria_id            = intval($parts[1]);
                            $criteria_data_primary  = Input::get('criteria_data_primary_' . $criteria_id);

                            if (($criteria_id > 0) && ($criteria_data_primary)) {
                                foreach (Config::get('settings.crm.search_criteria') as $criteria) {
                                    if ($criteria['ordinal'] == $value) {
                                        if ($criteria['relationship']) {
                                            $clients->whereHas( $criteria['relationship'],
                                                                function($query) use ($criteria, $criteria_data_primary) {
                                                                    switch ($criteria['match']) {
                                                                        case 'exact' :
                                                                            $query->where($criteria['column'], '=', $criteria_data_primary);
                                                                            break;

                                                                        case 'like' :
                                                                            $query->where($criteria['column'], 'like', '%' . $criteria_data_primary . '%');
                                                                            break;
                                                                    }
                                                                });
                                        } else {
                                            switch ($criteria['match']) {
                                                case 'exact' :
                                                    $clients->where($criteria['column'], '=', $criteria_data_primary);
                                                    break;

                                                case 'like' :
                                                    $clients->where($criteria['column'], 'like', '%' . $criteria_data_primary . '%');
                                                    break;
                                            }
                                        }

                                        array_push( $search_criteria,
                                                    array(
                                                        'type'      => $criteria['ordinal'],
                                                        'primary'   => $criteria_data_primary
                                                    ));
                                    }
                                }
                            }
                        }
                    }
                }

                if (self::isRoutePersonal()) {
                    $clients->where('assigned_to', '=', Auth::user()->id);
                }

                $clients->orderBy('type');

                return  View::make( 'crm.landing',
                                    array(
                                        'clients'               =>  $clients->get(),
                                        'search_types_simple'   =>  $search_types,
                                        'search_types'          =>  json_encode(Config::get('settings.crm.search_criteria')),
                                        'search_criteria'       =>  json_encode($search_criteria)
                                    ));
            } else {
                return  Redirect::route('dashboard')
                            ->with('failure', Lang::get('errors.crm.no_access.read'));
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get((self::isRoutePersonal()) ? 'errors.crm.no_access.personal' : 'errors.crm.no_access.master'));
        }
    }

    public function getClientCreate() {
        if (CRM::accessibleType()) {
            if (UserACL::can('create.crm')) {
                return  View::make( 'crm.crm-create',
                                    array(
                                        'users'         =>  User::where('installation', '=', Auth::user()->installation)
                                                                ->lists('name', 'id')
                                    ));
            } else {
                return  Redirect::route('crm')
                            ->with('failure', Lang::get('errors.crm.no_access.create'));
            }
        } else {
            return  Redirect::route('crm')
                        ->with('failure', Lang::get((self::isRoutePersonal()) ? 'errors.crm.no_access.personal' : 'errors.crm.no_access.master'));
        }
    }

    public function getClientUpdate($id) {
        if (CRM::accessibleType()) {
            if (CRM::accessible($id, 'update.crm')) {
                $crm                = (self::isRoutePersonal() ? CRM::with('attrs', 'authorizations', 'authorizations.zones', 'authorizations.zones.zone')->where('id', '=', $id)->where('assigned_to', '=', Auth::user()->id)->get() : CRM::with('attrs', 'authorizations', 'authorizations.zones', 'authorizations.zones.zone')->where('id', '=', $id)->get());

                if (!$crm->isEmpty()) {
                    return  View::make( 'crm.crm-update',
                                        array(
                                            'crm'           =>  $crm->first(),
                                            'users'         =>  User::where('installation', '=', Auth::user()->installation)
                                                                    ->lists('name', 'id')
                                        ));
                } else {
                    return  Redirect::route('crm')
                                ->with('failure', Lang::get('errors.crm.not_found'));
                }
            } else {
                return  Redirect::route('crm')
                            ->with('failure', Lang::get('errors.crm.no_access.update'));
            }
        } else {
            return  Redirect::route('crm')
                        ->with('failure', Lang::get((self::isRoutePersonal()) ? 'errors.crm.no_access.personal' : 'errors.crm.no_access.master'));
        }
    }

    public function postClientCreate() {
        if (CRM::accessibleType()) {
            if (UserACL::can('create.crm')) {
                $validator  = Validator::make(  Input::all(),
                                                array(
                                                    'name'              => 'required'
                                                ));

                if ($validator->passes()) {
                    // save client record
                    $crm                    = new CRM;
                    $crm->installation      = Auth::user()->installation;
                    $crm->type              = ((self::isRoutePersonal()) ? ((Input::has('type')) ? Input::get('type') : 'prospect') : 'client');
                    $crm->name              = Input::get('name');

                    if (self::isRoutePersonal()) {
                        $crm->assigned_to       = Auth::user()->id;
                    } else {
                        $crm->assigned_to       = Input::has('assigned_to') ? Input::get('assigned_to') : Auth::user()->id;
                    }

                    $crm->save();

                    // save client attributes
                    $attrs                  = new CRMAttribute;
                    $attrs->crm_id          = $crm->id;
                    $attrs->address1        = Input::get('address1');
                    $attrs->address2        = Input::get('address2');
                    $attrs->city            = Input::get('city');
                    $attrs->state           = Input::get('state');
                    $attrs->zip             = Input::get('zip');
                    $attrs->contact_name    = Input::get('contact_name');
                    $attrs->phone           = Input::get('phone');
                    $attrs->email           = Input::get('email');
                    $attrs->website         = Input::get('website');

                    // save client logo, if any
                    if (Input::hasFile('logo')) {
                        $logo                   = HelpersCRMController::saveLogo();

                        if ($logo) {
                            $attrs->logo            = $logo;
                        }
                    }

                    $attrs->save();

                    // save notes, if any
                    $this->_insertNotes($crm->id);

                    return  Redirect::route(self::isRoutePersonal() ? 'crm_personal' : 'crm')
                                ->with('success', Lang::get((($attrs->email) ? 'success.crm.create' : 'success.crm.create_no_email')));
                } else {
                    return  Redirect::route(self::isRoutePersonal() ? 'crm_personal_create_client' : 'crm_create_client')
                                ->withErrors($validator)
                                ->withInput();
                }
            } else {
                return  Redirect::route('crm')
                            ->with('failure', Lang::get('errors.crm.no_access.update'));
            }
        } else {
            return  Redirect::route('crm')
                        ->with('failure', Lang::get((self::isRoutePersonal()) ? 'errors.crm.no_access.personal' : 'errors.crm.no_access.master'));
        }
    }

    public function postClientUpdate($id) {
        if (CRM::accessibleType()) {
            if (CRM::accessible($id, 'update.crm')) {
                $crm        = CRM::where('id', '=', $id)->where('installation', '=', Auth::user()->installation);

                if ((!$crm->get()->isEmpty()) && ((!self::isRoutePersonal()) || ((self::isRoutePersonal()) && ($crm->first()->assigned_to == Auth::user()->id)))) {
                    $validator  = Validator::make(  Input::all(),
                                                    array(
                                                        'name'              => 'required'
                                                    ));

                    if ($validator->passes()) {
                        $crm                = $crm->first();
                        $crm->name          = Input::get('name');
                        $crm->type          = ((self::isRoutePersonal()) ? ((Input::has('type')) ? Input::get('type') : 'prospect') : 'client');

                        if (self::isRoutePersonal()) {
                            $crm->assigned_to       = Auth::user()->id;
                        } else {
                            $crm->assigned_to       = Input::has('assigned_to') ? Input::get('assigned_to') : Auth::user()->id;
                        }

                        $crm->save();

                        // update client attributes
                        $crm->attrs()->update((((Input::has('attrs')) && (is_array(Input::get('attrs')))) ? Input::get('attrs') : array()));

                        // update client logo, if any
                        if (Input::hasFile('logo')) {
                            $logo                   = HelpersCRMController::saveLogo();

                            if ($logo) {
                                $crm->attrs->logo            = $logo;
                                $crm->attrs->save();
                            }
                        }

                        // save notes, if any
                        $this->_insertNotes($crm->id);

                        return  Redirect::route(self::isRoutePersonal() ? 'crm_personal' : 'crm')
                                    ->with('success', Lang::get((($crm->attrs->email) ? 'success.crm.update' : 'success.crm.update_no_email')));
                    } else {
                        return  Redirect::route(self::isRoutePersonal() ? 'crm_personal_update_client' : 'crm_update_client', array($id))
                                    ->withErrors($validator)
                                    ->withInput();
                    }
                } else {
                    return  Redirect::route('crm')
                                ->with('failure', Lang::get('errors.crm.no_access.update'));
                }
            } else {
                return  Redirect::route('crm')
                            ->with('failure', Lang::get('errors.crm.not_found'));
            }
        } else {
            return  Redirect::route('crm')
                        ->with('failure', Lang::get((self::isRoutePersonal()) ? 'errors.crm.no_access.personal' : 'errors.crm.no_access.master'));
        }
    }

    public function getClientDelete($crm_id) {
        if (CRM::accessibleType()) {
            if (CRM::accessible($crm_id, 'delete.crm')) {
                CRM::find($crm_id)->delete();

                return  Redirect::route('crm')
                            ->with('success', Lang::get('success.crm.delete'));
            } else {
                return  Redirect::route('crm')
                            ->with('success', Lang::get('errors.crm.no_access.delete'));
            }
        } else {
            return  Redirect::route('crm')
                        ->with('failure', Lang::get((self::isRoutePersonal()) ? 'errors.crm.no_access.personal' : 'errors.crm.no_access.master'));
        }
    }

    private function _insertNotes($crm_id) {
        if (Input::get('notes_add')) {
            $notes                  = new CRMNote;
            $notes->crm_id          = $crm_id;
            $notes->created_by      = Auth::user()->id;
            $notes->notes           = Input::get('notes_add');
            $notes->save();
        }
    }

    public static function isRoutePersonal() {
        return (Str::contains(Route::currentRouteName(), 'personal') === true) ? true : false;
    }
}
