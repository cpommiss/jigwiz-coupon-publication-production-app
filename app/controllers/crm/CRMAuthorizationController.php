<?php

class CRMAuthorizationController extends Controller {
    public function getClientAuthorization() {
        if (UserACL::can('special.crm.send.publishing_auth')) {
            return  View::make( 'crm.crm-authorization',
                                array(
                                    'clients'       =>  CRM::where('installation', '=', Auth::user()->installation)
                                                            ->where('type', '=', 'client')
                                                            ->lists('name', 'id'),
                                    'mediatypes'    =>  MediaType::where('installation', '=', Auth::user()->installation)
                                                            ->lists('name', 'id')
                                ));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.crm.no_access.authorization'));
        }
    }

    public function getPublic_ClientAuthorization($key) {
        Session::put('visiting.publishing_auth_key', $key);

        $auth           =   CRMAuthorization::with('customer', 'mediatype')
                                ->where('key', '=', $key)
                                ->get();

        if (!$auth->isEmpty()) {
            $auth           = $auth->first();

            Session::put('visiting.installation', $auth->customer->installation);

            return  View::make( 'crm.client-crm-authorization',
                                array(
                                    'auth'      =>  $auth,
                                    'zones'     =>  Zone::zonelist_with_codes(
                                                        Zone::where('installation', '=', $auth->customer->installation)
                                                            ->get()
                                                    )
                                ));
        } else {
            return  Redirect::route('login')
                        ->with('failure', Lang::get('errors.crm.authorization.general_client'));
        }
    }

    public function postPublic_ClientAuthorization() {
        $validator  = Validator::make(  Input::all(),
                                        array(
                                            'key'               => 'required|min:32',
                                            'zone'              => 'required|array',
                                            'signature'         => 'required|min:1'
                                        ));

        if ($validator->passes()) {
            $auth           =   CRMAuthorization::with('customer', 'mediatype')
                                    ->where('key', '=', Input::get('key'))
                                    ->where('authorized', '=', false)
                                    ->get();

            if (!$auth->isEmpty()) {
                $auth               = $auth->first();

                $auth->authorized   = true;
                $auth->signature    = Input::get('signature');
                $auth->save();

                // save job zones
                DB::table('crm_authorizations_zones')->insert(  array_map(
                                                                    function($element) use ($auth) {
                                                                        return array(
                                                                            'auth_id'   =>  $auth->id,
                                                                            'zone_id'   =>  $element
                                                                        );
                                                                    },
                                                                    Input::get('zone')
                                                                ));

                foreach (HelpersCRMAuthorizationController::getCRMNNotifiees($auth->customer->id) as $user_id => $user_email) {
                    // dispatch notification
                    Notification::dispatch( 'settings.notifications.types.crm-authorization',
                                            $user_id,
                                            $auth->customer->id,
                                            route('crm_update_client', $auth->customer->id),
                                            date('Y-m-d H:i:s'),
                                            'notifications.crm.authorization',
                                            array(
                                                'customer'      => $auth->customer->name,
                                                'issue'         => $auth->auth_date->format('F Y'),
                                                'media'         => $auth->mediatype->name
                                            ));

                    // dispatch e-mail
                    Mail::send( 'emails.crm.internal-authorization-success',
                                array(
                                    'customer_name'     => $auth->customer->name,
                                    'issue_date'        => $auth->auth_date->format('F Y'),
                                    'media_name'        => $auth->mediatype->name
                                ),
                                function($message) use ($user_email, $auth) {
                                    $message
                                        ->to($user_email)
                                        ->subject(  Lang::get(  'emails.crm.authorization.internal',
                                                                array(
                                                                    'customer'  =>  $auth->customer->name
                                                                )));
                                });
                }

                return  Redirect::route('client_view_crm_request_authorization', $auth->key);
            } else {
                return  Redirect::route('login')
                            ->with('failure', Lang::get('errors.crm.authorization.general_client'));
            }
        } else {
            if (Input::get('key')) {
                return  Redirect::route('client_view_crm_request_authorization', Input::get('key'))
                            ->withErrors($validator)
                            ->withInput();
            } else {
                return  Redirect::route('login')
                            ->with('failure', Lang::get('errors.crm.authorization.general_client'));
            }
        }
    }
}
