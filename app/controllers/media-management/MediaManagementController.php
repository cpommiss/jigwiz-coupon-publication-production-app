<?php

class MediaManagementController extends Controller {
    public function getLanding() {
        if (UserACL::can('read.media_type')) {
            return  View::make( 'media-management/landing',
                                array(
                                    'mediatypes'    =>  MediaType::where('installation', '=', Auth::user()->installation)
                                                            ->get()
                                ));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.media_type.no_access.read'));
        }
    }

    public function getMediaCreate() {
        if (UserACL::can('create.media_type')) {
            return  View::make('media-management/media-type-create');
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.media_type.no_access.create'));
        }
    }

    public function getMediaUpdate($id) {
        if (MediaType::accessible($id, 'update.media_type')) {
            $mediatype          =   MediaType::where('id', '=', $id)
                                        ->where('installation', '=', Auth::user()->installation)
                                        ->get();

            if (!$mediatype->isEmpty()) {
                $mediatype          = $mediatype->first();

                return  View::make( 'media-management/media-type-update',
                                    array(
                                        'mediatype' => $mediatype
                                    ));
            } else {
                return  Redirect::route('dashboard')
                            ->with('failure', Lang::get('errors.media_type.not_found'));
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.media_type.no_access.update'));
        }
    }

    public function postMediaCreate() {
        if (UserACL::can('create.media_type')) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'name'              => 'required',
                                                'width'             => 'required|numeric',
                                                'height'            => 'required|numeric',
                                                'color'             => 'required',
                                                'premium'           => 'required',
                                                'max_rows'          => 'required|numeric|min:1',
                                                'max_cols'          => 'required|numeric|min:1'
                                            ));

            if ($validator->passes()) {
                // save client record
                $mediatype                      = new MediaType;
                $mediatype->installation        = Auth::user()->installation;
                $mediatype->name                = Input::get('name');
                $mediatype->width               = Input::get('width');
                $mediatype->height              = Input::get('height');
                $mediatype->color               = Input::get('color');
                $mediatype->premium             = Input::get('premium');
                $mediatype->max_rows            = Input::get('max_rows');
                $mediatype->max_cols            = Input::get('max_cols');
                $mediatype->save();

                return  Redirect::route('media_management')
                            ->with('success', Lang::get('success.media_type.create'));
            } else {
                return  Redirect::route('media_management_create_media')
                            ->withErrors($validator)
                            ->withInput();
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.media_type.no_access.create'));
        }
    }

    public function postMediaUpdate($id) {
        if (MediaType::accessible($id, 'update.media_type')) {
            $mediatype  =   MediaType::where('id', '=', $id)
                                ->where('installation', '=', Auth::user()->installation)
                                ->get();

            if (!$mediatype->isEmpty()) {
                $mediatype  = $mediatype->first();

                $validator  = Validator::make(  Input::all(),
                                                array(
                                                    'name'              => 'required',
                                                    'width'             => 'required|numeric',
                                                    'height'            => 'required|numeric',
                                                    'color'             => 'required',
                                                    'premium'           => 'required',
                                                    'max_rows'          => 'required|numeric|min:1',
                                                    'max_cols'          => 'required|numeric|min:1'
                                                ));

                if ($validator->passes()) {
                    // save client record
                    $mediatype->name                = Input::get('name');
                    $mediatype->width               = Input::get('width');
                    $mediatype->height              = Input::get('height');
                    $mediatype->color               = Input::get('color');
                    $mediatype->premium             = Input::get('premium');
                    $mediatype->max_rows            = Input::get('max_rows');
                    $mediatype->max_cols            = Input::get('max_cols');
                    $mediatype->save();

                    return  Redirect::route('media_management')
                                ->with('success', Lang::get('success.media_type.update'));
                } else {
                    return  Redirect::route('media_management_update_media')
                                ->withErrors($validator)
                                ->withInput();
                }
            } else {
                return  Redirect::route('dashboard')
                            ->with('failure', Lang::get('errors.media_type.not_found'));
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.media_type.no_access.update'));
        }
    }

    public function getMediaDelete($id) {
        if (MediaType::accessible($id, 'delete.media_type')) {
            MediaType::find($id)->delete();

            return  Redirect::route('media_management')
                        ->with('success', Lang::get('success.media_type.delete'));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.media_type.no_access.delete'));
        }
    }
}
