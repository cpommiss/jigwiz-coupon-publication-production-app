<?php

class DocumentController extends Controller {
    public function getLanding() {
        if (UserACL::can('read.document')) {
            return  View::make( 'document/landing',
                                array(
                                    'documents'     =>  Document::with('uploader')
                                                            ->where('installation', '=', Auth::user()->installation)
                                                            ->get()
                                ));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.document.no_access.read'));
        }
    }

    public function postDocumentUpload() {
        if (UserACL::can('create.document')) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'file'      => 'required'
                                            ));

            if ($validator->passes()) {
                $original_filename      = Input::file('file')->getClientOriginalName();
                $extension              = Input::file('file')->getClientOriginalExtension();

                if (in_array(strtolower($extension), Config::get('settings.document.files.allowed_extensions'))) {
                    $upload_path                    = public_path() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . 'installations' . DIRECTORY_SEPARATOR . Auth::user()->installation . DIRECTORY_SEPARATOR . 'documents' . DIRECTORY_SEPARATOR;
                    $upload_filename                = uniqid() . '.' . $extension;

                    if (!file_exists($upload_path)) {
                        mkdir($upload_path, 0777, true);
                    }

                    if (Input::file('file')->move($upload_path, $upload_filename)) {
                        $document                       = new Document;
                        $document->installation         = Auth::user()->installation;
                        $document->file                 = $upload_filename;
                        $document->original             = $original_filename;
                        $document->title                = ((Input::get('title')) ? Input::get('title') : $original_filename);
                        $document->notes                = Input::get('notes');
                        $document->created_by           = Auth::user()->id;
                        $document->save();

                        if ($document->id) {
                            return  Redirect::route('document')
                                        ->with('success', Lang::get('success.document.create'));
                        } else {
                            return  Redirect::route('document')
                                        ->with('failure', Lang::get('errors.document.create.general'));
                        }
                    } else {
                        return  Redirect::route('document')
                                    ->with('failure', Lang::get('errors.document.create.general'));
                    }
                } else {
                    return  Redirect::route('document')
                                ->with('failure', Lang::get('errors.document.create.disallowed_file_type'));
                }
            } else {
                return  Redirect::route('document')
                            ->withInput()
                            ->withErrors($validator);
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.document.no_access.create'));
        }
    }

    public function getDocumentDelete($id) {
        if (Document::accessible($id, 'delete.document')) {
            $document       =   Document::where('id', '=', $id)
                                    ->where('installation', '=', Auth::user()->installation)
                                    ->get();

            if (!$document->isEmpty()) {
                File::delete(public_path() . '/content/installations/' . Auth::user()->installation . '/documents/' . $document->first()->file);

                $document->first()->delete();

                return  Redirect::route('document')
                            ->with('success', Lang::get('success.document.delete'));
            } else {
                return  Redirect::route('dashboard')
                            ->with('failure', Lang::get('errors.document.no_access.delete'));
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.document.no_access.delete'));
        }
    }
}