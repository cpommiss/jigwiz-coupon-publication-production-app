<?php

class SystemMessagingController extends Controller {
    public function getMessageCreate() {
        if (UserACL::can('create.system_message')) {
            return  View::make('system-messaging/landing');
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.system_message.no_access.create'));
        }
    }

    public function postMessageCreate() {
        if (UserACL::can('create.system_message')) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'subject'           => 'required',
                                                'message'           => 'required'
                                            ));

            if ($validator->passes()) {
                // save system message
                $message                = new SystemMessage;
                $message->installation  = Auth::user()->installation;
                $message->created_by    = Auth::user()->id;
                $message->subject       = Input::get('subject');
                $message->message       = Input::get('message');
                $message->save();

                return  Redirect::route('system_messaging')
                            ->with('success', Lang::get('success.system_message.create'));
            } else {
                return  Redirect::route('system_messaging')
                            ->withErrors($validator);
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.system_message.no_access.create'));
        }
    }

    public function getMessageDelete($id) {
        if (SystemMessage::accessible($id, 'delete.system_message')) {
            SystemMessage::find($id)->delete();

            return  Redirect::route('dashboard')
                        ->with('success', Lang::get('success.system_message.delete'));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.system_message.no_access.delete'));
        }
    }
}
