<?php

class StylingController extends Controller {
    public function getStyling() {
        if (UserACL::can('special.system.update.branding')) {
            $attrs          =   InstallationAttribute::where('installation_id', '=', Auth::user()->installation)
                                    ->first();

            return  View::make( 'styling/landing',
                                array(
                                    'styling'   =>  $attrs
                                ));

        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.general.no_access.area'));
        }
    }

    public function postStyling() {
        if (UserACL::can('special.system.update.branding')) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'color_primary'         => 'required',
                                                'color_secondary'       => 'required',
                                                'color_text'            => 'required',
                                                'color_block'           => 'required',
                                                'background_body'       => 'required',
                                                'background_block'      => 'required',
                                                'font_size'             => 'required',
                                                'font_family'           => 'required',
                                                'font_headings_family'  => 'required',
                                                'font_headings_color'   => 'required'
                                            ));

            if ($validator->passes()) {
                // save styling attributes
                $record                 =   InstallationAttribute::where('installation_id', '=', Auth::user()->installation)
                                                ->first();

                if (!empty($record)) {
                    $record->color_primary          = Input::get('color_primary');
                    $record->color_secondary        = Input::get('color_secondary');
                    $record->color_text             = Input::get('color_text');
                    $record->color_block            = Input::get('color_block');
                    $record->background_body        = Input::get('background_body');
                    $record->background_block       = Input::get('background_block');
                    $record->font_size              = Input::get('font_size');
                    $record->font_family            = Input::get('font_family');
                    $record->font_headings_family   = Input::get('font_headings_family');
                    $record->font_headings_color    = Input::get('font_headings_color');

                    if (Input::hasFile('image_logo')) {
                        $upload_path                    = public_path() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . 'installations' . DIRECTORY_SEPARATOR . Auth::user()->installation . DIRECTORY_SEPARATOR;
                        $upload_logo_filename           = uniqid() . '.' . Input::file('image_logo')->getClientOriginalExtension();

                        if (!file_exists($upload_path)) {
                            mkdir($upload_path, 0777);
                        }

                        Input::file('image_logo')
                            ->move($upload_path, $upload_logo_filename);

                        if (file_exists($upload_path . $upload_logo_filename)) {
                            $record->image_logo         = $upload_logo_filename;
                        }
                    }

                    $record->save();

                    // update
                    $compiler                       = new lessc;
                    $compiler->setVariables(    array(
                                                    'huragaColor'           => $record->color_primary,
                                                    'templateDark'          => $record->color_secondary,
                                                    'textColor'             => $record->color_text,
                                                    'blockTextColor'        => $record->color_block,
                                                    'bodyBackground'        => $record->background_body,
                                                    'blockBackground'       => $record->background_block,
                                                    'baseFontSize'          => $record->font_size,
                                                    'baseFontFamily'        => $record->font_family,
                                                    'headingsFontFamily'    => $record->font_headings_family,
                                                    'headingsColor'         => $record->font_headings_color,
                                                    'templateLogo'          => '\'' . url('content/installations/' . Auth::user()->installation . '/' . $record->image_logo) . '\''
                                                ));
                    $compiler->compileFile( public_path() . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'style.less',
                                            public_path() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . 'installations' . DIRECTORY_SEPARATOR . Auth::user()->installation . DIRECTORY_SEPARATOR . 'style.css');

                    return  Redirect::route('styling')
                                ->with('success', Lang::get('success.branding.update'));
                } else {
                    return  Redirect::route('styling')
                                ->with('failure', Lang::get('errors.branding.update.general_error'));
                }
            } else {
                return  Redirect::route('styling')
                            ->withErrors($validator);
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.general.no_access.area'));
        }
    }
}
