<?php

class UserController extends Controller {
    public function getLogin() {
        Session::forget('visiting');

        if (Auth::check()) {
            return  Redirect::route('dashboard');
        }

        return  View::make( 'users/login',
                            array(
                                'container_class'   =>  'login'
                            ));
    }

    public function getProfile() {
        return  View::make('users/profile');
    }

    public function postLogin() {
        $validator  = Validator::make(  Input::all(),
                                        array(
                                            'username'  => 'required',
                                            'password'  => 'required'
                                        ));

        if ($validator->passes()) {
            $credentials        = array(
                'username'          => Input::get('username'),
                'password'          => Input::get('password')
            );

            if (Auth::attempt($credentials)) {
                return  Redirect::intended(route('dashboard'));
            } else {
                return  Redirect::route('login')
                            ->withErrors(
                                array(
                                    'password' => Lang::get('errors.user.credentials_incorrect')
                                ))
                            ->withInput(Input::except('password'));
            }
        }

        return  Redirect::route('login')
                    ->withInput(Input::except('password'))
                    ->withErrors($validator);
    }

    public function postProfile() {
        $validator  = Validator::make(  Input::all(),
                                        array(
                                            'name'      => 'required',
                                            'email'     => 'required|email'
                                        ));
        if ($validator->passes()) {
            $user               = User::find(Auth::user()->id);
            $user->name         = Input::get('name');
            $user->email        = Input::get('email');
            $user->title        = Input::get('title');

            if ((Input::get('password')) && (Input::get('password_confirm'))) {
                if (Input::get('password') === Input::get('password_confirm')) {
                    $user->password     = Hash::make(Input::get('password'));
                }
            }

            if (Input::hasFile('avatar')) {
                $avatar             = HelpersUserController::saveUserAvatar();

                if ($avatar) {
                    $user->avatar       = $avatar;
                }
            }

            $user->save();

            return  Redirect::route('profile')
                        ->with('success', Lang::get('success.user.authenticated_user.update'));
        } else {
            return  Redirect::route('profile')
                        ->withInput(Input::except('password', 'password_confirm'))
                        ->withErrors($validator);
        }
    }

    public function getLogout() {
        Auth::logout();

        return  Redirect::route('login');
    }
}