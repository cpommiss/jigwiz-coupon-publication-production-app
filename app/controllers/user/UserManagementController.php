<?php

class UserManagementController extends Controller {
    public function getUserLanding() {
        if (Auth::user()->admin) {
            return  View::make( 'users/landing',
                                array(
                                    'users'     =>  User::where('installation', '=', Auth::user()->installation)
                                                        ->get()
                                ));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.general.no_access.area'));
        }
    }

    public function getUserCreate() {
        if (Auth::user()->admin) {
            return  View::make('users/user-create');
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.general.no_access.area'));
        }
    }

    public function getUserUpdate($id) {
        if (Auth::user()->admin) {
            if (User::accessible($id)) {
                $user               =   User::where('id', '=', $id)
                                            ->where('installation', '=', Auth::user()->installation)
                                            ->get();

                if (!$user->isEmpty()) {
                    $user               = $user->first();
                    
                    if (Input::old('acl')) {
                        $acl                = Input::old('acl');
                    } else {
                        $acl                = $user->acl->lists('action');
                    }

                    if ((Input::old('flags')) || (Input::all())){
                        if (Input::old('flags')) {
                            $flags              = Input::old('flags');
                        } else {
                            $flags              = array();
                        }
                    } else {
                        $flags              = array();

                        foreach (Config::get('settings.acl_structure.flags') as $flag_key => $flag_title) {
                            if ($user->{$flag_key}) {
                                array_push($flags, $flag_key);
                            }
                        }
                    }

                    return  View::make( 'users/user-update',
                                        array(
                                            'user'  =>  $user,
                                            'acl'   =>  $acl,
                                            'flags' =>  $flags
                                        ));
                } else {
                    return  Redirect::route('dashboard')
                                ->with('failure', Lang::get('errors.user.no_access.update'));
                }
            } else {
                return  Redirect::route('dashboard')
                            ->with('failure', Lang::get('errors.user.no_access.update'));
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.general.no_access.area'));
        }
    }

    public function postUserCreate() {
        if (Auth::user()->admin) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'username'          => 'required|alpha|unique:users,username,NULL,id,installation,' . Auth::user()->installation,
                                                'name'              => 'required',
                                                'email'             => 'required|email',
                                                'password'          => 'required|min:5|same:password_confirm',
                                                'password_confirm'  => 'required',
                                                'acl'               => 'required|array'
                                            ),
                                            array(
                                                'acl.required'      => 'You must give this user access to at least one thing on the "Access" tab.'
                                            ));

            if ($validator->passes()) {
                $user                   = new User;
                $user->installation     = Auth::user()->installation;
                $user->username         = Input::get('username');
                $user->name             = Input::get('name');
                $user->email            = Input::get('email');
                $user->title            = Input::get('title');
                $user->password         = Hash::make(Input::get('password'));

                if (Input::exists('flags')) {
                    foreach (Config::get('settings.acl_structure.flags') as $flag_key => $flag_title) {
                        if (in_array($flag_key, Input::get('flags')) === true) {
                            $user->{$flag_key}      = true;
                        } else {
                            $user->{$flag_key}      = false;
                        }
                    }
                }

                if (Input::hasFile('avatar')) {
                    $avatar             = HelpersUserController::saveUserAvatar();

                    if ($avatar) {
                        $user->avatar       = $avatar;
                    }
                }

                $user->save();

                if ($user->id) {
                    $user_privileges    = array();

                    foreach (Input::get('acl') as $acl_action_id) {
                        array_push($user_privileges, array('user_id' => $user->id, 'action' => $acl_action_id));
                    }

                    DB::table('users_acl')
                        ->insert($user_privileges);

                    return  Redirect::route('user')
                                ->with('success', Lang::get('success.user.create'));
                } else {
                    return  Redirect::route('user_create')
                                ->withInput(Input::except('password', 'password_confirm'))
                                ->with('failure', Lang::get('errors.user.create.general'));
                }
            } else {
                return  Redirect::route('user_create')
                            ->withInput(Input::except('password', 'password_confirm'))
                            ->withErrors($validator);
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.general.no_access.area'));
        }
    }

    public function postUserUpdate($id) {
        if (Auth::user()->admin) {
            if (User::accessible($id)) {
                $user       =   User::where('id', '=', $id)
                                    ->where('installation', '=', Auth::user()->installation)
                                    ->get();

                if (!$user->isEmpty()) {
                    $user       = $user->first();

                    $validator  = Validator::make(  Input::all(),
                                                    array(
                                                        'username'          => 'required|alpha|unique:users,username,' . $id . ',id,installation,' . Auth::user()->installation,
                                                        'name'              => 'required',
                                                        'email'             => 'required|email',
                                                        'acl'               => 'required|array'
                                                    ),
                                                    array(
                                                        'acl.required'      => 'You must give this user access to at least one thing on the "Access" tab.'
                                                    ));

                    if ($validator->passes()) {
                        $user->username         = Input::get('username');
                        $user->name             = Input::get('name');
                        $user->email            = Input::get('email');
                        $user->title            = Input::get('title');

                        if ((Input::get('password')) && (Input::get('password_confirm'))) {
                            if (Input::get('password') === Input::get('password_confirm')) {
                                $user->password     = Hash::make(Input::get('password'));
                            }
                        }

                        foreach (Config::get('settings.acl_structure.flags') as $flag_key => $flag_title) {
                            if ((Input::exists('flags')) && (in_array($flag_key, Input::get('flags')) === true)) {
                                $user->{$flag_key}      = true;
                            } else {
                                $user->{$flag_key}      = false;
                            }
                        }

                        if (Input::hasFile('avatar')) {
                            $avatar             = HelpersUserController::saveUserAvatar();

                            if ($avatar) {
                                $user->avatar       = $avatar;
                            }
                        }

                        $user->save();

                        // save ACL data
                        $acl_existing           = $user->acl->lists('action');

                        $acl_removed_actions    = array_diff($acl_existing, Input::get('acl'));
                        $acl_added_actions      = array_diff(Input::get('acl'), $acl_existing);

                        if (count($acl_removed_actions)) {
                            UserACL::where('user_id', '=', $id)->whereIn('action', $acl_removed_actions)->delete();
                        }
                        if (count($acl_added_actions)) {
                            DB::table('users_acl')
                                ->insert(   array_map(
                                                function($element) use ($id) {
                                                    return array('user_id' => $id, 'action' => $element);
                                                },
                                                $acl_added_actions
                                            ));
                        }

                        return  Redirect::route('user')
                                    ->with('success', Lang::get('success.user.update'));
                    } else {
                        return  Redirect::route('user_update', $id)
                                    ->withInput(Input::except('password', 'password_confirm'))
                                    ->withErrors($validator);
                    }
                } else {
                    return  Redirect::route('dashboard')
                                ->with('failure', Lang::get('errors.user.no_access.update'));
                }
            } else {
                return  Redirect::route('dashboard')
                            ->with('failure', Lang::get('errors.user.no_access.update'));
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.general.no_access.area'));
        }
    }

    public function getUserDelete($id) {
        if (Auth::user()->admin) {
            if (User::accessible($id)) {
                User::find($id)->delete();

                return  Redirect::route('user')
                            ->with('success', Lang::get('success.user.delete'));
            } else {
                return  Redirect::route('dashboard')
                            ->with('failure', Lang::get('errors.user.no_access.delete'));
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.general.no_access.area'));
        }
    }
}