<?php

class DashboardController extends Controller {
    public function getDashboardLanding() {
        return  View::make( 'dashboard/landing',
                            array(  'messages'      =>  SystemMessage::with('poster')
                                                            ->orderBy('created_at', 'desc')
                                                            ->get(),
                                    'jobs'          =>  (
                                                            (UserACL::can('special.graphics_queue.read.assigned_only'))
                                                            ?
                                                            GraphicsJob::where('assigned_to', '=', Auth::user()->id)
                                                                            ->with( array(  'customer'  =>  function($query) {
                                                                                                $query->where('installation', '=', Auth::user()->installation);
                                                                                            }
                                                                                        ))
                                                                            ->get()
                                                            :
                                                            GraphicsJob::with(  array(  'customer'  =>  function($query) {
                                                                                            $query->where('installation', '=', Auth::user()->installation);
                                                                                        }
                                                                                    ))
                                                                            ->get()
                                                        )
                                    ));
    }

    public function postTestEvent() {
        Event::fire(Config::get('settings.notifications.event'), array('type' => 'test', 'message' => 'This is a test message', 'test_id' => 1));
    }
}
