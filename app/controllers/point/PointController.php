<?php

class PointController extends Controller {
    public function getLanding() {
        if (UserACL::can('read.point')) {
            $users          =   User::where('installation', '=', Auth::user()->installation)
                                    ->get();
            $jobs           =   GraphicsJob::with(  array(  'customer'      =>  function($query) {
                                                                                    $query->where('installation', '=', Auth::user()->installation);
                                                                                },
                                                            'creator',
                                                            'assignee'))
                                    ->where('status', '=', AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Completed'));

            if (Input::get('assigned_to')) {
                $jobs->where('assigned_to', '=', Input::get('assigned_to'));
            }
            if ((Input::get('start')) && (Input::get('stop'))) {
                $jobs->whereBetween('updated_at', array(Input::get('start'), Input::get('stop')));
            }

            return  View::make( 'point/landing',
                                array(
                                    'jobs'      =>  $jobs->get(),
                                    'users'     =>  $users->lists('name', 'id')
                                ));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.point.no_access.read'));
        }
    }
}
