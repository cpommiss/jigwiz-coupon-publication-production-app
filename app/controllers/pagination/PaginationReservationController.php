<?php

class PaginationReservationController extends Controller {
    public function handlePaginationReservations() {
        if (UserACL::can('create.reservation')) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'media'             => 'required|numeric',
                                                'month'             => 'required|numeric',
                                                'year'              => 'required|numeric'
                                            ));

            if ($validator->passes()) {
                if (MediaType::accessible(Input::get('media'))) {
                    // handle adding new reservations
                    foreach (Input::all() as $key => $value) {
                        if (Str::contains($key, 'add-client-id-')) {
                            $parts          = explode('-', $key);

                            if (count($parts) === 4) {
                                $add_id         = intval($parts[3]);

                                if (Input::has('add-zone-id-' . $add_id)) {
                                    $add_client         = intval($value);
                                    $add_zone           = Input::get('add-zone-id-' . $add_id, 0);
                                    $add_page           = Input::get('add-page-' . $add_id, 0);

                                    if (($add_page > 0) && ($add_zone > 0) && (Zone::accessible($add_zone)) && ($add_client > 0) && (CRM::accessible($add_client))) {
                                        $reservation                = new PaginationReservation;
                                        $reservation->media         = Input::get('media');
                                        $reservation->zone          = $add_zone;
                                        $reservation->page          = $add_page;
                                        $reservation->crm_id        = $add_client;
                                        $reservation->reserved_by   = Auth::user()->id;
                                        $reservation->issue_date    = date('Y-m-d', gmmktime(0, 0, 0, Input::get('month'), 1, Input::get('year')));
                                        $reservation->save();
                                    }
                                }
                            }
                        }
                    }

                    // actual placements
                    $pagination_placements      =   Pagination::whereMonth('issue_date', '=', Input::get('month'))
                                                        ->whereYear('issue_date', '=', Input::get('year'))
                                                        ->where('media', '=', Input::get('media'))
                                                        ->with( 'pages.placements',
                                                                'pages.placements.job',
                                                                'pages.placements.job.customer',
                                                                'pages.placements.job.customer.rep');

                    // reservations
                    $pagination_reservations    =   PaginationReservation::with(    array(  'client',
                                                                                            'rep'))
                                                        ->where('media', '=', Input::get('media'))
                                                        ->whereMonth('issue_date', '=', Input::get('month'))
                                                        ->whereYear('issue_date', '=', Input::get('year'));

                    // reservations - get premium pages in requested zones
                    $pagination_premium_pages   =   DB::table('pagination')
                                                        ->join('pagination_pages', 'pagination.id', '=', 'pagination_pages.pagination_id')
                                                        ->where('pagination.media', '=', Input::get('media'))
                                                        ->whereMonth('pagination.issue_date', '=', Input::get('month'))
                                                        ->whereYear('pagination.issue_date', '=', Input::get('year'))
                                                        ->where('pagination_pages.premium', '=', true)
                                                        ->orderBy('pagination_pages.page', 'asc');


                    if (Input::get('zone')) {
                        $pagination_placements->where('zone', '=', Input::get('zone'));
                        $pagination_reservations->where('zone', '=', Input::get('zone'));
                        $pagination_premium_pages->where('zone', '=', Input::get('zone'));
                    }

                    $pagination_placements->get();
                    $pagination_reservations->get();

                    // process
                    $reservations               = array();

                    if (count($pagination_placements)) {
                        foreach ($pagination_placements->get() as $pagination) {
                            if (!isset($reservations[$pagination->zone])) {
                                $reservations[$pagination->zone]        = array();
                            }

                            foreach ($pagination->pages as $pagination_page) {
                                if (!isset($reservations[$pagination->zone][$pagination_page->page])) {
                                    $reservations[$pagination->zone][$pagination_page->page]        = array();
                                }

                                foreach ($pagination_page->placements as $placement) {
                                    array_push( $reservations[$pagination->zone][$pagination_page->page],
                                                array(
                                                    'id'            => $placement->id,
                                                    'type'          => Config::get('settings.pagination.reservations.types.placement'),
                                                    'customer'      => array(
                                                        'id'            => $placement->job->customer->id,
                                                        'name'          => $placement->job->customer->name
                                                    ),
                                                    'issue_date'    => $pagination->issue_date,
                                                    'rep'           => array(
                                                        'id'            => $placement->job->customer->rep->id,
                                                        'name'          => $placement->job->customer->rep->name
                                                    ),
                                                    'page'          => $pagination_page->page,
                                                    'page_special'  => 0,
                                                    'rows'          => $placement->rows,
                                                    'cols'          => $placement->cols,
                                                    'date'          => $placement->updated_at
                                                ));
                                }
                            }
                        }
                    }

                    if (count($pagination_reservations)) {
                        foreach ($pagination_reservations->get() as $reservation) {
                            if (!isset($reservations[$reservation->zone])) {
                                $reservations[$reservation->zone]       = array();
                            }
                            if (!isset($reservations[$reservation->zone][$reservation->page])) {
                                $reservations[$reservation->zone][$reservation->page]   = array();
                            }

                            // check for placements that are also reserved
                            $placement_found            = false;

                            foreach ($reservations[$reservation->zone][$reservation->page] as &$reservation_data) {
                                if ($reservation_data['customer']['id'] == $reservation->client->id) {
                                    $placement_found                = true;
                                    $reservation_data['type']       = Config::get('settings.pagination.reservations.types.reserved-and-placed');
                                    $reservation_data['rep']        = array(
                                        'id'            => $reservation->rep->id,
                                        'name'          => $reservation->rep->name
                                    );
                                    break;
                                }
                            }

                            if (!$placement_found) {
                                array_push( $reservations[$reservation->zone][$reservation->page],
                                            array(
                                                'id'            => $reservation->id,
                                                'type'          => Config::get('settings.pagination.reservations.types.reservation'),
                                                'customer'      => array(
                                                    'id'            => $reservation->client->id,
                                                    'name'          => $reservation->client->name
                                                ),
                                                'issue_date'    => $reservation->issue_date,
                                                'rep'           => array(
                                                    'id'            => $reservation->rep->id,
                                                    'name'          => $reservation->rep->name
                                                ),
                                                'page'          => $reservation->page,
                                                'page_special'  => $reservation->page_special,
                                                'date'          => $reservation->updated_at
                                            ));
                            }
                        }
                    }

                    // remove duplicates (reservations that also have placements)
                    ksort($reservations);

                    foreach ($reservations as $key => $data) {
                        ksort($reservations[$key]);
                    }

                    return  View::make( 'pagination/pagination-reservations',
                                        array(  'reservations'      =>  $reservations,
                                                'premium_pages'     =>  $pagination_premium_pages->get(),
                                                'mediatypes'        =>  MediaType::where('installation', '=', Auth::user()->installation)
                                                                            ->lists('name', 'id'),
                                                'zones'             =>  Zone::zonelist_with_codes(
                                                                            Zone::where('installation', '=', Auth::user()->installation)
                                                                                ->with('statedata')
                                                                                ->get()
                                                                        ),
                                                'customers'         =>  (   (!UserACL::can('special.crm.use.master'))
                                                                            ?
                                                                            CRM::where('installation', '=', Auth::user()->installation)
                                                                                ->where('type', '=', 'client')
                                                                                ->where('assigned_to', '=', Auth::user()->id)
                                                                                ->get()
                                                                            :
                                                                            CRM::where('installation', '=', Auth::user()->installation)
                                                                                ->where('type', '=', 'client')
                                                                                ->get()
                                                                        )
                                            ));
                } else {
                    return  Redirect::route('dashboard')
                                ->with('failure', Lang::get('errors.media_type.not_found'));
                }
            } else {
                return  View::make( 'pagination/pagination-reservations',
                                    array(  'mediatypes'        =>  MediaType::where('installation', '=', Auth::user()->installation)
                                                                        ->lists('name', 'id'),
                                            'zones'             =>  Zone::zonelist_with_codes(
                                                                        Zone::where('installation', '=', Auth::user()->installation)
                                                                            ->with('statedata')
                                                                            ->get()
                                                                    ),
                                            'customers'         =>  (   (!UserACL::can('special.crm.use.master'))
                                                                        ?
                                                                        CRM::where('installation', '=', Auth::user()->installation)
                                                                            ->where('assigned_to', '=', Auth::user()->id)
                                                                            ->get()
                                                                        :
                                                                        CRM::where('installation', '=', Auth::user()->installation)
                                                                            ->get()
                                                                    )
                                    ));
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.reservation.no_access.update'));
        }
    }

    public function getPaginationReservationDelete($id) {
        if (PaginationReservation::accessible($id, 'delete.reservation')) {
            PaginationReservation::find($id)->delete();

            return  Redirect::route('pagination_reservations')
                        ->with('success', Lang::get('success.reservation.delete'));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.reservation.no_access.delete'));
        }
    }
}
