<?php

class PaginationReportController extends Controller {
    public function handlePaginationReportUsage() {
        if (UserACL::can('read.pagination')) {
            $pagination         = array();
            $indexes            = array();

            $validator          = Validator::make(  Input::all(),
                                                    array(
                                                        'media'             => 'required|numeric|min:1',
                                                        'zone'              => 'required|numeric|min:1',
                                                        'month'             => 'required|numeric',
                                                        'year'              => 'required|numeric'
                                                    ));

            if ($validator->passes()) {
                if ((MediaType::accessible(Input::get('media'))) && (Zone::accessible(Input::get('zone')))) {
                    $pagination         = Pagination::with( array(
                                                                'pages'     => function($query) {
                                                                    $query->orderBy('page', 'asc');
                                                                },
                                                                'pages.placements',
                                                                'pages.placements.job',
                                                                'pages.placements.job.customer',
                                                                'pages.placements.job.files'    => function($query) {
                                                                    $query->where('proof', '=', true)->orderBy('updated_at', 'desc')->take(1);
                                                                }
                                                            ))
                                                            ->where('media', '=', Input::get('media'))
                                                            ->where('zone', '=', Input::get('zone'))
                                                            ->whereMonth('issue_date', '=', Input::get('month'))
                                                            ->whereYear('issue_date', '=', Input::get('year'))
                                                            ->first();
                    if (Input::get('zone')) {
                        $indexes            = HelpersPaginationController::getPaginationIssueIndexes(Input::get('media'), Input::get('zone'), Input::get('year'));
                    }
                } else {
                    return  Redirect::route('pagination')
                                ->with('failure', Lang::get('errors.general.no_access.media_zone'));
                }
            }

            return  View::make( 'pagination/pagination-report-usage',
                                array(  'pagination'        =>  $pagination,
                                        'indexes'           =>  $indexes,
                                        'mediatypes'        =>  MediaType::where('installation', '=', Auth::user()->installation)
                                                                    ->lists('name', 'id'),
                                        'zones'             =>  Zone::zonelist_with_codes(
                                                                    Zone::with('statedata')
                                                                        ->where('installation', '=', Auth::user()->installation)
                                                                        ->get()
                                                                ),
                                        'customers'         =>  CRM::where('installation', '=', Auth::user()->installation)
                                                                    ->get()
                                ));
        } else {
            return  Redirect::route('pagination')
                        ->with('failure', Lang::get('errors.pagination.no_access.read'));
        }
    }
}
