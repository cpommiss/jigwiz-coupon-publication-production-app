<?php

class PaginationBatchController extends Controller {
    public function getPaginationBatch($id) {
        if (MediaType::accessible($id, 'special.pagination.create.batch_setup')) {
            $mediatype          =   MediaType::where('id', '=', $id)
                                        ->where('installation', '=', Auth::user()->installation)
                                        ->get();

            if (!$mediatype->isEmpty()) {
                $mediatype          = $mediatype->first();

                return  View::make( 'pagination/pagination-batch',
                                    array(  'mediatype'     => $mediatype,
                                            'zones'         =>  Zone::zonelist_with_codes(
                                                                    Zone::with('statedata')
                                                                        ->where('installation', '=', Auth::user()->installation)
                                                                        ->get()
                                                                )
                                    ));
            } else {
                return  Redirect::route('pagination')
                            ->with('failure', Lang::get('errors.media_type.not_found'));
            }
        } else {
            return  Redirect::route('pagination')
                        ->with('failure', Lang::get('errors.pagination.no_access.batch'));
        }
    }

    public function postPaginationBatch($id) {
        if (MediaType::accessible($id, 'special.pagination.create.batch_setup')) {
            $mediatype          =   MediaType::where('id', '=', $id)
                                        ->where('installation', '=', Auth::user()->installation)
                                        ->get();

            if (!$mediatype->isEmpty()) {
                $mediatype          = $mediatype->first();

                return  View::make( 'pagination/pagination-batch-process',
                                    array(  'mediatype'     =>  $mediatype,
                                            'zones'         =>  Zone::zonelist_with_codes(
                                                                    Zone::with('statedata')
                                                                        ->where('installation', '=', Auth::user()->installation)
                                                                        ->get()
                                                                )
                                    ));
            } else {
                return  Redirect::route('pagination')
                            ->with('failure', Lang::get('errors.media_type.not_found'));
            }
        } else {
            return  Redirect::route('pagination')
                        ->with('failure', Lang::get('errors.pagination.no_access.batch'));
        }
    }
}
