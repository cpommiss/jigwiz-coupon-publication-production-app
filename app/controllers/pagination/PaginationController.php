<?php

class PaginationController extends Controller {
    public function getLanding() {
        if (UserACL::can(array('read.pagination', 'read.media_type'))) {
            return  View::make( 'pagination/landing',
                                array(  'mediatypes'    =>  MediaType::where('installation', '=', Auth::user()->installation)
                                                                ->get()
                                ));
        } else {
            return  Redirect::route('pagination')
                        ->with('failure', Lang::get('errors.pagination.no_access.read'));
        }
    }

    public function getPaginationUpdate($id) {
        if (MediaType::accessible($id, 'update.pagination')) {
            $mediatype          =   MediaType::where('id', '=', $id)
                                        ->where('installation', '=', Auth::user()->installation)
                                        ->get();

            if (!$mediatype->isEmpty()) {
                $mediatype          = $mediatype->first();

                return  View::make( 'pagination/pagination-update',
                                    array(  'mediatype'     =>  $mediatype,
                                            'zones'         =>  Zone::zonelist_with_codes(
                                                                    Zone::with('statedata')
                                                                        ->where('installation', '=', Auth::user()->installation)
                                                                        ->get()
                                                                )
                                    ));
            } else {
                return  Redirect::route('pagination')
                            ->with('failure', Lang::get('errors.media_type.not_found'));
            }
        } else {
            return  Redirect::route('pagination')
                        ->with('failure', Lang::get('errors.pagination.no_access.update'));
        }
    }
}
