<?php

class HelpersGraphicsJobController extends Controller {
    public function postMultiFileUploads() {
        if (UserACL::can('create.graphics_queue')) {
            if ((Input::hasFile('upl')) && (Input::has('key'))) {
                $original_filename  = Input::file('upl')->getClientOriginalName();
                $extension          = Input::file('upl')->getClientOriginalExtension();

                if (in_array(strtolower($extension), Config::get('settings.graphics_queue.files.allowed_extensions'))) {
                    $upload_path                    = public_path() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . 'installations' . DIRECTORY_SEPARATOR . Auth::user()->installation . DIRECTORY_SEPARATOR . 'jobs' . DIRECTORY_SEPARATOR . Input::get('key') . DIRECTORY_SEPARATOR;
                    $upload_filename                = uniqid() . '.' . $extension;

                    if (!file_exists($upload_path)) {
                        mkdir($upload_path, 0777, true);
                    }

                    if (Input::file('upl')->move($upload_path, $upload_filename)) {
                        return  Response::json( array(
                                                    'status'    =>  'success',
                                                    'job_key'   =>  Input::get('key'),
                                                    'file'      =>  $upload_filename,
                                                    'original'  =>  $original_filename
                                                ));
                    } else {
                        return  Response::json( array(
                                                    'status'    =>  'error',
                                                    'reason'    =>  'Unable to move file into job folder'
                                                ));
                    }
                } else {
                    return  Response::json( array(
                                                'status'    =>  'error',
                                                'reason'    =>  Lang::get(  'errors.graphics_queue.upload.disallowed_file_type',
                                                                            array(
                                                                                'original'      =>  $original_filename,
                                                                                'extension'     =>  $extension,
                                                                                'allowed_list'  =>  implode(', ', Config::get('settings.graphics_queue.files.allowed_extensions'))
                                                                            ))
                                            ));
                }
            }

            return  Response::json( array(
                                        'status'    =>  'error',
                                        'reason'    =>  Lang::get('errors.graphics_queue.upload.no_file_found')
                                    ));
        } else {
            return  Response::json( array(
                                        'status'    =>  'error',
                                        'reason'    =>  Lang::get('errors.graphics_queue.no_access.create')
                                    ));
        }
    }

    public function postAnnotationCreate() {
        $validator          = Validator::make(  Input::all(),
                                                array(
                                                    'file_id'           => 'required|numeric|min:1',
                                                    'data'              => 'required'
                                                ));

        if ($validator->passes()) {
            if (GraphicsJobFile::accessible(Input::get('file_id'))) {
                $data                   = json_decode(Input::get('data'));

                if ($data) {
                    $annotation             = new GraphicsJobFileAnnotation;
                    $annotation->file_id    = Input::get('file_id');
                    $annotation->created_by = ((Auth::check()) ? Auth::user()->id : 0);
                    $annotation->data       = Input::get('data');
                    $annotation->save();

                    if ($annotation->id) {
                        // add annotation id and posted by to data object
                        $data->annotation_id    = $annotation->id;
                        $data->created_by       = $annotation->created_by;
                        $data->created_on       = date(Config::get('settings.datetime.formats.graphics_queue_notes'));

                        $annotation->data       = json_encode($data);
                        $annotation->save();

                        // lookup job to see if we need to change the status to "In Revisions" since an annotation has been posted on a proof
                        if ($annotation->file->proof) {
                            $job                    = $annotation->file->job;

                            if ($job->status != AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Unassigned')) {
                                $job->previous_status   = $job->status;
                                $job->status            = AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'In Revisions');
                                $job->save();
                            }
                        }

                        return  Response::json( array(
                                                    'status'    =>  'success',
                                                    'id'        =>  $annotation->id
                                                ));
                    } else {
                        return  Response::json( array(
                                                    'status'    =>  Lang::get('errors.graphics_queue.annotations.general')
                                                ));
                    }
                } else {
                    return  Response::json( array(
                                                'status'    =>  Lang::get('errors.graphics_queue.annotations.data_missing')
                                            ));
                }
            } else {
                return  Response::json( array(
                                            'status'    =>  Lang::get('errors.graphics_queue.annotations.not_found')
                                        ));
            }
        } else {
            return  Response::json( array(
                                        'status'    =>  Lang::get('errors.graphics_queue.annotations.data_missing')
                                    ));
        }
    }

    public function postAnnotationUpdate() {
        $validator          = Validator::make(  Input::all(),
                                                array(
                                                    'annotation_id' => 'required|numeric|min:1',
                                                    'data'          => 'required'
                                                ));

        if ($validator->passes()) {
            $annotation         =   GraphicsJobFileAnnotation::where('id', '=', Input::get('annotation_id'))
                                        ->where('created_by', '=', ((Auth::check()) ? Auth::user()->id : 0))
                                        ->get();

            if ((!$annotation->isEmpty()) && (GraphicsJobFile::accessible($annotation->first()->file_id))) {
                $annotation             = $annotation->first();
                $annotation->data       = Input::get('data');
                $annotation->save();

                return  Response::json( array(
                                            'status'    =>  'success'
                                        ));
            } else {
                return  Response::json( array(
                                            'status'    =>  Lang::get('errors.graphics_queue.annotations.not_found')
                                        ));
            }
        } else {
            return  Response::json( array(
                                        'status'    =>  Lang::get('errors.graphics_queue.annotations.data_missing')
                                    ));
        }
    }

    public function postAnnotationDelete() {
        $validator          = Validator::make(  Input::all(),
                                                array(
                                                    'annotation_id' => 'required|numeric|min:1'
                                                ));

        if ($validator->passes()) {
            $annotation         =   GraphicsJobFileAnnotation::where('id', '=', Input::get('annotation_id'))
                                        ->where('created_by', '=', ((Auth::check()) ? Auth::user()->id : 0))
                                        ->get();

            if ((!$annotation->isEmpty()) && (GraphicsJobFile::accessible($annotation->first()->file_id))) {
                $annotation             = $annotation->first();

                if ($annotation->created_by == ((Auth::check()) ? Auth::user()->id : 0)) {
                    $annotation->delete();

                    return  Response::json( array(
                                                'status'    =>  'success'
                                            ));
                } else {
                    return  Response::json( array(
                                                'status'    =>  Lang::get('errors.graphics_queue.annotations.not_found')
                                            ));
                }
            } else {
                return  Response::json( array(
                                            'status'    =>  Lang::get('errors.graphics_queue.annotations.not_found')
                                        ));
            }
        } else {
            return  Response::json( array(
                                        'status'    =>  Lang::get('errors.graphics_queue.annotations.data_missing')
                                    ));
        }
    }

    public function getJobPushNoteToClient($id) {
        if (GraphicsJobNote::accessible($id, 'special.graphics_queue.push.note_to_client')) {
            $note_source            =   GraphicsJobNote::with('job', 'job.customer', 'job.customer.attrs')
                                            ->where('id', '=', $id)
                                            ->get();

            if (!$note_source->isEmpty()) {
                $note_source            = $note_source->first();
                $note_source->pushed    = true;
                $note_source->save();

                $note                   = $note_source->replicate();

                $note->created_by       = Auth::user()->id;
                $note->visibility       = AppUtils::config_get_ordinal_by_value(Config::get('settings.graphics_queue.notes.visibility_types'), 'button_label', 'Client Notes');
                $note->pushed           = false;
                $note->save();

                // did this note have a file attachment?
                $file                   =   GraphicsJobFile::where('note_id', '=', $id)
                                                ->get();

                if (!$file->isEmpty()) {
                    $note_file              = $file->first()->replicate();

                    $note_file->created_by  = Auth::user()->id;
                    $note_file->note_id     = $note->id;
                    $note_file->save();
                }

                if (strlen($note_source->job->customer->attrs->email)) {
                    Mail::send( 'emails.graphics-queue.client-new-note',
                                array(
                                    'job_title'         => $note_source->job->title,
                                    'job_url'           => link_to('job/view/' . $note_source->job->key),
                                    'customer_name'     => $note_source->job->customer->name
                                ),
                                function($message) use ($note_source) {
                                    $message
                                        ->to($note_source->job->customer->attrs->email)
                                        ->subject(  Lang::get(  'emails.graphics_queue.note_posted.client',
                                                                array(
                                                                    'customer'      =>  $note_source->job->customer->name
                                                                )
                                                    ));
                                });
                }

                return  Redirect::route('graphics_queue_job_details', $note->job_id)
                            ->with('success', Lang::get('success.graphics_queue.note_push'));
            } else {
                return  Redirect::route('graphics_queue')
                            ->with('failure', Lang::get('errors.graphics_queue.note.not_found'));
            }
        } else {
            return  Redirect::route('graphics_queue')
                        ->with('failure', Lang::get('errors.graphics_queue.no_access.push_note'));
        }
    }

    public static function getJobParticipants($id) {
        $participants       = array();
        $job                = GraphicsJob::with(    array(  'customer',
                                                            'notes' => function($query) {
                                                                $query->where('created_by', '!=', 0);
                                                            }
                                                    ))
                                            ->where('id', '=', $id)
                                            ->get();

        if (!$job->isEmpty()) {
            $job                = $job->first();

            if (!$job->notes->isEmpty()) {
                foreach ($job->notes as $note) {
                    if (($note->poster->email) && (in_array($note->poster->email, $participants) === false)) {
                        $participants[$note->poster->id]    = $note->poster->email;
                    }
                }
            }

            // get art directors
            $directors          = User::where('installation', '=', $job->customer->installation)
                                        ->with( array(  'acl'   =>  function($query) {
                                                                        $query->where('action', '=', Config::get('acl.special.system.act_as.artist_director'));
                                                                    }
                                                ))
                                        ->get();

            if (!$directors->isEmpty()) {
                foreach ($directors as $director) {
                    if (($director->email) && (in_array($director->email, $participants) === false)) {
                        $participants[$director->id]    = $director->email;
                    }
                }
            }

            // get administrators
            $administrators     = User::where('installation', '=', $job->customer->installation)
                                    ->where('admin', '=', true)
                                    ->get();

            if (!$administrators->isEmpty()) {
                foreach ($administrators as $administrator) {
                    if (($administrator->email) && (in_array($administrator->email, $participants) === false)) {
                        $participants[$administrator->id]   = $administrator->email;
                    }
                }
            }
        }

        return $participants;
    }
}