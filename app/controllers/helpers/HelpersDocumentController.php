<?php

class HelpersDocumentController extends Controller {
    public function getDocumentPush($id) {
        if (UserACL::can('special.document.email.to_client')) {
            $document       = Document::where('id', '=', $id)
                                ->get();

            $customers      = CRM::where('installation', '=', Auth::user()->installation)
                                ->where('type', '=', 'client')
                                ->get();

            if ((!$document->isEmpty()) && (!$customers->isEmpty())) {
                $document       = $document->first();

                return  View::make( 'document.modals.push',
                                    array(
                                        'document'      =>  $document,
                                        'customers'     =>  $customers
                                    ));
            } else {
                return  View::make('document.modals.push-no-access');
            }
        } else {
            return  View::make('document.modals.push-no-access');
        }
    }

    public function postDocumentPush() {
        if (UserACL::can('special.document.email.to_client')) {
            $validator      = Validator::make(  Input::all(),
                                                array(
                                                    'document'      => 'required|numeric|min:1',
                                                    'customer'      => 'required|numeric|min:1'
                                                ));

            if (($validator->passes()) && (Document::accessible(Input::get('document'))) && (CRM::accessible(Input::get('customer')))) {
                $document       =   Document::find(Input::get('document'))
                                        ->get();
                $crm            =   CRM::where('id', '=', Input::get('customer'))
                                        ->with('attrs')
                                        ->get();

                if ((!$document->isEmpty()) && (!$crm->isEmpty())) {
                    $document       = $document->first();
                    $crm            = $crm->first();

                    if ($crm->attrs->email) {
                        Mail::send( 'emails.document.push',
                                    array(
                                        'customer_name'     => $crm->name,
                                        'sender_name'       => Auth::user()->name,
                                        'document_title'    => $document->title,
                                        'document_notes'    => Input::get('notes'),
                                        'document_url'      => asset('content/installations/' . $document->installation . '/documents/' . $document->file)
                                    ),
                                    function($message) use ($crm) {
                                        $message
                                            ->to($crm->attrs->email)
                                            ->subject(  Lang::get(  'emails.document.push.client',
                                                                    array(
                                                                        'customer'  =>  $crm->name
                                                                    )
                                                        ));
                                    });

                        return  Response::json( array(
                                                    'success'   =>  true,
                                                    'email'     =>  $crm->attrs->email
                                                ));
                    } else {
                        return  Response::json( array(
                                                    'success'   =>  false
                                                ));
                    }
                }
            }

            return  Response::json( array(
                                        'success'   =>  false
                                    ));
        } else {
            return  Response::json( array(
                                        'success'   =>  false
                                    ));
        }
    }
}
