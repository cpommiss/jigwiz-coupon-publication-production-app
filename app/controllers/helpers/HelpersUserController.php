<?php

class HelpersUserController extends Controller {
    public static function saveUserAvatar() {
        $avatar                 = '';

        $original_filename      = Input::file('avatar')->getClientOriginalName();
        $extension              = Input::file('avatar')->getClientOriginalExtension();

        if (in_array(strtolower($extension), Config::get('settings.user.files.allowed_image_extensions'))) {
            $upload_path                    = public_path() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . 'installations' . DIRECTORY_SEPARATOR . Auth::user()->installation . DIRECTORY_SEPARATOR . 'avatars' . DIRECTORY_SEPARATOR;
            $upload_filename                = uniqid() . '.' . $extension;

            if (!file_exists($upload_path)) {
                mkdir($upload_path, 0777, true);
            }

            if (Input::file('avatar')->move($upload_path, $upload_filename)) {
                $avatar                         = $upload_filename;
            }
        }

        return $avatar;
    }
}
