<?php

class HelpersCRMAuthorizationController extends Controller {
    public function pushCRMAuthorization() {
        if (UserACL::can('special.crm.send.publishing_auth')) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'client'            => 'required|numeric|min:1',
                                                'media'             => 'required|numeric|min:1',
                                                'month'             => 'required|numeric|min:1',
                                                'year'              => 'required|numeric|min:1970'
                                            ));

            if (($validator->passes()) && (CRM::accessible(Input::get('client'))) && (MediaType::accessible(Input::get('media')))) {
                $crm        =   CRM::with('attrs')
                                    ->where('id', '=', Input::get('client'))
                                    ->where('installation', '=', Auth::user()->installation)
                                    ->get();

                if (!$crm->isEmpty()) {
                    $crm        =   $crm->first();

                    $crm_auth   =   CRMAuthorization::where('crm_id', '=', $crm->id)
                                        ->whereMonth('auth_date', '=', Input::get('month'))
                                        ->whereYear('auth_date', '=', Input::get('year'))
                                        ->get();

                    if (!$crm_auth->isEmpty()) {
                        // existing authorization record found already
                        if ($crm_auth->first()->authorized) {
                            return  Response::json( array(
                                                        'success'   =>  false,
                                                        'message'   =>  Lang::en('errors.crm.authorization.already_authorized')
                                                    ));
                        } else {
                            // resend
                            if ($crm->attrs->email) {
                                Mail::send( 'emails.crm.client-authorization',
                                            array(
                                                'customer_name'     => $crm->name,
                                                'auth_url'          => link_to(route('client_view_crm_request_authorization', $crm_auth->first()->key))
                                            ),
                                            function($message) use ($crm) {
                                                $message
                                                    ->to($crm->attrs->email)
                                                    ->subject(  Lang::get(  'emails.crm.authorization.client_resend',
                                                                            array(
                                                                                'customer'  =>  $crm->name
                                                                            )));
                                            });
                            } else {
                                return  Response::json( array(
                                                            'success'   =>  false,
                                                            'message'   =>  Lang::en('errors.crm.authorization.no_email_on_file')
                                                        ));
                            }
                        }
                    } else {
                        if ($crm->attrs->email) {
                            // create authorization record
                            $crm_auth               = new CRMAuthorization;
                            $crm_auth->crm_id       = Input::get('client');
                            $crm_auth->media        = Input::get('media');
                            $crm_auth->authorized   = false;
                            $crm_auth->auth_date    = date('Y-m-d', gmmktime(0, 0, 0, Input::get('month'), 1, Input::get('year')));
                            $crm_auth->key          = CRMAuthorization::generateAuthorizationKe0y();
                            $crm_auth->save();

                            Mail::send( 'emails.crm.client-authorization',
                                        array(
                                            'customer_name'     => $crm->name,
                                            'auth_url'          => link_to(route('client_view_crm_request_authorization', $crm_auth->key))
                                        ),
                                        function($message) use ($crm) {
                                            $message
                                                ->to($crm->attrs->email)
                                                ->subject(  Lang::get(  'emails.crm.authorization.client',
                                                                        array(
                                                                            'customer'  =>  $crm->name
                                                                        )));
                                        });
                        } else {
                            return  Response::json( array(
                                                        'success'   =>  false,
                                                        'message'   =>  Lang::en('errors.crm.authorization.no_email_on_file')
                                                    ));
                        }
                    }

                    return  Response::json( array(
                                                'success'   =>  true,
                                                'message'   =>  Lang::get(  'success.crm.authorization.sent',
                                                                            array(
                                                                                'customer'      =>  $crm->name
                                                                            ))
                                            ));
                } else {
                    return  Response::json( array(
                                                'success'   =>  false,
                                                'message'   =>  Lang::en('errors.crm.not_found')
                                            ));
                }
            } else {
                return  Response::json( array(
                                            'success'   =>  false,
                                            'message'   =>  Lang::en('errors.crm.authorization.data_missing')
                                        ));
            }
        } else {
            return  Response::json( array(
                                        'success'   =>  false,
                                        'message'   =>  Lang::en('errors.crm.no_access.authorization')
                                    ));
        }
    }

    public static function getCRMNNotifiees($id) {
        $notifiees          =   array();
        $crm                =   CRM::with('rep')
                                    ->where('id', '=', $id)
                                    ->get();

        if (!$crm->isEmpty()) {
            $crm                = $crm->first();

            if ((!empty($crm->rep)) && ($crm->rep->email)) {
                $notifiees[$crm->rep->id]   = $crm->rep->email;
            }

            // get administrators
            $administrators     = User::where('installation', '=', $crm->installation)
                                    ->where('admin', '=', true)
                                    ->get();

            if (!$administrators->isEmpty()) {
                foreach ($administrators as $administrator) {
                    if (($administrator->email) && (in_array($administrator->email, $notifiees) === false)) {
                        $notifiees[$administrator->id]  = $administrator->email;
                    }
                }
            }
        }

        return $notifiees;
    }
}
