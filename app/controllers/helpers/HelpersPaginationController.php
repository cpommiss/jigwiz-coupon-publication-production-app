<?php

class HelpersPaginationController extends Controller {
    public static function getPaginationIssueIndexes() {
        if ((MediaType::accessible(Input::get('media'))) && (Zone::accessible(Input::get('zone')))) {
            return $issues         =    DB::table('pagination')
                                            ->where('media', '=', Input::get('media'))
                                            ->where('zone', '=', Input::get('zone'))
                                            ->whereYear('issue_date', '=', Input::get('year'))
                                            ->get();
        } else {
            return false;
        }
    }

    public function getPaginationData() {
        if ((MediaType::accessible(Input::get('media'))) && (Zone::accessible(Input::get('zone')))) {
            $response       =   array(  'found'         =>  false,
                                        'issues'        =>  HelpersPaginationController::getPaginationIssueIndexes(),
                                        'pagination'    =>  array());

            $pagination     =   Pagination::where('media', '=', Input::get('media'))
                                    ->where('zone', '=', Input::get('zone'))
                                    ->whereMonth('issue_date', '=', Input::get('month'))
                                    ->whereYear('issue_date', '=', Input::get('year'))
                                    ->first();

            if ($pagination) {
                $response['found']          = true;
                $response['pagination']     =$pagination->toArray();
            }

            return  Response::json($response);
        } else {
            return  Response::json(array());
        }
    }

    public function putPaginationData() {
        $response       = array('success' => false);

        if (UserACL::can('update.pagination')) {
            $validator      = Validator::make(  Input::all(),
                                                array(
                                                    'media'             => 'required|numeric|min:1',
                                                    'zone'              => 'required|numeric|min:1',
                                                    'month'             => 'required|numeric|min:1',
                                                    'year'              => 'required|numeric|min:1970'
                                                ));

            if (($validator->passes()) && (MediaType::accessible(Input::get('media'))) && (Zone::accessible(Input::get('zone'))) && (Input::get('data'))) {
                $media          =   MediaType::where('id', '=', Input::get('media'))
                                        ->where('installation', '=', Auth::user()->installation)
                                        ->get();

                if (!$media->isEmpty()) {
                    $media          =   $media->first();

                    $pagination     =   Pagination::where('media', '=', Input::get('media'))
                                            ->where('zone', '=', Input::get('zone'))
                                            ->whereMonth('issue_date', '=', Input::get('month'))
                                            ->whereYear('issue_date', '=', Input::get('year'))
                                            ->get();

                    if (!$pagination->isEmpty()) {
                        $pagination                 = $pagination->first();
                        $pagination->issue_index    = Input::get('issue_index');
                        $pagination->save();
                    } else {
                        $pagination                 = new Pagination;
                        $pagination->media          = Input::get('media');
                        $pagination->zone           = Input::get('zone');
                        $pagination->issue_date     = date('Y-m-d', gmmktime(0, 0, 0, Input::get('month'), 1, Input::get('year')));
                        $pagination->issue_index    = Input::get('issue_index');
                        $pagination->rows           = $media->max_rows;
                        $pagination->cols           = $media->max_cols;
                        $pagination->width          = $media->width;
                        $pagination->height         = $media->height;
                        $pagination->color          = $media->color;
                        $pagination->save();
                    }

                    if ($pagination->id) {
                        $data           =   Input::get('data');

                        if (count($data)) {
                            foreach ($data as $page_data) {
                                $page_id                = 0;

                                if ((intval($page_data['id']) === 0) && ($page_data['order']['current'] != -1)) {
                                    // new page
                                    $page                   = new PaginationPage;
                                    $page->pagination_id    = $pagination->id;
                                    $page->page             = $page_data['order']['current'];
                                    $page->premium          = $page_data['premium']['current'];
                                    $page->width            = $page_data['dimensions']['width'];
                                    $page->height           = $page_data['dimensions']['height'];
                                    $page->rows             = $page_data['dimensions']['rows'];
                                    $page->cols             = $page_data['dimensions']['cols'];
                                    $page->save();

                                    $page_id                = $page->id;
                                } else {
                                    // existing page
                                    $page_id                = intval($page_data['id']);

                                    // TODO: store old dimensions and compare against that to add to these conditionals
                                    /*
                                    if (($page_data['premium']['current'] != $page_data['premium']['original']) ||
                                        ($page_data['order']['current'] != $page_data['order']['original'])) {
                                    */
                                    $page                   = PaginationPage::where('id', '=', $page_id)
                                                                ->get();

                                    if (!$page->isEmpty()) {
                                        $page                   = $page->first();

                                        if ($page_data['order']['current'] == -1) {
                                            $page->delete();

                                            PaginationPagePlacement::where('pagination_page_id', '=', $page_id)->delete();
                                        } else {
                                            $page->premium          = $page_data['premium']['current'];
                                            $page->page             = $page_data['order']['current'];
                                            $page->width            = $page_data['dimensions']['width'];
                                            $page->height           = $page_data['dimensions']['height'];
                                            $page->rows             = $page_data['dimensions']['rows'];
                                            $page->cols             = $page_data['dimensions']['cols'];
                                            $page->save();
                                        }
                                    }
                                }

                                if (($page_id > 0) && (isset($page_data['placements'])) && (is_array($page_data['placements'])) && (count($page_data['placements']))) {
                                    foreach ($page_data['placements'] as $placement_data) {
                                        if (intval($placement_data['id']) === -1) {
                                            if (UserACL::can('delete.pagination')) {
                                                PaginationPagePlacement::where('pagination_page_id', '=', $page_id)->delete();
                                            }
                                        } else if (intval($placement_data['id']) === 0) {
                                            // new placement
                                            $placement_start_row            = intval($placement_data['start_row']);
                                            $placement_end_row              = intval($placement_data['end_row']);
                                            $placement_start_col            = intval($placement_data['start_col']);
                                            $placement_end_col              = intval($placement_data['end_col']);

                                            $placement                      = new PaginationPagePlacement;
                                            $placement->pagination_page_id  = $page_id;
                                            $placement->job_id              = $placement_data['job']['id'];
                                            $placement->rows                = (($placement_start_row === $placement_end_row) ? 1 : (($placement_end_row - $placement_start_row) + 1));
                                            $placement->cols                = (($placement_start_col === $placement_end_col) ? 1 : (($placement_end_col - $placement_start_col) + 1));
                                            $placement->rows_offset         = $placement_start_row;
                                            $placement->cols_offset         = $placement_start_col;
                                            $placement->save();
                                        } else {
                                            // TODO: update placement (maybe we'll allow users to drag the placements around the grid on the front-end sometime, or change the rows/cols of the placement)
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $response['success']        = true;
                }
            }
        }

        return Response::json($response);
    }

    public function postPaginationPremiumStatus($page_id) {
        $response       = array('success' => false);

        if (PaginationPage::accessible($page_id, 'special.pagination.assign.premium_page_status')) {
            $page           = PaginationPage::find($page_id);

            if ($page) {
                $validator      = Validator::make(  Input::all(),
                                                    array(
                                                        'premium'           => 'required|numeric'
                                                    ));

                if ($validator->passes()) {
                    $page->premium          = Input::get('premium');
                    $page->save();

                    $response['success']    = true;
                }
            }
        }

        return Response::json($response);
    }

    public function postPaginationBatch() {
        $response       = array('success' => false, 'message' => 'Failed.');

        if (UserACL::can('special.pagination.create.batch_setup')) {
            $validator      = Validator::make(  Input::all(),
                                                array(
                                                    'media'             => 'required|numeric|min:1',
                                                    'zone'              => 'required|numeric|min:1',
                                                    'month'             => 'required|numeric|min:1',
                                                    'year'              => 'required|numeric|min:1970',
                                                    'pages'             => 'required|numeric|min:2'
                                                ));

            if (($validator->passes()) && (MediaType::accessible(Input::get('media'))) && (Zone::accessible(Input::get('zone')))) {
                $pagination_start       = 0;        // we'll start inserting with the first page in nearly all cases, unless there's existing (blank) pagination found
                $pagination_id          = 0;        // this will hold the id of an existing pagination should we decide to append this batch to an existing pagination

                // check to see if this zone has already been paginated in any way
                $pagination_lookup  = Pagination::where('media', '=', Input::get('media'))
                                        ->where('zone', '=', Input::get('zone'))
                                        ->whereMonth('issue_date', '=', Input::get('month'))
                                        ->whereYear('issue_date', '=', Input::get('year'))
                                        ->get();

                if (!$pagination_lookup->isEmpty()) {
                    $pagination_lookup  = $pagination_lookup->first();
                    $pagination_id      = $pagination_lookup->id;

                    $pages_lookup       =   PaginationPage::where('pagination_id', '=', $pagination_lookup->id)
                                                ->get();

                    if ($pages_lookup->isEmpty()) {
                        // no pages found, but there's a pagination record found that we want to use
                        $response['success']    = true;
                    } else {
                        // we have pages, but do any of them have placements?
                        $placements_found   = false;

                        foreach ($pages_lookup as $page) {
                            if ($page->placements->isEmpty()) {
                                if ($page->page > $pagination_start) {
                                    $pagination_start       = $page->page;
                                }
                            } else {
                                $placements_found   = true;
                                break;
                            }
                        }

                        if ($placements_found) {
                            $response['success']    = false;
                            $response['message']    = Lang::get('errors.pagination.batch.pagination_exists');
                        } else {
                            $response['success']    = true;
                        }
                    }
                } else {
                    $response['success']    = true;
                }

                if ($response['success']) {
                    $media                      =   MediaType::where('id', '=', Input::get('media'))
                                                        ->where('installation', '=', Auth::user()->installation)
                                                        ->get();

                    if (!$media->isEmpty()) {
                        $media                      = $media->first();

                        if ($pagination_id === 0) {
                            $pagination                 = new Pagination;
                            $pagination->media          = $media->id;
                            $pagination->zone           = Input::get('zone');
                            $pagination->issue_date     = date('Y-m-d', gmmktime(0, 0, 0, Input::get('month'), 1, Input::get('year')));
                            $pagination->rows           = $media->max_rows;
                            $pagination->cols           = $media->max_cols;
                            $pagination->width          = $media->width;
                            $pagination->height         = $media->height;
                            $pagination->color          = $media->color;
                            $pagination->save();

                            $pagination_id              = $pagination->id;
                        }

                        if ($pagination_id) {
                            for ($counter = $pagination_start; $counter < ($pagination_start + (intval(Input::get('pages')) * 2)); $counter++) {
                                $page                   = new PaginationPage;
                                $page->pagination_id    = $pagination_id;
                                $page->page             = $counter + 1;
                                $page->premium          = false;
                                $page->width            = $media->width;
                                $page->height           = $media->height;
                                $page->rows             = $media->max_rows;
                                $page->cols             = $media->max_cols;
                                $page->save();
                            }
                        } else {
                            $response['success']    = false;
                            $response['message']    = Lang::get('errors.pagination.batch.partial');
                        }
                    } else {
                        $response['success']    = false;
                        $response['message']    =   Lang::get('errors.media_type.not_found');
                    }
                }
            } else {
                $response['message']    =   Lang::get('errors.pagination.batch.validation');
            }

            if ($response['success']) {
                $response['message']    =   Lang::get(  'success.pagination.batch_add',
                                                        array(
                                                            'pages'     =>  (intval(Input::get('pages')) * 2)
                                                        ));
            }
        } else {

        }

        return Response::json($response);
    }

    public function getCompletedJobs($media_id) {
        if (MediaType::accessible($media_id, 'update.pagination')) {
            $data           = array();
            $customers      = array();
            $media          = MediaType::find($media_id);

            if ($media) {
                $jobs           =   GraphicsJob::with('customer')
                                        ->where('media', '=', $media_id)
                                        ->where('status', '=', AppUtils::config_get_key_by_value(Config::get('settings.graphics_queue.statuses'), 'Completed'))
                                        ->get();

                if (!$jobs->isEmpty()) {
                    foreach ($jobs as $job) {
                        $proof_image            = '';

                        if (!$job->files->isEmpty()) {
                            foreach ($job->files as $job_file) {
                                if ($job_file->proof) {
                                    $proof_image        = $job_file->file;
                                }
                            }
                        }

                        if (!isset($customers[$job->crm_id])) {
                            $customers[$job->crm_id]        = $job->customer->name;
                        }

                        array_push( $data,
                                    array(
                                        'id'        => $job->id,
                                        'key'       => $job->key,
                                        'title'     => $job->title,
                                        'crm_id'    => $job->crm_id,
                                        'color'     => $job->color_type,
                                        'proof'     => $proof_image
                                    ));
                    }
                }
            }

            return  View::make( 'pagination/modals/pagination-update-select-job',
                                array(
                                    'media'         =>  $media,
                                    'customers'     =>  $customers,
                                    'data'          =>  $data
                                ));
        } else {
            return  View::make('pagination/modals/pagination-update-no-access');
        }
    }
}
