<?php

class HelpersNotificationController extends Controller {
    public function postDismissAll() {
        Notification::where('assigned_to', '=', Auth::user()->id)
            ->where('notified', '=', false)
            ->update(array(
                'notified'      =>  true
            ));
    }
}
