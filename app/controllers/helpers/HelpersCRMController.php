<?php

class HelpersCRMController extends Controller {
    public function getCRMReminders() {
        if (UserACL::can('special.crm.use.reminders')) {
            $events         =   CRMReminder::with('client')
                                    ->where('created_by', '=', Auth::user()->id)
                                    ->get();

            return  Response::json($this->_parseEventsForJSON($events));
        } else {
            return  Response::json(array());
        }
    }

    public function getCRMUpcomingReminders() {
        if (UserACL::can('special.crm.use.reminders')) {
            $events     =   CRMReminder::with('client')
                                ->where('created_by', '=', Auth::user()->id)
                                ->where('start', '>=', new DateTime('today'))
                                ->get();

            return  Response::json($this->_parseEventsForJSON($events));
        } else {
            return  Response::json(array());
        }
    }

    public function getCRMRemindersClient($id) {
        if (UserACL::can('special.crm.use.reminders')) {
            $events     =   CRMReminder::with('client')
                                ->where('created_by', '=', Auth::user()->id)
                                ->where('crm_id', '=', $id)
                                ->get();

            return  Response::json($this->_parseEventsForJSON($events));
        } else {
            return  Response::json(array());
        }
    }

    public function getCRMReminderModal() {
        if ((UserACL::can('special.crm.use.reminders')) && ((UserACL::can('special.crm.use.personal')) || (UserACL::can('special.crm.use.master')))) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'start'             => 'required',
                                                'end'               => 'required',
                                                'allday'            => 'required'
                                            ));

            if ($validator->passes()) {
                $customers_personal     = false;
                $customers_master       = false;

                if (UserACL::can('special.crm.use.personal')) {
                    $customers_personal     =   CRM::where('installation', '=', Auth::user()->installation)
                                                    ->where('assigned_to', '=', Auth::user()->id)
                                                    ->get();
                }
                if (UserACL::can('special.crm.use.master')) {
                    $customers_master       =   CRM::where('installation', '=', Auth::user()->installation)
                                                    ->where('type', '=', 'client')
                                                    ->get();
                }

                return  View::make( 'crm/modals/reminder',
                                    array(
                                        'customers_personal'    =>  $customers_personal,
                                        'customers_master'      =>  $customers_master
                                    ));
            } else {
                return  View::make('crm/modals/reminder-no-access');
            }
        } else {
            return  View::make('crm/modals/reminder-no-access');
        }
    }

    public function postCRMReminder() {
        if (UserACL::can('special.crm.use.reminders')) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'client'            => 'required|numeric|min:1',
                                                'start'             => 'required',
                                                'end'               => 'required',
                                                'text'              => 'required',
                                                'when'              => 'required|numeric'
                                            ));

            if (($validator->passes()) && (CRM::accessible(Input::get('client')))) {
                $record             = new CRMReminder;
                $record->crm_id     = Input::get('client');
                $record->created_by = Auth::user()->id;
                $record->start      = date('Y-m-d H:i:s', strtotime(Input::get('start')));
                $record->end        = date('Y-m-d H:i:s', strtotime(Input::get('end')));
                $record->allday     = Input::get('allday');
                $record->text       = Input::get('text');
                $record->when       = Input::get('when');
                $record->save();

                if ($record->id) {
                    return  Response::json( array(
                                                'success'   =>  true,
                                                'data'      =>  print_r(Input::all(), true)
                                            ));
                } else {
                    return  Response::json( array(
                                                'success'   =>  false
                                            ));
                }
            } else {
                return  Response::json( array(
                                            'success'   =>  false
                                        ));
            }
        } else {
            return  Response::json( array(
                                        'success'   =>  false
                                    ));
        }
    }

    public function postCRMReminderUpdate() {
        if (UserACL::can('special.crm.use.reminders')) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'id'                => 'required|numeric|min:1',
                                                'client'            => 'required|numeric|min:1',
                                                'start'             => 'required',
                                                'end'               => 'required',
                                                'text'              => 'required',
                                                'when'              => 'required|numeric'
                                            ));

            if (($validator->passes()) && (CRM::accessible(Input::get('client')))) {
                $record             = CRMReminder::findOrFail(Input::get('id'));
                $record->crm_id     = Input::get('client');
                $record->start      = date('Y-m-d H:i:s', strtotime(Input::get('start')));
                $record->end        = date('Y-m-d H:i:s', strtotime(((Input::get('end')) ? Input::get('end') : Input::get('start'))));
                $record->allday     = Input::get('allday');
                $record->text       = Input::get('text');
                $record->when       = Input::get('when');
                $record->save();

                return  Response::json( array(
                                            'success'   =>  true
                                        ));
            } else {
                return  Response::json( array(
                                            'success'   =>  false
                                        ));
            }
        } else {
            return  Response::json( array(
                                        'success'   =>  false
                                    ));
        }
    }

    private function _parseEventsForJSON($data) {
        $events         = array();

        date_default_timezone_set('GMT');

        if (!$data->isEmpty()) {
            foreach ($data as $event) {
                array_push( $events,
                            array(
                                'id'            => intval($event->id),
                                'created_by'    => intval($event->created_by),
                                'start'         => date('c', strtotime($event->start)),
                                'end'           => date('c', strtotime($event->end)),
                                'title'         => '<strong>' . $event->client->name . '</strong>: ' . $event->text,
                                'allDay'        => ($event->allday) ? true : false,
                                'when'          => $event->when,
                                'client_id'     => $event->crm_id,
                                'client_name'   => $event->client->name,
                                'text'          => $event->text
                            ));
            }
        }

        return $events;
    }

    public static function saveLogo() {
        $logo                   = '';

        $original_filename      = Input::file('logo')->getClientOriginalName();
        $extension              = Input::file('logo')->getClientOriginalExtension();

        if (in_array(strtolower($extension), Config::get('settings.crm.files.allowed_image_extensions'))) {
            $upload_path                    = public_path() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . 'installations' . DIRECTORY_SEPARATOR . Auth::user()->installation . DIRECTORY_SEPARATOR . 'logos' . DIRECTORY_SEPARATOR;
            $upload_filename                = uniqid() . '.' . $extension;

            if (!file_exists($upload_path)) {
                mkdir($upload_path, 0777, true);
            }

            if (Input::file('logo')->move($upload_path, $upload_filename)) {
                $logo                           = $upload_filename;
            }
        }

        return $logo;
    }
}
