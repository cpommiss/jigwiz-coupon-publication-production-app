<?php

class ZoneController extends Controller {
    public function getLanding() {
        if (UserACL::can('read.zone')) {
            return  View::make( 'zones/landing',
                                array(
                                    'zones'     =>  Zone::where('installation', '=', Auth::user()->installation)
                                                        ->get()
                                ));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.zone.no_access.read'));
        }
    }

    public function getZoneCreate() {
        if (UserACL::can('create.zone')) {
            return  View::make( 'zones/zone-create',
                                array(
                                    'states'    =>  ZoneState::all()
                                                        ->lists('name', 'id')
                                ));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.zone.no_access.create'));
        }
    }

    public function getZoneUpdate($id) {
        if (Zone::accessible($id, 'update.zone')) {
            $zone       =   Zone::where('id', '=', $id)
                                ->where('installation', '=', Auth::user()->installation)
                                ->get();

            if (!$zone->isEmpty()) {
                $zone       = $zone->first();

                return  View::make( 'zones/zone-update',
                                    array(
                                        'zone'      =>  $zone,
                                        'states'    =>  ZoneState::all()
                                                            ->lists('name', 'id')
                                    ));
            } else {
                return  Redirect::route('dashboard')
                            ->with('failure', Lang::get('errors.zone.not_found'));
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('errors.zone.no_access.update'));
        }
    }

    public function postZoneCreate() {
        if (UserACL::can('create.zone')) {
            $validator  = Validator::make(  Input::all(),
                                            array(
                                                'state'             => 'required|numeric',
                                                'area'              => 'required'
                                            ));

            if ($validator->passes()) {
                $zone                           = new Zone;
                $zone->installation             = Auth::user()->installation;
                $zone->state                    = Input::get('state');
                $zone->area                     = Input::get('area');
                $zone->code                     = Str::upper(Input::get('code'));
                $zone->save();

                if ($zone->id) {
                    return  Redirect::route('zone')
                                ->with('success', Lang::get('success.zone.create'));
                } else {
                    return  Redirect::route('dashboard')
                                ->with('failure', Lang::get('error.zone.create.general'));
                }
            } else {
                return  Redirect::route('zone_create')
                            ->withErrors($validator)
                            ->withInput();
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('error.zone.no_access.create'));
        }
    }

    public function postZoneUpdate($id) {
        if (Zone::accessible($id, 'update.zone')) {
            $zone       =   Zone::where('id', '=', $id)
                                ->where('installation', '=', Auth::user()->installation)
                                ->get();

            if (!$zone->isEmpty()) {
                $zone       = $zone->first();

                $validator  = Validator::make(  Input::all(),
                                                array(
                                                    'state'             => 'required|numeric',
                                                    'area'              => 'required'
                                                ));

                if ($validator->passes()) {
                    $zone->state                    = Input::get('state');
                    $zone->area                     = Input::get('area');
                    $zone->code                     = Str::upper(Input::get('code'));
                    $zone->save();

                    return  Redirect::route('zone')
                                ->with('success', Lang::get('success.zone.update'));
                } else {
                    return  Redirect::route('zone_update')
                                ->withErrors($validator)
                                ->withInput();
                }
            } else {
                return  Redirect::route('dashboard')
                            ->with('failure', Lang::get('error.zone.not_found'));
            }
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('error.zone.no_access.update'));
        }
    }

    public function getZoneDelete($id) {
        if (Zone::accessible($id, 'delete.zone')) {
            Zone::find($id)->delete();

            return  Redirect::route('zone')
                        ->with('success', Lang::get('success.zone.delete'));
        } else {
            return  Redirect::route('dashboard')
                        ->with('failure', Lang::get('error.zone.no_access.delete'));
        }
    }
}
