<?php

class SeedInitialInstallation extends Seeder {
    public function run() {
        $installation               = new Installation;
        $installation->active       = 1;
        $installation->plan         = 1;
        $installation->name         = 'Jigwiz Media';
        $installation->save();

        if ($installation->id) {
            // add attributes
            $installation_attrs                     = new InstallationAttribute;
            $installation_attrs->installation_id    = $installation->id;
            $installation_attrs->save();

            // add default users
            foreach (Config::get('settings.acl_structure.presets') as $preset) {
                $user                   = User::create( array(
                                                            'installation'  => $installation->id,
                                                            'username'      => Str::lower($preset['user']),
                                                            'password'      => Hash::make('password'),
                                                            'name'          => $preset['title'],
                                                            'title'         => $preset['title'],
                                                            'admin'         => ((isset($preset['flags']['admin'])) ? true : false),
                                                            'superadmin'    => ((isset($preset['flags']['admin'])) ? true : false)
                                                        ));

                if ($user->id) {
                    // add permissions
                    DB::table('users_acl')->insert( array_map(
                                                        function($element) use ($user) {
                                                            return array('user_id' => $user->id, 'action' => Config::get('acl.' . $element));
                                                        },
                                                        $preset['permissions']
                                                    ));
                }
            }
        }
    }
}