<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCrmTypeEnum extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('crm', function($table) {
            $table->dropColumn('visibility');
            $table->enum('type', array('prospect', 'client'))->default('client');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('crm', function($table) {
            $table->dropColumn('type');
            $table->enum('visibility', array('sales', 'full'))->default('sales');
        });
	}

}
