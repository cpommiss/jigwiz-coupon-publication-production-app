<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraphicsQueue extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('graphics_jobs', function($table) {
            $table->increments('id');
            $table->integer('crm_id');
            $table->string('key', 255);
            $table->integer('created_by');
            $table->integer('assigned_to');
            $table->smallInteger('status')->default(1);
            $table->integer('media');
            $table->integer('color_type');
            $table->string('title', 255);
            $table->integer('points')->default(0);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('graphics_jobs');
	}

}
