<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_types', function($table) {
            $table->increments('id');
            $table->integer('installation');
            $table->string('name', 255);
            $table->decimal('width', 3, 2);
            $table->decimal('height', 3, 2);
            $table->boolean('fullcolor')->default(true);
            $table->boolean('premium')->default(true);
            $table->smallInteger('max_per_page');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media_types');
	}

}
