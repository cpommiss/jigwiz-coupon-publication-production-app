<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RevampReminders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::drop('crm_reminders');
        Schema::create('crm_reminders', function($table) {
            $table->increments('id');
            $table->integer('crm_id');
            $table->integer('created_by');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->boolean('allday');
            $table->text('text');
            $table->smallInteger('when');
            $table->boolean('notified')->default(false);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('crm_reminders');
        Schema::create('crm_reminders', function($table) {
            $table->increments('id');
            $table->integer('crm_id');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->boolean('allday');
            $table->text('text');
            $table->smallInteger('when');
            $table->boolean('notified');
        });
	}

}
