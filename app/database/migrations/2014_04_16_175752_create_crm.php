<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrm extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('crm', function($table) {
            $table->increments('id');
            $table->integer('installation');
            $table->enum('visibility', array('sales', 'full'))->default('sales');
            $table->integer('assigned_to')->nullable();
            $table->string('name', 255)->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('crm');
	}

}
