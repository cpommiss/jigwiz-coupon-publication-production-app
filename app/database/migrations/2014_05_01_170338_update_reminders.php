<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReminders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('crm_reminders', function($table) {
            $table->dropColumn('notified');
        });
        Schema::table('crm_reminders', function($table) {
            $table->boolean('notified')->default(false);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('crm_reminders', function($table) {
            $table->dropColumn('notified');
        });
        Schema::table('crm_reminders', function($table) {
            $table->boolean('notified');
        });
	}

}
