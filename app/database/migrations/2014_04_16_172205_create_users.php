<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function($table) {
            $table->increments('id');
            $table->integer('installation')->default(1);
            $table->smallInteger('access')->default(1);
            $table->string('username', 64);
            $table->string('password', 64);
            $table->string('name', 255)->nullable();
            $table->string('last_ip', 45)->nullable();
            $table->dateTime('last_login')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('users');
	}

}
