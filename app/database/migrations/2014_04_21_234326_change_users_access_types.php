<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersAccessTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::dropIfExists('users_access');

		Schema::table('users', function($table) {
            $table->dropColumn('access');
        });

		Schema::create('users_acl', function($table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->smallInteger('action');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('users_acl');

        Schema::table('users', function($table) {
            $table->smallInteger('access')->default(1);
        });

        Schema::create('users_access', function($table) {
            $table->increments('id');
            $table->integer('installation')->default(1);
            $table->string('name', 255);
            $table->enum('type', array('admin', 'art', 'sales'));
            $table->timestamps();
        });

        DB::table('users_access')->insert(array(
            'installation'      => 1,
            'name'              => 'Administrator',
            'type'              => 'admin'
        ));
        DB::table('users_access')->insert(array(
            'installation'      => 1,
            'name'              => 'Art Department',
            'type'              => 'art'
        ));
        DB::table('users_access')->insert(array(
            'installation'      => 1,
            'name'              => 'Sales Department',
            'type'              => 'sales'
        ));
	}

}
