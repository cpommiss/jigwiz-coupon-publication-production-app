<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaginationPagesPlacement extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pagination_pages_placement', function($table) {
            $table->smallInteger('start_row')->default(1);
            $table->smallInteger('start_col')->default(1);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pagination_pages_placement', function($table) {
            $table->dropColumn('start_row');
            $table->dropColumn('start_col');
        });
	}

}
