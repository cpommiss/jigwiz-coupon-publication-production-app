<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaginationReservations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pagination_reservations', function($table) {
            $table->increments('id');
            $table->integer('media');
            $table->smallInteger('page');
            $table->integer('crm_id');
            $table->integer('reserved_by');
            $table->date('issue_date');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pagination_reservations');
	}

}
