<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaginationAddWidthHeight extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('pagination', function($table) {
            $table->decimal('width', 3, 2);
            $table->decimal('height', 3, 2);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('pagination', function($table) {
            $table->dropColumn('width');
            $table->dropColumn('height');
        });
	}

}
