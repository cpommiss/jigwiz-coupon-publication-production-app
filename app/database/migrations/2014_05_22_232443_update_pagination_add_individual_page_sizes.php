<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaginationAddIndividualPageSizes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pagination_pages', function($table) {
            $table->decimal('width', 3, 2);
            $table->decimal('height', 3, 2);
            $table->smallInteger('rows')->default(1);
            $table->smallInteger('cols')->default(1);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pagination_pages', function($table) {
            $table->dropColumn('width');
            $table->dropColumn('height');
            $table->dropColumn('rows');
            $table->dropColumn('cols');
        });
	}

}
