<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraphicsQueueFileAnnotations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('graphics_jobs_files_annotations', function($table) {
            $table->increments('id');
            $table->integer('file_id');
            $table->integer('created_by');
            $table->text('data');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('graphics_jobs_files_annotations');
	}

}
