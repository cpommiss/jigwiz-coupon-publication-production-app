<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeletes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('media_types', function($table) {
            $table->softDeletes();
        });
		Schema::table('crm', function($table) {
            $table->softDeletes();
        });
		Schema::table('graphics_jobs', function($table) {
            $table->softDeletes();
        });
		Schema::table('users', function($table) {
            $table->softDeletes();
        });
		Schema::table('zones', function($table) {
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media_types', function($table) {
            $table->dropSoftDeletes();
        });
		Schema::table('crm', function($table) {
            $table->dropSoftDeletes();
        });
		Schema::table('graphics_jobs', function($table) {
            $table->dropSoftDeletes();
        });
		Schema::table('users', function($table) {
            $table->dropSoftDeletes();
        });
		Schema::table('zones', function($table) {
            $table->dropSoftDeletes();
        });
	}

}
