<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePagination extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pagination', function($table) {
            $table->smallInteger('rows')->default(1);
            $table->smallInteger('cols')->default(1);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pagination', function($table) {
            $table->dropColumn('rows');
            $table->dropColumn('cols');
        });
	}

}
