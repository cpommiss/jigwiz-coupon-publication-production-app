<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCrmAuthorizations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_authorizations', function($table) {
            $table->increments('id');
            $table->integer('crm_id');
            $table->integer('media');
            $table->boolean('authorized')->default(false);
            $table->date('auth_date');
            $table->string('key', 255)->nullable();
            $table->string('signature', 100);
            $table->timestamps();
        });
		Schema::create('crm_authorizations_zones', function($table) {
            $table->increments('id');
            $table->integer('auth_id');
            $table->integer('zone_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('crm_authorizations_zones');
        Schema::drop('crm_authorizations');
	}

}
