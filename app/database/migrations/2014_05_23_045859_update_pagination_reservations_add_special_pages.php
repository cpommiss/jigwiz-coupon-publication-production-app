<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaginationReservationsAddSpecialPages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pagination_reservations', function($table) {
            $table->smallInteger('page_special');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pagination_reservations', function($table) {
            $table->dropColumn('page_special');
        });
	}

}
