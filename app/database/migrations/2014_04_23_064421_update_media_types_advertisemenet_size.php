<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMediaTypesAdvertisemenetSize extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('media_types', function($table) {
            $table->dropColumn('advertising_size');
        });
        Schema::table('media_types', function($table) {
            $table->string('advertising_size', 30);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media_types', function($table) {
            $table->dropColumn('advertising_size');
        });
		Schema::table('media_types', function($table) {
            $table->float('advertising_size');
        });
	}

}
