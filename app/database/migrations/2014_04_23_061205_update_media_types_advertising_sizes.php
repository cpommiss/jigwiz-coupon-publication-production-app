<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMediaTypesAdvertisingSizes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('media_types', function($table) {
            $table->float('advertising_size');
            $table->dropColumn('max_per_page');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media_types', function($table) {
            $table->dropColumn('advertising_size');
            $table->smallInteger('max_per_page');
        });
	}

}
