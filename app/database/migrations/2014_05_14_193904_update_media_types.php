<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMediaTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('media_types', function($table) {
            $table->dropColumn('advertising_size');
            $table->smallInteger('max_rows')->default(1);
            $table->smallInteger('max_cols')->default(1);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media_types', function($table) {
            $table->dropColumn('max_cols');
            $table->dropColumn('max_rows');
            $table->string('advertising_size', 30);
        });
	}

}
