<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagination extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pagination', function($table) {
            $table->increments('id');
            $table->integer('media');
            $table->integer('zone');
            $table->date('issue_date');
            $table->timestamps();
        });
        Schema::create('pagination_pages', function($table) {
            $table->increments('id');
            $table->integer('pagination_id');
            $table->smallInteger('page');
            $table->boolean('premium')->default(false);
            $table->timestamps();
        });
        Schema::create('pagination_pages_placement', function($table) {
            $table->increments('id');
            $table->integer('pagination_page_id');
            $table->integer('job_id');
            $table->smallInteger('rows')->default(1);
            $table->smallInteger('cols')->default(1);
            $table->smallInteger('rows_offset')->default(0);
            $table->smallInteger('cols_offset')->default(0);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pagination_pages_placement');
        Schema::drop('pagination_pages');
        Schema::drop('pagination');
	}

}
