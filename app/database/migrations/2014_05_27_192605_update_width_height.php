<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWidthHeight extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('media_types', function($table) {
            $table->dropColumn('width');
            $table->dropColumn('height');
        });
        Schema::table('pagination', function($table) {
            $table->dropColumn('width');
            $table->dropColumn('height');
        });
        Schema::table('pagination_pages', function($table) {
            $table->dropColumn('width');
            $table->dropColumn('height');
        });
		Schema::table('media_types', function($table) {
            $table->decimal('width', 6, 2);
            $table->decimal('height', 6, 2);
        });
        Schema::table('pagination', function($table) {
            $table->decimal('width', 6, 2);
            $table->decimal('height', 6, 2);
        });
        Schema::table('pagination_pages', function($table) {
            $table->decimal('width', 6, 2);
            $table->decimal('height', 6, 2);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media_types', function($table) {
            $table->dropColumn('width');
            $table->dropColumn('height');
        });
        Schema::table('pagination', function($table) {
            $table->dropColumn('width');
            $table->dropColumn('height');
        });
        Schema::table('pagination_pages', function($table) {
            $table->dropColumn('width');
            $table->dropColumn('height');
        });
		Schema::table('media_types', function($table) {
            $table->decimal('width', 3, 2);
            $table->decimal('height', 3, 2);
        });
        Schema::table('pagination', function($table) {
            $table->decimal('width', 3, 2);
            $table->decimal('height', 3, 2);
        });
        Schema::table('pagination_pages', function($table) {
            $table->decimal('width', 3, 2);
            $table->decimal('height', 3, 2);
        });
	}

}
