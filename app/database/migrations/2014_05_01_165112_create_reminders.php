<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReminders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('crm_reminders', function($table) {
            $table->increments('id');
            $table->integer('crm_id');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->boolean('allday');
            $table->text('text');
            $table->smallInteger('when');
            $table->boolean('notified');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('crm_reminders');
	}

}
