<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocuments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    Schema::create('documents', function($table) {
            $table->increments('id');
            $table->integer('installation');
            $table->string('title', 255)->nullable();
            $table->text('notes')->nullable();
            $table->string('file', 255);
            $table->string('original', 255);
            $table->integer('created_by');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('documents');
	}

}
