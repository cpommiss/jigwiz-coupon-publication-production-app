<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNotesVisibilityColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('graphics_jobs_notes', function($table) {
            $table->dropColumn('client_visible');
        });
		Schema::table('graphics_jobs_notes', function($table) {
            $table->smallInteger('visibility')->default(1);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('graphics_jobs_notes', function($table) {
            $table->dropColumn('visibility');
        });
		Schema::table('graphics_jobs_notes', function($table) {
            $table->boolean('client_visible')->default(false);
        });
	}

}
