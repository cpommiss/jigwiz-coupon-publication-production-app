<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraphicsQueueFiles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('graphics_jobs_files', function($table) {
            $table->increments('id');
            $table->integer('job_id');
            $table->integer('created_by');
            $table->string('file', 255);
            $table->string('original', 255);
            $table->boolean('proof')->default(false);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('graphics_jobs_files');
	}

}
