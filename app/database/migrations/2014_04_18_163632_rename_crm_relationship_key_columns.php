<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCrmRelationshipKeyColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('crm_attributes', function($table) {
            $table->renameColumn('customer_id', 'crm_id');
        });
		Schema::table('crm_notes', function($table) {
            $table->renameColumn('customer_id', 'crm_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('crm_attributes', function($table) {
            $table->renameColumn('crm_id', 'customer_id');
        });
        Schema::table('crm_attributes', function($table) {
            $table->renameColumn('crm_id', 'customer_id');
        });
	}

}
