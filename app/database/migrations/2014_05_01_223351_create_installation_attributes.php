<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstallationAttributes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('installations_attributes', function($table) {
            $table->increments('id');
            $table->integer('installation_id');
            $table->string('image_logo', 255)->default('logo.png');
            $table->string('color_primary', 25)->default('#4275e8');
            $table->string('color_secondary', 25)->default('#40444d');
            $table->string('background_body', 25)->default('#f5f5f5');
            $table->string('color_text', 25)->default('#939699');
            $table->string('color_block', 25)->default('#525459');
            $table->string('background_block', 25)->default('#ffffff');
            $table->string('font_family', 255)->default('"PT Sans", sans-serif');
            $table->string('font_size', 5)->default('13px');
            $table->string('font_headings_family', 255)->default('"PT Sans", sans-serif');
            $table->string('font_headings_color', 25)->default('#525252');
            $table->boolean('font_headings_bold')->default(true);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('installations_attributes');
	}

}
